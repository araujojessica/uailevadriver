var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
var ImageViewerComponent = /** @class */ (function () {
    function ImageViewerComponent(modalController) {
        this.modalController = modalController;
        this.imgSource = '';
        this.imgTitle = '';
        this.imgDescription = '';
        this.slideOpts = {
            centeredSlides: 'true'
        };
    }
    ImageViewerComponent.prototype.ngOnInit = function () { };
    ImageViewerComponent.prototype.closeModal = function () {
        this.modalController.dismiss();
    };
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], ImageViewerComponent.prototype, "imgSource", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], ImageViewerComponent.prototype, "imgTitle", void 0);
    __decorate([
        Input(),
        __metadata("design:type", Object)
    ], ImageViewerComponent.prototype, "imgDescription", void 0);
    ImageViewerComponent = __decorate([
        Component({
            selector: 'app-image-viewer',
            templateUrl: './image-viewer.component.html',
            styleUrls: ['./image-viewer.component.scss']
        }),
        __metadata("design:paramtypes", [ModalController])
    ], ImageViewerComponent);
    return ImageViewerComponent;
}());
export { ImageViewerComponent };
//# sourceMappingURL=image-viewer.component.js.map