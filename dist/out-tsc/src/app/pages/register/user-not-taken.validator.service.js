var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
import { RegisterService } from "./register.service";
import { debounceTime, switchMap, map, first } from 'rxjs/operators';
var UserNotTakenValidatorService = /** @class */ (function () {
    function UserNotTakenValidatorService(signUpService) {
        this.signUpService = signUpService;
    }
    UserNotTakenValidatorService.prototype.checkUserNameTaken = function () {
        var _this = this;
        console.log('entrou no taken');
        return function (control) {
            return control.valueChanges.pipe(debounceTime(300)).pipe(switchMap(function (userName) { return _this.signUpService.checkUserNameTaken(userName); })).pipe(map(function (isTaken) { return isTaken ? { userNameTaken: true } : null; })).pipe(first());
        };
    };
    UserNotTakenValidatorService = __decorate([
        Injectable({ providedIn: 'root' }),
        __metadata("design:paramtypes", [RegisterService])
    ], UserNotTakenValidatorService);
    return UserNotTakenValidatorService;
}());
export { UserNotTakenValidatorService };
//# sourceMappingURL=user-not-taken.validator.service.js.map