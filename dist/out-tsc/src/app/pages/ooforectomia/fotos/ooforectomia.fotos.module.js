var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { OoforectomiaFotos } from './ooforectomia.fotos';
var routes = [
    {
        path: '',
        component: OoforectomiaFotos
    }
];
var OoforectomiaFotosModule = /** @class */ (function () {
    function OoforectomiaFotosModule() {
    }
    OoforectomiaFotosModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            providers: [],
            bootstrap: [OoforectomiaFotos],
            declarations: [OoforectomiaFotos]
        })
    ], OoforectomiaFotosModule);
    return OoforectomiaFotosModule;
}());
export { OoforectomiaFotosModule };
//# sourceMappingURL=ooforectomia.fotos.module.js.map