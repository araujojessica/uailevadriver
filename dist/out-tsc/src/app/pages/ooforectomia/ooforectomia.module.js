var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { File } from '@ionic-native/file';
import { IonicModule } from '@ionic/angular';
import { OoforectomiaPage } from './ooforectomia.page';
var routes = [
    {
        path: '',
        component: OoforectomiaPage
    }
];
var OoforectomiaPageModule = /** @class */ (function () {
    function OoforectomiaPageModule() {
    }
    OoforectomiaPageModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            providers: [File, { provide: ErrorHandler }],
            declarations: [OoforectomiaPage]
        })
    ], OoforectomiaPageModule);
    return OoforectomiaPageModule;
}());
export { OoforectomiaPageModule };
//# sourceMappingURL=ooforectomia.module.js.map