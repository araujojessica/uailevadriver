var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
var API_URL = 'http://159.203.181.9:3000';
var InfoService = /** @class */ (function () {
    function InfoService(http) {
        this.http = http;
    }
    InfoService.prototype.listFromInfo = function () { return this.http.get(API_URL + "/info"); };
    InfoService.prototype.updateInfo = function (newInfo) {
        console.log(newInfo.title);
        return this.http.post(API_URL + "/info/", newInfo);
    };
    InfoService = __decorate([
        Injectable({ providedIn: 'root' }),
        __metadata("design:paramtypes", [HttpClient])
    ], InfoService);
    return InfoService;
}());
export { InfoService };
//# sourceMappingURL=info.service.js.map