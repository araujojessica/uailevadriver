var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Component } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { SearchFilterPage } from '../../pages/modal/search-filter/search-filter.page';
var AgfPage = /** @class */ (function () {
    function AgfPage(modalCtrl, navCtrl, router) {
        this.modalCtrl = modalCtrl;
        this.navCtrl = navCtrl;
        this.router = router;
    }
    AgfPage.prototype.ngOnInit = function () {
    };
    AgfPage.prototype.searchFilter = function () {
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: SearchFilterPage
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    AgfPage.prototype.hiperplasia = function () {
        this.router.navigateByUrl('/hiperplasia');
    };
    AgfPage.prototype.ooforectomia = function () {
        this.router.navigateByUrl('/ooforectomia');
    };
    AgfPage.prototype.laqueadura = function () {
        this.router.navigateByUrl('/laqueadura');
    };
    AgfPage.prototype.salpingectomia = function () {
        this.router.navigateByUrl('/salpingectomia');
    };
    AgfPage.prototype.carcinoma = function () {
        this.router.navigateByUrl('/carcinoma');
    };
    AgfPage.prototype.instrucoes_gerais = function () {
        this.router.navigateByUrl('/instrucoes_gerais');
    };
    AgfPage.prototype.curetagens = function () {
        this.router.navigateByUrl('/curetagens');
    };
    AgfPage.prototype.biopsia = function () {
        this.router.navigateByUrl('/biopsia');
    };
    AgfPage.prototype.cones_cervicais = function () {
        this.router.navigateByUrl('/cones_cervicais');
    };
    AgfPage.prototype.vulvectomia = function () {
        this.router.navigateByUrl('/vulvectomia');
    };
    AgfPage = __decorate([
        Component({
            selector: 'app-agf',
            templateUrl: './agf.page.html',
            styleUrls: ['./agf.page.scss'],
        }),
        __metadata("design:paramtypes", [ModalController, NavController, Router])
    ], AgfPage);
    return AgfPage;
}());
export { AgfPage };
//# sourceMappingURL=agf.page.js.map