(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-results-home-results-module"],{

/***/ "./src/app/components/popmenu/popmenu.component.html":
/*!***********************************************************!*\
  !*** ./src/app/components/popmenu/popmenu.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-fab vertical=\"bottom\" horizontal=\"center\" slot=\"fixed\" class=\"animated fadeInDown\">\r\n  <ion-fab-button (click)=\"togglePopupMenu()\">\r\n    <ion-ripple-effect></ion-ripple-effect>\r\n    <ion-icon name=\"apps\"></ion-icon>\r\n  </ion-fab-button>\r\n</ion-fab>\r\n\r\n<div class=\"popup-menu\">\r\n  <div class=\"popup-menu-overlay\" [ngClass]=\"{'in': openMenu}\"></div>\r\n  <div class=\"popup-menu-panel\" [ngClass]=\"{'in': openMenu}\">\r\n    <div class=\"popup-menu-item\">\r\n      <ion-icon name=\"cog\" slot=\"middle\" size=\"large\"></ion-icon>\r\n      <span>Config</span>\r\n    </div>\r\n    <div class=\"popup-menu-item\">\r\n      <ion-icon name=\"beer\" slot=\"middle\" size=\"large\"></ion-icon>\r\n      <span>Activities</span>\r\n    </div>\r\n    <div class=\"popup-menu-item\">\r\n      <ion-icon name=\"person\" slot=\"middle\" size=\"large\"></ion-icon>\r\n      <span>Settings</span>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/popmenu/popmenu.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/components/popmenu/popmenu.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".popup-menu-overlay {\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  z-index: 100;\n  opacity: 0;\n  visibility: hidden;\n  transition: all 0.15s ease-in-out;\n  background-image: linear-gradient(rgba(79, 36, 172, 0.85) 0%, rgba(79, 36, 172, 0.65) 100%); }\n  .popup-menu-overlay.in {\n    opacity: 1;\n    visibility: visible; }\n  .popup-menu-toggle {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  bottom: 10px;\n  left: 50%;\n  margin-left: -20px;\n  background-color: var(--ion-color-primary);\n  border-radius: 50%;\n  z-index: 101;\n  transition: all .25s ease-in-out; }\n  .popup-menu-toggle.out {\n    opacity: 0;\n    visibility: hidden;\n    transform: scale(0);\n    transition: all .15s ease-in-out; }\n  .popup-menu-toggle.out:before {\n      transition: all .15s ease-in-out;\n      transform: scale(0); }\n  .popup-menu-panel {\n  position: fixed;\n  width: 300px;\n  border-radius: 5%;\n  bottom: 80px;\n  left: 50%;\n  margin-left: -150px;\n  padding: 20px;\n  background-color: var(--ion-color-primary);\n  z-index: 102;\n  transition: all .25s ease-in-out;\n  transition-delay: .15s;\n  transform-origin: 50% 100%;\n  transform: scale(0);\n  display: -moz-flex;\n  display: flex;\n  flex-wrap: wrap; }\n  .popup-menu-panel .popup-menu-item {\n    margin: auto;\n    -moz-flex: 1 0 30%;\n    flex: 1 0 30%;\n    display: -moz-flex;\n    display: flex;\n    -moz-flex-direction: column;\n    flex-direction: column;\n    transform: scale(0);\n    opacity: 0;\n    transition: all .25s ease-in-out; }\n  .popup-menu-panel .popup-menu-item ion-icon {\n      margin: 0 auto;\n      text-align: center;\n      color: #fff; }\n  .popup-menu-panel .popup-menu-item span {\n      padding: 0;\n      margin: 0 0 auto 0;\n      color: #fff;\n      text-align: center;\n      font-size: 12px;\n      text-transform: uppercase;\n      font-weight: 500;\n      line-height: 18px; }\n  .popup-menu-panel .popup-menu-item:active i {\n      color: #dd4135;\n      transition: all 0.15s; }\n  .popup-menu-panel .popup-menu-item:active span {\n      color: #dd4135;\n      transition: all .15s; }\n  .popup-menu-panel.in {\n    transform: scale(1);\n    transition-delay: 0s; }\n  .popup-menu-panel.in .popup-menu-item {\n      transform: scale(1);\n      opacity: 1;\n      transition-delay: .15s; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9wb3BtZW51L0M6XFxVc2Vyc1xcamVzc2lcXERvY3VtZW50c1xcdWFpbGV2YWRyaXZlci9zcmNcXGFwcFxcY29tcG9uZW50c1xccG9wbWVudVxccG9wbWVudS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNJLGVBQWU7RUFDZixNQUFNO0VBQ04sT0FBTztFQUNQLFFBQVE7RUFDUixTQUFTO0VBQ1QsWUFBWTtFQUNaLFVBQVU7RUFDVixrQkFBa0I7RUFFbEIsaUNBQWlDO0VBRWpDLDJGQUFxRixFQUFBO0VBWnpGO0lBY1EsVUFBVTtJQUNWLG1CQUFtQixFQUFBO0VBSTNCO0VBQ0ksZUFBZTtFQUNmLFdBQVc7RUFDWCxZQUFZO0VBQ1osWUFBWTtFQUNaLFNBQVM7RUFDVCxrQkFBa0I7RUFDbEIsMENBQTBDO0VBQzFDLGtCQUFrQjtFQUNsQixZQUFZO0VBRVosZ0NBQWdDLEVBQUE7RUFYcEM7SUFhUSxVQUFVO0lBQ1Ysa0JBQWtCO0lBRWxCLG1CQUFtQjtJQUVuQixnQ0FBZ0MsRUFBQTtFQWxCeEM7TUFxQlksZ0NBQWdDO01BRWhDLG1CQUFtQixFQUFBO0VBSy9CO0VBQ0ksZUFBZTtFQUNmLFlBQVk7RUFFWixpQkFBaUI7RUFDakIsWUFBWTtFQUNaLFNBQVM7RUFDVCxtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLDBDQUEwQztFQUMxQyxZQUFZO0VBRVosZ0NBQWdDO0VBRWhDLHNCQUFzQjtFQUV0QiwwQkFBMEI7RUFFMUIsbUJBQW1CO0VBSW5CLGtCQUFrQjtFQUVsQixhQUFhO0VBSWIsZUFBZSxFQUFBO0VBNUJuQjtJQThCUSxZQUFZO0lBSVosa0JBQWtCO0lBRWxCLGFBQWE7SUFJYixrQkFBa0I7SUFFbEIsYUFBYTtJQUliLDJCQUEyQjtJQUUzQixzQkFBc0I7SUFFdEIsbUJBQW1CO0lBQ25CLFVBQVU7SUFFVixnQ0FBZ0MsRUFBQTtFQXJEeEM7TUF1RFksY0FBYztNQUNkLGtCQUFrQjtNQUNsQixXQUFXLEVBQUE7RUF6RHZCO01BNERZLFVBQVU7TUFDVixrQkFBa0I7TUFDbEIsV0FBVztNQUNYLGtCQUFrQjtNQUNsQixlQUFlO01BQ2YseUJBQXlCO01BQ3pCLGdCQUFnQjtNQUNoQixpQkFBaUIsRUFBQTtFQW5FN0I7TUF1RWdCLGNBQXFCO01BRXJCLHFCQUFxQixFQUFBO0VBekVyQztNQTRFZ0IsY0FBcUI7TUFFckIsb0JBQW9CLEVBQUE7RUE5RXBDO0lBb0ZRLG1CQUFtQjtJQUVuQixvQkFBb0IsRUFBQTtFQXRGNUI7TUF5RlksbUJBQW1CO01BQ25CLFVBQVU7TUFFVixzQkFBc0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcG9wbWVudS9wb3BtZW51LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gUG9wdXAgTWVudSAvL1xyXG4ucG9wdXAtbWVudS1vdmVybGF5IHtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIHRvcDogMDtcclxuICAgIGxlZnQ6IDA7XHJcbiAgICByaWdodDogMDtcclxuICAgIGJvdHRvbTogMDtcclxuICAgIHotaW5kZXg6IDEwMDtcclxuICAgIG9wYWNpdHk6IDA7XHJcbiAgICB2aXNpYmlsaXR5OiBoaWRkZW47XHJcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAwLjE1cyBlYXNlLWluLW91dDtcclxuICAgIHRyYW5zaXRpb246IGFsbCAwLjE1cyBlYXNlLWluLW91dDtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KHJnYmEoNzksMzYsMTcyLCAuODUpIDAlLCByZ2JhKDc5LDM2LDE3MiwgLjY1KSAxMDAlKTtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudChyZ2JhKDc5LDM2LDE3MiwgLjg1KSAwJSwgcmdiYSg3OSwzNiwxNzIsIC42NSkgMTAwJSk7XHJcbiAgICAmLmluIHtcclxuICAgICAgICBvcGFjaXR5OiAxO1xyXG4gICAgICAgIHZpc2liaWxpdHk6IHZpc2libGU7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5wb3B1cC1tZW51LXRvZ2dsZSB7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICB3aWR0aDogNDBweDtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIGJvdHRvbTogMTBweDtcclxuICAgIGxlZnQ6IDUwJTtcclxuICAgIG1hcmdpbi1sZWZ0OiAtMjBweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgIHotaW5kZXg6IDEwMTtcclxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIC4yNXMgZWFzZS1pbi1vdXQ7XHJcbiAgICB0cmFuc2l0aW9uOiBhbGwgLjI1cyBlYXNlLWluLW91dDtcclxuICAgICYub3V0IHtcclxuICAgICAgICBvcGFjaXR5OiAwO1xyXG4gICAgICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcclxuICAgICAgICAtd2Via2l0LXRyYW5zZm9ybTogc2NhbGUoMCk7XHJcbiAgICAgICAgdHJhbnNmb3JtOiBzY2FsZSgwKTtcclxuICAgICAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAuMTVzIGVhc2UtaW4tb3V0O1xyXG4gICAgICAgIHRyYW5zaXRpb246IGFsbCAuMTVzIGVhc2UtaW4tb3V0O1xyXG4gICAgICAgICY6YmVmb3JlIHtcclxuICAgICAgICAgICAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgLjE1cyBlYXNlLWluLW91dDtcclxuICAgICAgICAgICAgdHJhbnNpdGlvbjogYWxsIC4xNXMgZWFzZS1pbi1vdXQ7XHJcbiAgICAgICAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiBzY2FsZSgwKTtcclxuICAgICAgICAgICAgdHJhbnNmb3JtOiBzY2FsZSgwKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbi5wb3B1cC1tZW51LXBhbmVsIHtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIHdpZHRoOiAzMDBweDtcclxuXHJcbiAgICBib3JkZXItcmFkaXVzOiA1JTtcclxuICAgIGJvdHRvbTogODBweDtcclxuICAgIGxlZnQ6IDUwJTtcclxuICAgIG1hcmdpbi1sZWZ0OiAtMTUwcHg7XHJcbiAgICBwYWRkaW5nOiAyMHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgei1pbmRleDogMTAyO1xyXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgLjI1cyBlYXNlLWluLW91dDtcclxuICAgIHRyYW5zaXRpb246IGFsbCAuMjVzIGVhc2UtaW4tb3V0O1xyXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiAuMTVzO1xyXG4gICAgdHJhbnNpdGlvbi1kZWxheTogLjE1cztcclxuICAgIC13ZWJraXQtdHJhbnNmb3JtLW9yaWdpbjogNTAlIDEwMCU7XHJcbiAgICB0cmFuc2Zvcm0tb3JpZ2luOiA1MCUgMTAwJTtcclxuICAgIC13ZWJraXQtdHJhbnNmb3JtOiBzY2FsZSgwKTtcclxuICAgIHRyYW5zZm9ybTogc2NhbGUoMCk7XHJcbiAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuICAgIGRpc3BsYXk6IC1tb3otYm94O1xyXG4gICAgZGlzcGxheTogLXdlYmtpdC1mbGV4O1xyXG4gICAgZGlzcGxheTogLW1vei1mbGV4O1xyXG4gICAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgLXdlYmtpdC1mbGV4LXdyYXA6IHdyYXA7XHJcbiAgICAtbW96LWZsZXgtd3JhcDogd3JhcDtcclxuICAgIC1tcy1mbGV4LXdyYXA6IHdyYXA7XHJcbiAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICAucG9wdXAtbWVudS1pdGVtIHtcclxuICAgICAgICBtYXJnaW46IGF1dG87XHJcbiAgICAgICAgLXdlYmtpdC1ib3gtZmxleDogMSAwIDMwJTtcclxuICAgICAgICAtd2Via2l0LWZsZXg6IDEgMCAzMCU7XHJcbiAgICAgICAgLW1vei1ib3gtZmxleDogMSAwIDMwJTtcclxuICAgICAgICAtbW96LWZsZXg6IDEgMCAzMCU7XHJcbiAgICAgICAgLW1zLWZsZXg6IDEgMCAzMCU7XHJcbiAgICAgICAgZmxleDogMSAwIDMwJTtcclxuICAgICAgICBkaXNwbGF5OiAtd2Via2l0LWJveDtcclxuICAgICAgICBkaXNwbGF5OiAtbW96LWJveDtcclxuICAgICAgICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiAgICAgICAgZGlzcGxheTogLW1vei1mbGV4O1xyXG4gICAgICAgIGRpc3BsYXk6IC1tcy1mbGV4Ym94O1xyXG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAgICAgLXdlYmtpdC1ib3gtZGlyZWN0aW9uOiBub3JtYWw7XHJcbiAgICAgICAgLXdlYmtpdC1ib3gtb3JpZW50OiB2ZXJ0aWNhbDtcclxuICAgICAgICAtd2Via2l0LWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAgLW1vei1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgIC1tcy1mbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDApO1xyXG4gICAgICAgIHRyYW5zZm9ybTogc2NhbGUoMCk7XHJcbiAgICAgICAgb3BhY2l0eTogMDtcclxuICAgICAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAuMjVzIGVhc2UtaW4tb3V0O1xyXG4gICAgICAgIHRyYW5zaXRpb246IGFsbCAuMjVzIGVhc2UtaW4tb3V0O1xyXG4gICAgICAgIGlvbi1pY29uIHtcclxuICAgICAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHNwYW4ge1xyXG4gICAgICAgICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICAgICAgICBtYXJnaW46IDAgMCBhdXRvIDA7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcclxuICAgICAgICAgICAgbGluZS1oZWlnaHQ6IDE4cHg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgICY6YWN0aXZlIHtcclxuICAgICAgICAgICAgaSB7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogcmdiKDIyMSw2NSw1Myk7XHJcbiAgICAgICAgICAgICAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAuMTVzO1xyXG4gICAgICAgICAgICAgICAgdHJhbnNpdGlvbjogYWxsIDAuMTVzO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHNwYW4ge1xyXG4gICAgICAgICAgICAgICAgY29sb3I6IHJnYigyMjEsNjUsNTMpO1xyXG4gICAgICAgICAgICAgICAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgLjE1cztcclxuICAgICAgICAgICAgICAgIHRyYW5zaXRpb246IGFsbCAuMTVzO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG4gICAgJi5pbiB7XHJcbiAgICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDEpO1xyXG4gICAgICAgIHRyYW5zZm9ybTogc2NhbGUoMSk7XHJcbiAgICAgICAgLXdlYmtpdC10cmFuc2l0aW9uLWRlbGF5OiAwcztcclxuICAgICAgICB0cmFuc2l0aW9uLWRlbGF5OiAwcztcclxuICAgICAgICAucG9wdXAtbWVudS1pdGVtIHtcclxuICAgICAgICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDEpO1xyXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHNjYWxlKDEpO1xyXG4gICAgICAgICAgICBvcGFjaXR5OiAxO1xyXG4gICAgICAgICAgICAtd2Via2l0LXRyYW5zaXRpb24tZGVsYXk6IC4xNXM7XHJcbiAgICAgICAgICAgIHRyYW5zaXRpb24tZGVsYXk6IC4xNXM7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/components/popmenu/popmenu.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/components/popmenu/popmenu.component.ts ***!
  \*********************************************************/
/*! exports provided: PopmenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopmenuComponent", function() { return PopmenuComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PopmenuComponent = /** @class */ (function () {
    function PopmenuComponent(navCtrl) {
        this.navCtrl = navCtrl;
        this.openMenu = false;
    }
    PopmenuComponent.prototype.ngOnInit = function () {
    };
    PopmenuComponent.prototype.togglePopupMenu = function () {
        return this.openMenu = !this.openMenu;
    };
    PopmenuComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'popmenu',
            template: __webpack_require__(/*! ./popmenu.component.html */ "./src/app/components/popmenu/popmenu.component.html"),
            styles: [__webpack_require__(/*! ./popmenu.component.scss */ "./src/app/components/popmenu/popmenu.component.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"]])
    ], PopmenuComponent);
    return PopmenuComponent;
}());



/***/ }),

/***/ "./src/app/pages/home-results/home-results.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/home-results/home-results.module.ts ***!
  \***********************************************************/
/*! exports provided: HomeResultsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeResultsPageModule", function() { return HomeResultsPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _components_popmenu_popmenu_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../components/popmenu/popmenu.component */ "./src/app/components/popmenu/popmenu.component.ts");
/* harmony import */ var _home_results_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-results.page */ "./src/app/pages/home-results/home-results.page.ts");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "./node_modules/@ionic-native/splash-screen/ngx/index.js");
/* harmony import */ var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/google-maps */ "./node_modules/@ionic-native/google-maps/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var routes = [
    {
        path: '',
        component: _home_results_page__WEBPACK_IMPORTED_MODULE_6__["HomeResultsPage"]
    }
];
var HomeResultsPageModule = /** @class */ (function () {
    function HomeResultsPageModule() {
    }
    HomeResultsPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
            ],
            declarations: [_home_results_page__WEBPACK_IMPORTED_MODULE_6__["HomeResultsPage"], _components_popmenu_popmenu_component__WEBPACK_IMPORTED_MODULE_5__["PopmenuComponent"]],
            providers: [
                _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_7__["SplashScreen"],
                { provide: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ErrorHandler"] },
                _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_8__["GoogleMaps"]
            ]
        })
    ], HomeResultsPageModule);
    return HomeResultsPageModule;
}());



/***/ }),

/***/ "./src/app/pages/home-results/home-results.page.html":
/*!***********************************************************!*\
  !*** ./src/app/pages/home-results/home-results.page.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n <ion-header>\r\n    <nav class=\"navbar navbar-expand-lg navbar-light bg-dark\">\r\n        <a class=\"navbar-brand\" href=\"#\">\r\n        <img src=\"assets/img/logo-uaileva.png\" />\r\n        </a>\r\n        <ion-menu-button>\r\n            <button class=\"navbar-toggler\" >\r\n                <span class=\"navbar-toggler-icon\"></span>\r\n            </button>\r\n        </ion-menu-button>\r\n    </nav>\r\n</ion-header>\r\n<ion-content>\r\n    <div #map>\r\n        <br>\r\n        <div class=\"boxConteudo\" [hidden]=\"destination\">\r\n            <div class=\"saudacao\">\r\n                <ion-text color=\"medium\" *ngIf=\"(user$ | async) as user\">\r\n                    Olá Motorista {{user.fullname}} ! \r\n\r\n                </ion-text> \r\n                <br><br>\r\n\r\n                \r\n                <div *ngIf=\"users && users.length\">\r\n                  <div *ngFor=\"let user of users\">\r\n                    <p>{{user.status}}</p>\r\n                  </div>\r\n                </div>\r\n    <style>\r\n        body {\r\n        background: #824438;\r\n      }\r\n      \r\n      .vibra {\r\n        margin: 20px auto;\r\n        width: 300px;\r\n        animation: vibrador 0.82s cubic-bezier(.36,.07,.19,.97) both;\r\n        animation-iteration-count: infinite;\r\n        transform: translate3d(0, 0, 0);\r\n        backface-visibility: hidden;\r\n        perspective: 1000px;\r\n        background:white;\r\n        text-align:center;\r\n      }\r\n      \r\n      @keyframes vibrador {\r\n        10%, 90% {\r\n          transform: translate3d(-1px, 0, 0);\r\n        }\r\n        \r\n        20%, 80% {\r\n          transform: translate3d(2px, 0, 0);\r\n        }\r\n      \r\n        30%, 50%, 70% {\r\n          transform: translate3d(-4px, 0, 0);\r\n        }\r\n      \r\n        40%, 60% {\r\n          transform: translate3d(4px, 0, 0);\r\n        }\r\n      }\r\n      </style>\r\n      \r\n      <div class=\"vibra\" id = \"aceitar\" style=\"display:none\">\r\n        ACEITAR CORRIDA.\r\n\r\n        \r\n      </div>\r\n              \r\n            </div>\r\n        </div>\r\n       \r\n    </div>\r\n  </ion-content>\r\n  "

/***/ }),

/***/ "./src/app/pages/home-results/home-results.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/home-results/home-results.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n@import url(\"https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,400;0,500;1,400;1,500&display=swap\");\n@charset \"UTF-8\";\n@media (max-width: 992px) {\n  .navbar-collapse {\n    position: fixed;\n    top: 0px;\n    left: 0;\n    padding-left: 15px;\n    padding-right: 15px;\n    padding-bottom: 15px;\n    width: 75%;\n    height: 100%;\n    background: #1765e2;\n    text-transform: uppercase;\n    z-index: 9999;\n    box-shadow: 0px 0px 90px black; }\n  .navbar-collapse a.nav-link {\n    color: white !important;\n    font-weight: bold;\n    font-size: 19px; }\n  a.nav-link svg {\n    font-size: 25px;\n    margin-right: 10px; }\n  .navbar-dark .navbar-toggler {\n    background: #1b2c7b; }\n  .navbar-dark span.navbar-toggler-icon {\n    padding: 15px; }\n  .navbar-collapse.collapsing {\n    left: -75%;\n    transition: height 0s ease; }\n  .navbar-collapse.show {\n    margin-top: 0px;\n    left: 0;\n    transition: left 300ms ease-in-out; }\n  .navbar-toggler.collapsed ~ .navbar-collapse {\n    transition: left 500ms ease-in-out; } }\nbody {\n  background: url('bg-uai-leva.png') no-repeat #eaeaea;\n  font-family: 'Rubik', sans-serif;\n  background-position: 80px bottom;\n  background-size: contain;\n  height: 100vh; }\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  text-transform: uppercase;\n  margin: 20px 0;\n  font-weight: bold;\n  color: #123b7d; }\n.navbar.bg-dark {\n  background: #1c72fd !important; }\n.boxConteudo {\n  background: white;\n  padding: 10px;\n  border-radius: 20px;\n  box-shadow: 0px 0px 10px #e4e4e4;\n  position: absolute !important;\n  display: block;\n  margin-left: 13px;\n  margin-right: auto;\n  z-index: 9999 !important;\n  /* número máximo é 9999 */\n  width: calc(100% - 25px) !important; }\n.pagamentos {\n  background: white;\n  padding: 10px;\n  border-radius: 20px;\n  box-shadow: 0px 0px 10px #e4e4e4;\n  position: fixed !important;\n  z-index: 9999 !important;\n  /* número máximo é 9999 */\n  width: 100%;\n  height: 224px;\n  margin-top: 120%; }\nbutton.btn.btn-primary.btn-lg.rounded-pill.escolherDestino {\n  font-size: 15px; }\n.boxConteudo label {\n  text-transform: uppercase;\n  font-size: 12px; }\n.boxConteudo .form-control {\n  height: calc(1.5em + .75rem + 4px); }\n/*.buttonArea {\r\n    margin: 20px -20px 0px -20px;\r\n    background: #101a4c;\r\n    width: 100%;\r\n    display: block;\r\n    position: absolute;\r\n    z-index: 0;\r\n    bottom: -50px;\r\n    padding: 10px;\r\n    border-radius: 0 0 20px 20px;\r\n    }*/\n.buttonArea button {\n  width: 100%; }\n.buttonArea svg {\n  font-size: 25px;\n  margin-right: 5px; }\nbutton.navbar-toggler {\n  border-radius: 100px !important;\n  height: 55px !important;\n  background: #ffffff;\n  border: #92f328 solid 0px;\n  outline: none; }\n.boxConteudo .btn {\n  font-weight: bold; }\n.mapa iframe {\n  border: 0;\n  width: 100%;\n  height: 100vh;\n  position: relative;\n  top: 0px;\n  z-index: -1; }\n.saudacao {\n  text-align: center; }\n.homePassageiro .boxConteudo {\n  position: absolute !important;\n  top: 30px;\n  width: calc(100% - 0px);\n  margin: 0 auto;\n  padding: 15px 5px; }\n.homePassageiro .buttonArea button.rounded-pill {\n  font-size: 15px; }\nbutton.escolherDestino {\n  width: calc(100% - 60px) !important;\n  float: left; }\nbutton.embarqueRapido {\n  float: right;\n  padding: 0px 12px;\n  width: 45px !important;\n  height: 45px; }\nbutton.embarqueRapido svg {\n  width: 19px;\n  color: #416313; }\nbutton.btn.btn-primary.btn-lg.rounded-pill.embarqueRapido {\n  background: #b1ff49;\n  border: none; }\n.formDestino {\n  border: none;\n  margin-top: 5px; }\n.endDestinoRecente {\n  font-size: 11px;\n  background: #eee;\n  padding: 14px;\n  border-radius: 5px; }\n.endDestinoRecente b {\n  font-weight: normal;\n  color: darkgray; }\n.tituloDestinoRecente {\n  text-transform: uppercase;\n  font-weight: bold; }\nion-content {\n  --background: var(--ion-color-light); }\nion-menu-button {\n  background-color: white;\n  border-radius: 100px;\n  width: 50px;\n  height: 50px; }\n.navbar-light .navbar-toggler-icon {\n  padding: 15px;\n  background-color: white !important;\n  border-radius: 100px; }\nion-item {\n  border-radius: 0;\n  border-bottom: 1px dotted var(--ion-color-medium); }\nion-card.no-radius {\n  border-radius: 0; }\n#map {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  background: #eee; }\n.gmnoprint {\n  display: none !important; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS1yZXN1bHRzL0M6XFxVc2Vyc1xcamVzc2lcXERvY3VtZW50c1xcdWFpbGV2YWRyaXZlci9zcmNcXGFwcFxccGFnZXNcXGhvbWUtcmVzdWx0c1xcaG9tZS1yZXN1bHRzLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvaG9tZS1yZXN1bHRzL2hvbWUtcmVzdWx0cy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLDRHQUFZO0FDQVosZ0JBQWdCO0FER2hCO0VBRUk7SUFDSSxlQUFlO0lBQ2YsUUFBUTtJQUNSLE9BQU87SUFDUCxrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLG9CQUFvQjtJQUNwQixVQUFVO0lBQ1YsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQix5QkFBeUI7SUFDekIsYUFBYTtJQUNiLDhCQUE4QixFQUFBO0VBR2xDO0lBQ0EsdUJBQXVCO0lBQ3ZCLGlCQUFpQjtJQUNqQixlQUFlLEVBQUE7RUFHZjtJQUNBLGVBQWU7SUFDZixrQkFBa0IsRUFBQTtFQUdsQjtJQUNJLG1CQUFtQixFQUFBO0VBR3ZCO0lBQ0ksYUFBYSxFQUFBO0VBS2pCO0lBQ0ksVUFBVTtJQUNWLDBCQUEwQixFQUFBO0VBRzlCO0lBQ0ksZUFBZTtJQUNmLE9BQU87SUFDUCxrQ0FBa0MsRUFBQTtFQUd0QztJQUNJLGtDQUFrQyxFQUFBLEVBQ3JDO0FBSUY7RUFDQyxvREFBbUU7RUFDbkUsZ0NBQWdDO0VBQ2hDLGdDQUFnQztFQUNoQyx3QkFBd0I7RUFDeEIsYUFBYSxFQUFBO0FBR2I7RUFDQSxpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLHlCQUF5QjtFQUN6QixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGNBQWMsRUFBQTtBQUdkO0VBQ0ksOEJBQTRCLEVBQUE7QUFHaEM7RUFDSSxpQkFBaUI7RUFDakIsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixnQ0FBZ0M7RUFDaEMsNkJBQTZCO0VBRTdCLGNBQWM7RUFDZCxpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLHdCQUF3QjtFQUFFLHlCQUFBO0VBQzFCLG1DQUFtQyxFQUFBO0FBR3ZDO0VBQ0ksaUJBQWlCO0VBQ2pCLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsZ0NBQWdDO0VBQ2hDLDBCQUEwQjtFQUMxQix3QkFBd0I7RUFBRSx5QkFBQTtFQUMxQixXQUFXO0VBQ1gsYUFBYTtFQUNiLGdCQUFnQixFQUFBO0FBSXBCO0VBQ0ksZUFBZSxFQUFBO0FBRW5CO0VBQ0kseUJBQXlCO0VBQ3pCLGVBQWUsRUFBQTtBQUduQjtFQUNJLGtDQUFrQyxFQUFBO0FBR3RDOzs7Ozs7Ozs7O01DbEJFO0FEOEJGO0VBRUksV0FBVyxFQUFBO0FBS2Y7RUFDQSxlQUFlO0VBQ2YsaUJBQWlCLEVBQUE7QUFHakI7RUFDQSwrQkFBK0I7RUFDL0IsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQix5QkFBeUI7RUFDekIsYUFBYSxFQUFBO0FBR2I7RUFDQSxpQkFBaUIsRUFBQTtBQUdqQjtFQUNJLFNBQVM7RUFDVCxXQUFXO0VBQ1gsYUFBWTtFQUNaLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsV0FBVyxFQUFBO0FBR2Y7RUFDSSxrQkFBa0IsRUFBQTtBQUd0QjtFQUNJLDZCQUE2QjtFQUM3QixTQUFTO0VBQ1QsdUJBQXVCO0VBQ3ZCLGNBQWM7RUFDZCxpQkFBaUIsRUFBQTtBQUdyQjtFQUNJLGVBQWUsRUFBQTtBQUduQjtFQUNJLG1DQUFtQztFQUNuQyxXQUFXLEVBQUE7QUFHZjtFQUNJLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsc0JBQXNCO0VBQ3RCLFlBQVksRUFBQTtBQUdoQjtFQUNJLFdBQVc7RUFDWCxjQUFjLEVBQUE7QUFHbEI7RUFDSyxtQkFBbUI7RUFDbkIsWUFBWSxFQUFBO0FBR2pCO0VBQ0ksWUFBWTtFQUNaLGVBQWUsRUFBQTtBQUduQjtFQUNJLGVBQWU7RUFDZixnQkFBZ0I7RUFDaEIsYUFBYTtFQUNiLGtCQUFrQixFQUFBO0FBR3RCO0VBQ0ksbUJBQW1CO0VBQ25CLGVBQWUsRUFBQTtBQUduQjtFQUNJLHlCQUF5QjtFQUN6QixpQkFBaUIsRUFBQTtBQUlyQjtFQUNJLG9DQUFhLEVBQUE7QUFHakI7RUFDSSx1QkFBdUI7RUFDdkIsb0JBQW9CO0VBQ3BCLFdBQVc7RUFDWCxZQUFZLEVBQUE7QUFHaEI7RUFDSSxhQUFhO0VBQ2Isa0NBQWtDO0VBQ2xDLG9CQUFvQixFQUFBO0FBSXhCO0VBQ0ksZ0JBQWdCO0VBQ2hCLGlEQUFpRCxFQUFBO0FBR3JEO0VBRVEsZ0JBQWdCLEVBQUE7QUFLeEI7RUFDSSxrQkFBa0I7RUFDbEIsV0FBVztFQUNYLFlBQVk7RUFDWixnQkFBZ0IsRUFBQTtBQUdwQjtFQUNJLHdCQUF3QixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaG9tZS1yZXN1bHRzL2hvbWUtcmVzdWx0cy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0IHVybCgnaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3MyP2ZhbWlseT1SdWJpazppdGFsLHdnaHRAMCw0MDA7MCw1MDA7MSw0MDA7MSw1MDAmZGlzcGxheT1zd2FwJyk7XHJcblxyXG5cclxuQG1lZGlhIChtYXgtd2lkdGg6IDk5MnB4KSB7XHJcblxyXG4gICAgLm5hdmJhci1jb2xsYXBzZSB7XHJcbiAgICAgICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgICAgIHRvcDogMHB4O1xyXG4gICAgICAgIGxlZnQ6IDA7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xyXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDE1cHg7XHJcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDE1cHg7XHJcbiAgICAgICAgd2lkdGg6IDc1JTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzE3NjVlMjtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgIHotaW5kZXg6IDk5OTk7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDBweCA5MHB4IGJsYWNrO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAubmF2YmFyLWNvbGxhcHNlIGEubmF2LWxpbmsge1xyXG4gICAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGZvbnQtc2l6ZTogMTlweDtcclxuICAgIH1cclxuXHJcbiAgICBhLm5hdi1saW5rIHN2ZyB7XHJcbiAgICBmb250LXNpemU6IDI1cHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5uYXZiYXItZGFyayAubmF2YmFyLXRvZ2dsZXIge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICMxYjJjN2I7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5uYXZiYXItZGFyayBzcGFuLm5hdmJhci10b2dnbGVyLWljb24ge1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAvL2JhY2tncm91bmQtaW1hZ2U6IHVybChkYXRhOmltYWdlL3N2Zyt4bWwsJTNjc3ZnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zycgd2lkdGg9JzMwJyBoZWlnaHQ9JzMwJyB2aWV3Qm94PScwIDAgMzAgMzAnJTNlJTNjcGF0aCBzdHJva2U9J3JnYmElMjgyNTUsIDI1NSwgMjU1LCAwLjklMjknIHN0cm9rZS1saW5lY2FwPSdyb3VuZCcgc3Ryb2tlLW1pdGVybGltaXQ9JzEwJyBzdHJva2Utd2lkdGg9JzInIGQ9J000IDdoMjJNNCAxNWgyMk00IDIzaDIyJy8lM2UlM2Mvc3ZnJTNlKTtcclxuICAgIH1cclxuXHJcbiAgIFxyXG4gICAgLm5hdmJhci1jb2xsYXBzZS5jb2xsYXBzaW5nIHtcclxuICAgICAgICBsZWZ0OiAtNzUlO1xyXG4gICAgICAgIHRyYW5zaXRpb246IGhlaWdodCAwcyBlYXNlO1xyXG4gICAgfVxyXG5cclxuICAgIC5uYXZiYXItY29sbGFwc2Uuc2hvdyB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMHB4O1xyXG4gICAgICAgIGxlZnQ6IDA7XHJcbiAgICAgICAgdHJhbnNpdGlvbjogbGVmdCAzMDBtcyBlYXNlLWluLW91dDtcclxuICAgIH1cclxuXHJcbiAgICAubmF2YmFyLXRvZ2dsZXIuY29sbGFwc2VkIH4gLm5hdmJhci1jb2xsYXBzZSB7XHJcbiAgICAgICAgdHJhbnNpdGlvbjogbGVmdCA1MDBtcyBlYXNlLWluLW91dDtcclxuICAgIH1cclxuICAgIFxyXG59XHJcblxyXG4gICBib2R5IHtcclxuICAgIGJhY2tncm91bmQ6IHVybCgnc3JjL2Fzc2V0cy9pbWcvYmctdWFpLWxldmEucG5nJykgbm8tcmVwZWF0ICNlYWVhZWE7XHJcbiAgICBmb250LWZhbWlseTogJ1J1YmlrJywgc2Fucy1zZXJpZjtcclxuICAgIGJhY2tncm91bmQtcG9zaXRpb246IDgwcHggYm90dG9tO1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG4gICAgaGVpZ2h0OiAxMDB2aDtcclxufVxyXG5cclxuICAgIGgxIHtcclxuICAgIGZvbnQtc2l6ZTogMS41cmVtO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIG1hcmdpbjogMjBweCAwO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBjb2xvcjogIzEyM2I3ZDtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLm5hdmJhci5iZy1kYXJrIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiMxYzcyZmQhaW1wb3J0YW50XHJcbiAgICB9XHJcblxyXG4gICAgLmJveENvbnRldWRvIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDBweCAxMHB4ICNlNGU0ZTQ7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgLy9kaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7IFxyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxM3B4OyBcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgICAgICAgei1pbmRleDogOTk5OSAhaW1wb3J0YW50OyAvKiBuw7ptZXJvIG3DoXhpbW8gw6kgOTk5OSAqL1xyXG4gICAgICAgIHdpZHRoOiBjYWxjKDEwMCUgLSAyNXB4KSAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG5cclxuICAgIC5wYWdhbWVudG9zIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDBweCAxMHB4ICNlNGU0ZTQ7XHJcbiAgICAgICAgcG9zaXRpb246IGZpeGVkICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgei1pbmRleDogOTk5OSAhaW1wb3J0YW50OyAvKiBuw7ptZXJvIG3DoXhpbW8gw6kgOTk5OSAqL1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDogMjI0cHg7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMTIwJTtcclxuICAgIH1cclxuICAgIFxyXG5cclxuICAgIGJ1dHRvbi5idG4uYnRuLXByaW1hcnkuYnRuLWxnLnJvdW5kZWQtcGlsbC5lc2NvbGhlckRlc3Rpbm8ge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIH1cclxuICAgIC5ib3hDb250ZXVkbyBsYWJlbCB7XHJcbiAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5ib3hDb250ZXVkbyAuZm9ybS1jb250cm9sIHtcclxuICAgICAgICBoZWlnaHQ6IGNhbGMoMS41ZW0gKyAuNzVyZW0gKyA0cHgpO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAvKi5idXR0b25BcmVhIHtcclxuICAgIG1hcmdpbjogMjBweCAtMjBweCAwcHggLTIwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMTAxYTRjO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgICBib3R0b206IC01MHB4O1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDAgMCAyMHB4IDIwcHg7XHJcbiAgICB9Ki9cclxuICAgIFxyXG4gICAgLmJ1dHRvbkFyZWEgYnV0dG9uIHtcclxuICAgICAgICAvL3dpZHRoOiA5cHg7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgLy93aWR0aDogbWF4LWNvbnRlbnQ7XHJcbiAgICAgICAgLy93aWR0aDogY2FsYygxMDAlIC0gNjBweCkgIWltcG9ydGFudDtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLmJ1dHRvbkFyZWEgc3ZnIHtcclxuICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgIG1hcmdpbi1yaWdodDogNXB4O1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICBidXR0b24ubmF2YmFyLXRvZ2dsZXIge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTAwcHggIWltcG9ydGFudDtcclxuICAgIGhlaWdodDogNTVweCAhaW1wb3J0YW50O1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZmZmZjtcclxuICAgIGJvcmRlcjogIzkyZjMyOCBzb2xpZCAwcHg7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAuYm94Q29udGV1ZG8gLmJ0biB7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIH1cclxuXHJcbiAgICAubWFwYSBpZnJhbWUge1xyXG4gICAgICAgIGJvcmRlcjogMDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6MTAwdmg7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIHRvcDogMHB4O1xyXG4gICAgICAgIHotaW5kZXg6IC0xO1xyXG4gICAgfVxyXG5cclxuICAgIC5zYXVkYWNhbyB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG5cclxuICAgIC5ob21lUGFzc2FnZWlybyAuYm94Q29udGV1ZG8ge1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xyXG4gICAgICAgIHRvcDogMzBweDtcclxuICAgICAgICB3aWR0aDogY2FsYygxMDAlIC0gMHB4KTtcclxuICAgICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgICAgICBwYWRkaW5nOiAxNXB4IDVweDtcclxuICAgIH1cclxuXHJcbiAgICAuaG9tZVBhc3NhZ2Vpcm8gLmJ1dHRvbkFyZWEgYnV0dG9uLnJvdW5kZWQtcGlsbCB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgfVxyXG5cclxuICAgIGJ1dHRvbi5lc2NvbGhlckRlc3Rpbm8ge1xyXG4gICAgICAgIHdpZHRoOiBjYWxjKDEwMCUgLSA2MHB4KSAhaW1wb3J0YW50O1xyXG4gICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgfVxyXG5cclxuICAgIGJ1dHRvbi5lbWJhcnF1ZVJhcGlkbyB7XHJcbiAgICAgICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgICAgIHBhZGRpbmc6IDBweCAxMnB4O1xyXG4gICAgICAgIHdpZHRoOiA0NXB4ICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgaGVpZ2h0OiA0NXB4O1xyXG4gICAgfVxyXG5cclxuICAgIGJ1dHRvbi5lbWJhcnF1ZVJhcGlkbyBzdmcge1xyXG4gICAgICAgIHdpZHRoOiAxOXB4O1xyXG4gICAgICAgIGNvbG9yOiAjNDE2MzEzO1xyXG4gICAgfVxyXG5cclxuICAgIGJ1dHRvbi5idG4uYnRuLXByaW1hcnkuYnRuLWxnLnJvdW5kZWQtcGlsbC5lbWJhcnF1ZVJhcGlkbyB7XHJcbiAgICAgICAgIGJhY2tncm91bmQ6ICNiMWZmNDk7XHJcbiAgICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgIH1cclxuXHJcbiAgICAuZm9ybURlc3Rpbm8ge1xyXG4gICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgLmVuZERlc3Rpbm9SZWNlbnRlIHtcclxuICAgICAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2VlZTtcclxuICAgICAgICBwYWRkaW5nOiAxNHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIH1cclxuXHJcbiAgICAuZW5kRGVzdGlub1JlY2VudGUgYiB7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgICBjb2xvcjogZGFya2dyYXk7XHJcbiAgICB9XHJcblxyXG4gICAgLnRpdHVsb0Rlc3Rpbm9SZWNlbnRlIHtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgfVxyXG5cclxuXHJcbiAgICBpb24tY29udGVudCB7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG4gICAgfVxyXG5cclxuICAgIGlvbi1tZW51LWJ1dHRvbntcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMDBweDtcclxuICAgICAgICB3aWR0aDogNTBweDtcclxuICAgICAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLm5hdmJhci1saWdodCAubmF2YmFyLXRvZ2dsZXItaWNvbiB7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xyXG4gICAgICAgIC8vYmFja2dyb3VuZC1pbWFnZTogdXJsKGRhdGE6aW1hZ2Uvc3ZnK3htbCwlM2NzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyB3aWR0aD0nMzAnIGhlaWdodD0nMzAnIHZpZXdCb3g9JzAgMCAzMCAzMCclM2UlM2NwYXRoIHN0cm9rZT0ncmdiYSUyODAsIDAsIDAsIDAuNSUyOScgc3Ryb2tlLWxpbmVjYXA9J3JvdW5kJyBzdHJva2UtbWl0ZXJsaW1pdD0nMTAnIHN0cm9rZS13aWR0aD0nMicgZD0nTTQgN2gyMk00IDE1aDIyTTQgMjNoMjInLyUzZSUzYy9zdmclM2UpO1xyXG4gICAgfVxyXG5cclxuICAgIGlvbi1pdGVtIHtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBkb3R0ZWQgdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XHJcbiAgICB9XHJcblxyXG4gICAgaW9uLWNhcmQge1xyXG4gICAgICAgICYubm8tcmFkaXVzIHtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMDtcclxuICAgICAgICB9XHJcblx0XHRcclxuICAgIH1cclxuXHJcbiAgICAjbWFwIHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNlZWU7XHJcbiAgICB9XHJcblxyXG4gICAgLmdtbm9wcmludCB7XHJcbiAgICAgICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xyXG4gICAgfSIsIkBjaGFyc2V0IFwiVVRGLThcIjtcbkBpbXBvcnQgdXJsKFwiaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3MyP2ZhbWlseT1SdWJpazppdGFsLHdnaHRAMCw0MDA7MCw1MDA7MSw0MDA7MSw1MDAmZGlzcGxheT1zd2FwXCIpO1xuQG1lZGlhIChtYXgtd2lkdGg6IDk5MnB4KSB7XG4gIC5uYXZiYXItY29sbGFwc2Uge1xuICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICB0b3A6IDBweDtcbiAgICBsZWZ0OiAwO1xuICAgIHBhZGRpbmctbGVmdDogMTVweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xuICAgIHBhZGRpbmctYm90dG9tOiAxNXB4O1xuICAgIHdpZHRoOiA3NSU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGJhY2tncm91bmQ6ICMxNzY1ZTI7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICB6LWluZGV4OiA5OTk5O1xuICAgIGJveC1zaGFkb3c6IDBweCAwcHggOTBweCBibGFjazsgfVxuICAubmF2YmFyLWNvbGxhcHNlIGEubmF2LWxpbmsge1xuICAgIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGZvbnQtc2l6ZTogMTlweDsgfVxuICBhLm5hdi1saW5rIHN2ZyB7XG4gICAgZm9udC1zaXplOiAyNXB4O1xuICAgIG1hcmdpbi1yaWdodDogMTBweDsgfVxuICAubmF2YmFyLWRhcmsgLm5hdmJhci10b2dnbGVyIHtcbiAgICBiYWNrZ3JvdW5kOiAjMWIyYzdiOyB9XG4gIC5uYXZiYXItZGFyayBzcGFuLm5hdmJhci10b2dnbGVyLWljb24ge1xuICAgIHBhZGRpbmc6IDE1cHg7IH1cbiAgLm5hdmJhci1jb2xsYXBzZS5jb2xsYXBzaW5nIHtcbiAgICBsZWZ0OiAtNzUlO1xuICAgIHRyYW5zaXRpb246IGhlaWdodCAwcyBlYXNlOyB9XG4gIC5uYXZiYXItY29sbGFwc2Uuc2hvdyB7XG4gICAgbWFyZ2luLXRvcDogMHB4O1xuICAgIGxlZnQ6IDA7XG4gICAgdHJhbnNpdGlvbjogbGVmdCAzMDBtcyBlYXNlLWluLW91dDsgfVxuICAubmF2YmFyLXRvZ2dsZXIuY29sbGFwc2VkIH4gLm5hdmJhci1jb2xsYXBzZSB7XG4gICAgdHJhbnNpdGlvbjogbGVmdCA1MDBtcyBlYXNlLWluLW91dDsgfSB9XG5cbmJvZHkge1xuICBiYWNrZ3JvdW5kOiB1cmwoXCJzcmMvYXNzZXRzL2ltZy9iZy11YWktbGV2YS5wbmdcIikgbm8tcmVwZWF0ICNlYWVhZWE7XG4gIGZvbnQtZmFtaWx5OiAnUnViaWsnLCBzYW5zLXNlcmlmO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA4MHB4IGJvdHRvbTtcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICBoZWlnaHQ6IDEwMHZoOyB9XG5cbmgxIHtcbiAgZm9udC1zaXplOiAxLjVyZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgbWFyZ2luOiAyMHB4IDA7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzEyM2I3ZDsgfVxuXG4ubmF2YmFyLmJnLWRhcmsge1xuICBiYWNrZ3JvdW5kOiAjMWM3MmZkICFpbXBvcnRhbnQ7IH1cblxuLmJveENvbnRldWRvIHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIGJveC1zaGFkb3c6IDBweCAwcHggMTBweCAjZTRlNGU0O1xuICBwb3NpdGlvbjogYWJzb2x1dGUgIWltcG9ydGFudDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbi1sZWZ0OiAxM3B4O1xuICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gIHotaW5kZXg6IDk5OTkgIWltcG9ydGFudDtcbiAgLyogbsO6bWVybyBtw6F4aW1vIMOpIDk5OTkgKi9cbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDI1cHgpICFpbXBvcnRhbnQ7IH1cblxuLnBhZ2FtZW50b3Mge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgYm94LXNoYWRvdzogMHB4IDBweCAxMHB4ICNlNGU0ZTQ7XG4gIHBvc2l0aW9uOiBmaXhlZCAhaW1wb3J0YW50O1xuICB6LWluZGV4OiA5OTk5ICFpbXBvcnRhbnQ7XG4gIC8qIG7Dum1lcm8gbcOheGltbyDDqSA5OTk5ICovXG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDIyNHB4O1xuICBtYXJnaW4tdG9wOiAxMjAlOyB9XG5cbmJ1dHRvbi5idG4uYnRuLXByaW1hcnkuYnRuLWxnLnJvdW5kZWQtcGlsbC5lc2NvbGhlckRlc3Rpbm8ge1xuICBmb250LXNpemU6IDE1cHg7IH1cblxuLmJveENvbnRldWRvIGxhYmVsIHtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC1zaXplOiAxMnB4OyB9XG5cbi5ib3hDb250ZXVkbyAuZm9ybS1jb250cm9sIHtcbiAgaGVpZ2h0OiBjYWxjKDEuNWVtICsgLjc1cmVtICsgNHB4KTsgfVxuXG4vKi5idXR0b25BcmVhIHtcclxuICAgIG1hcmdpbjogMjBweCAtMjBweCAwcHggLTIwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMTAxYTRjO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgICBib3R0b206IC01MHB4O1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDAgMCAyMHB4IDIwcHg7XHJcbiAgICB9Ki9cbi5idXR0b25BcmVhIGJ1dHRvbiB7XG4gIHdpZHRoOiAxMDAlOyB9XG5cbi5idXR0b25BcmVhIHN2ZyB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7IH1cblxuYnV0dG9uLm5hdmJhci10b2dnbGVyIHtcbiAgYm9yZGVyLXJhZGl1czogMTAwcHggIWltcG9ydGFudDtcbiAgaGVpZ2h0OiA1NXB4ICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gIGJvcmRlcjogIzkyZjMyOCBzb2xpZCAwcHg7XG4gIG91dGxpbmU6IG5vbmU7IH1cblxuLmJveENvbnRldWRvIC5idG4ge1xuICBmb250LXdlaWdodDogYm9sZDsgfVxuXG4ubWFwYSBpZnJhbWUge1xuICBib3JkZXI6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMHZoO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogMHB4O1xuICB6LWluZGV4OiAtMTsgfVxuXG4uc2F1ZGFjYW8ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7IH1cblxuLmhvbWVQYXNzYWdlaXJvIC5ib3hDb250ZXVkbyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xuICB0b3A6IDMwcHg7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSAwcHgpO1xuICBtYXJnaW46IDAgYXV0bztcbiAgcGFkZGluZzogMTVweCA1cHg7IH1cblxuLmhvbWVQYXNzYWdlaXJvIC5idXR0b25BcmVhIGJ1dHRvbi5yb3VuZGVkLXBpbGwge1xuICBmb250LXNpemU6IDE1cHg7IH1cblxuYnV0dG9uLmVzY29saGVyRGVzdGlubyB7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSA2MHB4KSAhaW1wb3J0YW50O1xuICBmbG9hdDogbGVmdDsgfVxuXG5idXR0b24uZW1iYXJxdWVSYXBpZG8ge1xuICBmbG9hdDogcmlnaHQ7XG4gIHBhZGRpbmc6IDBweCAxMnB4O1xuICB3aWR0aDogNDVweCAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDQ1cHg7IH1cblxuYnV0dG9uLmVtYmFycXVlUmFwaWRvIHN2ZyB7XG4gIHdpZHRoOiAxOXB4O1xuICBjb2xvcjogIzQxNjMxMzsgfVxuXG5idXR0b24uYnRuLmJ0bi1wcmltYXJ5LmJ0bi1sZy5yb3VuZGVkLXBpbGwuZW1iYXJxdWVSYXBpZG8ge1xuICBiYWNrZ3JvdW5kOiAjYjFmZjQ5O1xuICBib3JkZXI6IG5vbmU7IH1cblxuLmZvcm1EZXN0aW5vIHtcbiAgYm9yZGVyOiBub25lO1xuICBtYXJnaW4tdG9wOiA1cHg7IH1cblxuLmVuZERlc3Rpbm9SZWNlbnRlIHtcbiAgZm9udC1zaXplOiAxMXB4O1xuICBiYWNrZ3JvdW5kOiAjZWVlO1xuICBwYWRkaW5nOiAxNHB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7IH1cblxuLmVuZERlc3Rpbm9SZWNlbnRlIGIge1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICBjb2xvcjogZGFya2dyYXk7IH1cblxuLnRpdHVsb0Rlc3Rpbm9SZWNlbnRlIHtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7IH1cblxuaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7IH1cblxuaW9uLW1lbnUtYnV0dG9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xuICB3aWR0aDogNTBweDtcbiAgaGVpZ2h0OiA1MHB4OyB9XG5cbi5uYXZiYXItbGlnaHQgLm5hdmJhci10b2dnbGVyLWljb24ge1xuICBwYWRkaW5nOiAxNXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xuICBib3JkZXItcmFkaXVzOiAxMDBweDsgfVxuXG5pb24taXRlbSB7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIGJvcmRlci1ib3R0b206IDFweCBkb3R0ZWQgdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7IH1cblxuaW9uLWNhcmQubm8tcmFkaXVzIHtcbiAgYm9yZGVyLXJhZGl1czogMDsgfVxuXG4jbWFwIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBiYWNrZ3JvdW5kOiAjZWVlOyB9XG5cbi5nbW5vcHJpbnQge1xuICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7IH1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/pages/home-results/home-results.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/home-results/home-results.page.ts ***!
  \*********************************************************/
/*! exports provided: HomeResultsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeResultsPage", function() { return HomeResultsPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/google-maps */ "./node_modules/@ionic-native/google-maps/index.js");
/* harmony import */ var src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/user/user.service */ "./src/app/core/user/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _viagensService__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./viagensService */ "./src/app/pages/home-results/viagensService.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var HomeResultsPage = /** @class */ (function () {
    function HomeResultsPage(platform, loadCtrl, ngZone, navCtrl, menuCtrl, popoverCtrl, alertCtrl, modalCtrl, toastCtrl, userService, router, viagensService) {
        this.platform = platform;
        this.loadCtrl = loadCtrl;
        this.ngZone = ngZone;
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.userService = userService;
        this.router = router;
        this.viagensService = viagensService;
        this.search = '';
        this.search2 = '';
        this.googleAutocomplete = new google.maps.places.AutocompleteService();
        this.searchResults = new Array();
        this.searchResults2 = new Array();
        this.googleDirectionsService = new google.maps.DirectionsService();
        this.matrix = new google.maps.DistanceMatrixService();
        this.endOrigin = '';
        this.users = [];
        this.user$ = userService.getUser();
        viagensService.authenticate();
    }
    HomeResultsPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(true);
        this.menuCtrl.close();
    };
    HomeResultsPage.prototype.ngOnInit = function () {
        this.mapElement = this.mapElement.nativeElement;
        this.mapElement.style.width = this.platform.width() + 'px';
        this.mapElement.style.height = this.platform.height() + 'px';
        this.loadMap();
    };
    HomeResultsPage.prototype.loadMap = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, mapOptions, error_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.loadCtrl.create({ message: 'Por favor aguarde ...' })];
                    case 1:
                        _a.loading = _b.sent();
                        return [4 /*yield*/, this.loading.present()];
                    case 2:
                        _b.sent();
                        _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["Environment"].setEnv({
                            'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyBBSA6wXmuyhUqg1fDMH4-WP3QZSRSDtr4',
                            'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyBBSA6wXmuyhUqg1fDMH4-WP3QZSRSDtr4'
                        });
                        mapOptions = {
                            controls: {
                                zoom: false
                            }
                        };
                        this.map = _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["GoogleMaps"].create(this.mapElement, mapOptions);
                        _b.label = 3;
                    case 3:
                        _b.trys.push([3, 5, , 6]);
                        return [4 /*yield*/, this.map.one(_ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["GoogleMapsEvent"].MAP_READY)];
                    case 4:
                        _b.sent();
                        this.addOrigenMarker();
                        return [3 /*break*/, 6];
                    case 5:
                        error_1 = _b.sent();
                        console.error(error_1);
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    HomeResultsPage.prototype.addOrigenMarker = function () {
        return __awaiter(this, void 0, void 0, function () {
            var myLocation, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, 4, 5]);
                        return [4 /*yield*/, this.map.getMyLocation()];
                    case 1:
                        myLocation = _a.sent();
                        return [4 /*yield*/, this.map.moveCamera({
                                target: myLocation.latLng,
                                zoom: 18
                            })];
                    case 2:
                        _a.sent();
                        this.originMarker = this.map.addMarkerSync({
                            title: 'Origem',
                            icon: '#000',
                            animation: _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["GoogleMapsAnimation"].DROP,
                            position: myLocation.latLng
                        });
                        return [3 /*break*/, 5];
                    case 3:
                        error_2 = _a.sent();
                        console.error(error_2);
                        return [3 /*break*/, 5];
                    case 4:
                        this.loading.dismiss();
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    HomeResultsPage.prototype.searchChanged = function () {
        var _this = this;
        if (!this.search.trim().length)
            return;
        this.googleAutocomplete.getPlacePredictions({ input: this.search }, function (predictions) {
            _this.ngZone.run(function () {
                _this.searchResults = predictions;
            });
        });
    };
    HomeResultsPage.prototype.searchChanged2 = function () {
        var _this = this;
        if (!this.search2.trim().length)
            return;
        this.googleAutocomplete.getPlacePredictions({ input: this.search2 }, function (predictions) {
            _this.ngZone.run(function () {
                _this.searchResults2 = predictions;
            });
        });
    };
    HomeResultsPage.prototype.calcRoute2 = function (item) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.search2 = '';
                this.origem = item;
                console.log(this.origem);
                return [2 /*return*/];
            });
        });
    };
    HomeResultsPage.prototype.calcRoute = function (item) {
        return __awaiter(this, void 0, void 0, function () {
            var info, markerDestination;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.search = '';
                        this.destination = item;
                        console.log(this.origem);
                        return [4 /*yield*/, _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["Geocoder"].geocode({ address: this.destination.description })];
                    case 1:
                        info = _a.sent();
                        markerDestination = this.map.addMarkerSync({
                            title: this.destination.description,
                            icon: '#000',
                            animation: _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["GoogleMapsAnimation"].DROP,
                            position: info[0].position
                        });
                        // console.log('distancia origem: ' + this.originMarker.getPosition().lat + " long :  " + this.originMarker.getPosition().lng)
                        // console.log('distancia destino: ' + markerDestination.getPosition());
                        this.googleDirectionsService.route({
                            origin: this.originMarker.getPosition(),
                            destination: markerDestination.getPosition(),
                            travelMode: 'DRIVING'
                        }, function (results) { return __awaiter(_this, void 0, void 0, function () {
                            function callback(response, status) {
                                var travelDetailsObject;
                                if (status !== "OK") {
                                    alert("Error was: " + status);
                                }
                                else {
                                    var origins = response.originAddresses;
                                    var destinations = response.destinationAddresses;
                                    console.log(origins);
                                    console.log(destinations);
                                    for (var i = 0; i < origins.length; i++) {
                                        var results = response.rows[i].elements;
                                        for (var j = 0; j < results.length; j++) {
                                            var element = results[j];
                                            var distance = element.distance.text;
                                            var duration = element.duration.text;
                                            var from = origins[i];
                                            var to = destinations[j];
                                            travelDetailsObject = {
                                                distance: distance,
                                                duration: duration
                                            };
                                        }
                                    }
                                    this.travelDetailsObject = travelDetailsObject;
                                    this.corrida = this.travelDetailsObject.distance;
                                    var res = this.travelDetailsObject.distance;
                                    this.valorCorrida = "R$ 50,00";
                                    this.tempoEstimadoCorrida = this.travelDetailsObject.duration;
                                    console.log('distancia : ' + this.travelDetailsObject.distance + ' no tempo de : ' + this.travelDetailsObject.duration);
                                }
                            }
                            var points, routes, i, origin1, destinationB;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        points = new Array();
                                        routes = results.routes[0].overview_path;
                                        for (i = 0; i < routes.length; i++) {
                                            points[i] = {
                                                lat: routes[i].lat(),
                                                lng: routes[i].lng()
                                            };
                                        }
                                        return [4 /*yield*/, this.map.addPolyline({
                                                points: points,
                                                color: '#000',
                                                width: 3
                                            })];
                                    case 1:
                                        _a.sent();
                                        origin1 = new google.maps.LatLng(this.originMarker.getPosition().lat, this.originMarker.getPosition().lng);
                                        destinationB = new google.maps.LatLng(markerDestination.getPosition().lat, markerDestination.getPosition().lng);
                                        this.matrix.getDistanceMatrix({
                                            origins: [origin1],
                                            destinations: [destinationB],
                                            travelMode: google.maps.TravelMode.DRIVING
                                        }, callback);
                                        this.map.moveCamera({ target: points });
                                        return [2 /*return*/];
                                }
                            });
                        }); });
                        return [2 /*return*/];
                }
            });
        });
    };
    HomeResultsPage.prototype.back = function () {
        return __awaiter(this, void 0, void 0, function () {
            var error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.map.clear()];
                    case 1:
                        _a.sent();
                        this.destination = null;
                        this.addOrigenMarker();
                        return [3 /*break*/, 3];
                    case 2:
                        error_3 = _a.sent();
                        console.error(error_3);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    HomeResultsPage.prototype.toggleTeste = function () {
        if (document.getElementById("collapseExample").style.display == 'none') {
            document.getElementById("collapseExample").style.display = "block";
        }
        else {
            document.getElementById("collapseExample").style.display = "none";
        }
    };
    HomeResultsPage.prototype.toggleTeste1 = function () {
        if (document.getElementById("collapseExample1").style.display == 'none') {
            document.getElementById("collapseExample1").style.display = "block";
        }
        else {
            document.getElementById("collapseExample1").style.display = "none";
        }
    };
    HomeResultsPage.prototype.embarqueRapido = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: '',
                            cssClass: '',
                            subHeader: '',
                            message: 'Embarque rápido : <br> • Pegue um UaiLeva <br> • Escaneie o QR Code <br> • Inicie a corrida <br> Registrar o Cartão',
                            buttons: ['X']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomeResultsPage.prototype.logout = function () {
        this.userService.logout();
        this.router.navigate(['']);
    };
    HomeResultsPage.prototype.aceitarViagem = function () {
        if (document.getElementById("aceitar").style.display == 'none') {
            document.getElementById("aceitar").style.display = "block";
        }
        else {
            document.getElementById("aceitar").style.display = "none";
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('map'),
        __metadata("design:type", Object)
    ], HomeResultsPage.prototype, "mapElement", void 0);
    HomeResultsPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home-results.page.html */ "./src/app/pages/home-results/home-results.page.html"),
            styles: [__webpack_require__(/*! ./home-results.page.scss */ "./src/app/pages/home-results/home-results.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["Platform"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["LoadingController"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["MenuController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["PopoverController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ToastController"],
            src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _viagensService__WEBPACK_IMPORTED_MODULE_5__["ViagensService"]])
    ], HomeResultsPage);
    return HomeResultsPage;
}());



/***/ }),

/***/ "./src/app/pages/home-results/viagensService.ts":
/*!******************************************************!*\
  !*** ./src/app/pages/home-results/viagensService.ts ***!
  \******************************************************/
/*! exports provided: ViagensService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViagensService", function() { return ViagensService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//const API_URL = 'http://159.203.181.9:3000';
var API_URL = 'http://localhost:3000';
var ViagensService = /** @class */ (function () {
    function ViagensService(http) {
        this.http = http;
        this.viagem = [];
    }
    ViagensService.prototype.authenticate = function () {
        var _this = this;
        return this.http.get(API_URL + '/viagens/findAllViagensaguardando/').subscribe(function (viagem) {
            console.log(viagem[0]);
            _this.viagem = viagem;
        });
    };
    ViagensService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ViagensService);
    return ViagensService;
}());



/***/ })

}]);
//# sourceMappingURL=pages-home-results-home-results-module.js.map