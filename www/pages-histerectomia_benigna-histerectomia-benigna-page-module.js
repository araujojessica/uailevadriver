(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-histerectomia_benigna-histerectomia-benigna-page-module"],{

/***/ "./src/app/pages/histerectomia_benigna/histerectomia.benigna.page.html":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/histerectomia_benigna/histerectomia.benigna.page.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar color=\"secondary\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button color=\"light\"></ion-menu-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n        Útero\r\n    </ion-title>\r\n  </ion-toolbar>\r\n<!--  <ion-toolbar color=\"light\"> \r\n    <ion-searchbar [(ngModel)]=\"searchKey\"></ion-searchbar>\r\n    \r\n  </ion-toolbar>-->\r\n</ion-header>\r\n\r\n<ion-content>\r\n <ion-card class=\"bg-white\" no-margin>\r\n    <ion-card-content>\r\n      <h2 margin-bottom>\r\n        <ion-text color=\"dark\"><strong>Histerectomia por lesões benignas/Orientações gerais </strong></ion-text>\r\n      </h2>\r\n      <br>O procedimento consiste na remoção do órgão, por completo. Atualmente, encontra-se em desuso remoções parciais como a histerectomia supracervical. \r\n      <br>As histerectomias se dividem em simples e radical. Nesta última, o procedimento é usual em casos malignos, e vem acompanhada de tecido mole parametriais e/ou paracervicais e coto vaginal.\r\n      <br>Podem vir acompanhadas, também, de salpingo-ooforectomia uni ou bilateral, variando de acordo com a idade do paciente e da etiologia da doença.\r\n      <br>São realizadas tanto pela cavidade abdominal quanto por via vaginal, sendo a vaginal mais comum em casos benignos.\r\n      \r\n      <br><br>\r\n      <h3 margin-bottom text-dark><b>Procedimentos macroscópicos</b></h3>\r\n    <br>\r\n      <p margin-bottom text-dark>\r\n       1.\tAtente-se: Existem instruções específicas para os casos de Histerectomia por neoplasia maligna do colo do útero, do endométrio e hiperplasia endometrial.\r\n        <br>&nbsp;\tCaso a etiologia da cirurgia não seja nenhuma citada acima, prossiga.\r\n        <br>2.\tNo primeiro momento, deve-se realizar a orientação espacial da peça  <a href = '#' (click)=\"viewImage(imgSource7, imgTitle7, imgDescription7)\">  (Figura 8.2)</a>.\r\n        <br>&nbsp;\ta.\tA reflexão peritoneal mais baixa encontra-se na face posterior.\r\n        <br>&nbsp;\tb.\tOs anexos, se presentes, são posteriores em relação ao ligamento redondo.\r\n        <br>3.\tIdentifique o procedimento cirúrgico (histerectomia simples, radical, com/sem anexectomia).\r\n        <br>4.\tQuando presentes identificar os anexos e retira-los para posterior descrição e realização dos cortes. \r\n        <br>5.\tPese e mensure as três dimensões –comprimento, dimensão transversal e ântero-posterior– do útero.\r\n        <br>6.\tDescreva a forma do útero –irregular, piriforme, globoso e/ou outros–.\r\n        <br>7.\tAvalie e descreva a serosa –aderências fibrosas, lisas, laceradas e/ou outros –.\r\n        <br>8.\tCaso o útero tenha sido recebido a fresco e intacto:\r\n        <br>&nbsp;a.\tAbra-o ao longo de suas paredes laterais. Isso deve ser feito desde o colo até o fundo uterino utilizando uma faca afiada <a href = '#' (click)=\"viewImage(imgSource2, imgTitle2, imgDescription2)\">  (Foto 8.8a)</a>. \r\n        <br>&nbsp;b.\tMarque a metade anterior da peça. \r\n        <br>&nbsp;c.\tCasos em que as metades sejam maiores que do 2,0 cm de espessura, realize cortes transversais incompletos, com intervalos de 1 cm, para melhor fixação do útero <a href = '#' (click)=\"viewImage(imgSource3, imgTitle3, imgDescription3)\">  (Foto 8.8c)</a>.\r\n        <br>9.\tAgora, fixe o material em formol tamponado com 10x o volume da peça. \r\n        <br>10.\tApós fixação, realize cortes transversais paralelos em cada uma das metades (do istmo até o fundo), com intervalos de 1 cm (quando efetuado procedimento 8c, finalize os cortes).\r\n        <br>11.\tDescreva o colo e faça diversos cortes longitudinais ao longo do canal endocervical <a href = '#' (click)=\"viewImage(imgSource4, imgTitle4, imgDescription4)\">  (Foto 8.10.11)</a>.\r\n        <br>12.\tDescreva a espessura máxima da parede e se há presença de alterações.\r\n        <br>13.\tDescreva o endométrio –aspecto, espessura, pólipos (maior dimensão e forma) e cistos.\r\n        <br>14.\tNa presença de nódulo/mioma identificado, descreva o número, a localização –subserosa, intramural ou submucosa–, as dimensões, o aspecto –séssil ou pediculado, hemorragia, necrose e, por fim, calcificação. Realize ao menos um corte em cada.\r\n        <br>15.\tDescreva os anexos (quando presentes, ir para a seção de ooforectomia e/ou salpingectomia).\r\n        <br>\r\n        <br><b>Nota: </b>Ocasionalmente a peça poderá ser recebida com a presença dos paramétrios e/ou manguito/coto vaginal. Neste caso ir para a seção de Histerectomia por Neoplasia maligna do colo do útero.\r\n      </p>\r\n      <br>\r\n      <h3 margin-bottom text-dark (click)=\"toggleDetails();\" style=\"width:100%; cursor:pointer\">\r\n          <div style=\"width: 100%; float:left; background: #878787; color: #fff; padding: 9px;\"><b>Ex. de descrição macroscópica</b>\r\n          <div style=\"width: 4%; float:right;\"><i class=\"fa fa-angle-double-down\" style=\"color:#fff\" ></i></div>\r\n        </div>\r\n          </h3>\r\n          \r\n          <br>\r\n        <br>\r\n          <div id=\"formDoc\" name=\"formDoc\" style=\"display:none;\">  \r\n                <div>\r\n                  <ion-grid>\r\n                        <div>\r\n                          <ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n                            <ion-row>\r\n                              <ion-col>\r\n                                <div><ion-label>Material recebido</ion-label></div>\r\n                              </ion-col>\r\n                            </ion-row>\r\n                            <ion-row style=\"margin-top: -10px;\">\r\n                              <ion-col>\r\n                                <div>\r\n                                  <ion-radio value=\"a fresco\"></ion-radio>&nbsp;\r\n                                  <ion-label>a fresco</ion-label>\r\n                                </div>\r\n                              </ion-col>\r\n                            </ion-row>\r\n                            <ion-row style=\"margin-top: -10px;\">\r\n                              <ion-col>\r\n                                <div>\r\n                                  <ion-radio  value=\"em formol tamponado a 10%\"></ion-radio>&nbsp;\r\n                                  <ion-label >em formol tamponado a 10%</ion-label>\r\n                                </div>\r\n                              </ion-col>\r\n                            </ion-row>\r\n                            <ion-row style=\"margin-top: -10px;\">\r\n                              <ion-col>\r\n                                <div>\r\n                                  <ion-radio value=\"texto\"></ion-radio>&nbsp;\r\n                                  <ion-label>outro (citar)</ion-label> &nbsp;\r\n                                  <input style = \"width: 50%;\" type=\"text\" (keyup)=\"inputTexto1($event)\">\r\n                                </div>\r\n                              </ion-col>\r\n                            </ion-row>\r\n                          </ion-radio-group>\r\n                        </div>\r\n                  </ion-grid>\r\n                  <ion-grid >\r\n                    <ion-row>\r\n                      <ion-col>\r\n                        <div><ion-label>que consiste de útero</ion-label></div>\r\n                      </ion-col>\r\n                    </ion-row>\r\n                    <ion-radio-group allow-empty-selection (ionChange)=\"opt2($event)\">\r\n                      <ion-row style=\"margin-top: -10px;\">\r\n                        <ion-col>\r\n                          <div>\r\n                            <ion-radio value=\"aberto\"></ion-radio>&nbsp;\r\n                            <ion-label >aberto</ion-label>\r\n                          </div>\r\n                        </ion-col>\r\n                        <ion-col>\r\n                          <div>\r\n                            <ion-radio value=\"fechado\"></ion-radio>&nbsp;\r\n                            <ion-label >fechado</ion-label>\r\n                          </div>\r\n                        </ion-col>\r\n                      </ion-row>\r\n                    </ion-radio-group>\r\n                  </ion-grid>\r\n        </div>\t \r\n        \r\n        <div>\t\r\n          <ion-grid>\r\n             <ion-row>\r\n              <ion-col>\r\n                <div><ion-label>colo</ion-label></div>\r\n              </ion-col>\r\n              </ion-row>\r\n              <ion-radio-group allow-empty-selection (ionChange)=\"opt3($event)\">\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-radio value=\"presente\"></ion-radio>&nbsp;\r\n                    <ion-label >presente</ion-label>\r\n                    </div>\r\n                  </ion-col>\r\n                \r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-radio value=\"parcial\"></ion-radio>&nbsp;\r\n                    <ion-label >parcial </ion-label>\r\n                    </div>\r\n                  </ion-col>\r\n                  </ion-row>\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                \r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-radio value=\"ausente\"></ion-radio>&nbsp;\r\n                    <ion-label >ausente</ion-label>\r\n                    </div>\r\n                  </ion-col>\r\n                  </ion-row>\r\n              </ion-radio-group>\r\n            </ion-grid>\r\n          </div>\r\n          \r\n          <div>\r\n            <ion-grid>\r\n              <ion-row>\r\n                <ion-col>\r\n                  <div><ion-label>anexos</ion-label></div>\r\n                </ion-col>\r\n              </ion-row>\r\n              <ion-radio-group allow-empty-selection (ionChange)=\"opt4($event)\">\r\n              <ion-row style=\"margin-top: -10px;\">\r\n              <ion-col>\r\n                <ion-radio value=\"com anexos\"></ion-radio>&nbsp;\r\n                <ion-label for=\"inlineRadio9\">presentes</ion-label>\r\n              </ion-col>\r\n              \r\n              \r\n              <ion-col>\r\n                <ion-radio value=\"sem anexos\"></ion-radio>&nbsp;\r\n                <ion-label for=\"inlineRadio10\">ausentes</ion-label>\r\n              </ion-col>\r\n              </ion-row>\r\n              </ion-radio-group>\r\n          </ion-grid>\r\n        </div>\r\n                      \r\n        <div>\r\n          <ion-grid>\r\n            <ion-row>\r\n            <ion-col>\r\n              <ion-label >que pesa</ion-label>&nbsp;\r\n              <input  style = \"width: 15%;\" type=\"text\" >&nbsp;\r\n              <ion-label >g</ion-label>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n              <ion-col>\r\n                <ion-label>e mede</ion-label>&nbsp;\r\n                <input style = \"width: 15%;\" type=\"text\">\r\n                <ion-label>x</ion-label>\r\n                <input style = \"width: 15%;\" type=\"text\" >\r\n                <ion-label>x</ion-label>\r\n                <input style = \"width: 15%;\" type=\"text\">&nbsp;\r\n                <ion-label>cm.</ion-label>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-grid>\r\n          </div>\r\n          \r\n          <div>\r\n          <ion-grid>\r\n            <ion-row>\r\n            <ion-col>\r\n              <div><ion-label>O corpo uterino é recoberto por serosa lisa e brilhante e apresenta aspecto</ion-label></div>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-radio-group allow-empty-selection (ionChange)=\"opt5($event)\">\r\n                <ion-row style=\"margin-top: -10px;\">\r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-radio value=\"irregular\"></ion-radio>&nbsp;\r\n                      <ion-label for=\"inlineRadio11\">irregular</ion-label>\r\n                    </div>\r\n                  </ion-col>\r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-radio value=\"piriforme\"></ion-radio>&nbsp;\r\n                      <ion-label for=\"inlineRadio12\">piriforme</ion-label>\r\n                    </div>\r\n                  </ion-col>\r\n                </ion-row>\r\n                <ion-row style=\"margin-top: -10px;\">\r\n                \r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-radio value=\"globoso\"></ion-radio>&nbsp;\r\n                      <ion-label for=\"inlineRadio13\">globoso</ion-label>\r\n                    </div>\r\n                  </ion-col>\r\n                </ion-row>\r\n          </ion-radio-group>\r\n          </ion-grid>\r\n          </div>\r\n          \r\n          <div>\r\n          <ion-grid>\r\n            <ion-row>\r\n            <ion-col>\r\n              <div><ion-label>Aos cortes</ion-label></div>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-row >\r\n                    <ion-col>\r\n                      <div>\r\n                        <ion-radio value=\"1\" (click)=\"ap();\"></ion-radio>&nbsp;\r\n                        <ion-label  for=\"inlineRadio14\">é firme, a parede mede até</ion-label>&nbsp;\r\n                        <input  style = \"width: 10%;\" type=\"text\" >\r\n                        cm de espessura,sendo o miométrio fasciculado e esbranquiçado e o endométrio róseo e macio com&nbsp;\r\n                        <input style = \"width: 10%;\" type=\"text\" >&nbsp;\r\n                        cm de espessura.\r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n              \r\n                  <ion-row>\r\n                    <ion-col>\r\n                      <div>\r\n                        <ion-radio value=\"3\"></ion-radio>&nbsp;\r\n                        <ion-label >há</ion-label>&nbsp;\r\n                        <input  style = \"width: 10%;\" type=\"text\" >&nbsp;\r\n                        formações nodulares, bem delimitadas, fasciculadas, firmes e brilhantes, medindo a maior&nbsp; \r\n                        <input style = \"width: 12%;\" type=\"text\" >&nbsp;\r\n                        cm de diâmetro, de localização(ões)&nbsp;\r\n                        <input style = \"width: 50%;\" type=\"text\">.\r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                  <ion-row>\r\n                    <ion-col class=\"ion-text-wrap\">\r\n                      <div class=\"ion-text-wrap\">\r\n                        <ion-radio value=\"\"></ion-radio>&nbsp;\r\n                        <ion-label for=\"inlineRadio16\">há formação nodular, bem delimitada, </ion-label>&nbsp;\r\n                        fasciculada, firme, medindo &nbsp;\r\n                        <input  style = \"width: 9%;\" type=\"text\">&nbsp;\r\n                        cm de diâmetro, de localização &nbsp;\r\n                        <input style = \"width: 30%;\" type=\"text\">.\r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n            </ion-radio-group>\r\n            <div id = \"ap\" style=\"display: block;\">\r\n              <ion-row class=\"ion-text-wrap\">\r\n                <ion-col>\r\n                  <div>\r\n                    <ion-radio value=\"2\"></ion-radio>&nbsp;\r\n                    <ion-label for=\"inlineRadio17\">Nota-se endométrio róseo e macio, medindo</ion-label>&nbsp;\r\n                    <input style = \"width: 9%;\" type=\"text\">&nbsp;     \r\n                    cm de espessura.\r\n                  </div>\r\n                </ion-col>\r\n              </ion-row>\r\n            </div>\r\n          <!--\r\n            <ion-row>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"\"></ion-radio>&nbsp;\r\n                  <ion-label for=\"inlineRadio16\" text-wrap>há formação nodular, bem delimitada. </ion-label>\r\n                  </ion-radio-group>\r\n                </div>\r\n              </ion-col>\r\n            </ion-row>\r\n          --> \r\n            <ion-row >\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n                      <ion-radio value=\"t\"></ion-radio>&nbsp;\r\n                      <ion-label for=\"inlineRadio18\">Observa-se, ainda, formação polipoide na </ion-label>&nbsp;\r\n                      cavidade endometrial, medindo&nbsp;\r\n                      <input style = \"width: 9%;\" type=\"text\">&nbsp;\r\n                        cm.\r\n                    </ion-radio-group>\r\n                </div>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-row >\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n                    <ion-radio value=\"2\"></ion-radio>&nbsp;\r\n                    <ion-label for=\"inlineRadio19\">O colo uterino exibe ectocérvice róseo-</ion-label>\r\n                    esbranquiçada e lisa, e o OE tem forma de &nbsp;\r\n                  </ion-radio-group>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt8($event)\">\r\n                      <ion-radio value=\"fenda\"></ion-radio>&nbsp;\r\n                      <ion-label for=\"inlineRadio20\">fenda</ion-label> &nbsp;\r\n                      <ion-radio value=\"circular\"></ion-radio>&nbsp;\r\n                      <ion-label for=\"inlineRadio21\">circular</ion-label>\r\n                </ion-radio-group>\r\n                </div>\r\n              </ion-col>\r\n            </ion-row>\r\n            \r\n            <ion-row>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n                  <ion-radio value=\"\"></ion-radio>&nbsp;\r\n                  <ion-label for=\"inlineRadio22\">Aos cortes o aspecto é o usual com pequenos cistos contendo material gelatinoide.</ion-label>\r\n                    </ion-radio-group>\r\n                </div>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-radio-group allow-empty-selection (ionChange)=\"opt9($event)\">\r\n                <ion-row>\r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-radio value=\"1\"></ion-radio>&nbsp;\r\n                      <ion-label for=\"inlineRadio23\">Os ovários possuem aspecto e localização usual,</ion-label>&nbsp;\r\n                      medindo o direito &nbsp;\r\n                      <input style = \"width: 9%;\" type=\"text\" >\r\n                      x\r\n                      <input style = \"width: 9%;\" type=\"text\" >\r\n                      x\r\n                      <input style = \"width: 9%;\" type=\"text\" >&nbsp;\r\n                      cm e pesando&nbsp;\r\n                      <input style = \"width: 9%;\" type=\"text\" >&nbsp;\r\n                      g e o esquerdo &nbsp;<br>\r\n                      <input style = \"width: 9%;\" type=\"text\" >\r\n                      x\r\n                      <input  style = \"width: 9%;\" type=\"text\">\r\n                      x\r\n                      <input  style = \"width: 9%;\" type=\"text\" >&nbsp;\r\n                      cm, com &nbsp;\r\n                      <input  style = \"width: 9%;\" type=\"text\">&nbsp;\r\n                      g. Aos cortes são elásticos, branco-acinzentados, brilhantes,levemente granulosos e com pequenos cistos de conteúdo claro.\r\n                    </div>\r\n                  </ion-col>\r\n                </ion-row>\r\n           \r\n                  <ion-row>\r\n                    <ion-col>\r\n                      <div>\r\n                        <ion-radio value=\"2\" (click)=\"direito();\"></ion-radio>&nbsp;\r\n                        <ion-label >Ovário</ion-label>&nbsp;\r\n                        </div>\r\n                    </ion-col>\r\n                    </ion-row>\r\n                  </ion-radio-group>\r\n                    <ion-row>\r\n                      <ion-col>\r\n                        <div>\r\n                        <ion-radio-group allow-empty-selection (ionChange)=\"opt11($event)\">\r\n                            <ion-radio value=\"direito\" disabled='{{certo}}'></ion-radio>&nbsp;\r\n                            <ion-label >direito</ion-label>&nbsp;\r\n                            <ion-radio value=\"esquerdo\" disabled='{{certo}}'></ion-radio>&nbsp;\r\n                            <ion-label >esquerdo</ion-label>&nbsp;\r\n                      </ion-radio-group>\r\n                        medindo&nbsp;\r\n                        <input style = \"width: 9%;\" type=\"text\">\r\n                        x\r\n                        <input style = \"width: 9%;\" type=\"text\">\r\n                        x\r\n                        <input  style = \"width: 9%;\" type=\"text\">&nbsp;\r\n                        cm, pesando &nbsp;\r\n                        <input  style = \"width: 9%;\" type=\"text\">&nbsp;\r\n                        g, com superfície pardo-clara, lobulada, e aos cortes é elástico com pequenos cistos e áreas pardo-amareladas.\r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n             \r\n              <ion-radio-group allow-empty-selection (ionChange)=\"opt10($event)\">\r\n                <ion-row>\r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-radio value=\"1\"></ion-radio>&nbsp;\r\n                      <ion-label for=\"inlineRadio27\">Tubas uterinas de aspecto e localização usual,</ion-label>&nbsp;\r\n                      medindo a direita&nbsp; \r\n                      <input style = \"width: 9%;\" type=\"text\">&nbsp;\r\n                      cm de comprimento e &nbsp;\r\n                      <input style = \"width: 9%;\" type=\"text\">&nbsp;\r\n                      cm de diâmetro, e a esquerda &nbsp;\r\n                      <input  style = \"width: 9%;\" type=\"text\">&nbsp;\r\n                      cm de comprimento e &nbsp;\r\n                      <input  style = \"width: 9%;\" type=\"text\">&nbsp;\r\n                      cm de diâmetro, aos cortes são elásticas e mostram luzes virtuais.\r\n                    </div>\r\n                  </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-radio value=\"2\" (click)= \"esquerdo()\"></ion-radio>&nbsp;\r\n                      <ion-label for=\"inlineRadio28\">Tuba uterina </ion-label>&nbsp;\r\n                      </div>\r\n                      </ion-col>\r\n                      </ion-row>\r\n                    </ion-radio-group>\r\n                      <ion-row>\r\n                        <ion-col>\r\n                          <div>\r\n                      <ion-radio-group allow-empty-selection (ionChange)=\"opt12($event)\">\r\n                        <ion-radio value=\"direita\" disabled='{{certo2}}'></ion-radio>&nbsp;\r\n                        <ion-label for=\"inlineRadio29\"> direita </ion-label>&nbsp;\r\n                        <ion-radio value=\"esquerda\" disabled='{{certo2}}'></ion-radio>&nbsp;\r\n                        <ion-label for=\"inlineRadio30\"> esquerda</ion-label>&nbsp;\r\n                      </ion-radio-group>\r\n                      medindo &nbsp;\r\n                      <input  style = \"width: 9%;\" type=\"text\">&nbsp;\r\n                      cm de comprimento e &nbsp;\r\n                      <input  style = \"width: 9%;\" type=\"text\">&nbsp;\r\n                        de diâmetro máximo, com serosa lisa e brilhante e aos cortes mostra luz virtual. <br> Fragmentos representativos são submetidos a exame histológico. Legenda:&nbsp;\r\n                      <input style = \"width: 30%;\" type=\"text\">.\r\n                    </div>\r\n                  </ion-col>\r\n                </ion-row>\r\n             \r\n          </ion-grid>\r\n          </div>\r\n         \r\n            <!--(click)='gerarPDF()'-->\r\n          <a class=\"btn btn-app\"style=\" width: 50%; padding: 5px 5px 5px 5px; border-radius: 3px;\" >\r\n            <i class=\"fa fa-lock fa-1x\" style=\"color:#878787\"></i>&nbsp;\r\n            <span class=\"fontItem\">Baixar Arquivo</span>\r\n          </a>\r\n          \r\n  <br>\r\n  <br>\r\n  </div>\r\n      <br>\r\n      <h3 margin-bottom><b>Cortes macroscópicos representativos</b></h3> <a href = '#' (click)=\"viewImage(imgSource6, imgTitle6, imgDescription6)\">  (Figura 8.AG)</a> <a href = '#' (click)=\"viewImage(imgSource5, imgTitle5, imgDescription5)\">  (Foto 8.AG)</a>\r\n    <p>\r\n  <br><b>Cassete A: </b>Colo uterino (um corte englobando ectocérvice, endocérvice/JEC). \r\n  <br><b>Cassete B: </b>Istmo (um corte transversal divido ao meio).\r\n  <br><b>Cassete C: </b>Miométrio e endométrio (um corte de cada, caso possível, contemple também a serosa).\r\n  <br><b>Cassete D: </b>Mioma (de um a três cortes por mioma, conforme característica e tamanho).\r\n  <br><b>Cassete E: </b>Pólipo cervical ou endometrial (representar por inteiro, se necessário, dividir em cassetes quando muito extensos).\r\n  <br><b>Cassete F: </b>Outras áreas (corte(s) de área(s) anormal(is)).\r\n  <br><b>Cassete G á___: </b>Anexos (ir para as seções de ooforectomia e/ou salpingectomia).\r\n\r\n\r\n    </p>\r\n  <br>\r\n    <p text-center>\r\n  <a class=\"btn btn-app\" style=\" width: 45%\"  (click)=\"desenhos()\">\r\n    <i class=\"fa fa-image fa-2x\" style=\"color:#e74a92\"></i><br>\r\n    <span>Figuras Ilustrativas</span>\r\n  </a>\r\n  <a class=\"btn btn-app\" style=\" width: 45%\" (click)=\"fotos()\">\r\n    <i class=\"fas fa-camera fa-2x\" style=\"color:#e74a92\"></i><br>\r\n    <span>Fotos Ilustrativas</span>\r\n  </a>\r\n  \r\n</p>       \r\n    </ion-card-content>\r\n    \r\n  </ion-card>\r\n  \r\n  \r\n\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/histerectomia_benigna/histerectomia.benigna.page.module.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/pages/histerectomia_benigna/histerectomia.benigna.page.module.ts ***!
  \**********************************************************************************/
/*! exports provided: HisterectomiaBenignaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HisterectomiaBenignaPageModule", function() { return HisterectomiaBenignaPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _histerectomia_benigna_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./histerectomia.benigna.page */ "./src/app/pages/histerectomia_benigna/histerectomia.benigna.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _histerectomia_benigna_page__WEBPACK_IMPORTED_MODULE_5__["HisterectomiaBenignaPage"]
    }
];
var HisterectomiaBenignaPageModule = /** @class */ (function () {
    function HisterectomiaBenignaPageModule() {
    }
    HisterectomiaBenignaPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_histerectomia_benigna_page__WEBPACK_IMPORTED_MODULE_5__["HisterectomiaBenignaPage"]]
        })
    ], HisterectomiaBenignaPageModule);
    return HisterectomiaBenignaPageModule;
}());



/***/ }),

/***/ "./src/app/pages/histerectomia_benigna/histerectomia.benigna.page.ts":
/*!***************************************************************************!*\
  !*** ./src/app/pages/histerectomia_benigna/histerectomia.benigna.page.ts ***!
  \***************************************************************************/
/*! exports provided: HisterectomiaBenignaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HisterectomiaBenignaPage", function() { return HisterectomiaBenignaPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/image-viewer/image-viewer.component */ "./src/app/components/image-viewer/image-viewer.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! pdfmake/build/pdfmake */ "./node_modules/pdfmake/build/pdfmake.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! pdfmake/build/vfs_fonts */ "./node_modules/pdfmake/build/vfs_fonts.js");
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var HisterectomiaBenignaPage = /** @class */ (function () {
    function HisterectomiaBenignaPage(modalController, router, navCtrl, formBuilder) {
        this.modalController = modalController;
        this.router = router;
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.imgSource = 'assets/img/histerectomia_benigna/fotos/DSC00564.png';
        this.imgTitle = '';
        this.imgDescription = 'Foto 8: Útero a fresco. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource2 = 'assets/img/histerectomia_benigna/fotos/DSC00575.png';
        this.imgTitle2 = '';
        this.imgDescription2 = 'Foto 8.8a: Corte inicial ao longo de suas paredes laterais, útero/benigno. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource3 = 'assets/img/histerectomia_benigna/fotos/DSC00577.png';
        this.imgTitle3 = '';
        this.imgDescription3 = 'Foto 8.8c: Cortes transversais incompletos, útero/benigno. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource4 = 'assets/img/histerectomia_benigna/fotos/DSC00596.png';
        this.imgTitle4 = '';
        this.imgDescription4 = 'Foto 8.10.11: Cortes finais completos, útero/benigno. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource5 = 'assets/img/histerectomia_benigna/fotos/DSC00598.png';
        this.imgTitle5 = '';
        this.imgDescription5 = 'Foto 8.AG: Cortes representativos para útero/benigno. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource6 = 'assets/img/histerectomia_benigna/img/1591562108545.png';
        this.imgTitle6 = '';
        this.imgDescription6 = 'Figura 8.AG: Cortes representativos para úteros com lesões benignas e anexos (adaptada do manual de macroscopia do Hospital São João, Universidade do Porto, versão II, 2013).';
        this.imgSource7 = 'assets/img/histerectomia_benigna/img/1591642433190.png';
        this.imgTitle7 = '';
        this.imgDescription7 = 'Figura 8.2: Orientação anatômica e corte inicial para útero (adaptada do manual de macroscopia do Hospital São João, Universidade do Porto, versão II, 2013).';
        this.certo = 'true';
        this.certo2 = 'true';
    }
    HisterectomiaBenignaPage.prototype.viewImage = function (src, title, description) {
        if (title === void 0) { title = ''; }
        if (description === void 0) { description = ''; }
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_2__["ImageViewerComponent"],
                            componentProps: {
                                imgSource: src,
                                imgTitle: title,
                                imgDescription: description
                            },
                            cssClass: 'modal-fullscreen',
                            keyboardClose: true,
                            showBackdrop: true
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    HisterectomiaBenignaPage.prototype.ngOnInit = function () {
    };
    HisterectomiaBenignaPage.prototype.fotos = function () {
        this.router.navigateByUrl('/histerectomia_benigna/fotos');
    };
    HisterectomiaBenignaPage.prototype.desenhos = function () {
        this.router.navigateByUrl('/histerectomia_benigna/imagens');
    };
    HisterectomiaBenignaPage.prototype.gerarPDF = function () {
        pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_4___default.a.vfs = pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_5___default.a.pdfMake.vfs;
        var docDefinition = {
            content: [
                {
                    columns: [
                        [
                            { text: 'Histerectomia por lesões benignas/ Instruções gerais', style: 'header' },
                            { text: '', style: 'sub_header' },
                            { text: '', style: 'text' },
                        ]
                    ]
                }
            ],
            styles: {
                header: {
                    bold: true,
                    fontSize: 20,
                    alignment: 'center'
                },
                sub_header: {
                    fontSize: 18,
                    alignment: 'center'
                },
                text: {
                    fontSize: 16,
                    margin: [0, 30, 0, 0],
                    alignment: 'left'
                }
            },
            pageSize: 'A4',
            pageOrientation: 'portrait'
        };
        pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_4___default.a.createPdf(docDefinition).open();
    };
    HisterectomiaBenignaPage.prototype.toggleDetails = function () {
        if (document.getElementById("formDoc").style.display == 'none') {
            document.getElementById("formDoc").style.display = "block";
        }
        else {
            document.getElementById("formDoc").style.display = "none";
        }
    };
    HisterectomiaBenignaPage.prototype.ap = function () {
        if (document.getElementById("ap").style.display == 'none') {
            document.getElementById("ap").style.display = "block";
        }
        else {
            document.getElementById("ap").style.display = "none";
        }
    };
    HisterectomiaBenignaPage.prototype.direito = function () {
        console.log('aqui');
        if (this.certo == 'true') {
            this.certo = 'false';
        }
        else {
            this.certo = 'true';
        }
    };
    HisterectomiaBenignaPage.prototype.esquerdo = function () {
        console.log('aqui');
        if (this.certo2 == 'true') {
            this.certo2 = 'false';
        }
        else {
            this.certo2 = 'true';
        }
    };
    HisterectomiaBenignaPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-histerectomiaBenigna',
            template: __webpack_require__(/*! ./histerectomia.benigna.page.html */ "./src/app/pages/histerectomia_benigna/histerectomia.benigna.page.html"),
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]])
    ], HisterectomiaBenignaPage);
    return HisterectomiaBenignaPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-histerectomia_benigna-histerectomia-benigna-page-module.js.map