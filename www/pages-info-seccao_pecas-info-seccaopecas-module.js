(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-info-seccao_pecas-info-seccaopecas-module"],{

/***/ "./src/app/pages/info/seccao_pecas/info.seccaopecas.html":
/*!***************************************************************!*\
  !*** ./src/app/pages/info/seccao_pecas/info.seccaopecas.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n    <ion-toolbar color=\"secondary\">\r\n      <ion-buttons slot=\"start\">\r\n        <ion-menu-button color=\"light\"></ion-menu-button>\r\n      </ion-buttons>\r\n      <ion-title>\r\n          <b>Informações necessárias para a secção das peças</b>\r\n      </ion-title>\r\n    </ion-toolbar>\r\n\r\n  </ion-header>\r\n  \r\n  <ion-content>\r\n   <ion-card class=\"bg-white\" no-margin>\r\n      <ion-card-content>\r\n        <h2 margin-bottom>\r\n          <ion-text color=\"dark\"><strong></strong></ion-text>\r\n        </h2>\r\n        <br> 1. Manusear a peça/material com cuidado, em todas as etapas (retirada do recipiente, lavagem, secções, entre outros). \r\n        <br> 2. Não friccionar as superfícies mucosas.\t\r\n        <br> 3. Abrir e/ou seccionar as peças maiores, imediatamente após a triagem, para que ocorra apropriada fixação. Seguir a normas indicadas nos procedimentos de cada peça/órgão.\r\n        <br><b>Nota: </b> Não hesite em pedir ajuda em casos de dúvidas.\r\n        <br> 4. Em casos com lesões macroscópicas evidentes, um dos fragmentos representados deve apresentar tecido adjacente integro. \r\n        <br> 5. As margens deverão ser identificadas e representadas sempre que designado no protocolo da peça. Pintá-las com tinta nanquim e em cores diferentes, que facilitarão as identificações da peça. Antes de iniciar o procedimento de pintar a peça, seque-a com papel toalha ou gaze, aplique a tinta e depois borrife o fixador de nanquim (solução formol-ácido acético a 5%). Os fragmentos representativos das margens, sempre que possível, devem ser seccionados perpendicularmente à superfície de corte de cada peça.\r\n        <br> 6. Os materiais protéticos e cirúrgicos (fios de sutura, grampo, entre outros) devem ser retirados cuidadosamente e guardados no recipiente da peça. Quando a qualidade do diagnóstico puder ser alterada devido a remoção do referido material, o fato deverá ser abordado com o médico requerente.\r\n        <br> 7. O tamanho de cada fragmento deverá ser no máximo 2,0 x 2,0 x 0,3 cm. Fragmentos maiores e mais espessos, não são fixados adequadamente.\r\n        <br> 8. Os tecidos ósseos são cortados através de serra manual e/ou mecânica e após os cortes e fixação do material, os mesmos deverão ser colocados em recipientes com líquido descalcificador, identificados com o número do exame, com a palavra “descalcificador” e com a data de início do procedimento.\r\n        <br> 9. Os fragmentos utilizados para exame peroperatório devem ser descritos e submetidos a parafina para exame final.\r\n        <br>\r\n        <br><b>Nota: </b>Recomenda-se a não utilização excessiva de material nos cassetes, principalmente, em casos de linfonodos. \r\n        <br>\r\n    \r\n    \r\n          \r\n      </ion-card-content>\r\n      \r\n    </ion-card>\r\n    \r\n    \r\n  \r\n  </ion-content>\r\n  "

/***/ }),

/***/ "./src/app/pages/info/seccao_pecas/info.seccaopecas.module.ts":
/*!********************************************************************!*\
  !*** ./src/app/pages/info/seccao_pecas/info.seccaopecas.module.ts ***!
  \********************************************************************/
/*! exports provided: InfoSeccaoPecasModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfoSeccaoPecasModule", function() { return InfoSeccaoPecasModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _info_seccaopecas__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./info.seccaopecas */ "./src/app/pages/info/seccao_pecas/info.seccaopecas.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _info_seccaopecas__WEBPACK_IMPORTED_MODULE_5__["InfoSeccaoPecas"]
    }
];
var InfoSeccaoPecasModule = /** @class */ (function () {
    function InfoSeccaoPecasModule() {
    }
    InfoSeccaoPecasModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_info_seccaopecas__WEBPACK_IMPORTED_MODULE_5__["InfoSeccaoPecas"]]
        })
    ], InfoSeccaoPecasModule);
    return InfoSeccaoPecasModule;
}());



/***/ }),

/***/ "./src/app/pages/info/seccao_pecas/info.seccaopecas.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/info/seccao_pecas/info.seccaopecas.ts ***!
  \*************************************************************/
/*! exports provided: InfoSeccaoPecas */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfoSeccaoPecas", function() { return InfoSeccaoPecas; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};


var InfoSeccaoPecas = /** @class */ (function () {
    function InfoSeccaoPecas(modalController) {
        this.modalController = modalController;
    }
    InfoSeccaoPecas.prototype.viewImage = function (src, title, description) {
        if (title === void 0) { title = ''; }
        if (description === void 0) { description = ''; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    InfoSeccaoPecas = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-infoseccaopecas',
            template: __webpack_require__(/*! ./info.seccaopecas.html */ "./src/app/pages/info/seccao_pecas/info.seccaopecas.html")
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"]])
    ], InfoSeccaoPecas);
    return InfoSeccaoPecas;
}());



/***/ })

}]);
//# sourceMappingURL=pages-info-seccao_pecas-info-seccaopecas-module.js.map