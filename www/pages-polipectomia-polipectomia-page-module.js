(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-polipectomia-polipectomia-page-module"],{

/***/ "./src/app/pages/polipectomia/polipectomia.page.html":
/*!***********************************************************!*\
  !*** ./src/app/pages/polipectomia/polipectomia.page.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n    <ion-toolbar color=\"secondary\">\r\n      <ion-buttons slot=\"start\">\r\n        <ion-menu-button color=\"light\"></ion-menu-button>\r\n      </ion-buttons>\r\n      <ion-title>\r\n          Útero\r\n      </ion-title>\r\n    </ion-toolbar>\r\n <!--   <ion-toolbar color=\"light\"> \r\n      <ion-searchbar [(ngModel)]=\"searchKey\"></ion-searchbar>\r\n    </ion-toolbar>-->\r\n</ion-header>\r\n  \r\n<ion-content>\r\n   <ion-card class=\"bg-white\" no-margin>\r\n      <ion-card-content>\r\n        <h2 margin-bottom>\r\n          <ion-text color=\"dark\"><strong> Polipectomia</strong></ion-text>\r\n        </h2>\r\n        <br>\r\n        <h3 margin-bottom text-dark><b>Procedimentos macroscópicos</b></h3>\r\n        <br>\r\n        <p margin-bottom text-dark>\r\n            1.\tLer toda a informação clínica descrita na requisição e verificar conformidade com a peça recebida.\r\n            <br>2.\tIdentificar e orientar o(s) pólipo(s), e/ou outros tipos de peças/fragmentos. No caso de ressecção histeroscópica, corar a margem de secção cirúrgica (quando possível).\r\n            <br>3.\tFixar os fragmentos (quando recebidos a fresco).\r\n            <br>4.\tMedir em três dimensões.\r\n            <br>5.\tDescrever consistência e tonalidade.\r\n            <br>\r\n            <br><b> Notas:</b>Os cortes, a serem realizados, variam conforme sua maior dimensão.\r\n            <br>\r\n            <br> a.\tSe pólipo/eixo ≤ 1 cm: corte no maior eixo pelo pedículo e/ou base. \r\n            <br>\r\n            <br>b.\tSe pólipo/eixo > 1cm e ≤ 2 cm: cortes sequenciais no maior eixo. Caso necessário, seguir orientação dos cortes de pólipos > a 2 cm.\r\n            <br>\r\n            <br>c.\tSe pólipos/eixo > a 2 cm: corte e separe o pedículo/margem cirúrgica, em seguida, realize uma secção vertical, central no maior eixo do pedículo, adiante, no restante do pólipo, realize cortes longitudinais sequencias <a href = '#' (click)=\"viewImage(imgSource4, imgTitle4, imgDescription4)\"> (Figura 7.c)</a>.\r\n        </p>\r\n        <br>\r\n        <h3 margin-bottom text-dark (click)=\"toggleDetails();\" style=\"width:100%; cursor:pointer\">\r\n            <div style=\"width: 100%; float:left; background: #878787; color: #fff; padding: 9px;\"><b>Ex. de descrição macroscópica</b>\r\n              <div style=\"width: 4%; float:right;\"><i class=\"fa fa-angle-double-down\" style=\"color:#fff\" ></i></div>\r\n            </div>\r\n        </h3>\r\n          \r\n        <br>\r\n        <br>\r\n\r\n        <div id=\"formDoc\" name=\"formDoc\" style=\"display:none;\">  \r\n            \r\n          <div>\r\n\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div><label>Material recebido</label></div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio value=\"a fresco\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\" > a fresco</ion-label>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio value=\"em formol tamponado a 10%\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">em formol tamponado a 10%</ion-label>\r\n\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio value = \"texto\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">outro (citar)</ion-label> &nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<input style = \"width: 50%;\" type=\"text\" (keyup)=\"inputTexto1($event)\">\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t</ion-radio-group>\r\n          </div>\r\n          <div>\r\n                <ion-row style=\"margin-top: -10px;\">\r\n                  <ion-col>\r\n                    <div>\r\n                      <label>que consiste de</label>\r\n                    </div>\r\n                  </ion-col>\r\n                </ion-row>\r\n                <ion-row style=\"margin-top: -10px;\">\r\n                  <ion-col>\r\n                    <div>\r\n                      <input style = \"width: 13%;\" type=\"text\" (keyup)=\"inputTexto2($event)\">&nbsp;\r\n                      <ion-label>estrutura(s) polipoide(s)</ion-label>\r\n                    </div>\r\n                  </ion-col>\r\n                </ion-row>\r\n                <ion-radio-group allow-empty-selection (ionChange)=\"opt2($event)\">\r\n                <ion-row style=\"margin-top: -10px;\">\r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-radio value=\"sim\"></ion-radio>&nbsp;\r\n                      <ion-label>séssil(s)</ion-label>\r\n                    </div>\r\n                  </ion-col>\r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-radio value=\"sim2\"></ion-radio>&nbsp;\r\n                      <ion-label >pediculada(s)</ion-label>\r\n                    </div>\r\n                  </ion-col>\r\n                </ion-row>\r\n              </ion-radio-group>\r\n                <ion-row style=\"margin-top: -10px;\">\r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-label>de tonalidade</ion-label>&nbsp;\r\n                      <input style = \"width: 22%;\" type=\"text\" (keyup)=\"inputTexto3($event)\">\r\n                    </div>\r\n                  </ion-col>\r\n                </ion-row>\r\n                <ion-row style=\"margin-top: -10px;\">\r\n                  <ion-col>\r\n                    <div>\r\n                      \r\n                      <ion-label>que mede(m) em conjunto</ion-label>&nbsp;\r\n                      <input style = \"width: 12%;\" type=\"text\" (keyup)=\"inputTexto4($event)\">&nbsp;\r\n                      <ion-label>x</ion-label>&nbsp;\r\n                      <input style = \"width: 12%;\" type=\"text\" (keyup)=\"inputTexto5($event)\">&nbsp;\r\n                      <ion-label>x</ion-label>&nbsp;\r\n                      <input style = \"width: 12%;\" type=\"text\" (keyup)=\"inputTexto6($event)\">&nbsp;\r\n                      <ion-label> cm</ion-label>\r\n                    </div>\r\n                  </ion-col>\r\n                </ion-row>\r\n                \r\n                <ion-row style=\"margin-top: -10px;\">\r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-label>e aos cortes é (são)</ion-label>\r\n                    </div>\r\n                  </ion-col>\r\n                </ion-row>\r\n                <ion-radio-group allow-empty-selection (ionChange)=\"opt3($event)\">\r\n                <ion-row style=\"margin-top: -10px;\">\r\n                  <ion-col>\r\n                    <div>\r\n                        <ion-radio  value=\"sim\"></ion-radio>&nbsp;\r\n                        <ion-label > elástica(s)</ion-label>\r\n                    </div>\r\n                  </ion-col>\r\n                  <ion-col>\r\n                    <div>\r\n                        <ion-radio value=\"sim2\"></ion-radio>&nbsp;\r\n                        <ion-label> macia(s)</ion-label>\r\n                    </div>\r\n                  </ion-col>\r\n                </ion-row>\r\n              </ion-radio-group>\r\n                <ion-row style=\"margin-top: -10px;\">\r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-label>Todo material é submetido a exame</ion-label>\r\n                    </div>\r\n                  </ion-col>\r\n                \r\n                </ion-row>\r\n                <ion-row style=\"margin-top: -10px;\">\r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-label>histológico </ion-label>&nbsp;\r\n                      <input style = \"text-align: center; width: 60%;\" type=\"text\" placeholder=\"(_fs/_bls/legenda_).\" (keyup)=\"inputTexto7($event)\">\r\n                    </div>\r\n                  </ion-col>\r\n                \r\n                </ion-row>\r\n            \r\n            </div>\r\n         \r\n          <a class=\"btn btn-app\"style=\" width: 50%; padding: 5px 5px 5px 5px; border-radius: 3px;\" (click)='gerarPDF()' >\r\n            <i class=\"far fa-file\" style=\"color:#878787\"></i>&nbsp;\r\n            <span class=\"fontItem\">Baixar Arquivo</span>\r\n          </a>\r\n          <br>\r\n\r\n      </div>\r\n      <br><h3 margin-bottom><b>Cortes macroscópicos representativos</b></h3>\r\n      <p>\r\n\t      \t<br>&nbsp;Inclusão total do material (cassetes sequenciais) <a href = '#' (click)=\"viewImage(imgSource2, imgTitle2, imgDescription2)\"> (Foto 7.bc)</a>.\r\n\t\t  </p>\r\n      <br>\r\n\r\n      <p text-center>\r\n        <a class=\"btn btn-app\" style=\" width: 45%\"  (click)=\"desenhos()\">\r\n          <i class=\"fa fa-image fa-2x\" style=\"color:#e74a92\"></i><br>\r\n          <span>Figuras Ilustrativas</span>\r\n        </a>\r\n        <a class=\"btn btn-app\" style=\" width: 45%\" (click)=\"fotos()\">\r\n          <i class=\"fas fa-camera fa-2x\" style=\"color:#e74a92\"></i><br>\r\n          <span>Fotos Ilustrativas</span>\r\n        </a>\r\n      </p>\r\n    </ion-card-content>\r\n  </ion-card>\r\n</ion-content>\r\n  "

/***/ }),

/***/ "./src/app/pages/polipectomia/polipectomia.page.module.ts":
/*!****************************************************************!*\
  !*** ./src/app/pages/polipectomia/polipectomia.page.module.ts ***!
  \****************************************************************/
/*! exports provided: PolipectomiaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PolipectomiaPageModule", function() { return PolipectomiaPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _polipectomia_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./polipectomia.page */ "./src/app/pages/polipectomia/polipectomia.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _polipectomia_page__WEBPACK_IMPORTED_MODULE_5__["PolipectomiaPage"]
    }
];
var PolipectomiaPageModule = /** @class */ (function () {
    function PolipectomiaPageModule() {
    }
    PolipectomiaPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            providers: [],
            declarations: [_polipectomia_page__WEBPACK_IMPORTED_MODULE_5__["PolipectomiaPage"]]
        })
    ], PolipectomiaPageModule);
    return PolipectomiaPageModule;
}());



/***/ }),

/***/ "./src/app/pages/polipectomia/polipectomia.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/polipectomia/polipectomia.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ion-content {\n  --background: linear-gradient(-135deg, var(--ion-color-medium), var(--ion-color-light))\r\n    ; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcG9saXBlY3RvbWlhL0M6XFxVc2Vyc1xcamVzc2lcXERvY3VtZW50c1xcQVBNYWNyby9zcmNcXGFwcFxccGFnZXNcXHBvbGlwZWN0b21pYVxccG9saXBlY3RvbWlhLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVRO0lBQWEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3BvbGlwZWN0b21pYS9wb2xpcGVjdG9taWEucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xyXG4gICAgaW9uLWNvbnRlbnQge1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KC0xMzVkZWcsIHZhcigtLWlvbi1jb2xvci1tZWRpdW0pLCB2YXIoLS1pb24tY29sb3ItbGlnaHQpKVxyXG4gICAgfVxyXG59Il19 */"

/***/ }),

/***/ "./src/app/pages/polipectomia/polipectomia.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/polipectomia/polipectomia.page.ts ***!
  \*********************************************************/
/*! exports provided: PolipectomiaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PolipectomiaPage", function() { return PolipectomiaPage; });
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! pdfmake/build/pdfmake */ "./node_modules/pdfmake/build/pdfmake.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! pdfmake/build/vfs_fonts */ "./node_modules/pdfmake/build/vfs_fonts.js");
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/image-viewer/image-viewer.component */ "./src/app/components/image-viewer/image-viewer.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/file-opener/ngx */ "./node_modules/@ionic-native/file-opener/ngx/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.vfs = pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default.a.pdfMake.vfs;





var PolipectomiaPage = /** @class */ (function () {
    function PolipectomiaPage(modalController, router, navCtrl, formBuilder, file, navController, fileOpener) {
        this.modalController = modalController;
        this.router = router;
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.file = file;
        this.navController = navController;
        this.fileOpener = fileOpener;
        this.imgSource = 'assets/img/polipectomia/fotos/DSC00408.png';
        this.imgTitle = '';
        this.imgDescription = 'Foto 7.b: Cortes gerais para pólipo. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource2 = 'assets/img/polipectomia/fotos/DSC00416.png';
        this.imgTitle2 = '';
        this.imgDescription2 = 'Foto 7.bc: Cortes representativos para pólipo. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource4 = 'assets/img/polipectomia/img/FiguraNc.png';
        this.imgTitle4 = '';
        this.imgDescription4 = 'Figura7.c: Procedimentos macroscópicos para pólipos/eixo > a 2 cm (adaptada do Manual de padronização de laudos histopatológicos – SBP).';
        this.pdfObj = null;
    }
    PolipectomiaPage.prototype.viewImage = function (src, title, description) {
        if (title === void 0) { title = ''; }
        if (description === void 0) { description = ''; }
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_5__["ImageViewerComponent"],
                            componentProps: {
                                imgSource: src,
                                imgTitle: title,
                                imgDescription: description
                            },
                            cssClass: 'modal-fullscreen',
                            keyboardClose: true,
                            showBackdrop: true
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    PolipectomiaPage.prototype.ngOnInit = function () { };
    PolipectomiaPage.prototype.fotos = function () {
        this.router.navigateByUrl('/polipectomia/fotos');
    };
    PolipectomiaPage.prototype.desenhos = function () {
        this.router.navigateByUrl('/polipectomia/imagens');
    };
    PolipectomiaPage.prototype.gerarPDF = function () {
        var _this = this;
        console.log(this.texto2);
        if (this.option1 == undefined) {
            this.option1 = '';
        }
        if (this.option2 == undefined) {
            this.option2 = '';
        }
        if (this.option3 == undefined) {
            this.option3 = '';
        }
        if (this.texto1 == undefined) {
            this.texto1 = '';
        }
        if (this.texto2 == undefined) {
            this.texto2 = '';
        }
        if (this.texto3 == undefined) {
            this.texto3 = '';
        }
        if (this.texto4 == undefined) {
            this.texto4 = '';
        }
        if (this.texto5 == undefined) {
            this.texto5 = '';
        }
        if (this.texto6 == undefined) {
            this.texto6 = '';
        }
        if (this.texto7 == undefined) {
            this.texto7 = '';
        }
        if (this.option1 == 'texto') {
            this.option1 = this.texto1;
        }
        if (this.texto2 <= '1') {
            if (this.option2 == 'sim') {
                this.a = 'séssil,';
            }
            else {
                this.a = '';
            }
            if (this.option2 == 'sim2') {
                this.a = 'pediculada,';
            }
            else {
                this.a = '';
            }
            if (this.texto3 != '') {
                this.texto3 = this.texto3;
            }
            this.option = 'e aos cortes é';
            if (this.option3 == 'sim') {
                this.c = 'elástica.';
            }
            else {
                this.c = '';
            }
            if (this.option3 == 'sim2') {
                this.c = 'macia.';
            }
            else {
                this.c = '';
            }
            this.option8 = 'que mede ' + this.texto4 + ' x ' + this.texto5 + ' x ' + this.texto6 + ' cm,';
            this.completo = 'Material recebido ' + this.option1 + ' que consiste de ' + this.texto2 + ' estrutura polipoide ' + this.a + ' de tonalidade ' + this.texto3 + ', ' + this.option8 + ' ' + this.option + ' ' + this.c + 'Todo material é submetido a exame histológico ' + this.texto7 + '.';
        }
        else {
            if (this.option2 == 'sim') {
                this.b = 'sésseis,';
            }
            if (this.option2 == 'sim2') {
                this.b = 'pediculadas,';
            }
            if (this.texto3 != '') {
                this.texto3 = this.texto3;
            }
            this.option = 'e aos cortes são';
            if (this.option3 == 'sim') {
                this.d = 'eláticas.';
            }
            if (this.option3 == 'sim2') {
                this.d = 'macias.';
            }
            this.option8 = 'que medem em conjunto ' + this.texto4 + ' x ' + this.texto5 + ' x ' + this.texto6 + ' cm,';
            this.completop = 'Material recebido ' + this.option1 + ' que consiste de ' + this.texto2 + ' estruturas polipoides ' + this.b + ' de tonalidade ' + this.texto3 + ', ' + this.option8 + ' ' + this.option + ' ' + this.d + 'Todo material é submetido a exame histológico ' + this.texto7 + '.';
        }
        if (this.completo == undefined) {
            this.completo = '';
        }
        if (this.completop == undefined) {
            this.completop = '';
        }
        pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.vfs = pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default.a.pdfMake.vfs;
        var docDefinition = {
            content: [
                {
                    columns: [
                        [
                            { text: 'Polipectomia', style: 'header' },
                            { text: '', style: 'sub_header' },
                            { text: '' + this.completo + '' + this.completop + '', style: 'text' },
                        ]
                    ]
                }
            ],
            styles: {
                header: {
                    bold: true,
                    fontSize: 20,
                    alignment: 'center'
                },
                sub_header: {
                    fontSize: 18,
                    alignment: 'center'
                },
                text: {
                    fontSize: 16,
                    margin: [0, 30, 0, 0],
                    alignment: 'left'
                }
            },
            pageSize: 'A4',
            pageOrientation: 'portrait'
        };
        //pdfMake.createPdf(docDefinition).open();
        this.pdfObj = pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.createPdf(docDefinition);
        //console.log(this.pdfObj.getBuffer());
        this.pdfObj.getBuffer(function (buffer) {
            var blob = new Blob([buffer], { type: 'application/pdf' });
            // Save the PDF to the data Directory of our App
            _this.file.writeFile(_this.file.dataDirectory, 'Polipectomia.pdf', blob, { replace: true });
            // Open the PDf with the correct OS tools
            _this.fileOpener.open(_this.file.dataDirectory + 'Polipectomia.pdf', 'application/pdf');
        });
        this.completo = '';
        this.completop = '';
    };
    PolipectomiaPage.prototype.toggleDetails = function () {
        if (document.getElementById("formDoc").style.display == 'none') {
            document.getElementById("formDoc").style.display = "block";
        }
        else {
            document.getElementById("formDoc").style.display = "none";
        }
    };
    PolipectomiaPage.prototype.opt1 = function (event) {
        this.option1 = event.target.value;
    };
    PolipectomiaPage.prototype.opt2 = function (event) {
        this.option2 = event.target.value;
    };
    PolipectomiaPage.prototype.opt3 = function (event) {
        this.option3 = event.target.value;
    };
    PolipectomiaPage.prototype.inputTexto1 = function (event) {
        this.texto1 = event.target.value;
        console.log(this.texto1);
    };
    PolipectomiaPage.prototype.inputTexto2 = function (event) {
        this.texto2 = event.target.value;
    };
    PolipectomiaPage.prototype.inputTexto3 = function (event) {
        this.texto3 = event.target.value;
    };
    PolipectomiaPage.prototype.inputTexto4 = function (event) {
        this.texto4 = event.target.value;
    };
    PolipectomiaPage.prototype.inputTexto5 = function (event) {
        this.texto5 = event.target.value;
    };
    PolipectomiaPage.prototype.inputTexto6 = function (event) {
        this.texto6 = event.target.value;
    };
    PolipectomiaPage.prototype.inputTexto7 = function (event) {
        this.texto7 = event.target.value;
    };
    PolipectomiaPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-polipectomia',
            template: __webpack_require__(/*! ./polipectomia.page.html */ "./src/app/pages/polipectomia/polipectomia.page.html"),
            styles: [__webpack_require__(/*! ./polipectomia.page.scss */ "./src/app/pages/polipectomia/polipectomia.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"], _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_0__["File"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
            _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_8__["FileOpener"]])
    ], PolipectomiaPage);
    return PolipectomiaPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-polipectomia-polipectomia-page-module.js.map