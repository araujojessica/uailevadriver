(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-histerectomia_maligna_endometrial-histerectomia-maligna-endometrial-page-module"],{

/***/ "./src/app/pages/histerectomia_maligna_endometrial/histerectomia.maligna.endometrial.page.html":
/*!*****************************************************************************************************!*\
  !*** ./src/app/pages/histerectomia_maligna_endometrial/histerectomia.maligna.endometrial.page.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar color=\"secondary\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-menu-button color=\"light\"></ion-menu-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n        Útero\r\n    </ion-title>\r\n  </ion-toolbar>\r\n<!--  <ion-toolbar color=\"light\"> \r\n    <ion-searchbar [(ngModel)]=\"searchKey\"></ion-searchbar>\r\n    \r\n  </ion-toolbar>-->\r\n</ion-header>\r\n\r\n<ion-content>\r\n<p></p>\r\n <ion-card class=\"bg-white\" no-margin>\r\n    <ion-card-content>\r\n      <h2 margin-bottom>\r\n        <ion-text color=\"dark\"><strong>Histerectomia por hiperplasia ou neoplasia maligna endometrial</strong></ion-text>\r\n      </h2>\r\n   <br>\r\n      <h3 margin-bottom text-dark><b>Procedimentos macroscópicos</b></h3>\r\n    <br>\r\n    <p margin-bottom text-dark>\r\n      1.\tSe a histerectomia for radical, os linfonodos poderão estar inclusos na peça. Neste caso, deve-se dissecar a fresco e separar entre as porções direita e esquerda os gânglios: obturadores, ilíacos e para-aórticos –nem todos esses grupos poderão estar presentes–. (Na maioria dos casos os cirurgiões já enviarão as linfadenectomias em frascos separados e identificados).\r\n      <br>2.\tOrientar a peça e seguir todos as instruções indicadas em: Histerectomia por lesões benignas, do item 2 ao 13.\r\n      <br>3.\tEm caso de tumor evidente: descrever localização, medida (três dimensões), o aspecto –sólido, vegetante, ulcerado, necrótico, hemorrágico–, tonalidade, e se há infiltração/extensão do tumor para miométrio, serosa, paramétrios e anexos.\r\n      <br>4.\tOvários e tuba uterina: sigas as instruções dos respectivos protocolos.\r\n      <br>5.\tLinfonodos: número, aspecto macroscópico e dimensão do maior linfonodo. Analise se há possível envolvimento tumoral.\r\n      <br>\r\n      <br><b>Nota: </b> Coto vaginal, paramétrios, ovários e tubas uterinas. Consulte: Histerectomia por neoplasia maligna do colo do útero e/ou ooforectomia; salpingectomia. \r\n    </p>\r\n    <br>\r\n      <br>\r\n      <h3 margin-bottom text-dark (click)=\"toggleDetails();\" style=\"width:100%; cursor:pointer\">\r\n          <div style=\"width: 100%; float:left; background: #878787; color: #fff; padding: 9px;\"><b>Ex. de descrição macroscópica</b>\r\n          <div style=\"width: 4%; float:right;\"><i class=\"fa fa-angle-double-down\" style=\"color:#fff\" ></i></div>\r\n        </div>\r\n          </h3>\r\n          \r\n          <br>\r\n        <br>\r\n          <div id=\"formDoc\" name=\"formDoc\" style=\"display:none;\">  \r\n           \r\n                    <div>\r\n\r\n                      <ion-grid>\r\n                          <ion-row>\r\n                            <ion-col>\r\n                              <div><label>Material recebido</label></div>\r\n                            </ion-col>\r\n                          </ion-row>\r\n                          <ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n                          <ion-row style=\"margin-top: -10px;\">\r\n                            <ion-col>\r\n                              <div>\r\n                                <ion-radio value=\"1\"></ion-radio>&nbsp;\r\n                                <label >a fresco</label>\r\n                              </div>\r\n                            </ion-col>\r\n                          </ion-row>\r\n                          <ion-row style=\"margin-top: -10px;\">\r\n                            <ion-col>\r\n                              <div>\r\n                                <ion-radio value=\"2\"></ion-radio>&nbsp;\r\n                                <label >em formol tamponado a 10%</label>\r\n                              </div>\r\n                            </ion-col>\r\n                          </ion-row>\r\n                          <ion-row style=\"margin-top: -10px;\">\r\n                            <ion-col>\r\n                              <div>\r\n                                <ion-radio value=\"3\"></ion-radio>&nbsp;\r\n                                <label>outro (citar)</label> &nbsp;\r\n                                <input style = \"width: 50%;\" type=\"text\" >\r\n                              </div>\r\n                            </ion-col>\r\n                          </ion-row>\r\n                          </ion-radio-group>\r\n                    </ion-grid>\r\n          <ion-grid >\r\n            <ion-row>\r\n            <ion-col>\r\n              <div><label>que consiste de útero</label></div>\r\n            </ion-col>\r\n            </ion-row>\r\n            <ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n            <ion-row style=\"margin-top: -10px;\">\r\n            <ion-col>\r\n              <div>\r\n                <ion-radio value=\"1\"></ion-radio>&nbsp;\r\n                <label >aberto</label>\r\n              </div>\r\n            </ion-col>\r\n            <ion-col>\r\n              <div>\r\n                <ion-radio value=\"2\"></ion-radio>&nbsp;\r\n                <label >fechado</label>\r\n              </div>\r\n            </ion-col>\r\n            </ion-row>\r\n          </ion-radio-group>\r\n          </ion-grid>\r\n        </div>\t \r\n        \r\n        <div>\t\r\n          <ion-grid>\r\n             <ion-row>\r\n              <ion-col>\r\n                <div><label>colo</label></div>\r\n              </ion-col>\r\n              </ion-row>\r\n              <ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-radio value=\"1\"></ion-radio>&nbsp;\r\n                    <label >presente</label>\r\n                    </div>\r\n                  </ion-col>\r\n                \r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-radio value=\"2\"></ion-radio>&nbsp;\r\n                    <label >parcial </label>\r\n                    </div>\r\n                  </ion-col>\r\n                  </ion-row>\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                \r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-radio value=\"3\"></ion-radio>&nbsp;\r\n                    <label >ausente</label>\r\n                    </div>\r\n                  </ion-col>\r\n                  </ion-row>\r\n              </ion-radio-group>\r\n              \r\n            </ion-grid>\r\n          </div>\r\n          \r\n          <div>\r\n            <ion-grid>\r\n              <ion-row>\r\n                <ion-col>\r\n                  <div><label>anexos</label></div>\r\n                </ion-col>\r\n              </ion-row>\r\n              <ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n              <ion-row style=\"margin-top: -10px;\">\r\n              <ion-col>\r\n                <ion-radio value=\"1\"></ion-radio>&nbsp;\r\n                <label >presentes</label>\r\n              </ion-col>\r\n              \r\n              \r\n              <ion-col>\r\n                <ion-radio value=\"2\"></ion-radio>&nbsp;\r\n                <label >ausentes</label>\r\n              </ion-col>\r\n              </ion-row>\r\n            </ion-radio-group>\r\n          </ion-grid>\r\n        </div>\r\n                      \r\n        <div>\r\n          <ion-grid>\r\n            <ion-row>\r\n            <ion-col>\r\n              <label >que pesa</label>&nbsp;\r\n              <input  style = \"width: 15%;\" type=\"text\" >&nbsp;\r\n              <label >g</label>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n              <ion-col>\r\n              <label>e mede</label>&nbsp;\r\n              <input style = \"width: 15%;\" type=\"text\" >\r\n              <label>x</label>\r\n              <input style = \"width: 15%;\" type=\"text\">\r\n              <label>x</label>\r\n              <input style = \"width: 15%;\" type=\"text\">&nbsp;\r\n              <label>cm.</label>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-grid>\r\n          </div>\r\n          \r\n          <div>\r\n          <ion-grid>\r\n            <ion-row>\r\n            <ion-col>\r\n              <div><label>O corpo uterino é recoberto por serosa lisa e brilhante e apresenta aspecto</label></div>\r\n              </ion-col>\r\n            </ion-row>\r\n\r\n            <ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n            <ion-row style=\"margin-top: -10px;\">\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio value=\"1\"></ion-radio>&nbsp;\r\n                  <label >piriforme</label>\r\n                </div>\r\n              </ion-col>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio value=\"2\"></ion-radio>&nbsp;\r\n                  <label>globoso</label>\r\n                </div>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-row style=\"margin-top: -10px;\">\r\n            \r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio value=\"3\"></ion-radio>&nbsp;\r\n                  <label>irregular</label>\r\n                </div>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-radio-group>\r\n          </ion-grid>\r\n          </div>\r\n          <div>\r\n            <ion-grid>\r\n              <ion-row style=\"margin-top: -10px;\">\r\n            \r\n                <ion-col>\r\n                  <div>\r\n                    <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                    <ion-radio value=\"d\"></ion-radio>&nbsp;\r\n                    <label>O colo uterino tem formato</label>\r\n                    </ion-radio-group>\r\n                  </div>\r\n                </ion-col>\r\n              </ion-row>\r\n              <ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n              <ion-row style=\"margin-top: -10px;\">\r\n            \r\n                <ion-col>\r\n                  <div>\r\n                    <ion-radio value=\"1\"></ion-radio>&nbsp;\r\n                    <label>circular</label>\r\n                  </div>\r\n                </ion-col>\r\n                <ion-col>\r\n                  <div>\r\n                    <ion-radio value=\"2\"></ion-radio>&nbsp;\r\n                    <label >irregular</label>\r\n                  </div>\r\n                </ion-col>\r\n              </ion-row>\r\n            </ion-radio-group>\r\n              <ion-row style=\"margin-top: -10px;\">\r\n            \r\n                <ion-col>\r\n                  <div>\r\n                    <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                    <ion-radio value=\"fff\"></ion-radio>&nbsp;\r\n                    <label >com ectocervice branco-amarelada</label>\r\n                    </ion-radio-group>\r\n                  </div>\r\n                </ion-col>\r\n              </ion-row>\r\n              <ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n              <ion-row style=\"margin-top: -10px;\">\r\n            \r\n                <ion-col>\r\n                  <div>\r\n                    <ion-radio value=\"lisa\"></ion-radio>&nbsp;\r\n                    <label>lisa</label>\r\n                  </div>\r\n                </ion-col>\r\n                <ion-col>\r\n                  <div>\r\n                    <ion-radio value=\"rugosa\"></ion-radio>&nbsp;\r\n                    <label>rugosa</label>\r\n                  </div>\r\n                </ion-col>\r\n              </ion-row>\r\n            </ion-radio-group>\r\n   \r\n              <ion-row style=\"margin-top: -10px;\">\r\n               <ion-col>\r\n                  <div><label>medindo</label>&nbsp;\r\n                  <input style = \"width: 15%;\" type=\"text\" >&nbsp;\r\n                  <label>cm de diâmetro, apresentando OE em</label></div>\r\n                </ion-col>\r\n              </ion-row>\r\n              <ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                    <ion-col>\r\n                      <div>\r\n                        <ion-radio value=\"fenda\"></ion-radio>&nbsp;\r\n                      <label>fenda</label>\r\n                    </div>\r\n                    </ion-col>\r\n                    <ion-col>\r\n                      <div>\r\n                        <ion-radio value=\"puntiforme\"></ion-radio>&nbsp;\r\n                      <label >puntiforme</label>\r\n                    </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                </ion-radio-group>\r\n            </ion-grid>\r\n          </div>\r\n\r\n          \r\n          <div>\r\n          <ion-grid>\r\n            <ion-row>\r\n            <ion-col>\r\n              <div><label>Aos cortes</label></div>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n            <ion-row >\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio value=\"1\"></ion-radio>&nbsp;\r\n                  <ion-label >o canal endocervical mede</ion-label>&nbsp;\r\n                  <input  style = \"width: 10%;\" type=\"text\">\r\n                  cm, e a cavidade uterina mede&nbsp;\r\n                  <input style = \"width: 10%;\" type=\"text\" >&nbsp;\r\n                  cm, ambos de comprimento.\r\n                </div>\r\n              </ion-col>\r\n            </ion-row>\r\n           \r\n            <ion-row>\r\n              <ion-col class=\"ion-text-wrap\">\r\n                <div class=\"ion-text-wrap\">\r\n                  <ion-radio value=\"2\"></ion-radio>&nbsp;\r\n                  <ion-label >a cavidade uterina mede</ion-label>&nbsp;\r\n                  <input  style = \"width: 9%;\" type=\"text\" >&nbsp;\r\n                  cm de comprimento&nbsp;\r\n                 \r\n                </div>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-radio-group>\r\n            <ion-row class=\"ion-text-wrap\">\r\n              <ion-col>\r\n                <div>\r\n                \r\n                  <ion-label >O miométrio mede</ion-label>&nbsp;\r\n                  <input style = \"width: 9%;\" type=\"text\" >&nbsp;     \r\n                  cm de espessura máxima, sendo trabeculado e esbranquiçado\r\n                </div>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-grid>\r\n          <ion-grid>\r\n            <ion-row >\r\n              <ion-col>\r\n                <div>\r\n                 \r\n                  <ion-label >O endométrio</ion-label>&nbsp;\r\n                 \r\n                </div>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-row >\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"ssss\"></ion-radio>&nbsp;\r\n                  <ion-label>apresenta lesão</ion-label>\r\n                  </ion-radio-group>\r\n                \r\n                  \r\n                </div>\r\n              </ion-col>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"gggg\"></ion-radio>&nbsp;\r\n                  <ion-label>ulcerada</ion-label> \r\n                  </ion-radio-group>\r\n                  \r\n                </div>\r\n              </ion-col>\r\n            </ion-row>\r\n            \r\n            <ion-row>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"fff\"></ion-radio>&nbsp;\r\n                  <ion-label >vegetante</ion-label>\r\n                  </ion-radio-group>\r\n                </div>\r\n              </ion-col>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"ffff\"></ion-radio>&nbsp;\r\n                  <ion-label >infiltrativa</ion-label>\r\n                  </ion-radio-group>\r\n                </div>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"ttt\"></ion-radio>&nbsp;\r\n                  <ion-label >outro (citar)</ion-label>&nbsp;\r\n               \r\n                  <input style = \"width: 50%;\" type=\"text\" >\r\n                  </ion-radio-group>\r\n                  </div>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n              <ion-col>\r\n                <div>\r\n                                \r\n                  <input style = \"width: 50%;text-align: center;\" placeholder=\"(coloração)\" type=\"text\" >\r\n                  </div>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"iiii\"></ion-radio>&nbsp;\r\n                  <ion-label for=\"inlineRadio26\">que mede</ion-label>&nbsp;\r\n                  <input style = \"width: 9%;\" type=\"text\" >\r\n                  x\r\n                  <input style = \"width: 9%;\" type=\"text\" >\r\n                  x\r\n                  <input  style = \"width: 9%;\" type=\"text\" >&nbsp;\r\n                  cm\r\n                  </ion-radio-group>\r\n                  </div>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n                <ion-row>\r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-radio value=\"1\"></ion-radio>&nbsp;\r\n                      <ion-label>lisa</ion-label>\r\n                    \r\n                    </div>\r\n                  </ion-col>\r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-radio value=\"2\"></ion-radio>&nbsp;\r\n                      <ion-label>granulosa</ion-label>\r\n                    \r\n                    </div>\r\n                  </ion-col>\r\n                </ion-row>\r\n            </ion-radio-group>\r\n            <ion-row>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"ffff\"></ion-radio>&nbsp;\r\n                  <ion-label>localizada em endométrio</ion-label>\r\n                  </ion-radio-group>\r\n                </div>\r\n              </ion-col>\r\n                       </ion-row>\r\n            <ion-row>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"ffff\"></ion-radio>&nbsp;\r\n                  <ion-label >anterior</ion-label>&nbsp;\r\n                  </ion-radio-group>\r\n                 \r\n                </div>\r\n              </ion-col>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"ddddd\"></ion-radio>&nbsp;\r\n                  <ion-label >posterior</ion-label>&nbsp;\r\n                  </ion-radio-group>\r\n                 \r\n                </div>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"yyy\"></ion-radio>&nbsp;\r\n                  <ion-label >A lesão compromete</ion-label>&nbsp;\r\n                  <input style = \"width: 09%;\" type=\"text\" >\r\n                  cm da parede miometrial\r\n                  </ion-radio-group>\r\n                  </div>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n                <ion-row>\r\n                  <ion-col>\r\n                    <div>\r\n                     \r\n                      <ion-radio value=\"1\"></ion-radio>&nbsp;\r\n                      <ion-label >infiltrando menos</ion-label>&nbsp;\r\n                     \r\n                    \r\n                    </div>\r\n                  </ion-col>\r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-radio value=\"2\"></ion-radio>&nbsp;\r\n                      <ion-label >infiltrando mais</ion-label>&nbsp;\r\n                    \r\n                    </div>\r\n                  </ion-col>\r\n                </ion-row>\r\n          </ion-radio-group>\r\n            <ion-row>\r\n              <ion-col>\r\n                <div>\r\n                  \r\n                  <ion-label >de 50% da sua espessura</ion-label>&nbsp;\r\n                 \r\n                </div>\r\n              </ion-col>\r\n            \r\n            </ion-row>\r\n            <ion-row>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"rrr\"></ion-radio>&nbsp;\r\n                  <ion-label >compromete também</ion-label>&nbsp;\r\n                  </ion-radio-group>\r\n                 \r\n                </div>\r\n              </ion-col>\r\n             \r\n            </ion-row>\r\n            <ion-row>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"rrrrr\"></ion-radio>&nbsp;\r\n                  <ion-label >serosa</ion-label>&nbsp;\r\n                  </ion-radio-group>\r\n                 \r\n                </div>\r\n              </ion-col>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"uuuu\"></ion-radio>&nbsp;\r\n                  <ion-label>istmo</ion-label>&nbsp;\r\n                  </ion-radio-group>\r\n                 \r\n                </div>\r\n              </ion-col>\r\n             \r\n            </ion-row>\r\n            <ion-row>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"dddd\"></ion-radio>&nbsp;\r\n                  <ion-label>colo</ion-label>&nbsp;\r\n                  </ion-radio-group>\r\n                 \r\n                </div>\r\n              </ion-col>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"ddddd\"></ion-radio>&nbsp;\r\n                  <ion-label >vagina</ion-label>&nbsp;\r\n                  </ion-radio-group>\r\n                 \r\n                </div>\r\n              </ion-col>\r\n             \r\n            </ion-row>\r\n            <ion-row>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"uuuuu\"></ion-radio>&nbsp;\r\n                  <ion-label >paramétrio(s)</ion-label>&nbsp;\r\n                  </ion-radio-group>\r\n                 \r\n                </div>\r\n              </ion-col>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"sssss\"></ion-radio>&nbsp;\r\n                  <ion-label >direito</ion-label>&nbsp;\r\n                  </ion-radio-group>\r\n                 \r\n                </div>\r\n              </ion-col>\r\n             \r\n            </ion-row>\r\n            <ion-row>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"tttt\"></ion-radio>&nbsp;\r\n                  <ion-label >esquerdo</ion-label>&nbsp;\r\n                  </ion-radio-group>\r\n                 \r\n                </div>\r\n              </ion-col>\r\n                          \r\n            </ion-row>\r\n\r\n            <ion-row>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"p\"></ion-radio>&nbsp;\r\n                  <ion-label >outros(citar)</ion-label>&nbsp;\r\n                  <input style = \"width: 50%;\" type=\"text\" >\r\n                  </ion-radio-group>\r\n                 \r\n                </div>\r\n              </ion-col>\r\n             \r\n            </ion-row>\r\n            <ion-row>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"uuuu\"></ion-radio>&nbsp;\r\n                  <ion-label >O endométrio remanescente mede</ion-label>&nbsp;\r\n                  <input style = \"width: 09%;\" type=\"text\">&nbsp;\r\n                  cm de espessura, sendo castanho- avermelhado e mole\r\n                  </ion-radio-group>\r\n                 \r\n                </div>\r\n              </ion-col>\r\n             \r\n            </ion-row>\r\n            <ion-row>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"dddd\"></ion-radio>&nbsp;\r\n                  <ion-label for=\"inlineRadio28\">O coto vaginal mede</ion-label>&nbsp;\r\n                  <input style = \"width: 09%;\" type=\"text\" >&nbsp;\r\n                  cm de comprimento, sendo a mucosa esbranquiçada e rugosa\r\n                 </ion-radio-group>\r\n                </div>\r\n              </ion-col>\r\n             \r\n            </ion-row>\r\n            <ion-row>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-radio value=\"rrrr\"></ion-radio>&nbsp;\r\n                  <ion-label for=\"inlineRadio28\">Os paramétrios possuem aspecto e localização usual, medindo o direito</ion-label>&nbsp;\r\n                 <br><input style = \"width: 9%;\" type=\"text\" >\r\n                  x\r\n                  <input style = \"width: 9%;\" type=\"text\" >\r\n                  cm e o esquerdo&nbsp;\r\n                  <input  style = \"width: 9%;\" type=\"text\" >\r\n                  x\r\n                  <input  style = \"width: 9%;\" type=\"text\" >&nbsp;\r\n                  cm\r\n                 </ion-radio-group>\r\n                </div>\r\n              </ion-col>\r\n             \r\n            </ion-row>\r\n            <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n            <ion-row>\r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-radio value=\"1\"></ion-radio>&nbsp;\r\n                      <ion-label for=\"inlineRadio28\">Os ovários possuem aspecto e localização usual, medindo o direito</ion-label>&nbsp;\r\n                      <input style = \"width: 9%;\" type=\"text\" >\r\n                      x\r\n                      <input style = \"width: 9%;\" type=\"text\" >\r\n                      x\r\n                      <input style = \"width: 9%;\" type=\"text\" >&nbsp;\r\n                      cm e pesando&nbsp;\r\n                      <input  style = \"width: 9%;\" type=\"text\" >\r\n                      g, e o esquerdo&nbsp;<br>\r\n                      <input  style = \"width: 9%;\" type=\"text\" >\r\n                      x\r\n                      <input  style = \"width: 9%;\" type=\"text\" >\r\n                      x\r\n                      <input  style = \"width: 9%;\" type=\"text\">&nbsp;\r\n                      cm, com \r\n                      <input  style = \"width: 9%;\" type=\"text\" >&nbsp;\r\n                      g. Aos cortes são elásticos,branco-acinzentados, brilhantes, levemente granulosos e com pequenos cistos de conteúdo claro \r\n                    </div>\r\n                  </ion-col>\r\n                </ion-row>\r\n                <ion-row>\r\n                  <ion-col>\r\n                      <div>\r\n                        <ion-radio value=\"2\" id='ovario' (click)=\"direito();\"></ion-radio>&nbsp;\r\n                        <ion-label for=\"inlineRadio24\">Ovário</ion-label>&nbsp;\r\n                      </div>\r\n                </ion-col>\r\n             </ion-row> \r\n             </ion-radio-group>\r\n             <ion-row>\r\n               <ion-col>\r\n                <div>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt9($event)\">\r\n                      <ion-radio value=\"direito\" disabled='{{certo}}'></ion-radio>&nbsp;\r\n                      <ion-label >direito</ion-label>&nbsp;\r\n                      <ion-radio value=\"esquerdo\" disabled='{{certo}}'></ion-radio>&nbsp;\r\n                      <ion-label>esquerdo,</ion-label>&nbsp;\r\n                    </ion-radio-group>\r\n                      medindo&nbsp;\r\n                      <input style = \"width: 9%;\" type=\"text\" >\r\n                      x\r\n                      <input style = \"width: 9%;\" type=\"text\" >\r\n                      x\r\n                      <input  style = \"width: 9%;\" type=\"text\" >&nbsp;\r\n                      cm, pesando &nbsp;\r\n                      <input  style = \"width: 9%;\" type=\"text\" >&nbsp;\r\n                      g, com superfície pardo-clara, lobulada, e aos cortes é elástico com pequenos cistos e áreas pardo-amareladas.\r\n                </div>\r\n              </ion-col>\r\n            \r\n            </ion-row>\r\n          \r\n          <ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n            <ion-row>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio value=\"1\"></ion-radio>&nbsp;\r\n                  <ion-label for=\"inlineRadio28\">Tubas uterinas de aspecto e localização usual, medindo a direita</ion-label>&nbsp;\r\n                  <input style = \"width: 9%;\" type=\"text\" >\r\n                  cm de comprimento e&nbsp;\r\n                  <input style = \"width: 9%;\" type=\"text\" >&nbsp;\r\n                  cm de diâmetro, e a esquerda&nbsp;\r\n                  <input style = \"width: 9%;\" type=\"text\" >&nbsp;\r\n                  cm de comprimento e &nbsp;\r\n                  <input style = \"width: 9%;\" type=\"text\" >\r\n                  cm de diâmetro, aos cortes são elásticas e mostram luzes virtuais&nbsp;\r\n                  \r\n                </div>\r\n              </ion-col>\r\n             \r\n            </ion-row>\r\n            <ion-row>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio value=\"2\"  (click)=\"esquerdo();\"></ion-radio>&nbsp;\r\n                  <ion-label>Tuba uterina </ion-label>&nbsp;\r\n                </div>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-radio-group>\r\n              <ion-row>\r\n              <ion-col>\r\n                  <div>  \r\n                  <ion-radio value=\"direita\" disabled='{{certo2}}'></ion-radio>&nbsp;\r\n                  <ion-label > direita </ion-label>&nbsp;\r\n                  <ion-radio value=\"esquerda\" disabled='{{certo2}}'></ion-radio>&nbsp;\r\n                  <ion-label > esquerda,</ion-label>&nbsp;\r\n                  medindo &nbsp;\r\n                  <input  style = \"width: 9%;\" type=\"text\">&nbsp;\r\n                  cm de comprimento e &nbsp;\r\n                  <input  style = \"width: 9%;\" type=\"text\" >&nbsp;\r\n                    de diâmetro máximo, com serosa lisa e brilhante e aos cortes mostra luz virtual. <br> Fragmentos representativos são submetidos a exame histológico. Legenda:&nbsp;\r\n                  <input style = \"width: 30%;\"type=\"text\" >.\r\n                </div>\r\n              </ion-col>\r\n            </ion-row>\r\n          \r\n           \r\n         \r\n          </ion-grid>\r\n          </div>\r\n        \r\n            <!--(click)='gerarPDF()'-->\r\n          <a class=\"btn btn-app\"style=\" width: 50%; padding: 5px 5px 5px 5px; border-radius: 3px;\" >\r\n            <i class=\"fa fa-lock fa-1x\" style=\"color:#878787\"></i>&nbsp;\r\n            <span class=\"fontItem\">Baixar Arquivo</span>\r\n          </a>\r\n          \r\n  <br>\r\n  <br>\r\n  </div>\r\n      <br>\r\n      <h3 margin-bottom><b>Cortes macroscópicos representativos</b></h3><br>\r\n      <p>\r\n       •\tEndométrio \r\n       <br>\r\n        <br>&nbsp;<b>1.\tCaso tumor seja identificado:</b> <a href = '#' (click)=\"viewImage(imgSource, imgTitle, imgDescription)\">  (Figura 10.2.1)</a><a href = '#' (click)=\"viewImage(imgSource4, imgTitle4, imgDescription4)\">  (Foto 10.1)</a>\r\n        <br>&nbsp;\t\ta.\tTrês fragmentos. Um deve conter a área de maior profundidade invadida pelo tumor e os demais toda a superfície do miométrio até a serosa (para fragmentos maiores que o cassete, deve-se dividir e coloca-los em cassetes separados. Deve-se, ainda, colocar legenda pertinente).\r\n        <br>&nbsp;\t\tb.\tDois fragmentos representando a transição do endométrio não neoplásico e tumor. Não há necessidade de representar toda a parede.\r\n        <br>&nbsp;\t\tc.\tQuando presente um fragmento da transição do tumor com outras áreas macroscopicamente comprometidas. Ex.: Tumor/Istmo, colo, ovário e/ou outros.\r\n        <br>&nbsp;<b>2.\tCaso tumor não seja identificado</b>\r\n        <br>&nbsp;\t\ta.\tInclua todo o endométrio. Preferencialmente na sequência endométrio logo após istmo até endométrio final e distinguindo a metade anterior da posterior.\r\n        <br>&nbsp;\t\tb.\tUm fragmento deve conter toda a superfície do miométrio até a serosa (para fragmentos maiores que o cassete, deve-se dividir e coloca-los em cassetes separados. Deve-se, ainda, colocar legenda pertinente).\r\n        <br>&nbsp;\r\n        <br>•\tRestante do útero, consulte: Histerectomia por lesões benignas. \r\n        <br>•\tCoto vaginal: deve ser representado por completo (seccione-o longitudinalmente e paralelamente à margem cirúrgica).\r\n        <br>•\tRepresente toda porção de tecido mole do paramétrio, tanto direito quanto esquerdo.\r\n        <br>•\tOvários e tubas uterinas: siga as respectivas instruções.\r\n        <br>•\tLinfonodos: representar todos, se possível por inteiro, caso necessário recomenda-se seccionar os linfonodos no maior eixo e incluir uma das metades identificando e seguindo as ordens dos grupos e lateralidades.\r\n        <br>\r\n        <br>&nbsp;<b>\tNota: </b>Quando não forem identificados linfonodos no tecido dissecado e o tecido for pequeno (até 3cm), fazer a inclusão total do material recebido.\r\n\r\n\r\n\r\n      </p>\r\n      <br>\r\n      \r\n    <p text-center>\r\n      <a class=\"btn btn-app\" style=\" width: 45%\"  (click)=\"desenhos()\">\r\n        <i class=\"fa fa-image fa-2x\" style=\"color:#e74a92\"></i><br>\r\n        <span>Figuras Ilustrativas</span>\r\n      </a>\r\n      <a class=\"btn btn-app\" style=\" width: 45%\" (click)=\"fotos()\">\r\n        <i class=\"fas fa-camera fa-2x\" style=\"color:#e74a92\"></i><br>\r\n        <span>Fotos Ilustrativas</span>\r\n      </a>\r\n      \r\n    </p>\r\n        \r\n        \r\n        \r\n    </ion-card-content>\r\n    \r\n  </ion-card>\r\n  \r\n  \r\n\r\n</ion-content>"

/***/ }),

/***/ "./src/app/pages/histerectomia_maligna_endometrial/histerectomia.maligna.endometrial.page.module.ts":
/*!**********************************************************************************************************!*\
  !*** ./src/app/pages/histerectomia_maligna_endometrial/histerectomia.maligna.endometrial.page.module.ts ***!
  \**********************************************************************************************************/
/*! exports provided: HisterectomiaMalignaEndometrialPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HisterectomiaMalignaEndometrialPageModule", function() { return HisterectomiaMalignaEndometrialPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _histerectomia_maligna_endometrial_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./histerectomia.maligna.endometrial.page */ "./src/app/pages/histerectomia_maligna_endometrial/histerectomia.maligna.endometrial.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _histerectomia_maligna_endometrial_page__WEBPACK_IMPORTED_MODULE_5__["HisterectomiaMalignaEndometrialPage"]
    }
];
var HisterectomiaMalignaEndometrialPageModule = /** @class */ (function () {
    function HisterectomiaMalignaEndometrialPageModule() {
    }
    HisterectomiaMalignaEndometrialPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_histerectomia_maligna_endometrial_page__WEBPACK_IMPORTED_MODULE_5__["HisterectomiaMalignaEndometrialPage"]]
        })
    ], HisterectomiaMalignaEndometrialPageModule);
    return HisterectomiaMalignaEndometrialPageModule;
}());



/***/ }),

/***/ "./src/app/pages/histerectomia_maligna_endometrial/histerectomia.maligna.endometrial.page.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/pages/histerectomia_maligna_endometrial/histerectomia.maligna.endometrial.page.ts ***!
  \***************************************************************************************************/
/*! exports provided: HisterectomiaMalignaEndometrialPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HisterectomiaMalignaEndometrialPage", function() { return HisterectomiaMalignaEndometrialPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/image-viewer/image-viewer.component */ "./src/app/components/image-viewer/image-viewer.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! pdfmake/build/pdfmake */ "./node_modules/pdfmake/build/pdfmake.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! pdfmake/build/vfs_fonts */ "./node_modules/pdfmake/build/vfs_fonts.js");
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var HisterectomiaMalignaEndometrialPage = /** @class */ (function () {
    function HisterectomiaMalignaEndometrialPage(modalController, router, navCtrl, formBuilder) {
        this.modalController = modalController;
        this.router = router;
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.imgSource4 = 'assets/img/hiperplasia_endometrial/fotos/DSC00333.png';
        this.imgTitle4 = '';
        this.imgDescription4 = 'Foto 10.1: Cortes representativos para útero/neoplasia maligna endometrial. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource = 'assets/img/hiperplasia_endometrial/img/1591642103182.png';
        this.imgTitle = '';
        this.imgDescription = 'Figura 10.2.1: Procedimentos macroscópicos para úteros com hiperplasia ou neoplasia maligna endometrial (adaptada in Westra WH, et al. Surgical pathology dissection: an illustrated guide).';
        this.certo = 'true';
        this.certo2 = 'true';
    }
    HisterectomiaMalignaEndometrialPage.prototype.viewImage = function (src, title, description) {
        if (title === void 0) { title = ''; }
        if (description === void 0) { description = ''; }
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_2__["ImageViewerComponent"],
                            componentProps: {
                                imgSource: src,
                                imgTitle: title,
                                imgDescription: description
                            },
                            cssClass: 'modal-fullscreen',
                            keyboardClose: true,
                            showBackdrop: true
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    HisterectomiaMalignaEndometrialPage.prototype.ngOnInit = function () {
    };
    HisterectomiaMalignaEndometrialPage.prototype.fotos = function () {
        this.router.navigateByUrl('/histerectomia_maligna_endometrial/fotos');
    };
    HisterectomiaMalignaEndometrialPage.prototype.desenhos = function () {
        this.router.navigateByUrl('histerectomia_maligna_endometrial/imagens');
    };
    HisterectomiaMalignaEndometrialPage.prototype.gerarPDF = function () {
        pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_4___default.a.vfs = pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_5___default.a.pdfMake.vfs;
        var docDefinition = {
            content: [
                {
                    columns: [
                        [
                            { text: 'Histerectomia por lesões benignas/ Instruções gerais', style: 'header' },
                            { text: '', style: 'sub_header' },
                            { text: '', style: 'text' },
                        ]
                    ]
                }
            ],
            styles: {
                header: {
                    bold: true,
                    fontSize: 20,
                    alignment: 'center'
                },
                sub_header: {
                    fontSize: 18,
                    alignment: 'center'
                },
                text: {
                    fontSize: 16,
                    margin: [0, 30, 0, 0],
                    alignment: 'left'
                }
            },
            pageSize: 'A4',
            pageOrientation: 'portrait'
        };
        pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_4___default.a.createPdf(docDefinition).open();
    };
    HisterectomiaMalignaEndometrialPage.prototype.toggleDetails = function () {
        if (document.getElementById("formDoc").style.display == 'none') {
            document.getElementById("formDoc").style.display = "block";
        }
        else {
            document.getElementById("formDoc").style.display = "none";
        }
    };
    HisterectomiaMalignaEndometrialPage.prototype.opt9 = function () { };
    HisterectomiaMalignaEndometrialPage.prototype.opt7 = function () { };
    HisterectomiaMalignaEndometrialPage.prototype.direito = function () {
        console.log('aqui');
        if (this.certo == 'true') {
            this.certo = 'false';
        }
        else {
            this.certo = 'true';
        }
    };
    HisterectomiaMalignaEndometrialPage.prototype.esquerdo = function () {
        console.log('aqui');
        if (this.certo2 == 'true') {
            this.certo2 = 'false';
        }
        else {
            this.certo2 = 'true';
        }
    };
    HisterectomiaMalignaEndometrialPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-histerectomiaMalignaEndometrial',
            template: __webpack_require__(/*! ./histerectomia.maligna.endometrial.page.html */ "./src/app/pages/histerectomia_maligna_endometrial/histerectomia.maligna.endometrial.page.html"),
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"]])
    ], HisterectomiaMalignaEndometrialPage);
    return HisterectomiaMalignaEndometrialPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-histerectomia_maligna_endometrial-histerectomia-maligna-endometrial-page-module.js.map