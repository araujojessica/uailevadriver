(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-contacts-contacts-module"],{

/***/ "./src/app/pages/contacts/contacts.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/contacts/contacts.module.ts ***!
  \***************************************************/
/*! exports provided: ContactsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactsPageModule", function() { return ContactsPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _contacts_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contacts.page */ "./src/app/pages/contacts/contacts.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _contacts_page__WEBPACK_IMPORTED_MODULE_5__["ContactsPage"]
    }
];
var ContactsPageModule = /** @class */ (function () {
    function ContactsPageModule() {
    }
    ContactsPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_contacts_page__WEBPACK_IMPORTED_MODULE_5__["ContactsPage"]]
        })
    ], ContactsPageModule);
    return ContactsPageModule;
}());



/***/ }),

/***/ "./src/app/pages/contacts/contacts.page.html":
/*!***************************************************!*\
  !*** ./src/app/pages/contacts/contacts.page.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n\t<ion-toolbar color=\"secondary\">\n\t  <ion-buttons slot=\"start\">\n\t\t<ion-menu-button color=\"light\"></ion-menu-button>\n\t  </ion-buttons>\n\t  <ion-title>\n\t\t  Autores/Desenvolvedores\n\t  </ion-title>\n\t</ion-toolbar>\n\t\n  </ion-header>\n  \n  <ion-content class=\"bg-white\">\n\t  \n\t  \n\t  <ion-list>\n\t\t \n  \n\t\t  <ion-item-group >\n\t\t\t<ion-item lines=\"none\">\n\t\t\t  <ion-label><p>Autor(a)</p></ion-label>\n\t\t\t</ion-item>\n  \n\t\t\t<ion-item >\n\t\t\t  <ion-label text-wrap>\n\t\t\t\t  <h2>Fernanda Silva Pereira</h2>\n\t\t\t\t  <p>Bióloga. Discente do Mestrado Profissional em Ciências Aplicadas à Saúde da Universidade do Vale do Sapucaí-UNIVAS.</p>\n\t\t\t  </ion-label>\n\t\t\t</ion-item>\n\t\t  </ion-item-group>\n\t\t  <ion-item-group>  \n\t\t\t<ion-item lines=\"none\">\n\t\t\t\t  <ion-label><p>Coautores(as)</p></ion-label>\n\t\t\t</ion-item>\n\t\t\t\n\t\t\t<ion-item>\n\t\t\t  <ion-label text-wrap>\n\t\t\t\t  <h2>João Vitor de Oliveira</h2>\n\t\t\t\t  <p>Discente da Universidade do Vale do Sapucaí-MG. Membro acadêmico do Colégio Brasileiro de Cirurgia e Pesquisador Júnior CNPq.</p>\n\t\t\t  </ion-label>\n\t\t\t</ion-item>\n\t\t\t\n\t\t\t<ion-item>\n\t\t\t  <ion-label text-wrap>\n\t\t\t\t  <h2>Antônio Pedro Pereira</h2>\n\t\t\t\t  <p>Discente do curso de Medicina da Universidade do Vale do Sapucaí-MG.</p>\n\t\t\t  </ion-label>\n\t\t\t  \n\t\t\t</ion-item>\n\t\t  </ion-item-group>\n\t\t  <ion-item-group>\n\t\t\t\n\t\t\t<ion-item lines=\"none\">\n\t\t\t  <ion-label><p>Orientador(a)</p></ion-label>\n\t\t\t</ion-item>\n\t\t\t  \n\t\t\t<ion-item >\n\t\t\t  <ion-label text-wrap>\n\t\t\t\t  <h2>Fiorita Gonzales Lopes Mundim</h2>\n\t\t\t\t  <p>Médica. Mestrado e Doutorado em Patologia pela Universidade Federal de São Paulo - UNIFESP-EPM. Docente do Mestrado Profissional em Ciências Aplicadas à Saúde da Universidade do Vale do Sapucaí-UNIVAS e docente na Universidade José do Rosário Vellano - UNIFENAS.</p>\n\t\t\t  </ion-label>\n\t\t\t</ion-item>\n\t\t  </ion-item-group>\n\t\t  <ion-item-group>\n\t\t\t<ion-item lines=\"none\">\n\t\t\t\t  <ion-label><p>Coorientadores(as)</p></ion-label>\n\t\t\t</ion-item>\n\t\t\t   \n\t\t\t<ion-item>\n\t\t\t  <ion-label text-wrap>\n\t\t\t\t  <h2>Geraldo Magela Salomé</h2>\n\t\t\t\t  <p>Enfermeiro. Professor Adjunto do Curso de Mestrado Profissional em Ciências Aplicadas à Saúde da Universidade do Vale do Sapucaí-UNIVAS. Possui Pós- Doutorado pelo Programa de Pós-Graduação em Cirurgia Plástica da Universidade Federal de São Paulo.</p>\n\t\t\t  </ion-label>\n\t\t\t</ion-item>\n\t\t\t\n\t\t\t<ion-item>\n\t\t\t  <ion-label text-wrap>\n\t\t\t\t  <h2>Adriana Rodrigues dos Anjos Mendonça</h2>\n\t\t\t\t  <p>Bióloga. Doutorado em Medicina (Hematologia) pela Universidade Federal de São Paulo. Coordenadora do Mestrado Profissional em Ciências Aplicadas à Saúde da Universidade do Vale do Sapucaí-UNIVAS.</p>\n  \n\t\t\t  </ion-label>\n\t\t\t  \n\t\t\t</ion-item>\n\t\t  </ion-item-group>\n\t\t  <ion-item-group>  \n\t\t\t<ion-item lines=\"none\">\n\t\t\t\t  <ion-label><p>Desenvolvedores(as)</p></ion-label>\n\t\t\t</ion-item>\n\t\t\t\n\t\t\t<ion-item>\n\t\t\t  <ion-label text-wrap>\n\t\t\t\t  <h2>Jessica Helena Araujo</h2>\n\t\t\t\t  <p>Engenheira da Computação pelo Instituto Nacional de Telecomunicações-INATEL. Aplicações web.</p>\n\t\t\t  </ion-label>\n\t\t\t</ion-item>\n\t\t\t\n\t\t\t<ion-item>\n\t\t\t  <ion-label text-wrap>\n\t\t\t\t  <h2>Michelle Cristina Alves de Andrade</h2>\n\t\t\t\t  <p>Analista de Sistemas. Bacharel em Ciência da Computação pela Universidade Federal de Lavras-UFLA.</p>\n\t\t\t  </ion-label>\n\t\t\t</ion-item>\n\t\t\t\n\t\t\t<ion-item >\n\t\t\t  <ion-label text-wrap>\n\t\t\t\t  <h2>Helder Fernandes Ribeiro</h2>\n\t\t\t\t  <p>Discente do 7° período do curso de Publicidade e Propaganda da Universidade do Vale do Sapucaí-UNIVAS.</p>\n\t\t\t  </ion-label>\n\t\t\t</ion-item>\n\t\t\t\n\t\t\t</ion-item-group>\n\t  </ion-list>\t\n  \n  </ion-content>\n  "

/***/ }),

/***/ "./src/app/pages/contacts/contacts.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/contacts/contacts.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NvbnRhY3RzL2NvbnRhY3RzLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/contacts/contacts.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/contacts/contacts.page.ts ***!
  \*************************************************/
/*! exports provided: ContactsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactsPage", function() { return ContactsPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ContactsPage = /** @class */ (function () {
    function ContactsPage() {
    }
    ContactsPage.prototype.ngOnInit = function () {
    };
    ContactsPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-contacts',
            template: __webpack_require__(/*! ./contacts.page.html */ "./src/app/pages/contacts/contacts.page.html"),
            styles: [__webpack_require__(/*! ./contacts.page.scss */ "./src/app/pages/contacts/contacts.page.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ContactsPage);
    return ContactsPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-contacts-contacts-module.js.map