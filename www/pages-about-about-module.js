(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-about-about-module"],{

/***/ "./src/app/pages/about/about.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/about/about.module.ts ***!
  \*********************************************/
/*! exports provided: AboutPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutPageModule", function() { return AboutPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _about_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./about.page */ "./src/app/pages/about/about.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _about_page__WEBPACK_IMPORTED_MODULE_5__["AboutPage"]
    }
];
var AboutPageModule = /** @class */ (function () {
    function AboutPageModule() {
    }
    AboutPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_about_page__WEBPACK_IMPORTED_MODULE_5__["AboutPage"]]
        })
    ], AboutPageModule);
    return AboutPageModule;
}());



/***/ }),

/***/ "./src/app/pages/about/about.page.html":
/*!*********************************************!*\
  !*** ./src/app/pages/about/about.page.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"secondary\">\n\t<ion-buttons slot=\"start\">\n      <ion-menu-button color=\"light\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n        Útero\n    </ion-title>\n  </ion-toolbar>\n  <ion-toolbar color=\"light\"> \n    <ion-searchbar [(ngModel)]=\"searchKey\"></ion-searchbar>\n    \n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n <ion-card class=\"bg-white\" no-margin>\n    <ion-card-content>\n      <h2 margin-bottom>\n        <ion-text color=\"dark\"><strong>Útero: <i>histerectomia</i> para hiperplasia endometrial ou carcinoma</strong></ion-text>\n      </h2>\n      <br>\n\t  <h3 margin-bottom text-dark><b>Procedimento</b></h3>\n\t  <br>\n      <p margin-bottom text-dark>\n\t  \n        1.\tSe histerectomia radical, linfonodos estarão inclusos na peça. Deve-se dissecá-los a fresco e separar em direitos e esquerdos: os obturadores, os interilíacos e ilíacos direito e esquerdo –nem todos esses grupos poderão estar presentes. \n\t\t<br>2.\tOrientar a peça e remover anexos. Se necessário, seguir os passos de Útero: histerectomia.\n\t\t<br>3.\tCaso ovários e tubas uterinas estejam presentes, sigas as instruções dos respectivos protocolos.\n\n      </p>\n\t  <br>\n      <h3 margin-bottom text-dark><b>Descrição</b></h3>\n\t  <br>\n      <p margin-bottom text-dark>\n        1.\tAvaliar qual o tipo de cirurgia: radical, total, com salpingectomia e  ooforectomia.\n\t\t<br>2.\tEm caso de tumor: dar a localização exata, indicar as três dimensões, o aspecto –sólido, papilar, ulcerado, necrótico, hemorrágico), cor, extensão do envolvimento endometrial, presença de extensão miometrial, serosa, paramétrios, mucosa cervical ou tubar.\n\t\t<br>4.\tRestante do útero, consulte: Útero: histerectomia.\n\t\t<br>3.\tOvários e tuba uterina: siga as respectivas instruções.\n\t\t<br>4.\tLinfonodos: número, aspecto macroscópico. Analise se há possível envolvimento tumoral.\n\n      </p>\n\t  <br>\n\t  <h3 margin-bottom><b>Cortes histológicos</b></h3><br>\n      <p>\n       1.\tCaso tumor identificado:\n\t\t<br>&nbsp;a.\tRealize três cortes. Um deve conter a área profunda invadida e os demais devem conter a superfície do endométrio e serosa.\n\t\t<br>&nbsp;b.\tFaça dois cortes do endométrio não neoplásico. Não há necessidade de seccionar toda a parede.\n\t\t<br>2.\tObtenha uma porção de tecido mole do paramétrio, tanto direito quanto esquerdo. \n\t\t<br>3.\tCaso tumor não seja identificado –irradiação prévia, carcinoma superficial, hiperplasia endometrial-.\n\t\t<br>&nbsp;a.\tInclua todo o  endométrio. Além disso, realize secções paralelas  de 2  a 3mm, de ambos as partes.\n\t\t<br>&nbsp;b.\tRestante do útero, consulte: Útero: histerectomia.\n\t\t<br>&nbsp;c.\tOvários e tuba uterina: siga as respectivas instruções.\n\t\t<br>&nbsp;d.\tLinfonodos:\n\t\t<br>&nbsp;&nbsp;i.\tObturador esquerdo\n\t\t<br>&nbsp;&nbsp;ii.\tObturador direito\n\t\t<br>&nbsp;&nbsp;iii.\tInterilíaco\n\t\t<br>&nbsp;&nbsp;iv.\tIlíaco esquerdo\n\t\t<br>&nbsp;&nbsp;v.\tIlíaco direito\n\n      </p>\n\t  <br>\n\t  <ion-grid>\n        <ion-row>\n\t\t\t<ion-col>\n\t\t\t\n\t\t\t\t\n\t\t\t\t\t<ion-img [src]=\"imgSource\" (click)=\"viewImage(imgSource, imgTitle, imgDescription)\"></ion-img>\n\t\t\t\t\n\t\t\t</ion-col>\n\t\t\t<ion-col>\n\t\t\t\n\t\t\t\t\n\t\t\t\t\t<ion-img [src]=\"imgSource2\" (click)=\"viewImage(imgSource2, imgTitle2, imgDescription2)\"></ion-img>\n\t\t\t\t\n\t\t\t</ion-col>\n\t\t</ion-row>\n\t  </ion-grid>\n\t\t\n\t\t\n\t\t\n\t\t\n    </ion-card-content>\n\t\n  </ion-card>\n  \n  \n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/about/about.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/about/about.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ion-content {\n  --background: linear-gradient(-135deg, var(--ion-color-medium), var(--ion-color-light))\n    ; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWJvdXQvQzpcXFVzZXJzXFxqZXNzaVxcRG9jdW1lbnRzXFxBUE1hY3JvL3NyY1xcYXBwXFxwYWdlc1xcYWJvdXRcXGFib3V0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVRO0lBQWEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Fib3V0L2Fib3V0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcbiAgICBpb24tY29udGVudCB7XG4gICAgICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KC0xMzVkZWcsIHZhcigtLWlvbi1jb2xvci1tZWRpdW0pLCB2YXIoLS1pb24tY29sb3ItbGlnaHQpKVxuICAgIH1cbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/about/about.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/about/about.page.ts ***!
  \*******************************************/
/*! exports provided: AboutPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutPage", function() { return AboutPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/image-viewer/image-viewer.component */ "./src/app/components/image-viewer/image-viewer.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var AboutPage = /** @class */ (function () {
    function AboutPage(modalController) {
        this.modalController = modalController;
        this.imgSource = 'assets/img/DSC00470-1.jpg';
        this.imgTitle = 'Útero';
        this.imgDescription = 'Legenda';
        this.imgSource2 = 'assets/img/1560445490590.png';
        this.imgTitle2 = 'Útero';
        this.imgDescription2 = 'Legenda';
    }
    AboutPage.prototype.viewImage = function (src, title, description) {
        if (title === void 0) { title = ''; }
        if (description === void 0) { description = ''; }
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_2__["ImageViewerComponent"],
                            componentProps: {
                                imgSource: src,
                                imgTitle: title,
                                imgDescription: description
                            },
                            cssClass: 'modal-fullscreen',
                            keyboardClose: true,
                            showBackdrop: true
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    AboutPage.prototype.ngOnInit = function () {
    };
    AboutPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-about',
            template: __webpack_require__(/*! ./about.page.html */ "./src/app/pages/about/about.page.html"),
            styles: [__webpack_require__(/*! ./about.page.scss */ "./src/app/pages/about/about.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"]])
    ], AboutPage);
    return AboutPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-about-about-module.js.map