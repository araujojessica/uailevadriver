(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-laqueadura-laqueadura-module"],{

/***/ "./src/app/pages/laqueadura/laqueadura.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/laqueadura/laqueadura.module.ts ***!
  \*******************************************************/
/*! exports provided: LaqueaduraPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LaqueaduraPageModule", function() { return LaqueaduraPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _laqueadura_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./laqueadura.page */ "./src/app/pages/laqueadura/laqueadura.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _laqueadura_page__WEBPACK_IMPORTED_MODULE_5__["LaqueaduraPage"]
    }
];
var LaqueaduraPageModule = /** @class */ (function () {
    function LaqueaduraPageModule() {
    }
    LaqueaduraPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_laqueadura_page__WEBPACK_IMPORTED_MODULE_5__["LaqueaduraPage"]]
        })
    ], LaqueaduraPageModule);
    return LaqueaduraPageModule;
}());



/***/ }),

/***/ "./src/app/pages/laqueadura/laqueadura.page.html":
/*!*******************************************************!*\
  !*** ./src/app/pages/laqueadura/laqueadura.page.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-toolbar color=\"secondary\">\r\n\t<ion-buttons slot=\"start\">\r\n      <ion-menu-button color=\"light\"></ion-menu-button>\r\n    </ion-buttons>\r\n    <ion-title>\r\n        Tuba uterina\r\n    </ion-title>\r\n  </ion-toolbar>\r\n <!-- <ion-toolbar color=\"light\"> \r\n    <ion-searchbar [(ngModel)]=\"searchKey\"></ion-searchbar>\r\n    \r\n  </ion-toolbar>-->\r\n</ion-header>\r\n\r\n<ion-content>\r\n <ion-card class=\"bg-white\" no-margin>\r\n    <ion-card-content>\r\n      \t<h2 margin-bottom>\r\n\t\t\t<ion-text color=\"dark\"><strong>Laqueadura </strong></ion-text>\r\n\t  \t</h2> \r\n\t \t<br>Este procedimento consiste, geralmente, na remoção de um segmento das tubas uterinas.\r\n\t  \t<br><p><b>Nota: </b> Em casos de excisão tubária completa, ir para o protocolo de salpingectomia.</p>\r\n\t  \t<br><h3 margin-bottom text-dark><b>Procedimentos macroscópicos</b></h3>\r\n\t \t<br>\r\n\t\t<p margin-bottom text-dark>\r\n\t\t\t1.\tMensurar o comprimento e o maior diâmetro de cada segmento.\r\n\t\t\t<br>2.\tAnalisar e descrever as características da superfície externa –usual, com rotura, irregular–.\r\n\t\t\t<br>3.\tRealizar cortes transversais sequenciais por todo o segmento com intervalos de 3mm.\r\n\t\t\t<br>4.\tAnalisar e descrever a superfície de corte e o aspecto da luz –virtual, dilatada–.\r\n\t\t</p>\r\n\t  \t<br>\r\n\t\t<h3 margin-bottom text-dark (click)=\"toggleDetailsLaqueadura();\" style=\"width:100%; cursor:pointer\">\r\n\t\t\t<div style=\"width: 100%; float:left; background: #878787; color: #fff; padding: 9px;\"><b>Ex. de descrição macroscópica</b>\r\n\t\t\t\t<div style=\"width: 4%; float:right;\"><i class=\"fa fa-angle-double-down\" style=\"color:#fff\" ></i></div>\r\n\t\t\t</div>\r\n\t\t</h3>\r\n\t   \t<br><br>\r\n     \t<div id=\"formDoc\" name=\"formDoc\" style=\"display:none;\">  \r\n\t\t\r\n\t\t\t\t<div>\r\n\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div><label>Material recebido</label></div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"a fresco\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label >a fresco</ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"em formol tamponado a 10%\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>em formol tamponado a 10%</ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"texto\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>outro (citar)</ion-label> &nbsp;\r\n\t\t\t\t\t\t\t\t\t<input style = \"width: 50%;\" type=\"text\" (keyup)=\"inputTexto1($event)\">\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t</ion-radio-group>\t\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<label>que consiste de segmento de tuba uterina</label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt2($event)\">\r\n\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"direita,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>direita</ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"esquerda,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label >esquerda</ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-label>medindo</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 16%;\"  type=\"text\" (keyup)=\"inputTexto2($event)\">&nbsp;\r\n\t\t\t\t\t\t\t\t<ion-label>cm de comprimento</ion-label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-label>e</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 16%;\"type=\"text\" (keyup)=\"inputTexto3($event)\">&nbsp;\r\n\t\t\t\t\t\t\t\t<ion-label>de diâmetro máximo</ion-label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-label>Apresenta serosa </ion-label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt3($event)\">\r\n\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\" lisa e brilhante,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label >lisa e brilhante</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt4($event)\">\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\" com rotura,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label >com rotura</ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt5($event)\">\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\" irregular,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>irregular</ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt6($event)\">\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio value=\" com\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<ion-label >com</ion-label>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\" sem\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>sem</ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\" fímbrias\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>fímbrias</ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt8($event)\">\r\n\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\", e aos cortes nota-se\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>e aos cortes nota-se</ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt9($event)\">\r\n\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\" luz virtual\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>luz virtual</ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\" luz dilatada\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>luz dilatada</ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-label>Toda espécime é submetida a exame</ion-label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-label>histológico</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 65%;text-align: center;\" placeholder=\"(__fs/__bls/legenda__)\" type=\"text\" (keyup)=\"inputTexto4($event)\" >\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\r\n\t\t\t\t</div>\r\n\r\n\t\t\t\r\n\t\t\t<br>\r\n\t\t\t<a class=\"btn btn-app\"style=\" width: 50%; padding: 5px 5px 5px 5px; border-radius: 3px;\" (click)='gerarPDF()' >\r\n\t\t\t\t<i class=\"far fa-file\" style=\"color:#878787\"></i>&nbsp;\r\n\t\t\t\t<span class=\"fontItem\">Baixar Arquivo</span>\r\n\t\t\t</a>\r\n\t\t\r\n\t\t</div>\r\n\t  \t<br>\r\n\t  \t<h3 margin-bottom><b>Cortes macroscópicos representativos</b></h3>\r\n     \t<p>\r\n\t\t\t<br>Utilize o material por inteiro. \r\n\t\t\t<br><br>\r\n\t\t\t<strong>Nota: </strong> Quando o material recebido contiver mais de um seguimento, no mesmo frasco, sem identificação de lateralidade, descrevê-los e, posteriormente, representa-los em cassetes distintos.\r\n\t    </p>\r\n\t \t<br>\r\n\t  <ion-grid>\r\n        <ion-row>\r\n\t\t\t<ion-col>\r\n\t\t\t\r\n\t\t\t\t\r\n\t\t\t\t<ion-img  class=\"img-thumbnail img-demo\" [src]=\"imgSource\" (click)=\"viewImage(imgSource, imgTitle, imgDescription)\"></ion-img>\r\n\t\t\t\t\r\n\t\t\t</ion-col>\r\n\t\t\t<ion-col>\r\n\t\t\t\r\n\t\t\t\t\r\n\t\t\t\t<ion-img class=\"img-thumbnail img-demo\" [src]=\"imgSource2\" (click)=\"viewImage(imgSource2, imgTitle2, imgDescription2)\"></ion-img>\r\n\t\t\t\t\r\n\t\t\t</ion-col>\r\n\t\t</ion-row>\r\n\t  </ion-grid>\r\n\r\n\t</ion-card-content>\r\n\t\r\n  </ion-card>\r\n  \r\n  \r\n\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/laqueadura/laqueadura.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/pages/laqueadura/laqueadura.page.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ion-content {\n  --background: linear-gradient(-135deg, var(--ion-color-medium), var(--ion-color-light))\r\n    ; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbGFxdWVhZHVyYS9DOlxcVXNlcnNcXGplc3NpXFxEb2N1bWVudHNcXEFQTWFjcm8vc3JjXFxhcHBcXHBhZ2VzXFxsYXF1ZWFkdXJhXFxsYXF1ZWFkdXJhLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVRO0lBQWEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2xhcXVlYWR1cmEvbGFxdWVhZHVyYS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XHJcbiAgICBpb24tY29udGVudCB7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoLTEzNWRlZywgdmFyKC0taW9uLWNvbG9yLW1lZGl1bSksIHZhcigtLWlvbi1jb2xvci1saWdodCkpXHJcbiAgICB9XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/laqueadura/laqueadura.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/laqueadura/laqueadura.page.ts ***!
  \*****************************************************/
/*! exports provided: LaqueaduraPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LaqueaduraPage", function() { return LaqueaduraPage; });
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! pdfmake/build/pdfmake */ "./node_modules/pdfmake/build/pdfmake.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! pdfmake/build/vfs_fonts */ "./node_modules/pdfmake/build/vfs_fonts.js");
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/image-viewer/image-viewer.component */ "./src/app/components/image-viewer/image-viewer.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/file-opener/ngx */ "./node_modules/@ionic-native/file-opener/ngx/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.vfs = pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default.a.pdfMake.vfs;





var LaqueaduraPage = /** @class */ (function () {
    function LaqueaduraPage(modalController, router, navCtrl, formBuilder, file, navController, fileOpener) {
        this.modalController = modalController;
        this.router = router;
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.file = file;
        this.navController = navController;
        this.fileOpener = fileOpener;
        this.imgSource = 'assets/img/laqueadura/fotos/DSC00528-1.png';
        this.imgTitle = '';
        this.imgDescription = 'Foto 2: Segmentos de tubas uterinas. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource2 = 'assets/img/laqueadura/fotos/DSC00530.png';
        this.imgTitle2 = '';
        this.imgDescription2 = 'Foto 2.3: Cortes representativos para segmentos de tubas uterinas. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.pdfObj = null;
    }
    LaqueaduraPage.prototype.viewImage = function (src, title, description) {
        if (title === void 0) { title = ''; }
        if (description === void 0) { description = ''; }
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_5__["ImageViewerComponent"],
                            componentProps: {
                                imgSource: src,
                                imgTitle: title,
                                imgDescription: description
                            },
                            cssClass: 'modal-fullscreen',
                            keyboardClose: true,
                            showBackdrop: true
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    LaqueaduraPage.prototype.ngOnInit = function () { };
    LaqueaduraPage.prototype.fotos = function () {
        this.router.navigateByUrl('/laqueadura/fotos');
    };
    LaqueaduraPage.prototype.desenhos = function () {
        this.router.navigateByUrl('/laqueadura/images');
    };
    LaqueaduraPage.prototype.gerarPDF = function () {
        var _this = this;
        if (this.option1 == undefined) {
            this.option1 = '';
        }
        if (this.option2 == undefined) {
            this.option2 = '';
        }
        if (this.option3 == undefined) {
            this.option3 = '';
        }
        if (this.option4 == undefined) {
            this.option4 = '';
        }
        if (this.option5 == undefined) {
            this.option5 = '';
        }
        if (this.option6 == undefined) {
            this.option6 = '';
        }
        if (this.option7 == undefined) {
            this.option7 = '';
        }
        if (this.option8 == undefined) {
            this.option8 = '';
        }
        if (this.option9 == undefined) {
            this.option9 = '';
        }
        if (this.texto1 == undefined) {
            this.texto1 = '';
        }
        if (this.texto2 == undefined) {
            this.texto2 = '';
        }
        if (this.texto3 == undefined) {
            this.texto3 = '';
        }
        if (this.texto4 == undefined) {
            this.texto4 = '';
        }
        if (this.option1 == 'texto') {
            this.option1 = this.texto1;
        }
        pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.vfs = pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default.a.pdfMake.vfs;
        var docDefinition = {
            content: [
                {
                    columns: [
                        [
                            { text: 'Laqueadura', style: 'header' },
                            { text: '', style: 'sub_header' },
                            { text: 'Material recebido ' + this.option1 + ' que consiste de segmento de tuba uterina ' + this.option2 + ' medindo ' + this.texto2 + ' cm de comprimento e ' + this.texto3 + ' de diâmetro máximo. ' +
                                    'Apresenta serosa' + this.option3 + '' + this.option4 + '' + this.option5 + '' + this.option6 + '' + this.option7 + '' + this.option8 + '' + this.option9 + '. Toda espécime é submetida a exame histológico ' + this.texto4 + '.', style: 'text' },
                        ]
                    ]
                }
            ],
            styles: {
                header: {
                    bold: true,
                    fontSize: 20,
                    alignment: 'center'
                },
                sub_header: {
                    fontSize: 18,
                    alignment: 'center'
                },
                text: {
                    fontSize: 16,
                    margin: [0, 30, 0, 0],
                    alignment: 'left'
                }
            },
            pageSize: 'A4',
            pageOrientation: 'portrait'
        };
        this.pdfObj = pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.createPdf(docDefinition);
        //console.log(this.pdfObj.getBuffer());
        this.pdfObj.getBuffer(function (buffer) {
            var blob = new Blob([buffer], { type: 'application/pdf' });
            // Save the PDF to the data Directory of our App
            _this.file.writeFile(_this.file.dataDirectory, 'Laqueadura.pdf', blob, { replace: true });
            // Open the PDf with the correct OS tools
            _this.fileOpener.open(_this.file.dataDirectory + 'Laqueadura.pdf', 'application/pdf');
        });
    };
    LaqueaduraPage.prototype.toggleDetailsLaqueadura = function () {
        if (document.getElementById("formDoc").style.display == 'none') {
            document.getElementById("formDoc").style.display = "block";
        }
        else {
            document.getElementById("formDoc").style.display = "none";
        }
    };
    LaqueaduraPage.prototype.opt1 = function (event) {
        this.option1 = event.target.value;
        if (this.option1 == undefined) {
            this.option1 = '';
        }
    };
    LaqueaduraPage.prototype.opt2 = function (event) {
        this.option2 = event.target.value;
        if (this.option2 == undefined) {
            this.option2 = '';
        }
    };
    LaqueaduraPage.prototype.opt3 = function (event) {
        this.option3 = event.target.value;
        if (this.option3 == undefined) {
            this.option3 = '';
        }
    };
    LaqueaduraPage.prototype.opt4 = function (event) {
        this.option4 = event.target.value;
        if (this.option4 == undefined) {
            this.option4 = '';
        }
    };
    LaqueaduraPage.prototype.opt5 = function (event) {
        this.option5 = event.target.value;
        if (this.option5 == undefined) {
            this.option5 = '';
        }
    };
    LaqueaduraPage.prototype.opt6 = function (event) {
        this.option6 = event.target.value;
        if (this.option6 == undefined) {
            this.option6 = '';
        }
    };
    LaqueaduraPage.prototype.opt7 = function (event) {
        this.option7 = event.target.value;
        if (this.option7 == undefined) {
            this.option7 = '';
        }
    };
    LaqueaduraPage.prototype.opt8 = function (event) {
        this.option8 = event.target.value;
        if (this.option8 == undefined) {
            this.option8 = '';
        }
    };
    LaqueaduraPage.prototype.opt9 = function (event) {
        this.option9 = event.target.value;
        if (this.option9 == undefined) {
            this.option9 = '';
        }
    };
    LaqueaduraPage.prototype.inputTexto1 = function (event) {
        this.texto1 = event.target.value;
    };
    LaqueaduraPage.prototype.inputTexto2 = function (event) {
        this.texto2 = event.target.value;
    };
    LaqueaduraPage.prototype.inputTexto3 = function (event) {
        this.texto3 = event.target.value;
    };
    LaqueaduraPage.prototype.inputTexto4 = function (event) {
        this.texto4 = event.target.value;
    };
    LaqueaduraPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-laqueadura',
            template: __webpack_require__(/*! ./laqueadura.page.html */ "./src/app/pages/laqueadura/laqueadura.page.html"),
            styles: [__webpack_require__(/*! ./laqueadura.page.scss */ "./src/app/pages/laqueadura/laqueadura.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"],
            _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_0__["File"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
            _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_8__["FileOpener"]])
    ], LaqueaduraPage);
    return LaqueaduraPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-laqueadura-laqueadura-module.js.map