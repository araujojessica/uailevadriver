(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-agf-agf-module"],{

/***/ "./src/app/pages/agf/agf.module.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/agf/agf.module.ts ***!
  \*****************************************/
/*! exports provided: AgfPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgfPageModule", function() { return AgfPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _agf_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./agf.page */ "./src/app/pages/agf/agf.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _agf_page__WEBPACK_IMPORTED_MODULE_5__["AgfPage"]
    }
];
var AgfPageModule = /** @class */ (function () {
    function AgfPageModule() {
    }
    AgfPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_agf_page__WEBPACK_IMPORTED_MODULE_5__["AgfPage"]]
        })
    ], AgfPageModule);
    return AgfPageModule;
}());



/***/ }),

/***/ "./src/app/pages/agf/agf.page.html":
/*!*****************************************!*\
  !*** ./src/app/pages/agf/agf.page.html ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n\t<ion-toolbar color=\"secondary\">\n\t  <ion-buttons slot=\"start\">\n\t\t<ion-menu-button color=\"light\"></ion-menu-button>\n\t  </ion-buttons>\n\t  <ion-title>\n\t\t  Aparelho Genital Feminino\n\t  </ion-title>\n\t</ion-toolbar>\n<!--\t<ion-toolbar color=\"light\"> \n\t  <ion-searchbar [(ngModel)]=\"searchKey\"></ion-searchbar>\n\t  \n\t</ion-toolbar>-->\n  </ion-header>\n  \n  <ion-content class=\"bg-white\">\n  \n\t  \n\t\t  <ion-list>\n\t\t\t  <ion-item-group>\n\t\t\t\t  \n\t\t\t\t  <ion-item-divider color=\"light\">Ovário</ion-item-divider>\n\t\t\t\t  \n\t\t\t\t  <ion-item class=\"classItemAgf\" (click)=\"ooforectomia()\">\n\t\t\t\t\t  <ion-label>Ooforectomia ou Ooforoplastia</ion-label>\n\t\t\t\t  </ion-item>\n\t\t\t  \n\t\t\t  </ion-item-group>\n\t\t\t  \n\t\t\t  <ion-item-group>\n\t\t\t\t  \n\t\t\t\t  <ion-item-divider color=\"light\">Tuba uterina</ion-item-divider>\n\t\t\t\t  \n\t\t\t\t  <ion-item class=\"classItemAgf\" (click)=\"laqueadura()\">\n\t\t\t\t\t  <ion-label>Laqueadura</ion-label>\n\t\t\t\t  </ion-item>\n\t\t\t\t  <ion-item class=\"classItemAgf\" (click)=\"salpingectomia()\">\n\t\t\t\t\t  <ion-label>Salpingectomia</ion-label>\n\t\t\t\t  </ion-item>\n\t\t\t  \n\t\t\t  </ion-item-group>\n\t\t\t  \n\t\t\t  <ion-item-group>\n\t\t\t\t  \n\t\t\t\t  <ion-item-divider color=\"light\">Útero</ion-item-divider>\n\t\t\t\t  \n\t\t\t\t  <ion-item class=\"classItemAgf\" (click)=\"biopsia()\">\n\t\t\t\t\t  <ion-label>Biópsia do colo do útero</ion-label>\n\t\t\t\t  </ion-item>\n\t\t\t\t  <ion-item class=\"classItemAgf\" (click)=\"cones_cervicais()\">\n\t\t\t\t\t  <ion-label>Conização</ion-label>\n\t\t\t\t  </ion-item>\n\t\t\t\t  <ion-item class=\"classItemAgf\" (click)=\"polipectomia()\">\n\t\t\t\t\t  <ion-label>Polipectomia</ion-label>\n\t\t\t\t  </ion-item>\n\t\t\t\t  <ion-item class=\"classItemAgf\" (click)=\"biopsia_endometrial()\">\n\t\t\t\t\t  <ion-label>Biópsia de endométrio ou curetagem semiótica</ion-label>\n\t\t\t\t  </ion-item>\n\t\t\t\t  <ion-item class=\"classItemAgf\" (click)=\"histerectomia_benigna()\">\n\t\t\t\t\t<ion-label>Histerectomia por lesões benignas/Orientações gerais</ion-label>\n\t\t\t\t</ion-item>\n\t\t\t\t<ion-item class=\"classItemAgf\" (click)=\"histerectomia_neoplasia_utero()\">\n\t\t\t\t\t<ion-label>Histerectomia por neoplasia malígna do colo do útero</ion-label>\n\t\t\t\t</ion-item>\n\t\t\t\t<ion-item class=\"classItemAgf\" (click)=\"histerectomia_maligna_endometrial()\">\n\t\t\t\t\t<ion-label>Histerectomia por hiperplasia ou neoplasia malígna endometrial</ion-label>\n\t\t\t\t</ion-item>\n\t\t\t\t  \n\t\t\t  </ion-item-group>\n\t\t\t  <ion-item-group>\n\t\t\t\t  \n\t\t\t\t  <ion-item-divider color=\"light\">Vulva</ion-item-divider>\n\t\t\t\t  \n\t\t\t\t  <ion-item class=\"classItemAgf\" (click)=\"vulvectomia()\">\n\t\t\t\t\t  <ion-label>Vulvectomia</ion-label>\n\t\t\t\t  </ion-item>\n\t\t\t  \n\t\t\t  </ion-item-group>\n\t\t\t  \n\t\t\t  \n\t\t  \n  \n\t\t  </ion-list>\n  \n  </ion-content>\n  "

/***/ }),

/***/ "./src/app/pages/agf/agf.page.scss":
/*!*****************************************!*\
  !*** ./src/app/pages/agf/agf.page.scss ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FnZi9hZ2YucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/agf/agf.page.ts":
/*!***************************************!*\
  !*** ./src/app/pages/agf/agf.page.ts ***!
  \***************************************/
/*! exports provided: AgfPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgfPage", function() { return AgfPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _pages_modal_search_filter_search_filter_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../pages/modal/search-filter/search-filter.page */ "./src/app/pages/modal/search-filter/search-filter.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var AgfPage = /** @class */ (function () {
    function AgfPage(modalCtrl, navCtrl, router) {
        this.modalCtrl = modalCtrl;
        this.navCtrl = navCtrl;
        this.router = router;
    }
    AgfPage.prototype.ngOnInit = function () {
    };
    AgfPage.prototype.searchFilter = function () {
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: _pages_modal_search_filter_search_filter_page__WEBPACK_IMPORTED_MODULE_3__["SearchFilterPage"]
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    AgfPage.prototype.hiperplasia = function () {
        this.router.navigateByUrl('/hiperplasia');
    };
    AgfPage.prototype.ooforectomia = function () {
        this.router.navigateByUrl('/ooforectomia');
    };
    AgfPage.prototype.laqueadura = function () {
        this.router.navigateByUrl('/laqueadura');
    };
    AgfPage.prototype.salpingectomia = function () {
        this.router.navigateByUrl('/salpingectomia');
    };
    AgfPage.prototype.carcinoma = function () {
        this.router.navigateByUrl('/carcinoma');
    };
    AgfPage.prototype.instrucoes_gerais = function () {
        this.router.navigateByUrl('/instrucoes_gerais');
    };
    AgfPage.prototype.polipectomia = function () {
        this.router.navigateByUrl('/polipectomia');
    };
    AgfPage.prototype.biopsia = function () {
        this.router.navigateByUrl('/biopsia');
    };
    AgfPage.prototype.cones_cervicais = function () {
        this.router.navigateByUrl('/conizacao_colo');
    };
    AgfPage.prototype.vulvectomia = function () {
        this.router.navigateByUrl('/vulvectomia');
    };
    AgfPage.prototype.biopsia_endometrial = function () {
        this.router.navigateByUrl('/biopsia_endometrial');
    };
    AgfPage.prototype.histerectomia_benigna = function () {
        this.router.navigateByUrl('/histerectomia_benigna');
    };
    AgfPage.prototype.histerectomia_maligna_endometrial = function () {
        this.router.navigateByUrl('/histerectomia_maligna_endometrial');
    };
    AgfPage.prototype.histerectomia_neoplasia_utero = function () {
        this.router.navigateByUrl('/histerectomia_neoplasia_utero');
    };
    AgfPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-agf',
            template: __webpack_require__(/*! ./agf.page.html */ "./src/app/pages/agf/agf.page.html"),
            styles: [__webpack_require__(/*! ./agf.page.scss */ "./src/app/pages/agf/agf.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], AgfPage);
    return AgfPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-agf-agf-module.js.map