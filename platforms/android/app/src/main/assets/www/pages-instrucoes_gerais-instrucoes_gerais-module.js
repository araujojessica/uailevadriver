(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-instrucoes_gerais-instrucoes_gerais-module"],{

/***/ "./src/app/pages/instrucoes_gerais/instrucoes_gerais.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/instrucoes_gerais/instrucoes_gerais.module.ts ***!
  \*********************************************************************/
/*! exports provided: InstrucoesGeraisPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InstrucoesGeraisPageModule", function() { return InstrucoesGeraisPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _instrucoes_gerais_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./instrucoes_gerais.page */ "./src/app/pages/instrucoes_gerais/instrucoes_gerais.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _instrucoes_gerais_page__WEBPACK_IMPORTED_MODULE_5__["InstrucoesGeraisPage"]
    }
];
var InstrucoesGeraisPageModule = /** @class */ (function () {
    function InstrucoesGeraisPageModule() {
    }
    InstrucoesGeraisPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_instrucoes_gerais_page__WEBPACK_IMPORTED_MODULE_5__["InstrucoesGeraisPage"]]
        })
    ], InstrucoesGeraisPageModule);
    return InstrucoesGeraisPageModule;
}());



/***/ }),

/***/ "./src/app/pages/instrucoes_gerais/instrucoes_gerais.page.html":
/*!*********************************************************************!*\
  !*** ./src/app/pages/instrucoes_gerais/instrucoes_gerais.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n    <ion-toolbar color=\"secondary\">\r\n      <ion-buttons slot=\"start\">\r\n        <ion-menu-button color=\"light\"></ion-menu-button>\r\n      </ion-buttons>\r\n      <ion-title>\r\n          Útero\r\n      </ion-title>\r\n    </ion-toolbar>\r\n    <ion-toolbar color=\"light\"> \r\n      <ion-searchbar [(ngModel)]=\"searchKey\"></ion-searchbar>\r\n      \r\n    </ion-toolbar>\r\n  </ion-header>\r\n  \r\n  <ion-content>\r\n   <ion-card class=\"bg-white\" no-margin>\r\n      <ion-card-content>\r\n        <h2 margin-bottom>\r\n          <ion-text color=\"dark\"><strong>Útero: histerectomia para hiperplasia endometrial ou carcinoma</strong></ion-text>\r\n        </h2>\r\n        <br>\r\n        <h3 margin-bottom text-dark><b>Procedimento</b></h3>\r\n        <br>\r\n        <p margin-bottom text-dark>\r\n        \r\n          1.\tSe histerectomia radical, linfonodos estarão inclusos na peça. Deve-se dissecá-los a fresco e separar em direitos e esquerdos: os obturadores, os interilíacos e ilíacos direito e esquerdo –nem todos esses grupos poderão estar presentes. \r\n          <br>2.\tOrientar a peça e remover anexos. Se necessário, seguir os passos de Útero: histerectomia.\r\n          <br>3.\tCaso ovários e tubas uterinas estejam presentes, sigas as instruções dos respectivos protocolos.\r\n  \r\n        </p>\r\n        <br>\r\n        <h3 margin-bottom text-dark><b>Descrição</b></h3>\r\n        <br>\r\n        <p margin-bottom text-dark>\r\n          1.\tAvaliar qual o tipo de cirurgia: radical, total, com salpingectomia e  ooforectomia.\r\n          <br>2.\tEm caso de tumor: dar a localização exata, indicar as três dimensões, o aspecto –sólido, papilar, ulcerado, necrótico, hemorrágico), cor, extensão do envolvimento endometrial, presença de extensão miometrial, serosa, paramétrios, mucosa cervical ou tubar.\r\n          <br>4.\tRestante do útero, consulte: Útero: histerectomia.\r\n          <br>3.\tOvários e tuba uterina: siga as respectivas instruções.\r\n          <br>4.\tLinfonodos: número, aspecto macroscópico. Analise se há possível envolvimento tumoral.\r\n  \r\n        </p>\r\n        <br>\r\n        <h3 margin-bottom><b>Cortes histológicos</b></h3><br>\r\n        <p>\r\n         1.\tCaso tumor identificado:\r\n          <br>&nbsp;a.\tRealize três cortes. Um deve conter a área profunda invadida e os demais devem conter a superfície do endométrio e serosa.\r\n          <br>&nbsp;b.\tFaça dois cortes do endométrio não neoplásico. Não há necessidade de seccionar toda a parede.\r\n          <br>2.\tObtenha uma porção de tecido mole do paramétrio, tanto direito quanto esquerdo. \r\n          <br>3.\tCaso tumor não seja identificado –irradiação prévia, carcinoma superficial, hiperplasia endometrial-.\r\n          <br>&nbsp;a.\tInclua todo o  endométrio. Além disso, realize secções paralelas  de 2  a 3mm, de ambos as partes.\r\n          <br>&nbsp;b.\tRestante do útero, consulte: Útero: histerectomia.\r\n          <br>&nbsp;c.\tOvários e tuba uterina: siga as respectivas instruções.\r\n          <br>&nbsp;d.\tLinfonodos:\r\n          <br>&nbsp;&nbsp;i.\tObturador esquerdo\r\n          <br>&nbsp;&nbsp;ii.\tObturador direito\r\n          <br>&nbsp;&nbsp;iii.\tInterilíaco\r\n          <br>&nbsp;&nbsp;iv.\tIlíaco esquerdo\r\n          <br>&nbsp;&nbsp;v.\tIlíaco direito\r\n  \r\n        </p>\r\n        <br>\r\n        <ion-grid>\r\n          <ion-row>\r\n              <ion-col>\r\n              \r\n                  \r\n                      <ion-img [src]=\"imgSource\" (click)=\"viewImage(imgSource, imgTitle, imgDescription)\"></ion-img>\r\n                  \r\n              </ion-col>\r\n              <ion-col>\r\n              \r\n                  \r\n                      <ion-img [src]=\"imgSource2\" (click)=\"viewImage(imgSource2, imgTitle2, imgDescription2)\"></ion-img>\r\n                  \r\n              </ion-col>\r\n          </ion-row>\r\n        </ion-grid>\r\n          \r\n          \r\n          \r\n          \r\n      </ion-card-content>\r\n      \r\n    </ion-card>\r\n    \r\n    \r\n  \r\n  </ion-content>\r\n  "

/***/ }),

/***/ "./src/app/pages/instrucoes_gerais/instrucoes_gerais.page.scss":
/*!*********************************************************************!*\
  !*** ./src/app/pages/instrucoes_gerais/instrucoes_gerais.page.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ion-content {\n  --background: linear-gradient(-135deg, var(--ion-color-medium), var(--ion-color-light))\r\n    ; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaW5zdHJ1Y29lc19nZXJhaXMvQzpcXFVzZXJzXFxqZXNzaVxcRG9jdW1lbnRzXFxBUE1hY3JvL3NyY1xcYXBwXFxwYWdlc1xcaW5zdHJ1Y29lc19nZXJhaXNcXGluc3RydWNvZXNfZ2VyYWlzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVRO0lBQWEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2luc3RydWNvZXNfZ2VyYWlzL2luc3RydWNvZXNfZ2VyYWlzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcclxuICAgIGlvbi1jb250ZW50IHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgtMTM1ZGVnLCB2YXIoLS1pb24tY29sb3ItbWVkaXVtKSwgdmFyKC0taW9uLWNvbG9yLWxpZ2h0KSlcclxuICAgIH1cclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/instrucoes_gerais/instrucoes_gerais.page.ts":
/*!*******************************************************************!*\
  !*** ./src/app/pages/instrucoes_gerais/instrucoes_gerais.page.ts ***!
  \*******************************************************************/
/*! exports provided: InstrucoesGeraisPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InstrucoesGeraisPage", function() { return InstrucoesGeraisPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/image-viewer/image-viewer.component */ "./src/app/components/image-viewer/image-viewer.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};



var InstrucoesGeraisPage = /** @class */ (function () {
    function InstrucoesGeraisPage(modalController) {
        this.modalController = modalController;
        this.imgSource = 'assets/img/DSC00470-1.jpg';
        this.imgTitle = 'Útero';
        this.imgDescription = 'Legenda';
        this.imgSource2 = 'assets/img/1560445490590.png';
        this.imgTitle2 = 'Útero';
        this.imgDescription2 = 'Legenda';
    }
    InstrucoesGeraisPage.prototype.viewImage = function (src, title, description) {
        if (title === void 0) { title = ''; }
        if (description === void 0) { description = ''; }
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_2__["ImageViewerComponent"],
                            componentProps: {
                                imgSource: src,
                                imgTitle: title,
                                imgDescription: description
                            },
                            cssClass: 'modal-fullscreen',
                            keyboardClose: true,
                            showBackdrop: true
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    InstrucoesGeraisPage.prototype.ngOnInit = function () {
    };
    InstrucoesGeraisPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-instrucoes_gerias',
            template: __webpack_require__(/*! ./instrucoes_gerais.page.html */ "./src/app/pages/instrucoes_gerais/instrucoes_gerais.page.html"),
            styles: [__webpack_require__(/*! ./instrucoes_gerais.page.scss */ "./src/app/pages/instrucoes_gerais/instrucoes_gerais.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"]])
    ], InstrucoesGeraisPage);
    return InstrucoesGeraisPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-instrucoes_gerais-instrucoes_gerais-module.js.map