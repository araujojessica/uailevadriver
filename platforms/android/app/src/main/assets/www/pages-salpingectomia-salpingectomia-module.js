(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-salpingectomia-salpingectomia-module"],{

/***/ "./src/app/pages/salpingectomia/salpingectomia.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/salpingectomia/salpingectomia.module.ts ***!
  \***************************************************************/
/*! exports provided: SalpingectomiaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalpingectomiaPageModule", function() { return SalpingectomiaPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _salpingectomia_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./salpingectomia.page */ "./src/app/pages/salpingectomia/salpingectomia.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _salpingectomia_page__WEBPACK_IMPORTED_MODULE_5__["SalpingectomiaPage"]
    }
];
var SalpingectomiaPageModule = /** @class */ (function () {
    function SalpingectomiaPageModule() {
    }
    SalpingectomiaPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_salpingectomia_page__WEBPACK_IMPORTED_MODULE_5__["SalpingectomiaPage"]]
        })
    ], SalpingectomiaPageModule);
    return SalpingectomiaPageModule;
}());



/***/ }),

/***/ "./src/app/pages/salpingectomia/salpingectomia.page.html":
/*!***************************************************************!*\
  !*** ./src/app/pages/salpingectomia/salpingectomia.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n\t<ion-toolbar color=\"secondary\">\r\n\t\t<ion-buttons slot=\"start\">\r\n\t\t\t<ion-menu-button color=\"light\"></ion-menu-button>\r\n\t\t</ion-buttons>\r\n\t\t<ion-title>\r\n\t\t\tTuba uterina\r\n\t\t</ion-title>\r\n\t</ion-toolbar>\r\n <!-- <ion-toolbar color=\"light\"> \r\n    <ion-searchbar [(ngModel)]=\"searchKey\"></ion-searchbar>\r\n    \r\n  </ion-toolbar>-->\r\n</ion-header>\r\n\r\n<ion-content>\r\n <ion-card class=\"bg-white\" no-margin>\r\n    <ion-card-content>\r\n\t\t<h2 margin-bottom>\r\n\t\t\t<ion-text color=\"dark\"><strong>Salpingectomia</strong></ion-text>\r\n\t\t</h2>\r\n\r\n      \t<br>\tRemoção uni ou bilateral da(s) tuba(s) uterina(s), comumente acompanhadas as histerectomias totais, ou realizadas em casos como: gravidez ectópica tubária rota, hidrossalpinge, neoplasia de tuba uterina (neoplasia rara), profilaxia (BRCA) e/ou outros.\r\n\t  \t<br><br>\r\n\t \t<h3 margin-bottom text-dark><b>Procedimentos macroscópicos</b></h3>\r\n\t\t<br>\r\n\t\t<p margin-bottom text-dark>\r\n\t\t\t1.\tMensurar comprimento, maior e menor diâmetro.\r\n\t\t\t<br>2.\tAnalisar e descrever a característica da serosa–usual, com fibrina, rotura, irregular, e/ou outros–, e presença ou ausência de fimbria.\r\n\t\t\t<br>3.\tPintar a serosa e a margem cirúrgica, em casos de neoplasia.\r\n\t\t\t<br>4.\tSeccionar longitudinalmente o infundíbulo e a extremidade distal (fimbria), e seccionar transversalmente o restante da tuba uterina com intervalos de 3 a 4 mm <a href = '#' (click)=\"viewImage(imgSource2, imgTitle2, imgDescription2)\"> (Figura 3.4)</a>.\r\n\t\t\t<br>5.\tAnalisar e descrever a superfície de corte e o aspecto da luz –patente, obliterada e sinais de cirurgia –, com presença –de material purulento, hemorrágico e/ou outros–.\r\n\t\t\t<br>6.\tSe existência de tumor: tamanho, consistência, tonalidade, distância da margem cirúrgica e da serosa.\r\n\t\t\t<br>7.\tCaso haja cisto descrever tamanho, tonalidade, conteúdo e aspecto –séssil ou pediculado–. \r\n\t\t</p>\r\n\t  \t<br>\r\n\t\t<h3 margin-bottom text-dark (click)=\"toggleDetails();\" style=\"width:100%; cursor:pointer\">\r\n\t\t\t<div style=\"width: 100%; float:left; background: #878787; color: #fff; padding: 9px;\"><b>Ex. de descrição macroscópica</b>\r\n\t\t\t\t<div style=\"width: 4%; float:right;\"><i class=\"fa fa-angle-double-down\" style=\"color:#fff\" ></i></div>\r\n\t\t\t</div>\r\n\t  \t</h3>\r\n\t    <br><br>\r\n\t\r\n\t\t<div id=\"formDoc\" name=\"formDoc\" style=\"display:none;\">  \r\n\t\t\t\r\n\t\t\t\t<div>\r\n\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div><ion-label>Material recebido</ion-label></div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"a fresco\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>a fresco</ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio  value=\"em formol tamponado a 10%\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label >em formol tamponado a 10%</ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"texto\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>outro (citar)</ion-label> &nbsp;\r\n\t\t\t\t\t\t\t\t\t<input style = \"width: 50%;\" type=\"text\" (keyup)=\"inputTexto1($event)\">\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t</div>\r\n\t\t\t\t<div>\r\n\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div><ion-label>que consiste de tuba uterina</ion-label></div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt2($event)\">\r\n\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"direita,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>direita</ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"esquerda,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label >esquerda</ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-label>medindo</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 13%;\" type=\"text\" (keyup)=\"inputTexto2($event)\">&nbsp;\r\n\t\t\t\t\t\t\t\t<ion-label>cm de comprimento</ion-label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<input style = \"width: 13%;\" type=\"text\" (keyup)=\"inputTexto3($event)\">&nbsp;\r\n\t\t\t\t\t\t\t\t<ion-label>e</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 13%;\" type=\"text\" (keyup)=\"inputTexto4($event)\">&nbsp;\r\n\t\t\t\t\t\t\t\t<ion-label>cm de diâmetro mínimo</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t<ion-label>e máximo</ion-label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-label>Apresenta serosa</ion-label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt3($event)\">\r\n\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"lisa e brilhante\" ></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label for=\"inlineRadio06\">lisa e brilhante</ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"de aspecto usual\" (click)=\"lisaBrilhante();\" ></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label >de aspecto usual</ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t<div id=\"lisaBrilhante\">\r\n\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt4($event)\">\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio value=\", com fibrina\" disabled='{{certo}}'></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<ion-label>com fibrina</ion-label>\r\n\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt5($event)\">\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio value=\", com rotura\" disabled='{{certo}}'></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<ion-label >com rotura</ion-label>\r\n\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt6($event)\">\r\n\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"texto\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label >outros (citar)</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<input style = \"width: 50%;\"  type=\"text\" (keyup)=\"inputTexto5($event)\">\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-label>Aos cortes é elástica, notando-se</ion-label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"luz patente\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label> luz patente</ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"luz obliterada\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>luz obliterada</ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt8($event)\">\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\" e sinais prévios de cirurgia\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>e sinais prévios de cirurgia</ion-label>\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt9($event)\">\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\" lesão de aspecto\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>lesão de aspecto</ion-label>\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt10($event)\">\r\n\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\" sólido,\" ></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>sólido</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\" cístico,\" ></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>cístico</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\" sólido-cístico,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>sólido-cístico</ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt11($event)\">\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"de\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<input style = \"width: 40%; text-align: center;\" type=\"text\" (keyup)=\"inputTexto6($event)\" placeholder = \"(tonalidade/ conteúdo)\" >&nbsp;\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt12($event)\">\r\n\t\t\t\t\t\t\t\t\t<ion-radio value = \"medindo\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>medindo</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<input style = \"width: 13%;\" type=\"text\" (keyup)=\"inputTexto7($event)\">&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>x</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<input style = \"width: 13%;\" type=\"text\" (keyup)=\"inputTexto8($event)\">&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>x</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<input style = \"width: 13%;\" type=\"text\" (keyup)=\"inputTexto9($event)\">&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>cm </ion-label>\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt13($event)\">\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"e distando\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>e distando</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<input style = \"width: 13%;\" type=\"text\" (keyup)=\"inputTexto10($event)\">&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>cm da margem cirúrgica e</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<input style = \"width: 13%;\" type=\"text\" (keyup)=\"inputTexto11($event)\">&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>cm da serosa</ion-label>\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt14($event)\">\r\n\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\". Notam-se, ainda, estruturas císticas acopladas à serosa, de tonalidade pardo-clara, medindo até\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>Notam-se, ainda, estruturas</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>císticas acopladas à serosa,de tonalidade pardo-clara medindo até</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<input style = \"width: 13%;\" type=\"text\" (keyup)=\"inputTexto12($event)\">&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>cm, e que aos cortes</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>são elásticas e preenchidas por líquido claro.</ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt15($event)\">\r\n\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"Toda espécime é submetida a exame histológico.\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>Toda espécime é submetida</ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"Fragmentos representativos são submetidos a exame histológico.\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<ion-label>Fragmentos representativos são </ion-label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-label>submetidos a exame histológico</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width:70%; text-align: center;\" placeholder = \"(_fs/_bls/legenda_)\" type = \"text\" (keyup)=\"inputTexto13($event)\">\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t</div>\r\n      \t<br>\r\n\t\t<a class=\"btn btn-app\"style=\" width: 50%; padding: 5px 5px 5px 5px; border-radius: 3px;\" (click)='gerarPDF()'  >\r\n\t\t\t<i class=\"far fa-file\" style=\"color:#878787\"></i>&nbsp;\r\n\t\t\t<span class=\"fontItem\">Baixar Arquivo</span>\r\n\t\t</a>\r\n\t\t<br>\r\n\t\t<br>\r\n\t </div>\r\n      \r\n\t  <br>\r\n\t   <h3 margin-bottom><b>Cortes macroscópicos representativos</b></h3><br>\r\n      <p><strong>\r\n       1.\tTuba uterina normal:</strong> um corte longitudinal do infundíbulo com a extremidade distal (fimbria), um corte transversal do istmo e outro da região de ampola <a href = '#' (click)=\"viewImage(imgSource3, imgTitle3, imgDescription3)\"> (Foto 3.1)</a>.\r\n\t\t<br>\r\n\t\t<br><strong>2. Tuba uterina com lesões císticas:\r\n\t\t<br>&nbsp;\tCassete 1 à __:</strong> seguir instruções de corte da tuba uterina não tumoral e acrescentar um corte por cada cm da lesão, quando possível transição(ões) da(s) lesão(ões) com parênquima integro.\r\n\t\t<br>\r\n\t\t<br><strong>3.\tTuba uterina com neoplasia:</strong> <a href = '#' (click)=\"viewImage(imgSource4, imgTitle4, imgDescription4)\"> (Figura 3.4.3)</a>\r\n\t\t<br>&nbsp;\t<strong>Cassete A_ à A_:</strong> Tumor (cortes representativos da neoplasia proporcional ao tamanho, estes devem conter, maior profundidade de invasão em relação a serosa e transição tumor/parênquima normal e tumor/ovário, quando aplicável).\r\n\t\t<br>&nbsp;\t<strong>Cassete B:</strong> Tuba uterina sem alterações aparentes e fímbria.\r\n\t\t<br>&nbsp;\t<strong>Cassete C:</strong> Margem proximal (corte transversal 3 a 4 mm).\r\n\t\t<br>\r\n\t\t<br><strong>4.\tTuba uterina para profilaxia (BRCA):</strong> Representar a espécime em sua totalidade.\r\n\r\n\r\n      </p>\r\n\t  <br>\r\n\r\n\t  <p text-center>\r\n\t\t<a class=\"btn btn-app\" style=\" width: 45%\"  (click)=\"desenhos()\">\r\n\t\t\t<i class=\"fa fa-image fa-2x\" style=\"color:#e74a92\"></i><br>\r\n\t\t\t<span>Figuras Ilustrativas</span>\r\n\t\t</a>\r\n\t\t<a class=\"btn btn-app\" style=\" width: 45%\" (click)=\"fotos()\">\r\n\t\t\t<i class=\"fas fa-camera fa-2x\" style=\"color:#e74a92\"></i><br>\r\n\t\t\t<span>Fotos Ilustrativas</span>\r\n\t\t</a>\r\n\t\t\r\n\t</p>\r\n\t\t\r\n\t\t\r\n    </ion-card-content>\r\n\t\r\n  </ion-card>\r\n  \r\n  \r\n\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/salpingectomia/salpingectomia.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/salpingectomia/salpingectomia.page.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ion-content {\n  --background: linear-gradient(-135deg, var(--ion-color-medium), var(--ion-color-light))\r\n    ; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc2FscGluZ2VjdG9taWEvQzpcXFVzZXJzXFxqZXNzaVxcRG9jdW1lbnRzXFxBUE1hY3JvL3NyY1xcYXBwXFxwYWdlc1xcc2FscGluZ2VjdG9taWFcXHNhbHBpbmdlY3RvbWlhLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVRO0lBQWEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3NhbHBpbmdlY3RvbWlhL3NhbHBpbmdlY3RvbWlhLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcclxuICAgIGlvbi1jb250ZW50IHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgtMTM1ZGVnLCB2YXIoLS1pb24tY29sb3ItbWVkaXVtKSwgdmFyKC0taW9uLWNvbG9yLWxpZ2h0KSlcclxuICAgIH1cclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/salpingectomia/salpingectomia.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/salpingectomia/salpingectomia.page.ts ***!
  \*************************************************************/
/*! exports provided: SalpingectomiaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalpingectomiaPage", function() { return SalpingectomiaPage; });
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! pdfmake/build/pdfmake */ "./node_modules/pdfmake/build/pdfmake.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! pdfmake/build/vfs_fonts */ "./node_modules/pdfmake/build/vfs_fonts.js");
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/image-viewer/image-viewer.component */ "./src/app/components/image-viewer/image-viewer.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/file-opener/ngx */ "./node_modules/@ionic-native/file-opener/ngx/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.vfs = pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default.a.pdfMake.vfs;





var SalpingectomiaPage = /** @class */ (function () {
    function SalpingectomiaPage(modalController, router, navCtrl, formBuilder, file, navController, fileOpener) {
        this.modalController = modalController;
        this.router = router;
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.file = file;
        this.navController = navController;
        this.fileOpener = fileOpener;
        this.imgSource = 'assets/img/salpingectomia/fotos/DSC00568.png';
        this.imgTitle = '';
        this.imgDescription = 'Foto 3: Tuba uterina normal. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource2 = 'assets/img/salpingectomia/fotos/DSC00570.png';
        this.imgTitle2 = '';
        this.imgDescription2 = 'Foto 3.4: Cortes gerais para tuba uterina normal. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource3 = 'assets/img/salpingectomia/fotos/DSC00571.png';
        this.imgTitle3 = '';
        this.imgDescription3 = 'Foto 3.1: Cortes representativos para tuba uterina normal. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource4 = 'assets/img/salpingectomia/img/Figura3.png';
        this.imgTitle4 = '';
        this.imgDescription4 = 'Figura 3.4.3: Cortes representativos para tuba uterina com lesão (adaptada in Allen DC, Cameron RI. Histopathology Specimens: Clinical, Pathological and Laboratory Aspects).';
        this.pdfObj = null;
        this.certo = 'false';
    }
    SalpingectomiaPage.prototype.viewImage = function (src, title, description) {
        if (title === void 0) { title = ''; }
        if (description === void 0) { description = ''; }
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_5__["ImageViewerComponent"],
                            componentProps: {
                                imgSource: src,
                                imgTitle: title,
                                imgDescription: description
                            },
                            cssClass: 'modal-fullscreen',
                            keyboardClose: true,
                            showBackdrop: true
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    SalpingectomiaPage.prototype.ngOnInit = function () { };
    SalpingectomiaPage.prototype.fotos = function () {
        this.router.navigateByUrl('/salpingectomia/fotos');
    };
    SalpingectomiaPage.prototype.desenhos = function () {
        this.router.navigateByUrl('/salpingectomia/imagens');
    };
    SalpingectomiaPage.prototype.gerarPDF = function () {
        var _this = this;
        if (this.option1 == undefined) {
            this.option1 = '';
        }
        if (this.option2 == undefined) {
            this.option2 = '';
        }
        if (this.option3 == undefined) {
            this.option3 = '';
        }
        if (this.option4 == undefined) {
            this.option4 = '';
        }
        if (this.option5 == undefined) {
            this.option5 = '';
        }
        if (this.option6 == undefined) {
            this.option6 = '';
        }
        if (this.option7 == undefined) {
            this.option7 = '';
        }
        if (this.option8 == undefined) {
            this.option8 = '';
        }
        if (this.option9 == undefined) {
            this.option9 = '';
        }
        if (this.option10 == undefined) {
            this.option10 = '';
        }
        if (this.option11 == undefined) {
            this.option11 = '';
        }
        if (this.option12 == undefined) {
            this.option12 = '';
        }
        if (this.option13 == undefined) {
            this.option13 = '';
        }
        if (this.option14 == undefined) {
            this.option14 = '';
        }
        if (this.option15 == undefined) {
            this.option15 = '';
        }
        if (this.option16 == undefined) {
            this.option16 = '';
        }
        if (this.texto1 == undefined) {
            this.texto1 = '';
        }
        if (this.texto2 == undefined) {
            this.texto2 = '';
        }
        if (this.texto3 == undefined) {
            this.texto3 = '';
        }
        if (this.texto4 == undefined) {
            this.texto4 = '';
        }
        if (this.texto5 == undefined) {
            this.texto5 = '';
        }
        if (this.texto6 == undefined) {
            this.texto6 = '';
        }
        if (this.texto7 == undefined) {
            this.texto7 = '';
        }
        if (this.texto8 == undefined) {
            this.texto8 = '';
        }
        if (this.texto9 == undefined) {
            this.texto9 = '';
        }
        if (this.texto10 == undefined) {
            this.texto10 = '';
        }
        if (this.texto11 == undefined) {
            this.texto11 = '';
        }
        if (this.texto12 == undefined) {
            this.texto12 = '';
        }
        if (this.texto13 == undefined) {
            this.texto13 = '';
        }
        if (this.option1 == 'texto') {
            this.option1 = this.texto1;
        }
        if (this.option3 == 'lisa e brilhante') {
            this.option3 = this.option3 + '' + this.option4 + '' + this.option5;
        }
        if (this.option6 == 'texto') {
            this.option6 = ' ' + this.texto5;
        }
        if (this.option11 == 'de') {
            this.option11 = ' ' + this.texto6;
        }
        if (this.option12 == 'medindo') {
            this.option12 = ' medindo ' + this.texto7 + ' x ' + this.texto8 + ' x ' + this.texto9 + ' cm,';
        }
        if (this.option13 == 'e distando') {
            this.option13 = 'e distando ' + this.texto10 + ' cm da margem cirúrgica e ' + this.texto11 + ' cm da serosa';
        }
        if (this.option14 == '. Notam-se, ainda, estruturas císticas acopladas à serosa, de tonalidade pardo-clara, medindo até') {
            this.option14 = this.option14 + ' ' + this.texto12 + ' cm, e que aos cortes são elásticas e preenchidas por líquido claro.';
        }
        if (this.texto13 != '') {
            this.texto13 = this.texto13 + '.';
        }
        pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.vfs = pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default.a.pdfMake.vfs;
        var docDefinition = {
            content: [
                {
                    columns: [
                        [
                            { text: 'Salpingectomia', style: 'header' },
                            { text: '', style: 'sub_header' },
                            { text: 'Material recebido ' + this.option1 + ' que consiste de tuba uterina ' + this.option2 + ' medindo ' + this.texto2 + ' cm de comprimento, ' + this.texto3 + ' e ' + this.texto4 + ' cm de diâmetro mínimo e máximo.' +
                                    ' Apresenta serosa ' + this.option3 + '' + this.option6 + '. Aos cortes é elástica, notando-se ' + this.option7 + '' + this.option8 + '' + this.option9 + '' + this.option10 + '' + this.option11 + '' +
                                    '' + this.option12 + '' + this.option13 + '' + this.option14 + ' ' + this.option15 + ' ' + this.texto13 + '', style: 'text' },
                        ]
                    ]
                }
            ],
            styles: {
                header: {
                    bold: true,
                    fontSize: 20,
                    alignment: 'center'
                },
                sub_header: {
                    fontSize: 18,
                    alignment: 'center'
                },
                text: {
                    fontSize: 16,
                    margin: [0, 30, 0, 0],
                    alignment: 'left'
                }
            },
            pageSize: 'A4',
            pageOrientation: 'portrait'
        };
        this.pdfObj = pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.createPdf(docDefinition);
        //console.log(this.pdfObj.getBuffer());
        this.pdfObj.getBuffer(function (buffer) {
            var blob = new Blob([buffer], { type: 'application/pdf' });
            // Save the PDF to the data Directory of our App
            _this.file.writeFile(_this.file.dataDirectory, 'Salpingectomia.pdf', blob, { replace: true });
            // Open the PDf with the correct OS tools
            _this.fileOpener.open(_this.file.dataDirectory + 'Salpingectomia.pdf', 'application/pdf');
        });
    };
    SalpingectomiaPage.prototype.toggleDetails = function () {
        if (document.getElementById("formDoc").style.display == 'none') {
            document.getElementById("formDoc").style.display = "block";
        }
        else {
            document.getElementById("formDoc").style.display = "none";
        }
    };
    SalpingectomiaPage.prototype.opt1 = function (event) {
        this.option1 = event.target.value;
    };
    SalpingectomiaPage.prototype.opt2 = function (event) {
        this.option2 = event.target.value;
    };
    SalpingectomiaPage.prototype.opt3 = function (event) {
        this.option3 = event.target.value;
    };
    SalpingectomiaPage.prototype.opt4 = function (event) {
        this.option4 = event.target.value;
    };
    SalpingectomiaPage.prototype.opt5 = function (event) {
        this.option5 = event.target.value;
    };
    SalpingectomiaPage.prototype.opt6 = function (event) {
        this.option6 = event.target.value;
    };
    SalpingectomiaPage.prototype.opt7 = function (event) {
        this.option7 = event.target.value;
    };
    SalpingectomiaPage.prototype.opt8 = function (event) {
        this.option8 = event.target.value;
    };
    SalpingectomiaPage.prototype.opt9 = function (event) {
        this.option9 = event.target.value;
    };
    SalpingectomiaPage.prototype.opt10 = function (event) {
        this.option10 = event.target.value;
        console.log(this.option10);
    };
    SalpingectomiaPage.prototype.opt11 = function (event) {
        this.option11 = event.target.value;
        console.log(this.option11);
    };
    SalpingectomiaPage.prototype.opt12 = function (event) {
        this.option12 = event.target.value;
        console.log(this.option12);
    };
    SalpingectomiaPage.prototype.opt13 = function (event) {
        this.option13 = event.target.value;
        console.log(this.option13);
    };
    SalpingectomiaPage.prototype.opt14 = function (event) {
        this.option14 = event.target.value;
    };
    SalpingectomiaPage.prototype.opt15 = function (event) {
        this.option15 = event.target.value;
    };
    SalpingectomiaPage.prototype.opt16 = function (event) {
        this.option16 = event.target.value;
    };
    SalpingectomiaPage.prototype.inputTexto1 = function (event) {
        this.texto1 = event.target.value;
    };
    SalpingectomiaPage.prototype.inputTexto2 = function (event) {
        this.texto2 = event.target.value;
    };
    SalpingectomiaPage.prototype.inputTexto3 = function (event) {
        this.texto3 = event.target.value;
    };
    SalpingectomiaPage.prototype.inputTexto4 = function (event) {
        this.texto4 = event.target.value;
    };
    SalpingectomiaPage.prototype.inputTexto5 = function (event) {
        this.texto5 = event.target.value;
    };
    SalpingectomiaPage.prototype.inputTexto6 = function (event) {
        this.texto6 = event.target.value;
    };
    SalpingectomiaPage.prototype.inputTexto7 = function (event) {
        this.texto7 = event.target.value;
    };
    SalpingectomiaPage.prototype.inputTexto8 = function (event) {
        this.texto8 = event.target.value;
    };
    SalpingectomiaPage.prototype.inputTexto9 = function (event) {
        this.texto9 = event.target.value;
    };
    SalpingectomiaPage.prototype.inputTexto10 = function (event) {
        this.texto10 = event.target.value;
    };
    SalpingectomiaPage.prototype.inputTexto11 = function (event) {
        this.texto11 = event.target.value;
    };
    SalpingectomiaPage.prototype.inputTexto12 = function (event) {
        this.texto12 = event.target.value;
        console.log(this.texto12);
    };
    SalpingectomiaPage.prototype.inputTexto13 = function (event) {
        this.texto13 = event.target.value;
    };
    SalpingectomiaPage.prototype.lisaBrilhante = function () {
        console.log('entrou aqui');
        if (this.certo == 'false') {
            this.certo = 'true';
        }
        else {
            this.certo = 'false';
        }
    };
    SalpingectomiaPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-salpingectomia',
            template: __webpack_require__(/*! ./salpingectomia.page.html */ "./src/app/pages/salpingectomia/salpingectomia.page.html"),
            styles: [__webpack_require__(/*! ./salpingectomia.page.scss */ "./src/app/pages/salpingectomia/salpingectomia.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"], _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_0__["File"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
            _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_8__["FileOpener"]])
    ], SalpingectomiaPage);
    return SalpingectomiaPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-salpingectomia-salpingectomia-module.js.map