(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-info-info-module"],{

/***/ "./src/app/pages/info/info.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/info/info.module.ts ***!
  \*******************************************/
/*! exports provided: InfoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfoPageModule", function() { return InfoPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _info_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./info.page */ "./src/app/pages/info/info.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _info_page__WEBPACK_IMPORTED_MODULE_5__["InfoPage"]
    }
];
var InfoPageModule = /** @class */ (function () {
    function InfoPageModule() {
    }
    InfoPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_info_page__WEBPACK_IMPORTED_MODULE_5__["InfoPage"]]
        })
    ], InfoPageModule);
    return InfoPageModule;
}());



/***/ }),

/***/ "./src/app/pages/info/info.page.html":
/*!*******************************************!*\
  !*** ./src/app/pages/info/info.page.html ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n    <ion-toolbar color=\"secondary\">\n      <ion-buttons slot=\"start\">\n        <ion-menu-button color=\"light\"></ion-menu-button>\n      </ion-buttons>\n      <ion-title >Informações Gerais</ion-title>\n    </ion-toolbar>\n <!--   <ion-toolbar color=\"light\"> \n      <ion-searchbar [(ngModel)]=\"searchKey\"></ion-searchbar>\n      \n    </ion-toolbar>-->\n  </ion-header>\n  \n  <ion-content>\n   <ion-card class=\"bg-white\" no-margin>\n      <ion-card-content>\n        <p text-justify padding>\n          <ion-text class=\"text09\" color=\"medium\">\n           Este aplicativo disponibilizará aos profissionais de saúde informações cientificas e técnicas sobre a rotina macroscópica dos Laboratórios de Anatomia Patológica, com textos, desenhos ilustrativos e um acervo de fotos das biópsias e peças cirúrgicas. \n          </ion-text>\n        \n            <ion-text class=\"text09\" color=\"medium\" >\n             <br> <b>Nota:</b>\tCada peça é exclusiva e, em caso de dúvidas, deve ser discutido com algum componente da equipe que seja mais experiente.\n            </ion-text> \n           \n            </p>\n       \n      <p text-center>\n        <a class=\"btn btn-app\"  style=\" width: 85%;\" (click)=\"acondicionamento()\">\n        \n          <span>Acondicionamento e fixação do material</span>\n        </a>\n        <a class=\"btn btn-app\" style=\" width: 85%;\"  (click)=\"seccoes()\">\n            <span>Etapas fundamentais para iniciar as descrições e secções</span>\n        </a>\n       \n        <a class=\"btn btn-app\"  style=\" width: 85%;\" (click)=\"macroscopica()\">\n         \n          <span>Informações necessárias na descrição mascroscópica</span>\n        </a>\n\n        <a class=\"btn btn-app\"  style=\" width: 85%;\" (click)=\"seccao_pecas()\">\n       \n          <span>Informações necessárias para a secção das peças</span>\n        </a>\n\n        <a class=\"btn btn-app\"  style=\" width: 85%;\" (click)=\"fragmentos()\">\n        \n          <span>Fragmentos de tecidos de pequenos tamanhos</span>\n        </a>\n        <a class=\"btn btn-app\"  style=\" width: 85%;\" (click)=\"regras()\">\n     \n          <span>Identificação dos cassetes</span>\n        </a>\n        <a class=\"btn btn-app\"  style=\" width: 85%;\" (click)=\"descalcificacao()\">\n        \n          <span>Descalcificação</span>\n        </a>\n        <a class=\"btn btn-app\"  style=\" width: 85%;\" (click)=\"fotografico()\">\n         \n          <span>Arquivo Fotográfico</span>\n        </a>\n\n        <a class=\"btn btn-app\"  style=\" width: 85%;\" (click)=\"descarte_tecido()\">\n         \n          <span>Descarte dos tecidos</span>\n        </a>\n        \n          </p>\n          \n          \n          \n      </ion-card-content>\n      \n    </ion-card>\n    \n    \n  \n  </ion-content>\n  "

/***/ }),

/***/ "./src/app/pages/info/info.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/info/info.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2luZm8vaW5mby5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/info/info.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/info/info.page.ts ***!
  \*****************************************/
/*! exports provided: InfoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfoPage", function() { return InfoPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/core/user/user.service */ "./src/app/core/user/user.service.ts");
/* harmony import */ var _service_info_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./service/info.service */ "./src/app/pages/info/service/info.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var InfoPage = /** @class */ (function () {
    function InfoPage(userService, infoService, router) {
        var _this = this;
        this.userService = userService;
        this.infoService = infoService;
        this.router = router;
        this.infos = [];
        this.user$ = userService.getUser();
        infoService.listFromInfo().subscribe(function (infos) {
            console.log(infos);
            _this.infos = infos;
        });
    }
    InfoPage.prototype.seccoes = function () {
        this.router.navigateByUrl('/info/etapasfundamentais');
    };
    InfoPage.prototype.acondicionamento = function () {
        this.router.navigateByUrl('/info/acondicionamento');
    };
    InfoPage.prototype.macroscopica = function () {
        this.router.navigateByUrl('/info/macroscopica');
    };
    InfoPage.prototype.seccao_pecas = function () {
        this.router.navigateByUrl('/info/seccaopecas');
    };
    InfoPage.prototype.fragmentos = function () {
        this.router.navigateByUrl('/info/fragmentos');
    };
    InfoPage.prototype.descalcificacao = function () {
        this.router.navigateByUrl('/info/descalcificacao');
    };
    InfoPage.prototype.regras = function () {
        this.router.navigateByUrl('/info/regras');
    };
    InfoPage.prototype.fotografico = function () {
        this.router.navigateByUrl('/info/fotograficos');
    };
    InfoPage.prototype.descarte_tecido = function () {
        this.router.navigateByUrl('/info/desctecidos');
    };
    InfoPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-info',
            template: __webpack_require__(/*! ./info.page.html */ "./src/app/pages/info/info.page.html"),
            styles: [__webpack_require__(/*! ./info.page.scss */ "./src/app/pages/info/info.page.scss")]
        }),
        __metadata("design:paramtypes", [src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_1__["UserService"], _service_info_service__WEBPACK_IMPORTED_MODULE_2__["InfoService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], InfoPage);
    return InfoPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-info-info-module.js.map