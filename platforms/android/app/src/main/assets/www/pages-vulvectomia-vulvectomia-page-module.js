(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-vulvectomia-vulvectomia-page-module"],{

/***/ "./src/app/pages/vulvectomia/vulvectomia.page.html":
/*!*********************************************************!*\
  !*** ./src/app/pages/vulvectomia/vulvectomia.page.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n\t<ion-toolbar color=\"secondary\">\r\n\t  <ion-buttons slot=\"start\">\r\n\t\t<ion-menu-button color=\"light\"></ion-menu-button>\r\n\t  </ion-buttons>\r\n\t  <ion-title>\r\n\t\t  Vulva\r\n\t  </ion-title>\r\n\t</ion-toolbar>\r\n<!--\t<ion-toolbar color=\"light\"> \r\n\t  <ion-searchbar [(ngModel)]=\"searchKey\"></ion-searchbar>\r\n\t  \r\n\t</ion-toolbar>-->\r\n  </ion-header>\r\n  \r\n  <ion-content>\r\n   <ion-card class=\"bg-white\" no-margin>\r\n\t  <ion-card-content>\r\n\t\t<h2 margin-bottom>\r\n\t\t  <ion-text color=\"dark\"><strong>Vulvectomia </strong></ion-text>\r\n\t\t</h2>\r\n\t\t<br>A extensão e a profundidade da ressecção para obtenção da peça é variável. Quanto a extensão, pode ser realizada uma excisão local, vulvectomia parcial ou total. \r\n\t\tQuanto a profundidade da ressecção, são classificadas em superficial, simples ou profunda. Os linfonodos podem ser recebidos aderidos ou separados da peça.\r\n\t\t\r\n\t\t<br><br>\r\n\t\t\t<h3 margin-bottom text-dark><b>Procedimentos macroscópicos</b></h3>\r\n\t  \t<br>\r\n\t\t<p margin-bottom text-dark>\r\n\t\t  1.\tAvalie o tipo de vulvectomia: parcial, superficial, total, radical, profunda.\r\n\t\t  <br>2.\tEm caso de vulvectomia radical, separe os linfonodos em grupo, lateralidades e coloque-os em recipientes separados. \r\n\t\t  <br>3.\tCaso a peça tenha sido recebida a fresco e intacta:\r\n\t\t  <br>&nbsp; \tColoque a peça em uma placa de cortiça ou papelão e prenda com alfinetes as margens externas e vaginal, para fixar. \r\n\t\t  <br>4.\tA peça deve ser fotografada.\r\n\t\t  <br>5.\tMensure as três dimensões da peça (comprimento, largura e espessura), isso deve incluir, a região inguinal (caso esteja presente).\r\n\t\t  <br>6.\tDescreva a(s) lesão (ões): tamanho (3 dimensões), localização, extensão, invasão de estruturas adjacentes, tonalidade, superfície, bordas, invasão profunda ou estromal e distância em relação às margens.\r\n\t\t  <br>7.\tIdentifique e pinte as margens com tinta <i>Nankin</i>, preferencialmente, com cores distintas <a href = '#' (click)=\"viewImage(imgSource, imgTitle, imgDescription)\">  (Figura 11.7.AG)</a>.\r\n\t\t  <br>8.\tDescreva quando aplicável a característica do tecido não neoplásico: atrofia, ceratose, ulceração e/ou outros.\r\n\t\t  <br>9.\tLinfonodos: quantidades isoladas, dimensão do maior e comprometimento. \r\n\t\t</p>\r\n\t\t<br>\r\n\t\t<h3 margin-bottom text-dark (click)=\"toggleDetails();\" style=\"width:100%; cursor:pointer\">\r\n\t\t\t<div style=\"width: 100%; float:left; background: #878787; color: #fff; padding: 9px;\"><b>Ex. de descrição macroscópica</b>\r\n\t\t\t\t<div style=\"width: 4%; float:right;\"><i class=\"fa fa-angle-double-down\" style=\"color:#fff\" ></i></div>\r\n\t\t\t</div>\r\n\t\t</h3>\r\n\t\t  \r\n\t\t<br>\r\n\t\t<br>\r\n\t\t<div id=\"formDoc\" name=\"formDoc\" style=\"display:none;\">  \r\n\t\t\t\r\n\t\t\t\t<div>\r\n\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div><label>Material recebido</label></div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"4\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label>a fresco</label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"5\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label>em formol tamponado a 10%</label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"3\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label>outro (citar)</label> &nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 50%;\" type=\"text\">\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<label>que consiste de vulva</label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"sss\" (click)=\"apMassa();\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<label for=\"inlineRadio4\">parcial</label>&nbsp;&nbsp;&nbsp;\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"ssss\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio5\">direita</label>&nbsp;&nbsp;&nbsp;\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"ddddd\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio6\">esquerda</label>\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"\" (click)=\"apMassa2();\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<label for=\"inlineRadio7\">superficial</label>&nbsp;&nbsp;&nbsp;\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<div id=\"total\" style=\"display: block;\">\r\n\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"ssss\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<label for=\"inlineRadio8\"> total</label>&nbsp;&nbsp;&nbsp;\r\n\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"ddd\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<label for=\"inlineRadio9\"> radical</label>\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"ddd\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio11\">simples</label>\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<div id=\"total2\" style=\"display: block;\">\r\n\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"dddd\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<label for=\"inlineRadio11\">profunda</label>\r\n\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"2\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<label for=\"inlineRadio12\">de aspecto</label>\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"2\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio13\"> ovoide</label>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"1\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio14\"> elíptica</label>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"3\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio15\"> irregular</label>&nbsp;\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t<label >de cor</label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 30%;\" type=\"text\" >\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<label> com</label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 13%;\"  type=\"text\" >&nbsp;\r\n\t\t\t\t\t\t\t\t<label>x</label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 13%;\"   type=\"text\" >&nbsp;\r\n\t\t\t\t\t\t\t\t<label>x</label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 13%;\"  type=\"text\" >&nbsp;\r\n\t\t\t\t\t\t\t\t<label>cm.</label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<label>Na superfície nota-se lesão </label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\r\n\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"1\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<label for=\"inlineRadio16\">vegetante</label>&nbsp;&nbsp;\r\n\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"2\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<label for=\"inlineRadio17\">ulcerada</label>&nbsp;&nbsp;\r\n\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t<ion-radio value=\"3\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t<label for=\"inlineRadio18\">plana</label>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"ddddd\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio19\"> outros (citar)</label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 30%;\" type=\"text\" >\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<!--<input type=\"radio\" id=\"inlineRadio15\" formControlName = \"option15\" >&nbsp;-->\r\n\t\t\t\t\t\t\t\t<input style = \"width: 30%;text-align: center;\" placeholder=\"(tonalidade)\" type=\"text\" >\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio16\">, localizada em </label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"dddd\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio20\"> grande lábio</label>\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"ddddd\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio21\"> pequeno lábio</label>\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"fffff\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio22\"> direito</label>&nbsp;&nbsp;&nbsp;\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"dddd\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio23\"> esquerdo</label>&nbsp;&nbsp;&nbsp;\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"dddd\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio24\">clitóris</label>\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"dddd\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio25\">outros (citar)</label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 30%;\"  type=\"text\" >\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<label>medindo</label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 13%;\" type=\"text\"  >&nbsp;\r\n\t\t\t\t\t\t\t\t<label>x</label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 13%;\" type=\"text\" >&nbsp;\r\n\t\t\t\t\t\t\t\t<label>cm e</label>&nbsp;\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<input style = \"width: 13%;\" type=\"text\">&nbsp;\r\n\t\t\t\t\t\t\t\t<label> de profundidade máxima</label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<label>A lesão dista</label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 09%;\" type=\"text\" >&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio23\"> cm da margem, </label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<input style = \"width: 50%;\" type=\"text\"  >&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio23\">a mais próxima da resseção</label>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio23\">cirúrgica.</label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<label>O parênquima adjacente apresenta-se</label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"dddd\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio26\">sem características neoplásicas</label>\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"ddddd\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio27\">com atrofia</label>\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"dddd\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio29\">com ceratose</label>\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"ttttt\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio30\">com ulceração</label>\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"ffffff\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label > outros (citar)</label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 30%;\"  type=\"text\" >\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label >Do tecido adiposo inguinal</label>\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"ggggg\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label >direito foi(ram) encontrada(s)</label>&nbsp;\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<input style = \"width:13%;\"  type=\"text\">&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio31\">estrutura(s) nodular(es),</label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio31\"> medindo a maior</label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 13%;\"  type=\"text\" >&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio31\">cm</label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"fffff\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio34\">e do esquerdo</label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 09%;\"  type=\"text\">&nbsp;\r\n\t\t\t\t\t\t\t\t<label>estrutura(s)</label>\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<label>nodular(es),medindo a maior</label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 09%;\"  type=\"text\">&nbsp;\r\n\t\t\t\t\t\t\t\t<label>cm.</label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"gggg\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio35\">Do tecido adiposo femoral</label>\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"ffff\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio36\">direito foi(ram) encontrada(s) </label>\r\n\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t<input style = \"width: 09%;\"  type=\"text\">&nbsp;\r\n\t\t\t\t\t\t\t\t<label>estrutura(s) nodular(es),</label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<label>medindo a maior</label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 13%;\"  type=\"text\">&nbsp;\r\n\t\t\t\t\t\t\t\t<label>cm.</label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"llll\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio37\">e do esquerdo</label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 09%;\"  type=\"text\">&nbsp;\r\n\t\t\t\t\t\t\t\t<label>estrutura(s)</label>\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<label> nodular(es), medindo a maior</label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 09%;\"  type=\"text\">&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio36\">cm</label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"kkkkk\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio38\">Do tecido adiposo pélvico</label>\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"jjjj\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label for=\"inlineRadio39\">direito foi(ram) encontrada(s)</label>\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<input style = \"width: 13%;\"  type=\"text\" >&nbsp;\r\n\t\t\t\t\t\t\t\t<label>estrutura(s) nodular(es),</label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<label>medindo a maior</label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input  style = \"width: 13%;\"  type=\"text\">&nbsp;\r\n\t\t\t\t\t\t\t\t<label>cm</label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-radio value=\"pppp\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t<label>e do esquerdo </label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 13%;\"  type=\"text\">&nbsp;\r\n\t\t\t\t\t\t\t\t<label>estrutura(s)</label>\r\n\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<label> nodular(es), medindo a maior</label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 13%;\"  type=\"text\">&nbsp;\r\n\t\t\t\t\t\t\t\t<label>cm.</label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<label>Fragmentos representativos são submetidos a exame histológico.</label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t<label>Legenda: </label>&nbsp;\r\n\t\t\t\t\t\t\t\t<input style = \"width: 50%;\"  type=\"text\">\r\n\t\t\t\t\t\t\t\t<label>.</label>\r\n\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t</ion-row>\r\n\t\t\t\r\n\t\t\t</div>\r\n\t\t\r\n\t\r\n\t\t<br>\r\n\t\t  <!--(click)='gerarPDF()'-->\r\n          <a class=\"btn btn-app\"style=\" width: 50%; padding: 5px 5px 5px 5px; border-radius: 3px;\"  >\r\n            <i class=\"fa fa-lock fa-1x\" style=\"color:#878787\"></i>&nbsp;\r\n            <span class=\"fontItem\">Baixar Arquivo</span>\r\n          </a>\r\n\t\t<br>\r\n\t\t<br>\r\n\t\t</div>\r\n\t\t<br><h3 margin-bottom><b>Cortes macroscópicos representativos</b></h3>\r\n\t\t<a href = '#' (click)=\"viewImage(imgSource, imgTitle, imgDescription)\">  (Figura 11.7.AG)</a><a href = '#' (click)=\"viewImage(imgSource3, imgTitle3, imgDescription3)\">  (Foto 11.AG)</a>\r\n\t\t\t<br><p>\r\n\t\t\tCassete A: Tumor (Três fragmentos, um deve conter a área de maior profundidade invadida pelo tumor e os demais, quando possível, devem conter transição da lesão com tecido adjacente normal (para fragmentos maiores que o cassete, deve-se dividir e coloca-los em cassetes separados. Deve-se, ainda, colocar legenda pertinente)).\r\n\t\t\t<br>Cassete B: Margem vaginal e profunda mais próximas (cortes perpendiculares à margem de ressecção/área corada com <i>Nankin</i>).\r\n\t\t\t<br>Cassete C: Clitóris e margem vaginal anterior (corte longitudinal).\r\n\t\t\t<br>Cassete D: Margem grande lábio direito (corte transversal).\r\n\t\t\t<br>Cassete E: Margem grande lábio esquerdo (corte transversal).\r\n\t\t\t<br>Cassete F: Fúrcula e margem perineal (corte longitudinal).\r\n\t\t\t<br>Cassete G: Margem cutânea mais próxima (corte perpendicular à margem de ressecção).\r\n\t\t\t<br>Cassete H: Linfonodos: representar todos, se possível por inteiro, caso necessário recomenda-se seccionar os linfonodos no maior eixo e incluir uma das metades identificando o grupo e lateralidade.\r\n\t\r\n\t\t\t</p>\r\n\t\t\t<br>\r\n\t\t\r\n\t\t\t\r\n\t\t\t<p text-center>\r\n\t\t\t\t<a class=\"btn btn-app\" style=\" width: 45%\"  (click)=\"desenhos()\">\r\n\t\t\t\t\t<i class=\"fa fa-image fa-2x\" style=\"color:#e74a92\"></i><br>\r\n\t\t\t\t\t<span>Figuras Ilustrativas</span>\r\n\t\t\t\t</a>\r\n\t\t\t\t<a class=\"btn btn-app\" style=\" width: 45%\" (click)=\"fotos()\">\r\n\t\t\t\t\t<i class=\"fas fa-camera fa-2x\" style=\"color:#e74a92\"></i><br>\r\n\t\t\t\t\t<span>Fotos Ilustrativas</span>\r\n\t\t\t\t</a>\r\n\t\t\t</p>\r\n\r\n\t\t  \r\n\t\t  \r\n\t\t  \r\n\t\t</ion-card-content>\r\n\t  \r\n\t</ion-card>\r\n</ion-content>\r\n  "

/***/ }),

/***/ "./src/app/pages/vulvectomia/vulvectomia.page.module.ts":
/*!**************************************************************!*\
  !*** ./src/app/pages/vulvectomia/vulvectomia.page.module.ts ***!
  \**************************************************************/
/*! exports provided: VulvectomiaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VulvectomiaPageModule", function() { return VulvectomiaPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _vulvectomia_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./vulvectomia.page */ "./src/app/pages/vulvectomia/vulvectomia.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _vulvectomia_page__WEBPACK_IMPORTED_MODULE_5__["VulvectomiaPage"]
    }
];
var VulvectomiaPageModule = /** @class */ (function () {
    function VulvectomiaPageModule() {
    }
    VulvectomiaPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_vulvectomia_page__WEBPACK_IMPORTED_MODULE_5__["VulvectomiaPage"]]
        })
    ], VulvectomiaPageModule);
    return VulvectomiaPageModule;
}());



/***/ }),

/***/ "./src/app/pages/vulvectomia/vulvectomia.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/vulvectomia/vulvectomia.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ion-content {\n  --background: linear-gradient(-135deg, var(--ion-color-medium), var(--ion-color-light))\r\n    ; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdnVsdmVjdG9taWEvQzpcXFVzZXJzXFxqZXNzaVxcRG9jdW1lbnRzXFxBUE1hY3JvL3NyY1xcYXBwXFxwYWdlc1xcdnVsdmVjdG9taWFcXHZ1bHZlY3RvbWlhLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVRO0lBQWEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3Z1bHZlY3RvbWlhL3Z1bHZlY3RvbWlhLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcclxuICAgIGlvbi1jb250ZW50IHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgtMTM1ZGVnLCB2YXIoLS1pb24tY29sb3ItbWVkaXVtKSwgdmFyKC0taW9uLWNvbG9yLWxpZ2h0KSlcclxuICAgIH1cclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/vulvectomia/vulvectomia.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/vulvectomia/vulvectomia.page.ts ***!
  \*******************************************************/
/*! exports provided: VulvectomiaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VulvectomiaPage", function() { return VulvectomiaPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../components/image-viewer/image-viewer.component */ "./src/app/components/image-viewer/image-viewer.component.ts");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! pdfmake/build/pdfmake */ "./node_modules/pdfmake/build/pdfmake.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! pdfmake/build/vfs_fonts */ "./node_modules/pdfmake/build/vfs_fonts.js");
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_3___default.a.vfs = pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_4___default.a.pdfMake.vfs;
var VulvectomiaPage = /** @class */ (function () {
    function VulvectomiaPage(modalController, router, navCtrl, formBuilder) {
        this.modalController = modalController;
        this.router = router;
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.imgSource = 'assets/img/vulvectomia/img/1591558874913.png';
        this.imgTitle = '';
        this.imgDescription = 'Figura 11.7.AG: Procedimentos macroscópicos para vulva com neoplasia (adaptada in Westra WH, et al. Surgical pathology dissection: an illustrated guide).';
        this.imgSource3 = 'assets/img/vulvectomia/fotos/DSC00681.png';
        this.imgTitle3 = '';
        this.imgDescription3 = 'Foto 11.AG: Cortes representativos para vulva. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
    }
    VulvectomiaPage.prototype.viewImage = function (src, title, description) {
        if (title === void 0) { title = ''; }
        if (description === void 0) { description = ''; }
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_2__["ImageViewerComponent"],
                            componentProps: {
                                imgSource: src,
                                imgTitle: title,
                                imgDescription: description
                            },
                            cssClass: 'modal-fullscreen',
                            keyboardClose: true,
                            showBackdrop: true
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    VulvectomiaPage.prototype.ngOnInit = function () {
        this.vulvectomiaForm = this.formBuilder.group({
            option1: [''],
            option2: [''],
            option3: [''],
            option4: [''],
            option5: [''],
            option6: [''],
            option7: [''],
            option8: [''],
            option9: [''],
            option10: [''],
            option11: [''],
            option12: [''],
            option13: [''],
            option14: [''],
            option15: [''],
            option16: [''],
            option17: [''],
            option18: [''],
            option19: [''],
            option20: [''],
            option21: [''],
            option22: [''],
            option23: [''],
            option24: [''],
            option25: [''],
            option26: [''],
            option27: [''],
            option28: [''],
            option29: [''],
            option30: [''],
            option31: [''],
            option32: [''],
            option33: [''],
            option34: [''],
            option35: [''],
            option36: [''],
            option37: [''],
            option38: [''],
            option39: [''],
            option40: [''],
            option41: [''],
            texto: [''],
            texto01: [''],
            texto02: [''],
            texto03: [''],
            texto04: [''],
            texto05: [''],
            texto06: [''],
            texto07: [''],
            texto08: [''],
            texto09: [''],
            texto10: [''],
            texto11: [''],
            texto12: [''],
            texto13: [''],
            texto14: [''],
            texto15: [''],
            texto16: [''],
            texto17: [''],
            texto18: [''],
            texto19: [''],
            texto20: [''],
            texto21: [''],
            texto22: [''],
            texto23: [''],
            texto24: [''],
            texto25: [''],
            texto26: [''],
            texto27: ['']
        });
    };
    VulvectomiaPage.prototype.fotos = function () {
        this.router.navigateByUrl('/vulvectomia/fotos');
    };
    VulvectomiaPage.prototype.desenhos = function () {
        this.router.navigateByUrl('/vulvectomia/imagens');
    };
    VulvectomiaPage.prototype.gerarPDF = function () {
        console.log('entrou em gerar pdf');
        var option = this.vulvectomiaForm.getRawValue();
        console.log(option);
        var option1 = '';
        var option2 = '';
        var option3 = '';
        var option4 = '';
        var option5 = '';
        var option6 = '';
        var option7 = '';
        var option8 = '';
        var option9 = '';
        var option10 = '';
        var option11 = '';
        var option12 = '';
        var option13 = '';
        var option14 = '';
        var option15 = '';
        var option16 = '';
        var option17 = '';
        var option18 = '';
        var option19 = '';
        var option20 = '';
        var option21 = '';
        var option22 = '';
        var option23 = '';
        var option24 = '';
        var option25 = '';
        var option26 = '';
        var option27 = '';
        var option28 = '';
        var option29 = '';
        var option30 = '';
        var option31 = '';
        var option32 = '';
        var option33 = '';
        var option34 = '';
        var option35 = '';
        var option36 = '';
        var option37 = '';
        var option38 = '';
        var option39 = '';
        var option40 = '';
        var option41 = '';
        var texto = '';
        var texto01 = '';
        var texto02 = '';
        var texto03 = '';
        var texto04 = '';
        var texto05 = '';
        var texto06 = '';
        var texto07 = '';
        var texto08 = '';
        var texto09 = '';
        var texto10 = '';
        var texto11 = '';
        var texto12 = '';
        var texto13 = '';
        var texto14 = '';
        var texto15 = '';
        var texto16 = '';
        var texto17 = '';
        var texto18 = '';
        var texto19 = '';
        var texto20 = '';
        var texto21 = '';
        var texto22 = '';
        var texto23 = '';
        var texto24 = '';
        var texto25 = '';
        var texto26 = '';
        var texto27 = '';
        option1 = option.option1;
        if (option1 == 'texto') {
            option1 = option.texto;
        }
        option2 = option.option2;
        option3 = option.option3;
        option4 = option.option4;
        option5 = option.option5;
        option6 = option.option6;
        option7 = option.option7;
        option8 = option.option8;
        option9 = option.option9;
        option10 = option.option10;
        option11 = option.option11;
        if (option.option12 == 'texto') {
            option12 = option.texto05 + ",";
        }
        option13 = option.option13;
        option14 = option.option14;
        option15 = option.option15;
        option16 = option.option16;
        option17 = option.option17;
        option18 = option.option18;
        if (option18 == 'texto') {
            option18 = option.texto06 + ",";
        }
        option19 = option.option19;
        option20 = option.option20;
        option21 = option.option21;
        option22 = option.option22;
        option23 = option.option23;
        option24 = option.option24;
        option25 = option.option25;
        if (option.option26 == 'texto') {
            option26 = option.texto13;
        }
        option27 = option.option27;
        if (option.option28 == 'direito foi(ram) encontrada(s)') {
            option28 = option.option28 + ' ' + option.texto14 + ' estrutura(s) nodular(es), medindo a maior  ' + option.texto15 + ' cm';
        }
        if (option.option29 == 'e do esquerdo') {
            option29 = option.option29 + ' ' + option.texto16 + ' estrutura(s) nodular(es), medindo a maior  ' + option.texto17 + ' cm';
        }
        option30 = option.option30;
        if (option.option31 == 'direito foi(ram) encontrada(s)') {
            option31 = option.option31 + ' ' + option.texto18 + ' estrutura(s) nodular(es), medindo a maior  ' + option.texto19 + ' cm';
        }
        if (option.option32 == 'e do esquerdo') {
            option32 = option.option32 + ' ' + option.texto20 + ' estrutura(s) nodular(es), medindo a maior  ' + option.texto21 + ' cm';
        }
        option33 = option.option33;
        if (option.option34 == 'direito foi(ram) encontrada(s)') {
            option34 = option.option34 + ' ' + option.texto22 + ' estrutura(s) nodular(es), medindo a maior  ' + option.texto23 + ' cm';
        }
        if (option.option35 == 'e do esquerdo') {
            option35 = option.option34 + ' ' + option.texto24 + ' estrutura(s) nodular(es), medindo a maior  ' + option.texto25 + ' cm';
        }
        pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_3___default.a.vfs = pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_4___default.a.pdfMake.vfs;
        var docDefinition = {
            content: [
                {
                    columns: [
                        [
                            { text: 'Vulvectomia', style: 'header' },
                            { text: '', style: 'sub_header' },
                            { text: 'Material recebido ' + option1 + ' que consiste de vulva ' + option2 + ' ' + option3 + ' ' + option4 + ' ' + option5 + ' ' + option6 + ' ' + option7 + ' ' + option8 + ' ' + option9 + ' ' + option10 + ' ' +
                                    'de cor ' + option.texto01 + ' com ' + option.texto02 + ' x ' + option.texto03 + ' x ' + option.texto04 + ' cm. ' +
                                    'Na superfície nota-se lesão ' + option11 + ' ' + option12 + ' localizada em ' + option13 + ' ' + option14 + ' ' + option15 + ' ' + option16 + ' ' + option17 + ' ' + option18 + ' ' +
                                    'medindo ' + option.texto07 + ' x ' + option.texto08 + ' cm e ' + option.texto09 + ' de profundidade máxima. A lesão dista ' + option.texto10 + ' cm da margem ' + option.texto11 + ', a mais próxima da resseção cirúrgica. ' +
                                    'O parânquima adjacente apresenta-se ' + option22 + ' ' + option23 + ' ' + option24 + ' ' + option25 + ' ' + option26 + '. ' + option27 + ' ' + option28 + ' ' + option29 + '. ' +
                                    '' + option30 + ' ' + option31 + ' ' + option32 + ' ' + option33 + ' ' + option34 + ' ' + option35 + '. ' +
                                    'Fragmentos representativos são submetidos a exame histológico. Legenda: ' + option.texto26 + '.', style: 'text' },
                        ]
                    ]
                }
            ],
            styles: {
                header: {
                    bold: true,
                    fontSize: 20,
                    alignment: 'center'
                },
                sub_header: {
                    fontSize: 18,
                    alignment: 'center'
                },
                text: {
                    fontSize: 16,
                    margin: [0, 30, 0, 0],
                    alignment: 'left'
                }
            },
            pageSize: 'A4',
            pageOrientation: 'portrait'
        };
        pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_3___default.a.createPdf(docDefinition).open();
    };
    VulvectomiaPage.prototype.toggleDetails = function () {
        if (document.getElementById("formDoc").style.display == 'none') {
            document.getElementById("formDoc").style.display = "block";
        }
        else {
            document.getElementById("formDoc").style.display = "none";
        }
    };
    VulvectomiaPage.prototype.opt1 = function (event) {
        //this.option1 = event.target.value;
        console.log(event.target.value);
    };
    VulvectomiaPage.prototype.apMassa = function () {
        console.log('entrou aqui');
        if (document.getElementById("total").style.display == 'block') {
            document.getElementById("total").style.display = "none";
        }
        else {
            document.getElementById("total").style.display = "block";
        }
    };
    VulvectomiaPage.prototype.apMassa2 = function () {
        console.log('entrou aqui');
        if (document.getElementById("total2").style.display == 'block') {
            document.getElementById("total2").style.display = "none";
        }
        else {
            document.getElementById("total2").style.display = "block";
        }
    };
    VulvectomiaPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-vulvectomia',
            template: __webpack_require__(/*! ./vulvectomia.page.html */ "./src/app/pages/vulvectomia/vulvectomia.page.html"),
            styles: [__webpack_require__(/*! ./vulvectomia.page.scss */ "./src/app/pages/vulvectomia/vulvectomia.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"]])
    ], VulvectomiaPage);
    return VulvectomiaPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-vulvectomia-vulvectomia-page-module.js.map