(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-results-home-results-module"],{

/***/ "./src/app/components/popmenu/popmenu.component.html":
/*!***********************************************************!*\
  !*** ./src/app/components/popmenu/popmenu.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-fab vertical=\"bottom\" horizontal=\"center\" slot=\"fixed\" class=\"animated fadeInDown\">\r\n  <ion-fab-button (click)=\"togglePopupMenu()\">\r\n    <ion-ripple-effect></ion-ripple-effect>\r\n    <ion-icon name=\"apps\"></ion-icon>\r\n  </ion-fab-button>\r\n</ion-fab>\r\n\r\n<div class=\"popup-menu\">\r\n  <div class=\"popup-menu-overlay\" [ngClass]=\"{'in': openMenu}\"></div>\r\n  <div class=\"popup-menu-panel\" [ngClass]=\"{'in': openMenu}\">\r\n    <div class=\"popup-menu-item\">\r\n      <ion-icon name=\"cog\" slot=\"middle\" size=\"large\"></ion-icon>\r\n      <span>Config</span>\r\n    </div>\r\n    <div class=\"popup-menu-item\">\r\n      <ion-icon name=\"beer\" slot=\"middle\" size=\"large\"></ion-icon>\r\n      <span>Activities</span>\r\n    </div>\r\n    <div class=\"popup-menu-item\">\r\n      <ion-icon name=\"person\" slot=\"middle\" size=\"large\"></ion-icon>\r\n      <span>Settings</span>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/components/popmenu/popmenu.component.scss":
/*!***********************************************************!*\
  !*** ./src/app/components/popmenu/popmenu.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".popup-menu-overlay {\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  z-index: 100;\n  opacity: 0;\n  visibility: hidden;\n  transition: all 0.15s ease-in-out;\n  background-image: linear-gradient(rgba(79, 36, 172, 0.85) 0%, rgba(79, 36, 172, 0.65) 100%); }\n  .popup-menu-overlay.in {\n    opacity: 1;\n    visibility: visible; }\n  .popup-menu-toggle {\n  position: fixed;\n  width: 40px;\n  height: 40px;\n  bottom: 10px;\n  left: 50%;\n  margin-left: -20px;\n  background-color: var(--ion-color-primary);\n  border-radius: 50%;\n  z-index: 101;\n  transition: all .25s ease-in-out; }\n  .popup-menu-toggle.out {\n    opacity: 0;\n    visibility: hidden;\n    transform: scale(0);\n    transition: all .15s ease-in-out; }\n  .popup-menu-toggle.out:before {\n      transition: all .15s ease-in-out;\n      transform: scale(0); }\n  .popup-menu-panel {\n  position: fixed;\n  width: 300px;\n  border-radius: 5%;\n  bottom: 80px;\n  left: 50%;\n  margin-left: -150px;\n  padding: 20px;\n  background-color: var(--ion-color-primary);\n  z-index: 102;\n  transition: all .25s ease-in-out;\n  transition-delay: .15s;\n  transform-origin: 50% 100%;\n  transform: scale(0);\n  display: -moz-flex;\n  display: flex;\n  flex-wrap: wrap; }\n  .popup-menu-panel .popup-menu-item {\n    margin: auto;\n    -moz-flex: 1 0 30%;\n    flex: 1 0 30%;\n    display: -moz-flex;\n    display: flex;\n    -moz-flex-direction: column;\n    flex-direction: column;\n    transform: scale(0);\n    opacity: 0;\n    transition: all .25s ease-in-out; }\n  .popup-menu-panel .popup-menu-item ion-icon {\n      margin: 0 auto;\n      text-align: center;\n      color: #fff; }\n  .popup-menu-panel .popup-menu-item span {\n      padding: 0;\n      margin: 0 0 auto 0;\n      color: #fff;\n      text-align: center;\n      font-size: 12px;\n      text-transform: uppercase;\n      font-weight: 500;\n      line-height: 18px; }\n  .popup-menu-panel .popup-menu-item:active i {\n      color: #dd4135;\n      transition: all 0.15s; }\n  .popup-menu-panel .popup-menu-item:active span {\n      color: #dd4135;\n      transition: all .15s; }\n  .popup-menu-panel.in {\n    transform: scale(1);\n    transition-delay: 0s; }\n  .popup-menu-panel.in .popup-menu-item {\n      transform: scale(1);\n      opacity: 1;\n      transition-delay: .15s; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9wb3BtZW51L0M6XFxVc2Vyc1xcamVzc2lcXERvY3VtZW50c1xcYml0YnVja2V0XFx1YWlMZXZhL3NyY1xcYXBwXFxjb21wb25lbnRzXFxwb3BtZW51XFxwb3BtZW51LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0ksZUFBZTtFQUNmLE1BQU07RUFDTixPQUFPO0VBQ1AsUUFBUTtFQUNSLFNBQVM7RUFDVCxZQUFZO0VBQ1osVUFBVTtFQUNWLGtCQUFrQjtFQUVsQixpQ0FBaUM7RUFFakMsMkZBQXFGLEVBQUE7RUFaekY7SUFjUSxVQUFVO0lBQ1YsbUJBQW1CLEVBQUE7RUFJM0I7RUFDSSxlQUFlO0VBQ2YsV0FBVztFQUNYLFlBQVk7RUFDWixZQUFZO0VBQ1osU0FBUztFQUNULGtCQUFrQjtFQUNsQiwwQ0FBMEM7RUFDMUMsa0JBQWtCO0VBQ2xCLFlBQVk7RUFFWixnQ0FBZ0MsRUFBQTtFQVhwQztJQWFRLFVBQVU7SUFDVixrQkFBa0I7SUFFbEIsbUJBQW1CO0lBRW5CLGdDQUFnQyxFQUFBO0VBbEJ4QztNQXFCWSxnQ0FBZ0M7TUFFaEMsbUJBQW1CLEVBQUE7RUFLL0I7RUFDSSxlQUFlO0VBQ2YsWUFBWTtFQUVaLGlCQUFpQjtFQUNqQixZQUFZO0VBQ1osU0FBUztFQUNULG1CQUFtQjtFQUNuQixhQUFhO0VBQ2IsMENBQTBDO0VBQzFDLFlBQVk7RUFFWixnQ0FBZ0M7RUFFaEMsc0JBQXNCO0VBRXRCLDBCQUEwQjtFQUUxQixtQkFBbUI7RUFJbkIsa0JBQWtCO0VBRWxCLGFBQWE7RUFJYixlQUFlLEVBQUE7RUE1Qm5CO0lBOEJRLFlBQVk7SUFJWixrQkFBa0I7SUFFbEIsYUFBYTtJQUliLGtCQUFrQjtJQUVsQixhQUFhO0lBSWIsMkJBQTJCO0lBRTNCLHNCQUFzQjtJQUV0QixtQkFBbUI7SUFDbkIsVUFBVTtJQUVWLGdDQUFnQyxFQUFBO0VBckR4QztNQXVEWSxjQUFjO01BQ2Qsa0JBQWtCO01BQ2xCLFdBQVcsRUFBQTtFQXpEdkI7TUE0RFksVUFBVTtNQUNWLGtCQUFrQjtNQUNsQixXQUFXO01BQ1gsa0JBQWtCO01BQ2xCLGVBQWU7TUFDZix5QkFBeUI7TUFDekIsZ0JBQWdCO01BQ2hCLGlCQUFpQixFQUFBO0VBbkU3QjtNQXVFZ0IsY0FBcUI7TUFFckIscUJBQXFCLEVBQUE7RUF6RXJDO01BNEVnQixjQUFxQjtNQUVyQixvQkFBb0IsRUFBQTtFQTlFcEM7SUFvRlEsbUJBQW1CO0lBRW5CLG9CQUFvQixFQUFBO0VBdEY1QjtNQXlGWSxtQkFBbUI7TUFDbkIsVUFBVTtNQUVWLHNCQUFzQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9wb3BtZW51L3BvcG1lbnUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBQb3B1cCBNZW51IC8vXHJcbi5wb3B1cC1tZW51LW92ZXJsYXkge1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgdG9wOiAwO1xyXG4gICAgbGVmdDogMDtcclxuICAgIHJpZ2h0OiAwO1xyXG4gICAgYm90dG9tOiAwO1xyXG4gICAgei1pbmRleDogMTAwO1xyXG4gICAgb3BhY2l0eTogMDtcclxuICAgIHZpc2liaWxpdHk6IGhpZGRlbjtcclxuICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIDAuMTVzIGVhc2UtaW4tb3V0O1xyXG4gICAgdHJhbnNpdGlvbjogYWxsIDAuMTVzIGVhc2UtaW4tb3V0O1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQocmdiYSg3OSwzNiwxNzIsIC44NSkgMCUsIHJnYmEoNzksMzYsMTcyLCAuNjUpIDEwMCUpO1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHJnYmEoNzksMzYsMTcyLCAuODUpIDAlLCByZ2JhKDc5LDM2LDE3MiwgLjY1KSAxMDAlKTtcclxuICAgICYuaW4ge1xyXG4gICAgICAgIG9wYWNpdHk6IDE7XHJcbiAgICAgICAgdmlzaWJpbGl0eTogdmlzaWJsZTtcclxuICAgIH1cclxufVxyXG5cclxuLnBvcHVwLW1lbnUtdG9nZ2xlIHtcclxuICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgIHdpZHRoOiA0MHB4O1xyXG4gICAgaGVpZ2h0OiA0MHB4O1xyXG4gICAgYm90dG9tOiAxMHB4O1xyXG4gICAgbGVmdDogNTAlO1xyXG4gICAgbWFyZ2luLWxlZnQ6IC0yMHB4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgei1pbmRleDogMTAxO1xyXG4gICAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgLjI1cyBlYXNlLWluLW91dDtcclxuICAgIHRyYW5zaXRpb246IGFsbCAuMjVzIGVhc2UtaW4tb3V0O1xyXG4gICAgJi5vdXQge1xyXG4gICAgICAgIG9wYWNpdHk6IDA7XHJcbiAgICAgICAgdmlzaWJpbGl0eTogaGlkZGVuO1xyXG4gICAgICAgIC13ZWJraXQtdHJhbnNmb3JtOiBzY2FsZSgwKTtcclxuICAgICAgICB0cmFuc2Zvcm06IHNjYWxlKDApO1xyXG4gICAgICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIC4xNXMgZWFzZS1pbi1vdXQ7XHJcbiAgICAgICAgdHJhbnNpdGlvbjogYWxsIC4xNXMgZWFzZS1pbi1vdXQ7XHJcbiAgICAgICAgJjpiZWZvcmUge1xyXG4gICAgICAgICAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAuMTVzIGVhc2UtaW4tb3V0O1xyXG4gICAgICAgICAgICB0cmFuc2l0aW9uOiBhbGwgLjE1cyBlYXNlLWluLW91dDtcclxuICAgICAgICAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDApO1xyXG4gICAgICAgICAgICB0cmFuc2Zvcm06IHNjYWxlKDApO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuLnBvcHVwLW1lbnUtcGFuZWwge1xyXG4gICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgd2lkdGg6IDMwMHB4O1xyXG5cclxuICAgIGJvcmRlci1yYWRpdXM6IDUlO1xyXG4gICAgYm90dG9tOiA4MHB4O1xyXG4gICAgbGVmdDogNTAlO1xyXG4gICAgbWFyZ2luLWxlZnQ6IC0xNTBweDtcclxuICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XHJcbiAgICB6LWluZGV4OiAxMDI7XHJcbiAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAuMjVzIGVhc2UtaW4tb3V0O1xyXG4gICAgdHJhbnNpdGlvbjogYWxsIC4yNXMgZWFzZS1pbi1vdXQ7XHJcbiAgICAtd2Via2l0LXRyYW5zaXRpb24tZGVsYXk6IC4xNXM7XHJcbiAgICB0cmFuc2l0aW9uLWRlbGF5OiAuMTVzO1xyXG4gICAgLXdlYmtpdC10cmFuc2Zvcm0tb3JpZ2luOiA1MCUgMTAwJTtcclxuICAgIHRyYW5zZm9ybS1vcmlnaW46IDUwJSAxMDAlO1xyXG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDApO1xyXG4gICAgdHJhbnNmb3JtOiBzY2FsZSgwKTtcclxuICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xyXG4gICAgZGlzcGxheTogLW1vei1ib3g7XHJcbiAgICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XHJcbiAgICBkaXNwbGF5OiAtbW96LWZsZXg7XHJcbiAgICBkaXNwbGF5OiAtbXMtZmxleGJveDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAtd2Via2l0LWZsZXgtd3JhcDogd3JhcDtcclxuICAgIC1tb3otZmxleC13cmFwOiB3cmFwO1xyXG4gICAgLW1zLWZsZXgtd3JhcDogd3JhcDtcclxuICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICAgIC5wb3B1cC1tZW51LWl0ZW0ge1xyXG4gICAgICAgIG1hcmdpbjogYXV0bztcclxuICAgICAgICAtd2Via2l0LWJveC1mbGV4OiAxIDAgMzAlO1xyXG4gICAgICAgIC13ZWJraXQtZmxleDogMSAwIDMwJTtcclxuICAgICAgICAtbW96LWJveC1mbGV4OiAxIDAgMzAlO1xyXG4gICAgICAgIC1tb3otZmxleDogMSAwIDMwJTtcclxuICAgICAgICAtbXMtZmxleDogMSAwIDMwJTtcclxuICAgICAgICBmbGV4OiAxIDAgMzAlO1xyXG4gICAgICAgIGRpc3BsYXk6IC13ZWJraXQtYm94O1xyXG4gICAgICAgIGRpc3BsYXk6IC1tb3otYm94O1xyXG4gICAgICAgIGRpc3BsYXk6IC13ZWJraXQtZmxleDtcclxuICAgICAgICBkaXNwbGF5OiAtbW96LWZsZXg7XHJcbiAgICAgICAgZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICAtd2Via2l0LWJveC1kaXJlY3Rpb246IG5vcm1hbDtcclxuICAgICAgICAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xyXG4gICAgICAgIC13ZWJraXQtZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICAtbW96LWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAgLW1zLWZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgICAgICAtd2Via2l0LXRyYW5zZm9ybTogc2NhbGUoMCk7XHJcbiAgICAgICAgdHJhbnNmb3JtOiBzY2FsZSgwKTtcclxuICAgICAgICBvcGFjaXR5OiAwO1xyXG4gICAgICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIC4yNXMgZWFzZS1pbi1vdXQ7XHJcbiAgICAgICAgdHJhbnNpdGlvbjogYWxsIC4yNXMgZWFzZS1pbi1vdXQ7XHJcbiAgICAgICAgaW9uLWljb24ge1xyXG4gICAgICAgICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgICAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICB9XHJcbiAgICAgICAgc3BhbiB7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDA7XHJcbiAgICAgICAgICAgIG1hcmdpbjogMCAwIGF1dG8gMDtcclxuICAgICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgICAgICBmb250LXdlaWdodDogNTAwO1xyXG4gICAgICAgICAgICBsaW5lLWhlaWdodDogMThweDtcclxuICAgICAgICB9XHJcbiAgICAgICAgJjphY3RpdmUge1xyXG4gICAgICAgICAgICBpIHtcclxuICAgICAgICAgICAgICAgIGNvbG9yOiByZ2IoMjIxLDY1LDUzKTtcclxuICAgICAgICAgICAgICAgIC13ZWJraXQtdHJhbnNpdGlvbjogYWxsIC4xNXM7XHJcbiAgICAgICAgICAgICAgICB0cmFuc2l0aW9uOiBhbGwgMC4xNXM7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgc3BhbiB7XHJcbiAgICAgICAgICAgICAgICBjb2xvcjogcmdiKDIyMSw2NSw1Myk7XHJcbiAgICAgICAgICAgICAgICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAuMTVzO1xyXG4gICAgICAgICAgICAgICAgdHJhbnNpdGlvbjogYWxsIC4xNXM7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAmLmluIHtcclxuICAgICAgICAtd2Via2l0LXRyYW5zZm9ybTogc2NhbGUoMSk7XHJcbiAgICAgICAgdHJhbnNmb3JtOiBzY2FsZSgxKTtcclxuICAgICAgICAtd2Via2l0LXRyYW5zaXRpb24tZGVsYXk6IDBzO1xyXG4gICAgICAgIHRyYW5zaXRpb24tZGVsYXk6IDBzO1xyXG4gICAgICAgIC5wb3B1cC1tZW51LWl0ZW0ge1xyXG4gICAgICAgICAgICAtd2Via2l0LXRyYW5zZm9ybTogc2NhbGUoMSk7XHJcbiAgICAgICAgICAgIHRyYW5zZm9ybTogc2NhbGUoMSk7XHJcbiAgICAgICAgICAgIG9wYWNpdHk6IDE7XHJcbiAgICAgICAgICAgIC13ZWJraXQtdHJhbnNpdGlvbi1kZWxheTogLjE1cztcclxuICAgICAgICAgICAgdHJhbnNpdGlvbi1kZWxheTogLjE1cztcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/components/popmenu/popmenu.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/components/popmenu/popmenu.component.ts ***!
  \*********************************************************/
/*! exports provided: PopmenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopmenuComponent", function() { return PopmenuComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PopmenuComponent = /** @class */ (function () {
    function PopmenuComponent(navCtrl) {
        this.navCtrl = navCtrl;
        this.openMenu = false;
    }
    PopmenuComponent.prototype.ngOnInit = function () {
    };
    PopmenuComponent.prototype.togglePopupMenu = function () {
        return this.openMenu = !this.openMenu;
    };
    PopmenuComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'popmenu',
            template: __webpack_require__(/*! ./popmenu.component.html */ "./src/app/components/popmenu/popmenu.component.html"),
            styles: [__webpack_require__(/*! ./popmenu.component.scss */ "./src/app/components/popmenu/popmenu.component.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"]])
    ], PopmenuComponent);
    return PopmenuComponent;
}());



/***/ }),

/***/ "./src/app/pages/home-results/home-results.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/home-results/home-results.module.ts ***!
  \***********************************************************/
/*! exports provided: HomeResultsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeResultsPageModule", function() { return HomeResultsPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _components_popmenu_popmenu_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../components/popmenu/popmenu.component */ "./src/app/components/popmenu/popmenu.component.ts");
/* harmony import */ var _home_results_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-results.page */ "./src/app/pages/home-results/home-results.page.ts");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "./node_modules/@ionic-native/splash-screen/ngx/index.js");
/* harmony import */ var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/google-maps */ "./node_modules/@ionic-native/google-maps/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var routes = [
    {
        path: '',
        component: _home_results_page__WEBPACK_IMPORTED_MODULE_6__["HomeResultsPage"]
    }
];
var HomeResultsPageModule = /** @class */ (function () {
    function HomeResultsPageModule() {
    }
    HomeResultsPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
            ],
            declarations: [_home_results_page__WEBPACK_IMPORTED_MODULE_6__["HomeResultsPage"], _components_popmenu_popmenu_component__WEBPACK_IMPORTED_MODULE_5__["PopmenuComponent"]],
            providers: [
                _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_7__["SplashScreen"],
                { provide: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ErrorHandler"] },
                _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_8__["GoogleMaps"]
            ]
        })
    ], HomeResultsPageModule);
    return HomeResultsPageModule;
}());



/***/ }),

/***/ "./src/app/pages/home-results/home-results.page.html":
/*!***********************************************************!*\
  !*** ./src/app/pages/home-results/home-results.page.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n <ion-header>\r\n    <nav class=\"navbar navbar-expand-lg navbar-light bg-dark\">\r\n        <a class=\"navbar-brand\" href=\"#\">\r\n        <img src=\"assets/img/logo-uaileva.png\" />\r\n        </a>\r\n        <ion-menu-button>\r\n            <button class=\"navbar-toggler\" >\r\n                <span class=\"navbar-toggler-icon\"></span>\r\n            </button>\r\n        </ion-menu-button>\r\n    </nav>\r\n</ion-header>\r\n<ion-content>\r\n    <div #map>\r\n        <ion-button class=\"ion-margin-top\" (click)=\"back()\" color=\"dark\" fill=\"clear\" size=\"small\" [hidden]=\"!destination\">\r\n            <ion-icon slot=\"icon-only\" name= \"arrow-back\"></ion-icon>\r\n        </ion-button>\r\n        <br>\r\n        <div class=\"boxConteudo\" [hidden]=\"destination\">\r\n            <div class=\"saudacao\" *ngIf=\"(user$ | async) as user;else login\">\r\n                Olá <b> {{phone}} !</b>\r\n                <br><br>\r\n            </div>\r\n            <button type=\"button\" class=\"btn btn-primary btn-lg rounded-pill escolherDestino\" (click)=\"toggleTeste();\">\r\n                ESCOLHER DESTINO\r\n            </button>\r\n            <button type=\"button\" class=\"btn btn-primary btn-lg rounded-pill embarqueRapido\" (click) = \"embarqueRapido();\">\r\n                <svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" fill=\"currentColor\" id=\"Capa_1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 401.994 401.994\" style=\"enable-background:new 0 0 401.994 401.994;\" xml:space=\"preserve\">\r\n                <g>\r\n                    <g>\r\n                        <path d=\"M0,401.991h182.724V219.265H0V401.991z M36.542,255.813h109.636v109.352H36.542V255.813z\"/>\r\n                        <rect x=\"73.089\" y=\"292.355\" width=\"36.544\" height=\"36.549\"/>\r\n                        <rect x=\"292.352\" y=\"365.449\" width=\"36.553\" height=\"36.545\"/>\r\n                        <rect x=\"365.442\" y=\"365.449\" width=\"36.552\" height=\"36.545\"/>\r\n                        <polygon points=\"365.446,255.813 328.904,255.813 328.904,219.265 219.265,219.265 219.265,401.991 255.813,401.991     255.813,292.355 292.352,292.355 292.352,328.904 401.991,328.904 401.991,219.265 401.991,219.265 365.446,219.265   \"/>\r\n                        <path d=\"M0,182.728h182.724V0H0V182.728z M36.542,36.542h109.636v109.636H36.542V36.542z\"/>\r\n                        <rect x=\"73.089\" y=\"73.089\" width=\"36.544\" height=\"36.547\"/>\r\n                        <path d=\"M219.265,0v182.728h182.729V0H219.265z M365.446,146.178H255.813V36.542h109.633V146.178z\"/>\r\n                        <rect x=\"292.352\" y=\"73.089\" width=\"36.553\" height=\"36.547\"/>\r\n                    </g>\r\n                </g>\r\n            </svg>\r\n            </button>\r\n\r\n            <div class=\"clearfix\"></div>\r\n\r\n            <div class=\"collapse\" id=\"collapseExample\" name=\"collapseExample\" style=\"display:none\">\r\n                  \r\n                  <ion-searchbar [(ngModel)] = \"search2\" (ionChange) = \"searchChanged2()\" placeholder=\"localização atual\" ></ion-searchbar>\r\n                   \r\n                  <ion-searchbar [(ngModel)] = \"search\" (ionChange) = \"searchChanged()\" placeholder=\"Onde cê vai ?\" ></ion-searchbar>\r\n                    <ion-list class =\"ion-margin-horizontal\" [hidden]=\"!search.length\">\r\n                        <ion-item (click) = \"calcRoute(result)\" *ngFor= \"let result of searchResults\">\r\n                        {{result.description}}\r\n                        </ion-item>\r\n                    </ion-list>\r\n                \r\n                    <div>\r\n                        Destinos Recentes\r\n                    </div>\r\n                    <div>\r\n                        <div class=\"iconeDestinoRecente\">\r\n\r\n                        </div>\r\n                        <div class=\"endDestinoRecente\">\r\n                            <div class=\"tituloDestinoRecente\">Avenida Antônio Junqueira de Souza</div>\r\n                            <b>Av. Avenida Antônio Junqueira de Souza, 615, São Louren..</b>\r\n                        </div>\r\n\r\n                    </div>\r\n                    \r\n            </div>\r\n\r\n            <div class=\"collapse\" id=\"collapseExample1\" name=\"collapseExample\" style=\"display:none\">\r\n                  \r\n                <h1>Cadastrar cartão de crédito</h1>\r\n                  \r\n            </div>\r\n            \r\n        </div>\r\n    \r\n        <div class=\"pagamentos\" [hidden]=\"!destination\">\r\n            <h1>formas de pagamento</h1>\r\n            <p>{{corrida}}</p>\r\n            <p>{{tempoEstimadoCorrida}}</p>\r\n            <p>R${{valorCorrida}}</p>\r\n        </div>\r\n    \r\n    </div>\r\n  </ion-content>\r\n  "

/***/ }),

/***/ "./src/app/pages/home-results/home-results.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/home-results/home-results.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n@import url(\"https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,400;0,500;1,400;1,500&display=swap\");\n@charset \"UTF-8\";\n@media (max-width: 992px) {\n  .navbar-collapse {\n    position: fixed;\n    top: 0px;\n    left: 0;\n    padding-left: 15px;\n    padding-right: 15px;\n    padding-bottom: 15px;\n    width: 75%;\n    height: 100%;\n    background: #1765e2;\n    text-transform: uppercase;\n    z-index: 9999;\n    box-shadow: 0px 0px 90px black; }\n  .navbar-collapse a.nav-link {\n    color: white !important;\n    font-weight: bold;\n    font-size: 19px; }\n  a.nav-link svg {\n    font-size: 25px;\n    margin-right: 10px; }\n  .navbar-dark .navbar-toggler {\n    background: #1b2c7b; }\n  .navbar-dark span.navbar-toggler-icon {\n    padding: 15px; }\n  .navbar-collapse.collapsing {\n    left: -75%;\n    transition: height 0s ease; }\n  .navbar-collapse.show {\n    margin-top: 0px;\n    left: 0;\n    transition: left 300ms ease-in-out; }\n  .navbar-toggler.collapsed ~ .navbar-collapse {\n    transition: left 500ms ease-in-out; } }\nbody {\n  background: url('bg-uai-leva.png') no-repeat #eaeaea;\n  font-family: 'Rubik', sans-serif;\n  background-position: 80px bottom;\n  background-size: contain;\n  height: 100vh; }\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  text-transform: uppercase;\n  margin: 20px 0;\n  font-weight: bold;\n  color: #123b7d; }\n.navbar.bg-dark {\n  background: #1c72fd !important; }\n.boxConteudo {\n  background: white;\n  padding: 10px;\n  border-radius: 20px;\n  box-shadow: 0px 0px 10px #e4e4e4;\n  position: absolute !important;\n  display: block;\n  margin-left: 13px;\n  margin-right: auto;\n  z-index: 9999 !important;\n  /* número máximo é 9999 */\n  width: calc(100% - 25px) !important; }\n.pagamentos {\n  background: white;\n  padding: 10px;\n  border-radius: 20px;\n  box-shadow: 0px 0px 10px #e4e4e4;\n  position: fixed !important;\n  z-index: 9999 !important;\n  /* número máximo é 9999 */\n  width: 100%;\n  height: 224px;\n  margin-top: 120%; }\nbutton.btn.btn-primary.btn-lg.rounded-pill.escolherDestino {\n  font-size: 15px; }\n.boxConteudo label {\n  text-transform: uppercase;\n  font-size: 12px; }\n.boxConteudo .form-control {\n  height: calc(1.5em + .75rem + 4px); }\n/*.buttonArea {\r\n    margin: 20px -20px 0px -20px;\r\n    background: #101a4c;\r\n    width: 100%;\r\n    display: block;\r\n    position: absolute;\r\n    z-index: 0;\r\n    bottom: -50px;\r\n    padding: 10px;\r\n    border-radius: 0 0 20px 20px;\r\n    }*/\n.buttonArea button {\n  width: 100%; }\n.buttonArea svg {\n  font-size: 25px;\n  margin-right: 5px; }\nbutton.navbar-toggler {\n  border-radius: 100px !important;\n  height: 55px !important;\n  background: #ffffff;\n  border: #92f328 solid 0px;\n  outline: none; }\n.boxConteudo .btn {\n  font-weight: bold; }\n.mapa iframe {\n  border: 0;\n  width: 100%;\n  height: 100vh;\n  position: relative;\n  top: 0px;\n  z-index: -1; }\n.saudacao {\n  text-align: center; }\n.homePassageiro .boxConteudo {\n  position: absolute !important;\n  top: 30px;\n  width: calc(100% - 0px);\n  margin: 0 auto;\n  padding: 15px 5px; }\n.homePassageiro .buttonArea button.rounded-pill {\n  font-size: 15px; }\nbutton.escolherDestino {\n  width: calc(100% - 60px) !important;\n  float: left; }\nbutton.embarqueRapido {\n  float: right;\n  padding: 0px 12px;\n  width: 45px !important;\n  height: 45px; }\nbutton.embarqueRapido svg {\n  width: 19px;\n  color: #416313; }\nbutton.btn.btn-primary.btn-lg.rounded-pill.embarqueRapido {\n  background: #b1ff49;\n  border: none; }\n.formDestino {\n  border: none;\n  margin-top: 5px; }\n.endDestinoRecente {\n  font-size: 11px;\n  background: #eee;\n  padding: 14px;\n  border-radius: 5px; }\n.endDestinoRecente b {\n  font-weight: normal;\n  color: darkgray; }\n.tituloDestinoRecente {\n  text-transform: uppercase;\n  font-weight: bold; }\nion-content {\n  --background: var(--ion-color-light); }\nion-menu-button {\n  background-color: white;\n  border-radius: 100px;\n  width: 50px;\n  height: 50px; }\n.navbar-light .navbar-toggler-icon {\n  padding: 15px;\n  background-color: white !important;\n  border-radius: 100px; }\nion-item {\n  border-radius: 0;\n  border-bottom: 1px dotted var(--ion-color-medium); }\nion-card.no-radius {\n  border-radius: 0; }\n#map {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  background: #eee; }\n.gmnoprint {\n  display: none !important; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS1yZXN1bHRzL0M6XFxVc2Vyc1xcamVzc2lcXERvY3VtZW50c1xcYml0YnVja2V0XFx1YWlMZXZhL3NyY1xcYXBwXFxwYWdlc1xcaG9tZS1yZXN1bHRzXFxob21lLXJlc3VsdHMucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9ob21lLXJlc3VsdHMvaG9tZS1yZXN1bHRzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsNEdBQVk7QUNBWixnQkFBZ0I7QURHaEI7RUFFSTtJQUNJLGVBQWU7SUFDZixRQUFRO0lBQ1IsT0FBTztJQUNQLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsb0JBQW9CO0lBQ3BCLFVBQVU7SUFDVixZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLHlCQUF5QjtJQUN6QixhQUFhO0lBQ2IsOEJBQThCLEVBQUE7RUFHbEM7SUFDQSx1QkFBdUI7SUFDdkIsaUJBQWlCO0lBQ2pCLGVBQWUsRUFBQTtFQUdmO0lBQ0EsZUFBZTtJQUNmLGtCQUFrQixFQUFBO0VBR2xCO0lBQ0ksbUJBQW1CLEVBQUE7RUFHdkI7SUFDSSxhQUFhLEVBQUE7RUFLakI7SUFDSSxVQUFVO0lBQ1YsMEJBQTBCLEVBQUE7RUFHOUI7SUFDSSxlQUFlO0lBQ2YsT0FBTztJQUNQLGtDQUFrQyxFQUFBO0VBR3RDO0lBQ0ksa0NBQWtDLEVBQUEsRUFDckM7QUFJRjtFQUNDLG9EQUFtRTtFQUNuRSxnQ0FBZ0M7RUFDaEMsZ0NBQWdDO0VBQ2hDLHdCQUF3QjtFQUN4QixhQUFhLEVBQUE7QUFHYjtFQUNBLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIseUJBQXlCO0VBQ3pCLGNBQWM7RUFDZCxpQkFBaUI7RUFDakIsY0FBYyxFQUFBO0FBR2Q7RUFDSSw4QkFBNEIsRUFBQTtBQUdoQztFQUNJLGlCQUFpQjtFQUNqQixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLGdDQUFnQztFQUNoQyw2QkFBNkI7RUFFN0IsY0FBYztFQUNkLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIsd0JBQXdCO0VBQUUseUJBQUE7RUFDMUIsbUNBQW1DLEVBQUE7QUFHdkM7RUFDSSxpQkFBaUI7RUFDakIsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixnQ0FBZ0M7RUFDaEMsMEJBQTBCO0VBQzFCLHdCQUF3QjtFQUFFLHlCQUFBO0VBQzFCLFdBQVc7RUFDWCxhQUFhO0VBQ2IsZ0JBQWdCLEVBQUE7QUFJcEI7RUFDSSxlQUFlLEVBQUE7QUFFbkI7RUFDSSx5QkFBeUI7RUFDekIsZUFBZSxFQUFBO0FBR25CO0VBQ0ksa0NBQWtDLEVBQUE7QUFHdEM7Ozs7Ozs7Ozs7TUNsQkU7QUQ4QkY7RUFFSSxXQUFXLEVBQUE7QUFLZjtFQUNBLGVBQWU7RUFDZixpQkFBaUIsRUFBQTtBQUdqQjtFQUNBLCtCQUErQjtFQUMvQix1QkFBdUI7RUFDdkIsbUJBQW1CO0VBQ25CLHlCQUF5QjtFQUN6QixhQUFhLEVBQUE7QUFHYjtFQUNBLGlCQUFpQixFQUFBO0FBR2pCO0VBQ0ksU0FBUztFQUNULFdBQVc7RUFDWCxhQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixXQUFXLEVBQUE7QUFHZjtFQUNJLGtCQUFrQixFQUFBO0FBR3RCO0VBQ0ksNkJBQTZCO0VBQzdCLFNBQVM7RUFDVCx1QkFBdUI7RUFDdkIsY0FBYztFQUNkLGlCQUFpQixFQUFBO0FBR3JCO0VBQ0ksZUFBZSxFQUFBO0FBR25CO0VBQ0ksbUNBQW1DO0VBQ25DLFdBQVcsRUFBQTtBQUdmO0VBQ0ksWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixzQkFBc0I7RUFDdEIsWUFBWSxFQUFBO0FBR2hCO0VBQ0ksV0FBVztFQUNYLGNBQWMsRUFBQTtBQUdsQjtFQUNLLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7QUFHakI7RUFDSSxZQUFZO0VBQ1osZUFBZSxFQUFBO0FBR25CO0VBQ0ksZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2Isa0JBQWtCLEVBQUE7QUFHdEI7RUFDSSxtQkFBbUI7RUFDbkIsZUFBZSxFQUFBO0FBR25CO0VBQ0kseUJBQXlCO0VBQ3pCLGlCQUFpQixFQUFBO0FBSXJCO0VBQ0ksb0NBQWEsRUFBQTtBQUdqQjtFQUNJLHVCQUF1QjtFQUN2QixvQkFBb0I7RUFDcEIsV0FBVztFQUNYLFlBQVksRUFBQTtBQUdoQjtFQUNJLGFBQWE7RUFDYixrQ0FBa0M7RUFDbEMsb0JBQW9CLEVBQUE7QUFJeEI7RUFDSSxnQkFBZ0I7RUFDaEIsaURBQWlELEVBQUE7QUFHckQ7RUFFUSxnQkFBZ0IsRUFBQTtBQUt4QjtFQUNJLGtCQUFrQjtFQUNsQixXQUFXO0VBQ1gsWUFBWTtFQUNaLGdCQUFnQixFQUFBO0FBR3BCO0VBQ0ksd0JBQXdCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ob21lLXJlc3VsdHMvaG9tZS1yZXN1bHRzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgdXJsKCdodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2NzczI/ZmFtaWx5PVJ1YmlrOml0YWwsd2dodEAwLDQwMDswLDUwMDsxLDQwMDsxLDUwMCZkaXNwbGF5PXN3YXAnKTtcclxuXHJcblxyXG5AbWVkaWEgKG1heC13aWR0aDogOTkycHgpIHtcclxuXHJcbiAgICAubmF2YmFyLWNvbGxhcHNlIHtcclxuICAgICAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAgICAgdG9wOiAwcHg7XHJcbiAgICAgICAgbGVmdDogMDtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDE1cHg7XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTVweDtcclxuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMTVweDtcclxuICAgICAgICB3aWR0aDogNzUlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMTc2NWUyO1xyXG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICAgICAgei1pbmRleDogOTk5OTtcclxuICAgICAgICBib3gtc2hhZG93OiAwcHggMHB4IDkwcHggYmxhY2s7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5uYXZiYXItY29sbGFwc2UgYS5uYXYtbGluayB7XHJcbiAgICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgZm9udC1zaXplOiAxOXB4O1xyXG4gICAgfVxyXG5cclxuICAgIGEubmF2LWxpbmsgc3ZnIHtcclxuICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLm5hdmJhci1kYXJrIC5uYXZiYXItdG9nZ2xlciB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzFiMmM3YjtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLm5hdmJhci1kYXJrIHNwYW4ubmF2YmFyLXRvZ2dsZXItaWNvbiB7XHJcbiAgICAgICAgcGFkZGluZzogMTVweDtcclxuICAgIC8vYmFja2dyb3VuZC1pbWFnZTogdXJsKGRhdGE6aW1hZ2Uvc3ZnK3htbCwlM2NzdmcgeG1sbnM9J2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJyB3aWR0aD0nMzAnIGhlaWdodD0nMzAnIHZpZXdCb3g9JzAgMCAzMCAzMCclM2UlM2NwYXRoIHN0cm9rZT0ncmdiYSUyODI1NSwgMjU1LCAyNTUsIDAuOSUyOScgc3Ryb2tlLWxpbmVjYXA9J3JvdW5kJyBzdHJva2UtbWl0ZXJsaW1pdD0nMTAnIHN0cm9rZS13aWR0aD0nMicgZD0nTTQgN2gyMk00IDE1aDIyTTQgMjNoMjInLyUzZSUzYy9zdmclM2UpO1xyXG4gICAgfVxyXG5cclxuICAgXHJcbiAgICAubmF2YmFyLWNvbGxhcHNlLmNvbGxhcHNpbmcge1xyXG4gICAgICAgIGxlZnQ6IC03NSU7XHJcbiAgICAgICAgdHJhbnNpdGlvbjogaGVpZ2h0IDBzIGVhc2U7XHJcbiAgICB9XHJcblxyXG4gICAgLm5hdmJhci1jb2xsYXBzZS5zaG93IHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgICAgICAgbGVmdDogMDtcclxuICAgICAgICB0cmFuc2l0aW9uOiBsZWZ0IDMwMG1zIGVhc2UtaW4tb3V0O1xyXG4gICAgfVxyXG5cclxuICAgIC5uYXZiYXItdG9nZ2xlci5jb2xsYXBzZWQgfiAubmF2YmFyLWNvbGxhcHNlIHtcclxuICAgICAgICB0cmFuc2l0aW9uOiBsZWZ0IDUwMG1zIGVhc2UtaW4tb3V0O1xyXG4gICAgfVxyXG4gICAgXHJcbn1cclxuXHJcbiAgIGJvZHkge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXNzZXRzL2ltZy9iZy11YWktbGV2YS5wbmcnKSBuby1yZXBlYXQgI2VhZWFlYTtcclxuICAgIGZvbnQtZmFtaWx5OiAnUnViaWsnLCBzYW5zLXNlcmlmO1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogODBweCBib3R0b207XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICBoZWlnaHQ6IDEwMHZoO1xyXG59XHJcblxyXG4gICAgaDEge1xyXG4gICAgZm9udC1zaXplOiAxLjVyZW07XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgbWFyZ2luOiAyMHB4IDA7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGNvbG9yOiAjMTIzYjdkO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAubmF2YmFyLmJnLWRhcmsge1xyXG4gICAgICAgIGJhY2tncm91bmQ6IzFjNzJmZCFpbXBvcnRhbnRcclxuICAgIH1cclxuXHJcbiAgICAuYm94Q29udGV1ZG8ge1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgICAgICBib3gtc2hhZG93OiAwcHggMHB4IDEwcHggI2U0ZTRlNDtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGUgIWltcG9ydGFudDtcclxuICAgICAgICAvL2Rpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICBkaXNwbGF5OiBibG9jazsgXHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDEzcHg7IFxyXG4gICAgICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgICAgICB6LWluZGV4OiA5OTk5ICFpbXBvcnRhbnQ7IC8qIG7Dum1lcm8gbcOheGltbyDDqSA5OTk5ICovXHJcbiAgICAgICAgd2lkdGg6IGNhbGMoMTAwJSAtIDI1cHgpICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgLnBhZ2FtZW50b3Mge1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgICAgICBib3gtc2hhZG93OiAwcHggMHB4IDEwcHggI2U0ZTRlNDtcclxuICAgICAgICBwb3NpdGlvbjogZml4ZWQgIWltcG9ydGFudDtcclxuICAgICAgICB6LWluZGV4OiA5OTk5ICFpbXBvcnRhbnQ7IC8qIG7Dum1lcm8gbcOheGltbyDDqSA5OTk5ICovXHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OiAyMjRweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxMjAlO1xyXG4gICAgfVxyXG4gICAgXHJcblxyXG4gICAgYnV0dG9uLmJ0bi5idG4tcHJpbWFyeS5idG4tbGcucm91bmRlZC1waWxsLmVzY29saGVyRGVzdGlubyB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgfVxyXG4gICAgLmJveENvbnRldWRvIGxhYmVsIHtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLmJveENvbnRldWRvIC5mb3JtLWNvbnRyb2wge1xyXG4gICAgICAgIGhlaWdodDogY2FsYygxLjVlbSArIC43NXJlbSArIDRweCk7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC8qLmJ1dHRvbkFyZWEge1xyXG4gICAgbWFyZ2luOiAyMHB4IC0yMHB4IDBweCAtMjBweDtcclxuICAgIGJhY2tncm91bmQ6ICMxMDFhNGM7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgei1pbmRleDogMDtcclxuICAgIGJvdHRvbTogLTUwcHg7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMCAwIDIwcHggMjBweDtcclxuICAgIH0qL1xyXG4gICAgXHJcbiAgICAuYnV0dG9uQXJlYSBidXR0b24ge1xyXG4gICAgICAgIC8vd2lkdGg6IDlweDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICAvL3dpZHRoOiBtYXgtY29udGVudDtcclxuICAgICAgICAvL3dpZHRoOiBjYWxjKDEwMCUgLSA2MHB4KSAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAuYnV0dG9uQXJlYSBzdmcge1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIGJ1dHRvbi5uYXZiYXItdG9nZ2xlciB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMDBweCAhaW1wb3J0YW50O1xyXG4gICAgaGVpZ2h0OiA1NXB4ICFpbXBvcnRhbnQ7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xyXG4gICAgYm9yZGVyOiAjOTJmMzI4IHNvbGlkIDBweDtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5ib3hDb250ZXVkbyAuYnRuIHtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgfVxyXG5cclxuICAgIC5tYXBhIGlmcmFtZSB7XHJcbiAgICAgICAgYm9yZGVyOiAwO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGhlaWdodDoxMDB2aDtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgdG9wOiAwcHg7XHJcbiAgICAgICAgei1pbmRleDogLTE7XHJcbiAgICB9XHJcblxyXG4gICAgLnNhdWRhY2FvIHtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgLmhvbWVQYXNzYWdlaXJvIC5ib3hDb250ZXVkbyB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgdG9wOiAzMHB4O1xyXG4gICAgICAgIHdpZHRoOiBjYWxjKDEwMCUgLSAwcHgpO1xyXG4gICAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHggNXB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5ob21lUGFzc2FnZWlybyAuYnV0dG9uQXJlYSBidXR0b24ucm91bmRlZC1waWxsIHtcclxuICAgICAgICBmb250LXNpemU6IDE1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgYnV0dG9uLmVzY29saGVyRGVzdGlubyB7XHJcbiAgICAgICAgd2lkdGg6IGNhbGMoMTAwJSAtIDYwcHgpICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICB9XHJcblxyXG4gICAgYnV0dG9uLmVtYmFycXVlUmFwaWRvIHtcclxuICAgICAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICAgICAgcGFkZGluZzogMHB4IDEycHg7XHJcbiAgICAgICAgd2lkdGg6IDQ1cHggIWltcG9ydGFudDtcclxuICAgICAgICBoZWlnaHQ6IDQ1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgYnV0dG9uLmVtYmFycXVlUmFwaWRvIHN2ZyB7XHJcbiAgICAgICAgd2lkdGg6IDE5cHg7XHJcbiAgICAgICAgY29sb3I6ICM0MTYzMTM7XHJcbiAgICB9XHJcblxyXG4gICAgYnV0dG9uLmJ0bi5idG4tcHJpbWFyeS5idG4tbGcucm91bmRlZC1waWxsLmVtYmFycXVlUmFwaWRvIHtcclxuICAgICAgICAgYmFja2dyb3VuZDogI2IxZmY0OTtcclxuICAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgfVxyXG5cclxuICAgIC5mb3JtRGVzdGlubyB7XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDVweDtcclxuICAgIH1cclxuXHJcbiAgICAuZW5kRGVzdGlub1JlY2VudGUge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTFweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjZWVlO1xyXG4gICAgICAgIHBhZGRpbmc6IDE0cHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5lbmREZXN0aW5vUmVjZW50ZSBiIHtcclxuICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICAgIGNvbG9yOiBkYXJrZ3JheTtcclxuICAgIH1cclxuXHJcbiAgICAudGl0dWxvRGVzdGlub1JlY2VudGUge1xyXG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIGlvbi1jb250ZW50IHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1jb2xvci1saWdodCk7XHJcbiAgICB9XHJcblxyXG4gICAgaW9uLW1lbnUtYnV0dG9ue1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xyXG4gICAgICAgIHdpZHRoOiA1MHB4O1xyXG4gICAgICAgIGhlaWdodDogNTBweDtcclxuICAgIH1cclxuXHJcbiAgICAubmF2YmFyLWxpZ2h0IC5uYXZiYXItdG9nZ2xlci1pY29uIHtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTAwcHg7XHJcbiAgICAgICAgLy9iYWNrZ3JvdW5kLWltYWdlOiB1cmwoZGF0YTppbWFnZS9zdmcreG1sLCUzY3N2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPSczMCcgaGVpZ2h0PSczMCcgdmlld0JveD0nMCAwIDMwIDMwJyUzZSUzY3BhdGggc3Ryb2tlPSdyZ2JhJTI4MCwgMCwgMCwgMC41JTI5JyBzdHJva2UtbGluZWNhcD0ncm91bmQnIHN0cm9rZS1taXRlcmxpbWl0PScxMCcgc3Ryb2tlLXdpZHRoPScyJyBkPSdNNCA3aDIyTTQgMTVoMjJNNCAyM2gyMicvJTNlJTNjL3N2ZyUzZSk7XHJcbiAgICB9XHJcblxyXG4gICAgaW9uLWl0ZW0ge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDA7XHJcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IGRvdHRlZCB2YXIoLS1pb24tY29sb3ItbWVkaXVtKTtcclxuICAgIH1cclxuXHJcbiAgICBpb24tY2FyZCB7XHJcbiAgICAgICAgJi5uby1yYWRpdXMge1xyXG4gICAgICAgICAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgICAgIH1cclxuXHRcdFxyXG4gICAgfVxyXG5cclxuICAgICNtYXAge1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2VlZTtcclxuICAgIH1cclxuXHJcbiAgICAuZ21ub3ByaW50IHtcclxuICAgICAgICBkaXNwbGF5OiBub25lICFpbXBvcnRhbnQ7XHJcbiAgICB9IiwiQGNoYXJzZXQgXCJVVEYtOFwiO1xuQGltcG9ydCB1cmwoXCJodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2NzczI/ZmFtaWx5PVJ1YmlrOml0YWwsd2dodEAwLDQwMDswLDUwMDsxLDQwMDsxLDUwMCZkaXNwbGF5PXN3YXBcIik7XG5AbWVkaWEgKG1heC13aWR0aDogOTkycHgpIHtcbiAgLm5hdmJhci1jb2xsYXBzZSB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIHRvcDogMHB4O1xuICAgIGxlZnQ6IDA7XG4gICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDE1cHg7XG4gICAgcGFkZGluZy1ib3R0b206IDE1cHg7XG4gICAgd2lkdGg6IDc1JTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgYmFja2dyb3VuZDogIzE3NjVlMjtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIHotaW5kZXg6IDk5OTk7XG4gICAgYm94LXNoYWRvdzogMHB4IDBweCA5MHB4IGJsYWNrOyB9XG4gIC5uYXZiYXItY29sbGFwc2UgYS5uYXYtbGluayB7XG4gICAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgZm9udC1zaXplOiAxOXB4OyB9XG4gIGEubmF2LWxpbmsgc3ZnIHtcbiAgICBmb250LXNpemU6IDI1cHg7XG4gICAgbWFyZ2luLXJpZ2h0OiAxMHB4OyB9XG4gIC5uYXZiYXItZGFyayAubmF2YmFyLXRvZ2dsZXIge1xuICAgIGJhY2tncm91bmQ6ICMxYjJjN2I7IH1cbiAgLm5hdmJhci1kYXJrIHNwYW4ubmF2YmFyLXRvZ2dsZXItaWNvbiB7XG4gICAgcGFkZGluZzogMTVweDsgfVxuICAubmF2YmFyLWNvbGxhcHNlLmNvbGxhcHNpbmcge1xuICAgIGxlZnQ6IC03NSU7XG4gICAgdHJhbnNpdGlvbjogaGVpZ2h0IDBzIGVhc2U7IH1cbiAgLm5hdmJhci1jb2xsYXBzZS5zaG93IHtcbiAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgbGVmdDogMDtcbiAgICB0cmFuc2l0aW9uOiBsZWZ0IDMwMG1zIGVhc2UtaW4tb3V0OyB9XG4gIC5uYXZiYXItdG9nZ2xlci5jb2xsYXBzZWQgfiAubmF2YmFyLWNvbGxhcHNlIHtcbiAgICB0cmFuc2l0aW9uOiBsZWZ0IDUwMG1zIGVhc2UtaW4tb3V0OyB9IH1cblxuYm9keSB7XG4gIGJhY2tncm91bmQ6IHVybChcInNyYy9hc3NldHMvaW1nL2JnLXVhaS1sZXZhLnBuZ1wiKSBuby1yZXBlYXQgI2VhZWFlYTtcbiAgZm9udC1mYW1pbHk6ICdSdWJpaycsIHNhbnMtc2VyaWY7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IDgwcHggYm90dG9tO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gIGhlaWdodDogMTAwdmg7IH1cblxuaDEge1xuICBmb250LXNpemU6IDEuNXJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBtYXJnaW46IDIwcHggMDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjMTIzYjdkOyB9XG5cbi5uYXZiYXIuYmctZGFyayB7XG4gIGJhY2tncm91bmQ6ICMxYzcyZmQgIWltcG9ydGFudDsgfVxuXG4uYm94Q29udGV1ZG8ge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcGFkZGluZzogMTBweDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgYm94LXNoYWRvdzogMHB4IDBweCAxMHB4ICNlNGU0ZTQ7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZSAhaW1wb3J0YW50O1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luLWxlZnQ6IDEzcHg7XG4gIG1hcmdpbi1yaWdodDogYXV0bztcbiAgei1pbmRleDogOTk5OSAhaW1wb3J0YW50O1xuICAvKiBuw7ptZXJvIG3DoXhpbW8gw6kgOTk5OSAqL1xuICB3aWR0aDogY2FsYygxMDAlIC0gMjVweCkgIWltcG9ydGFudDsgfVxuXG4ucGFnYW1lbnRvcyB7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICBib3gtc2hhZG93OiAwcHggMHB4IDEwcHggI2U0ZTRlNDtcbiAgcG9zaXRpb246IGZpeGVkICFpbXBvcnRhbnQ7XG4gIHotaW5kZXg6IDk5OTkgIWltcG9ydGFudDtcbiAgLyogbsO6bWVybyBtw6F4aW1vIMOpIDk5OTkgKi9cbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMjI0cHg7XG4gIG1hcmdpbi10b3A6IDEyMCU7IH1cblxuYnV0dG9uLmJ0bi5idG4tcHJpbWFyeS5idG4tbGcucm91bmRlZC1waWxsLmVzY29saGVyRGVzdGlubyB7XG4gIGZvbnQtc2l6ZTogMTVweDsgfVxuXG4uYm94Q29udGV1ZG8gbGFiZWwge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXNpemU6IDEycHg7IH1cblxuLmJveENvbnRldWRvIC5mb3JtLWNvbnRyb2wge1xuICBoZWlnaHQ6IGNhbGMoMS41ZW0gKyAuNzVyZW0gKyA0cHgpOyB9XG5cbi8qLmJ1dHRvbkFyZWEge1xyXG4gICAgbWFyZ2luOiAyMHB4IC0yMHB4IDBweCAtMjBweDtcclxuICAgIGJhY2tncm91bmQ6ICMxMDFhNGM7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgei1pbmRleDogMDtcclxuICAgIGJvdHRvbTogLTUwcHg7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMCAwIDIwcHggMjBweDtcclxuICAgIH0qL1xuLmJ1dHRvbkFyZWEgYnV0dG9uIHtcbiAgd2lkdGg6IDEwMCU7IH1cblxuLmJ1dHRvbkFyZWEgc3ZnIHtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDsgfVxuXG5idXR0b24ubmF2YmFyLXRvZ2dsZXIge1xuICBib3JkZXItcmFkaXVzOiAxMDBweCAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDU1cHggIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgYm9yZGVyOiAjOTJmMzI4IHNvbGlkIDBweDtcbiAgb3V0bGluZTogbm9uZTsgfVxuXG4uYm94Q29udGV1ZG8gLmJ0biB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkOyB9XG5cbi5tYXBhIGlmcmFtZSB7XG4gIGJvcmRlcjogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwdmg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgdG9wOiAwcHg7XG4gIHotaW5kZXg6IC0xOyB9XG5cbi5zYXVkYWNhbyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjsgfVxuXG4uaG9tZVBhc3NhZ2Vpcm8gLmJveENvbnRldWRvIHtcbiAgcG9zaXRpb246IGFic29sdXRlICFpbXBvcnRhbnQ7XG4gIHRvcDogMzBweDtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDBweCk7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBwYWRkaW5nOiAxNXB4IDVweDsgfVxuXG4uaG9tZVBhc3NhZ2Vpcm8gLmJ1dHRvbkFyZWEgYnV0dG9uLnJvdW5kZWQtcGlsbCB7XG4gIGZvbnQtc2l6ZTogMTVweDsgfVxuXG5idXR0b24uZXNjb2xoZXJEZXN0aW5vIHtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDYwcHgpICFpbXBvcnRhbnQ7XG4gIGZsb2F0OiBsZWZ0OyB9XG5cbmJ1dHRvbi5lbWJhcnF1ZVJhcGlkbyB7XG4gIGZsb2F0OiByaWdodDtcbiAgcGFkZGluZzogMHB4IDEycHg7XG4gIHdpZHRoOiA0NXB4ICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogNDVweDsgfVxuXG5idXR0b24uZW1iYXJxdWVSYXBpZG8gc3ZnIHtcbiAgd2lkdGg6IDE5cHg7XG4gIGNvbG9yOiAjNDE2MzEzOyB9XG5cbmJ1dHRvbi5idG4uYnRuLXByaW1hcnkuYnRuLWxnLnJvdW5kZWQtcGlsbC5lbWJhcnF1ZVJhcGlkbyB7XG4gIGJhY2tncm91bmQ6ICNiMWZmNDk7XG4gIGJvcmRlcjogbm9uZTsgfVxuXG4uZm9ybURlc3Rpbm8ge1xuICBib3JkZXI6IG5vbmU7XG4gIG1hcmdpbi10b3A6IDVweDsgfVxuXG4uZW5kRGVzdGlub1JlY2VudGUge1xuICBmb250LXNpemU6IDExcHg7XG4gIGJhY2tncm91bmQ6ICNlZWU7XG4gIHBhZGRpbmc6IDE0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDsgfVxuXG4uZW5kRGVzdGlub1JlY2VudGUgYiB7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIGNvbG9yOiBkYXJrZ3JheTsgfVxuXG4udGl0dWxvRGVzdGlub1JlY2VudGUge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXdlaWdodDogYm9sZDsgfVxuXG5pb24tY29udGVudCB7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTsgfVxuXG5pb24tbWVudS1idXR0b24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XG4gIHdpZHRoOiA1MHB4O1xuICBoZWlnaHQ6IDUwcHg7IH1cblxuLm5hdmJhci1saWdodCAubmF2YmFyLXRvZ2dsZXItaWNvbiB7XG4gIHBhZGRpbmc6IDE1cHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4OyB9XG5cbmlvbi1pdGVtIHtcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IGRvdHRlZCB2YXIoLS1pb24tY29sb3ItbWVkaXVtKTsgfVxuXG5pb24tY2FyZC5uby1yYWRpdXMge1xuICBib3JkZXItcmFkaXVzOiAwOyB9XG5cbiNtYXAge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQ6ICNlZWU7IH1cblxuLmdtbm9wcmludCB7XG4gIGRpc3BsYXk6IG5vbmUgIWltcG9ydGFudDsgfVxuIl19 */"

/***/ }),

/***/ "./src/app/pages/home-results/home-results.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/home-results/home-results.page.ts ***!
  \*********************************************************/
/*! exports provided: HomeResultsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeResultsPage", function() { return HomeResultsPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/google-maps */ "./node_modules/@ionic-native/google-maps/index.js");
/* harmony import */ var src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/user/user.service */ "./src/app/core/user/user.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var HomeResultsPage = /** @class */ (function () {
    function HomeResultsPage(platform, loadCtrl, ngZone, navCtrl, menuCtrl, popoverCtrl, alertCtrl, modalCtrl, toastCtrl, userService, router) {
        this.platform = platform;
        this.loadCtrl = loadCtrl;
        this.ngZone = ngZone;
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.toastCtrl = toastCtrl;
        this.userService = userService;
        this.router = router;
        this.search = '';
        this.search2 = '';
        this.googleAutocomplete = new google.maps.places.AutocompleteService();
        this.searchResults = new Array();
        this.searchResults2 = new Array();
        this.googleDirectionsService = new google.maps.DirectionsService();
        this.matrix = new google.maps.DistanceMatrixService();
        this.endOrigin = '';
        this.user$ = userService.getUser();
        this.corrida;
    }
    HomeResultsPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(true);
        this.menuCtrl.close();
    };
    HomeResultsPage.prototype.ngOnInit = function () {
        this.mapElement = this.mapElement.nativeElement;
        this.mapElement.style.width = this.platform.width() + 'px';
        this.mapElement.style.height = this.platform.height() + 'px';
        this.loadMap();
    };
    HomeResultsPage.prototype.loadMap = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, mapOptions, error_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.loadCtrl.create({ message: 'Por favor aguarde ...' })];
                    case 1:
                        _a.loading = _b.sent();
                        return [4 /*yield*/, this.loading.present()];
                    case 2:
                        _b.sent();
                        _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["Environment"].setEnv({
                            'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyBBSA6wXmuyhUqg1fDMH4-WP3QZSRSDtr4',
                            'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyBBSA6wXmuyhUqg1fDMH4-WP3QZSRSDtr4'
                        });
                        mapOptions = {
                            controls: {
                                zoom: false
                            }
                        };
                        this.map = _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["GoogleMaps"].create(this.mapElement, mapOptions);
                        _b.label = 3;
                    case 3:
                        _b.trys.push([3, 5, , 6]);
                        return [4 /*yield*/, this.map.one(_ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["GoogleMapsEvent"].MAP_READY)];
                    case 4:
                        _b.sent();
                        this.addOrigenMarker();
                        return [3 /*break*/, 6];
                    case 5:
                        error_1 = _b.sent();
                        console.error(error_1);
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    HomeResultsPage.prototype.addOrigenMarker = function () {
        return __awaiter(this, void 0, void 0, function () {
            var myLocation, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, 4, 5]);
                        return [4 /*yield*/, this.map.getMyLocation()];
                    case 1:
                        myLocation = _a.sent();
                        return [4 /*yield*/, this.map.moveCamera({
                                target: myLocation.latLng,
                                zoom: 18
                            })];
                    case 2:
                        _a.sent();
                        this.originMarker = this.map.addMarkerSync({
                            title: 'Origem',
                            icon: '#000',
                            animation: _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["GoogleMapsAnimation"].DROP,
                            position: myLocation.latLng
                        });
                        return [3 /*break*/, 5];
                    case 3:
                        error_2 = _a.sent();
                        console.error(error_2);
                        return [3 /*break*/, 5];
                    case 4:
                        this.loading.dismiss();
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    HomeResultsPage.prototype.searchChanged = function () {
        var _this = this;
        if (!this.search.trim().length)
            return;
        this.googleAutocomplete.getPlacePredictions({ input: this.search }, function (predictions) {
            _this.ngZone.run(function () {
                _this.searchResults = predictions;
            });
        });
    };
    HomeResultsPage.prototype.searchChanged2 = function () {
        var _this = this;
        if (!this.search2.trim().length)
            return;
        this.googleAutocomplete.getPlacePredictions({ input: this.search2 }, function (predictions) {
            _this.ngZone.run(function () {
                _this.searchResults2 = predictions;
            });
        });
    };
    HomeResultsPage.prototype.calcRoute2 = function (item) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.search2 = '';
                this.origem = item;
                console.log(this.origem);
                return [2 /*return*/];
            });
        });
    };
    HomeResultsPage.prototype.calcRoute = function (item) {
        return __awaiter(this, void 0, void 0, function () {
            var info, markerDestination;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.search = '';
                        this.destination = item;
                        console.log(this.origem);
                        return [4 /*yield*/, _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["Geocoder"].geocode({ address: this.destination.description })];
                    case 1:
                        info = _a.sent();
                        markerDestination = this.map.addMarkerSync({
                            title: this.destination.description,
                            icon: '#000',
                            animation: _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_2__["GoogleMapsAnimation"].DROP,
                            position: info[0].position
                        });
                        // console.log('distancia origem: ' + this.originMarker.getPosition().lat + " long :  " + this.originMarker.getPosition().lng)
                        // console.log('distancia destino: ' + markerDestination.getPosition());
                        this.googleDirectionsService.route({
                            origin: this.originMarker.getPosition(),
                            destination: markerDestination.getPosition(),
                            travelMode: 'DRIVING'
                        }, function (results) { return __awaiter(_this, void 0, void 0, function () {
                            function callback(response, status) {
                                var travelDetailsObject;
                                if (status !== "OK") {
                                    alert("Error was: " + status);
                                }
                                else {
                                    var origins = response.originAddresses;
                                    var destinations = response.destinationAddresses;
                                    console.log(origins);
                                    console.log(destinations);
                                    for (var i = 0; i < origins.length; i++) {
                                        var results = response.rows[i].elements;
                                        for (var j = 0; j < results.length; j++) {
                                            var element = results[j];
                                            var distance = element.distance.text;
                                            var duration = element.duration.text;
                                            var from = origins[i];
                                            var to = destinations[j];
                                            travelDetailsObject = {
                                                distance: distance,
                                                duration: duration
                                            };
                                        }
                                    }
                                    this.travelDetailsObject = travelDetailsObject;
                                    this.corrida = this.travelDetailsObject.distance;
                                    var res = this.travelDetailsObject.distance;
                                    this.valorCorrida = "R$ 50,00";
                                    this.tempoEstimadoCorrida = this.travelDetailsObject.duration;
                                    console.log('distancia : ' + this.travelDetailsObject.distance + ' no tempo de : ' + this.travelDetailsObject.duration);
                                }
                            }
                            var points, routes, i, origin1, destinationB;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        points = new Array();
                                        routes = results.routes[0].overview_path;
                                        for (i = 0; i < routes.length; i++) {
                                            points[i] = {
                                                lat: routes[i].lat(),
                                                lng: routes[i].lng()
                                            };
                                        }
                                        return [4 /*yield*/, this.map.addPolyline({
                                                points: points,
                                                color: '#000',
                                                width: 3
                                            })];
                                    case 1:
                                        _a.sent();
                                        origin1 = new google.maps.LatLng(this.originMarker.getPosition().lat, this.originMarker.getPosition().lng);
                                        destinationB = new google.maps.LatLng(markerDestination.getPosition().lat, markerDestination.getPosition().lng);
                                        this.matrix.getDistanceMatrix({
                                            origins: [origin1],
                                            destinations: [destinationB],
                                            travelMode: google.maps.TravelMode.DRIVING
                                        }, callback);
                                        this.map.moveCamera({ target: points });
                                        return [2 /*return*/];
                                }
                            });
                        }); });
                        return [2 /*return*/];
                }
            });
        });
    };
    HomeResultsPage.prototype.back = function () {
        return __awaiter(this, void 0, void 0, function () {
            var error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.map.clear()];
                    case 1:
                        _a.sent();
                        this.destination = null;
                        this.addOrigenMarker();
                        return [3 /*break*/, 3];
                    case 2:
                        error_3 = _a.sent();
                        console.error(error_3);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    HomeResultsPage.prototype.toggleTeste = function () {
        if (document.getElementById("collapseExample").style.display == 'none') {
            document.getElementById("collapseExample").style.display = "block";
        }
        else {
            document.getElementById("collapseExample").style.display = "none";
        }
    };
    HomeResultsPage.prototype.toggleTeste1 = function () {
        if (document.getElementById("collapseExample1").style.display == 'none') {
            document.getElementById("collapseExample1").style.display = "block";
        }
        else {
            document.getElementById("collapseExample1").style.display = "none";
        }
    };
    HomeResultsPage.prototype.embarqueRapido = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: '',
                            cssClass: '',
                            subHeader: '',
                            message: 'Embarque rápido : <br> • Pegue um UaiLeva <br> • Escaneie o QR Code <br> • Inicie a corrida <br> Registrar o Cartão',
                            buttons: ['X']
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomeResultsPage.prototype.logout = function () {
        this.userService.logout();
        this.router.navigate(['']);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('map'),
        __metadata("design:type", Object)
    ], HomeResultsPage.prototype, "mapElement", void 0);
    HomeResultsPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home-results.page.html */ "./src/app/pages/home-results/home-results.page.html"),
            styles: [__webpack_require__(/*! ./home-results.page.scss */ "./src/app/pages/home-results/home-results.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["Platform"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["LoadingController"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgZone"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["MenuController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["PopoverController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ToastController"],
            src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], HomeResultsPage);
    return HomeResultsPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-home-results-home-results-module.js.map