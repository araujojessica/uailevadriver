(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-login-module"],{

/***/ "./node_modules/@ionic-native/email-composer/ngx/index.js":
/*!****************************************************************!*\
  !*** ./node_modules/@ionic-native/email-composer/ngx/index.js ***!
  \****************************************************************/
/*! exports provided: EmailComposer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailComposer", function() { return EmailComposer; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/core */ "./node_modules/@ionic-native/core/index.js");



var EmailComposer = /** @class */ (function (_super) {
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"])(EmailComposer, _super);
    function EmailComposer() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    EmailComposer.prototype.hasPermission = function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "hasPermission", { "successIndex": 0, "errorIndex": 2 }, arguments); };
    EmailComposer.prototype.requestPermission = function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "requestPermission", { "successIndex": 0, "errorIndex": 2 }, arguments); };
    EmailComposer.prototype.hasAccount = function () {
        var _this = this;
        return (function () {
            if (Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["checkAvailability"])(_this) === true) {
                return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["getPromise"])(function (resolve) {
                    EmailComposer.getPlugin().hasAccount(function (result) {
                        if (result) {
                            resolve(true);
                        }
                        else {
                            resolve(false);
                        }
                    });
                });
            }
        })();
    };
    EmailComposer.prototype.hasClient = function (app) {
        var _this = this;
        return (function () {
            if (Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["checkAvailability"])(_this) === true) {
                return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["getPromise"])(function (resolve) {
                    if (app) {
                        EmailComposer.getPlugin().hasClient(app, function (result) {
                            if (result) {
                                resolve(true);
                            }
                            else {
                                resolve(false);
                            }
                        });
                    }
                    else {
                        EmailComposer.getPlugin().getClients(function (apps) {
                            resolve(apps && apps.length > 0);
                        });
                    }
                });
            }
        })();
    };
    EmailComposer.prototype.getClients = function () {
        var _this = this;
        return (function () {
            if (Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["checkAvailability"])(_this) === true) {
                return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["getPromise"])(function (resolve) {
                    EmailComposer.getPlugin().getClients(function (apps) {
                        if (Object.prototype.toString.call(apps) === '[object String]') {
                            apps = [apps];
                        }
                        resolve(apps);
                    });
                });
            }
        })();
    };
    EmailComposer.prototype.isAvailable = function (app) {
        var _this = this;
        return (function () {
            if (Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["checkAvailability"])(_this) === true) {
                return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["getPromise"])(function (resolve) {
                    Promise.all([_this.hasAccount, _this.hasClient(app)]).then(function (results) {
                        return resolve(results.length === 2 && results[0] && results[1]);
                    });
                });
            }
        })();
    };
    EmailComposer.prototype.open = function (options, scope) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "open", { "successIndex": 1, "errorIndex": 3 }, arguments); };
    EmailComposer.prototype.addAlias = function (alias, packageName) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "addAlias", {}, arguments); };
    EmailComposer.pluginName = "EmailComposer";
    EmailComposer.plugin = "cordova-plugin-email-composer";
    EmailComposer.pluginRef = "cordova.plugins.email";
    EmailComposer.repo = "https://github.com/katzer/cordova-plugin-email-composer";
    EmailComposer.platforms = ["Amazon Fire OS", "Android", "Browser", "iOS", "Windows", "macOS"];
    EmailComposer = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], EmailComposer);
    return EmailComposer;
}(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["IonicNativePlugin"]));

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9zcmMvQGlvbmljLW5hdGl2ZS9wbHVnaW5zL2VtYWlsLWNvbXBvc2VyL25neC9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLGlEQUFvRCxVQUFVLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQzs7SUFtSS9ELGlDQUFpQjs7OztJQVNsRCxxQ0FBYTtJQVliLHlDQUFpQjtJQVVqQixrQ0FBVTs7O21EQUFpQjtnQkFDekIsT0FBTyxVQUFVLENBQVUsVUFBQSxPQUFPO29CQUNoQyxhQUFhLENBQUMsU0FBUyxFQUFFLENBQUMsVUFBVSxDQUFDLFVBQUMsTUFBZTt3QkFDbkQsSUFBSSxNQUFNLEVBQUU7NEJBQ1YsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO3lCQUNmOzZCQUFNOzRCQUNMLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDaEI7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7YUFDSjs7O0lBVUQsaUNBQVMsYUFBQyxHQUFZOzs7bURBQWdCO2dCQUNwQyxPQUFPLFVBQVUsQ0FBVSxVQUFBLE9BQU87b0JBQ2hDLElBQUksR0FBRyxFQUFFO3dCQUNQLGFBQWEsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLFVBQUMsTUFBZTs0QkFDdkQsSUFBSSxNQUFNLEVBQUU7Z0NBQ1YsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDOzZCQUNmO2lDQUFNO2dDQUNMLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQzs2QkFDaEI7d0JBQ0gsQ0FBQyxDQUFDLENBQUM7cUJBQ0o7eUJBQU07d0JBQ0wsYUFBYSxDQUFDLFNBQVMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxVQUFDLElBQWM7NEJBQ2xELE9BQU8sQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQzt3QkFDbkMsQ0FBQyxDQUFDLENBQUM7cUJBQ0o7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7YUFDSjs7O0lBU0Qsa0NBQVU7OzttREFBc0I7Z0JBQzlCLE9BQU8sVUFBVSxDQUFXLFVBQUEsT0FBTztvQkFDakMsYUFBYSxDQUFDLFNBQVMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxVQUFDLElBQVM7d0JBQzdDLElBQUksTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLGlCQUFpQixFQUFFOzRCQUM5RCxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQzt5QkFDZjt3QkFDRCxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ2hCLENBQUMsQ0FBQyxDQUFDO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2FBQ0o7OztJQVNELG1DQUFXLGFBQUMsR0FBWTs7O21EQUFnQjtnQkFDdEMsT0FBTyxVQUFVLENBQVUsVUFBQSxPQUFPO29CQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSSxDQUFDLFVBQVUsRUFBRSxLQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxPQUFPO3dCQUM5RCxPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsTUFBTSxLQUFLLENBQUMsSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ25FLENBQUMsQ0FBQyxDQUFDO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2FBQ0o7OztJQWFELDRCQUFJLGFBQUMsT0FBNkIsRUFBRSxLQUFXO0lBVy9DLGdDQUFRLGFBQUMsS0FBYSxFQUFFLFdBQW1COzs7Ozs7SUE1SGhDLGFBQWE7UUFEekIsVUFBVSxFQUFFO09BQ0EsYUFBYTt3QkFwSTFCO0VBb0ltQyxpQkFBaUI7U0FBdkMsYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvcmRvdmEsIENvcmRvdmFDaGVjaywgSW9uaWNOYXRpdmVQbHVnaW4sIFBsdWdpbiwgZ2V0UHJvbWlzZSB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvY29yZSc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgRW1haWxDb21wb3Nlck9wdGlvbnMge1xuICAvKipcbiAgICogQXBwIHRvIHNlbmQgdGhlIGVtYWlsIHdpdGhcbiAgICovXG4gIGFwcD86IHN0cmluZztcblxuICAvKipcbiAgICogRW1haWwgYWRkcmVzcyhlcykgZm9yIFRvIGZpZWxkXG4gICAqL1xuICB0bz86IHN0cmluZyB8IHN0cmluZ1tdO1xuXG4gIC8qKlxuICAgKiBFbWFpbCBhZGRyZXNzKGVzKSBmb3IgQ0MgZmllbGRcbiAgICovXG4gIGNjPzogc3RyaW5nIHwgc3RyaW5nW107XG5cbiAgLyoqXG4gICAqIEVtYWlsIGFkZHJlc3MoZXMpIGZvciBCQ0MgZmllbGRcbiAgICovXG4gIGJjYz86IHN0cmluZyB8IHN0cmluZ1tdO1xuXG4gIC8qKlxuICAgKiBGaWxlIHBhdGhzIG9yIGJhc2U2NCBkYXRhIHN0cmVhbXNcbiAgICovXG4gIGF0dGFjaG1lbnRzPzogc3RyaW5nW107XG5cbiAgLyoqXG4gICAqIFN1YmplY3Qgb2YgdGhlIGVtYWlsXG4gICAqL1xuICBzdWJqZWN0Pzogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBFbWFpbCBib2R5IChmb3IgSFRNTCwgc2V0IGlzSHRtbCB0byB0cnVlKVxuICAgKi9cbiAgYm9keT86IHN0cmluZztcblxuICAvKipcbiAgICogSW5kaWNhdGVzIGlmIHRoZSBib2R5IGlzIEhUTUwgb3IgcGxhaW4gdGV4dFxuICAgKi9cbiAgaXNIdG1sPzogYm9vbGVhbjtcblxuICAvKipcbiAgICogIENvbnRlbnQgdHlwZSBvZiB0aGUgZW1haWwgKEFuZHJvaWQgb25seSlcbiAgICovXG4gIHR5cGU/OiBzdHJpbmc7XG59XG5cbi8qKlxuICogQG5hbWUgRW1haWwgQ29tcG9zZXJcbiAqIEBwcmVtaWVyIGVtYWlsLWNvbXBvc2VyXG4gKiBAZGVzY3JpcHRpb25cbiAqXG4gKiBSZXF1aXJlcyBDb3Jkb3ZhIHBsdWdpbjogY29yZG92YS1wbHVnaW4tZW1haWwtY29tcG9zZXIuIEZvciBtb3JlIGluZm8sIHBsZWFzZSBzZWUgdGhlIFtFbWFpbCBDb21wb3NlciBwbHVnaW4gZG9jc10oaHR0cHM6Ly9naXRodWIuY29tL2h5cGVyeTJrL2NvcmRvdmEtZW1haWwtcGx1Z2luKS5cbiAqXG4gKlxuICogQHVzYWdlXG4gKiBgYGB0eXBlc2NyaXB0XG4gKiBpbXBvcnQgeyBFbWFpbENvbXBvc2VyIH0gZnJvbSAnQGlvbmljLW5hdGl2ZS9lbWFpbC1jb21wb3Nlci9uZ3gnO1xuICpcbiAqIGNvbnN0cnVjdG9yKHByaXZhdGUgZW1haWxDb21wb3NlcjogRW1haWxDb21wb3NlcikgeyB9XG4gKlxuICogLi4uXG4gKlxuICpcbiAqIHRoaXMuZW1haWxDb21wb3Nlci5nZXRDbGllbnRzKCkudGhlbigoYXBwczogW10pID0+IHtcbiAqICAgIC8vIFJldHVybnMgYW4gYXJyYXkgb2YgY29uZmlndXJlZCBlbWFpbCBjbGllbnRzIGZvciB0aGUgZGV2aWNlXG4gKiB9KTtcbiAqXG4gKiB0aGlzLmVtYWlsQ29tcG9zZXIuaGFzQ2xpZW50KCkudGhlbihhcHAsIChpc1ZhbGlkOiBib29sZWFuKSA9PiB7XG4gKiAgaWYgKGlzVmFsaWQpIHtcbiAqICAgIC8vIE5vdyB3ZSBrbm93IHdlIGhhdmUgYSB2YWxpZCBlbWFpbCBjbGllbnQgY29uZmlndXJlZFxuICogICAgLy8gTm90IHNwZWNpZnlpbmcgYW4gYXBwIHdpbGwgcmV0dXJuIHRydWUgaWYgYXQgbGVhc3Qgb25lIGVtYWlsIGNsaWVudCBpcyBjb25maWd1cmVkXG4gKiAgfVxuICogfSk7XG4gKlxuICogdGhpcy5lbWFpbENvbXBvc2VyLmhhc0FjY291bnQoKS50aGVuKChpc1ZhbGlkOiBib29sZWFuKSA9PiB7XG4gKiAgaWYgKGlzVmFsaWQpIHtcbiAqICAgIC8vIE5vdyB3ZSBrbm93IHdlIGhhdmUgYSB2YWxpZCBlbWFpbCBhY2NvdW50IGNvbmZpZ3VyZWRcbiAqICB9XG4gKiB9KTtcbiAqXG4gKiB0aGlzLmVtYWlsQ29tcG9zZXIuaXNBdmFpbGFibGUoKS50aGVuKGFwcCwgKGF2YWlsYWJsZTogYm9vbGVhbikgPT4ge1xuICogIGlmKGF2YWlsYWJsZSkge1xuICogICAgLy8gTm93IHdlIGtub3cgd2UgY2FuIHNlbmQgYW4gZW1haWwsIGNhbGxzIGhhc0NsaWVudCBhbmQgaGFzQWNjb3VudFxuICogICAgLy8gTm90IHNwZWNpZnlpbmcgYW4gYXBwIHdpbGwgcmV0dXJuIHRydWUgaWYgYXQgbGVhc3Qgb25lIGVtYWlsIGNsaWVudCBpcyBjb25maWd1cmVkXG4gKiAgfVxuICogfSk7XG4gKlxuICogbGV0IGVtYWlsID0ge1xuICogICB0bzogJ21heEBtdXN0ZXJtYW5uLmRlJyxcbiAqICAgY2M6ICdlcmlrYUBtdXN0ZXJtYW5uLmRlJyxcbiAqICAgYmNjOiBbJ2pvaG5AZG9lLmNvbScsICdqYW5lQGRvZS5jb20nXSxcbiAqICAgYXR0YWNobWVudHM6IFtcbiAqICAgICAnZmlsZTovL2ltZy9sb2dvLnBuZycsXG4gKiAgICAgJ3JlczovL2ljb24ucG5nJyxcbiAqICAgICAnYmFzZTY0Omljb24ucG5nLy9pVkJPUncwS0dnb0FBQUFOU1VoRVVnLi4uJyxcbiAqICAgICAnZmlsZTovL1JFQURNRS5wZGYnXG4gKiAgIF0sXG4gKiAgIHN1YmplY3Q6ICdDb3Jkb3ZhIEljb25zJyxcbiAqICAgYm9keTogJ0hvdyBhcmUgeW91PyBOaWNlIGdyZWV0aW5ncyBmcm9tIExlaXB6aWcnLFxuICogICBpc0h0bWw6IHRydWVcbiAqIH1cbiAqXG4gKiAvLyBTZW5kIGEgdGV4dCBtZXNzYWdlIHVzaW5nIGRlZmF1bHQgb3B0aW9uc1xuICogdGhpcy5lbWFpbENvbXBvc2VyLm9wZW4oZW1haWwpO1xuICogYGBgXG4gKlxuICogWW91IGNhbiBhbHNvIGFzc2lnbiBhbGlhc2VzIHRvIGVtYWlsIGFwcHNcbiAqIGBgYHRzXG4gKiAvLyBhZGQgYWxpYXNcbiAqIHRoaXMuZW1haWwuYWRkQWxpYXMoJ2dtYWlsJywgJ2NvbS5nb29nbGUuYW5kcm9pZC5nbScpO1xuICpcbiAqIC8vIHRoZW4gdXNlIGFsaWFzIHdoZW4gc2VuZGluZyBlbWFpbFxuICogdGhpcy5lbWFpbC5vcGVuKHtcbiAqICAgYXBwOiAnZ21haWwnLFxuICogICAuLi5cbiAqIH0pO1xuICogYGBgXG4gKiBAaW50ZXJmYWNlc1xuICogRW1haWxDb21wb3Nlck9wdGlvbnNcbiAqL1xuQFBsdWdpbih7XG4gIHBsdWdpbk5hbWU6ICdFbWFpbENvbXBvc2VyJyxcbiAgcGx1Z2luOiAnY29yZG92YS1wbHVnaW4tZW1haWwtY29tcG9zZXInLFxuICBwbHVnaW5SZWY6ICdjb3Jkb3ZhLnBsdWdpbnMuZW1haWwnLFxuICByZXBvOiAnaHR0cHM6Ly9naXRodWIuY29tL2thdHplci9jb3Jkb3ZhLXBsdWdpbi1lbWFpbC1jb21wb3NlcicsXG4gIHBsYXRmb3JtczogWydBbWF6b24gRmlyZSBPUycsICdBbmRyb2lkJywgJ0Jyb3dzZXInLCAnaU9TJywgJ1dpbmRvd3MnLCAnbWFjT1MnXSxcbn0pXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgRW1haWxDb21wb3NlciBleHRlbmRzIElvbmljTmF0aXZlUGx1Z2luIHtcbiAgLyoqXG4gICAqIENoZWNrcyBpZiB0aGUgYXBwIGhhcyBhIHBlcm1pc3Npb24gdG8gYWNjZXNzIGVtYWlsIGFjY291bnRzIGluZm9ybWF0aW9uXG4gICAqIEByZXR1cm4ge1Byb21pc2U8Ym9vbGVhbj59IHJldHVybnMgYSBwcm9taXNlIHRoYXQgcmVzb2x2ZXMgd2l0aCBhIGJvb2xlYW4gdGhhdCBpbmRpY2F0ZXMgaWYgdGhlIHBlcm1pc3Npb24gd2FzIGdyYW50ZWRcbiAgICovXG4gIEBDb3Jkb3ZhKHtcbiAgICBzdWNjZXNzSW5kZXg6IDAsXG4gICAgZXJyb3JJbmRleDogMixcbiAgfSlcbiAgaGFzUGVybWlzc2lvbigpOiBQcm9taXNlPGJvb2xlYW4+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogUmVxdWVzdCBwZXJtaXNzaW9uIHRvIGFjY2VzcyBlbWFpbCBhY2NvdW50cyBpbmZvcm1hdGlvblxuICAgKiBAcmV0dXJuIHtQcm9taXNlPGJvb2xlYW4+fSByZXR1cm5zIGEgcHJvbWlzZSB0aGF0IHJlc29sdmVzIHdpdGggYSBib29sZWFuIHRoYXQgaW5kaWNhdGVzIGlmIHRoZSBwZXJtaXNzaW9uIHdhcyBncmFudGVkXG4gICAqL1xuICBAQ29yZG92YSh7XG4gICAgc3VjY2Vzc0luZGV4OiAwLFxuICAgIGVycm9ySW5kZXg6IDIsXG4gIH0pXG4gIHJlcXVlc3RQZXJtaXNzaW9uKCk6IFByb21pc2U8Ym9vbGVhbj4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBWZXJpZmllcyBpZiBhbiBlbWFpbCBhY2NvdW50IGlzIGNvbmZpZ3VyZWQgb24gdGhlIGRldmljZS5cbiAgICpcbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn0gUmVzb2x2ZXMgaWYgYXZhaWxhYmxlLCByZWplY3RzIGlmIG5vdCBhdmFpbGFibGVcbiAgICovXG4gIEBDb3Jkb3ZhQ2hlY2soKVxuICBoYXNBY2NvdW50KCk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuIGdldFByb21pc2U8Ym9vbGVhbj4ocmVzb2x2ZSA9PiB7XG4gICAgICBFbWFpbENvbXBvc2VyLmdldFBsdWdpbigpLmhhc0FjY291bnQoKHJlc3VsdDogYm9vbGVhbikgPT4ge1xuICAgICAgICBpZiAocmVzdWx0KSB7XG4gICAgICAgICAgcmVzb2x2ZSh0cnVlKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZXNvbHZlKGZhbHNlKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogVmVyaWZpZXMgaWYgYSBzcGVjaWZpYyBlbWFpbCBjbGllbnQgaXMgaW5zdGFsbGVkIG9uIHRoZSBkZXZpY2UuXG4gICAqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBbYXBwXSBBcHAgaWQgb3IgdXJpIHNjaGVtZS5cbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn0gUmVzb2x2ZXMgaWYgYXZhaWxhYmxlLCByZWplY3RzIGlmIG5vdCBhdmFpbGFibGVcbiAgICovXG5cbiAgQENvcmRvdmFDaGVjaygpXG4gIGhhc0NsaWVudChhcHA/OiBzdHJpbmcpOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybiBnZXRQcm9taXNlPGJvb2xlYW4+KHJlc29sdmUgPT4ge1xuICAgICAgaWYgKGFwcCkge1xuICAgICAgICBFbWFpbENvbXBvc2VyLmdldFBsdWdpbigpLmhhc0NsaWVudChhcHAsIChyZXN1bHQ6IGJvb2xlYW4pID0+IHtcbiAgICAgICAgICBpZiAocmVzdWx0KSB7XG4gICAgICAgICAgICByZXNvbHZlKHRydWUpO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXNvbHZlKGZhbHNlKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgRW1haWxDb21wb3Nlci5nZXRQbHVnaW4oKS5nZXRDbGllbnRzKChhcHBzOiBzdHJpbmdbXSkgPT4ge1xuICAgICAgICAgIHJlc29sdmUoYXBwcyAmJiBhcHBzLmxlbmd0aCA+IDApO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBSZXR1cm5zIGFuIGFycmF5IG9mIGVtYWlsIGNsaWVudHMgaW5zdGFsbGVkIG9uIHRoZSBkZXZpY2UuXG4gICAqXG4gICAqIEByZXR1cm5zIHtQcm9taXNlPHN0cmluZ1tdPn0gUmVzb2x2ZXMgaWYgYXZhaWxhYmxlLCByZWplY3RzIGlmIG5vdCBhdmFpbGFibGVcbiAgICovXG4gIEBDb3Jkb3ZhQ2hlY2soKVxuICBAQ29yZG92YSh7IHBsYXRmb3JtczogWydBbmRyb2lkJ10gfSlcbiAgZ2V0Q2xpZW50cygpOiBQcm9taXNlPHN0cmluZ1tdPiB7XG4gICAgcmV0dXJuIGdldFByb21pc2U8c3RyaW5nW10+KHJlc29sdmUgPT4ge1xuICAgICAgRW1haWxDb21wb3Nlci5nZXRQbHVnaW4oKS5nZXRDbGllbnRzKChhcHBzOiBhbnkpID0+IHtcbiAgICAgICAgaWYgKE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChhcHBzKSA9PT0gJ1tvYmplY3QgU3RyaW5nXScpIHtcbiAgICAgICAgICBhcHBzID0gW2FwcHNdO1xuICAgICAgICB9XG4gICAgICAgIHJlc29sdmUoYXBwcyk7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBWZXJpZmllcyBpZiBzZW5kaW5nIGVtYWlscyBpcyBzdXBwb3J0ZWQgb24gdGhlIGRldmljZS5cbiAgICpcbiAgICogQHBhcmFtIHtzdHJpbmd9IFthcHBdIEFwcCBpZCBvciB1cmkgc2NoZW1lLlxuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+fSBSZXNvbHZlcyBpZiBhdmFpbGFibGUsIHJlamVjdHMgaWYgbm90IGF2YWlsYWJsZVxuICAgKi9cbiAgQENvcmRvdmFDaGVjaygpXG4gIGlzQXZhaWxhYmxlKGFwcD86IHN0cmluZyk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuIGdldFByb21pc2U8Ym9vbGVhbj4ocmVzb2x2ZSA9PiB7XG4gICAgICBQcm9taXNlLmFsbChbdGhpcy5oYXNBY2NvdW50LCB0aGlzLmhhc0NsaWVudChhcHApXSkudGhlbihyZXN1bHRzID0+IHtcbiAgICAgICAgcmV0dXJuIHJlc29sdmUocmVzdWx0cy5sZW5ndGggPT09IDIgJiYgcmVzdWx0c1swXSAmJiByZXN1bHRzWzFdKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIERpc3BsYXlzIHRoZSBlbWFpbCBjb21wb3NlciBwcmUtZmlsbGVkIHdpdGggZGF0YS5cbiAgICpcbiAgICogQHBhcmFtIHtFbWFpbENvbXBvc2VyT3B0aW9uc30gb3B0aW9ucyBFbWFpbFxuICAgKiBAcGFyYW0ge2FueX0gW3Njb3BlXSBTY29wZSBmb3IgdGhlIHByb21pc2VcbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn0gUmVzb2x2ZXMgcHJvbWlzZSB3aGVuIHRoZSBFbWFpbENvbXBvc2VyIGhhcyBiZWVuIG9wZW5lZFxuICAgKi9cbiAgQENvcmRvdmEoe1xuICAgIHN1Y2Nlc3NJbmRleDogMSxcbiAgICBlcnJvckluZGV4OiAzLFxuICB9KVxuICBvcGVuKG9wdGlvbnM6IEVtYWlsQ29tcG9zZXJPcHRpb25zLCBzY29wZT86IGFueSk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIEFkZHMgYSBuZXcgbWFpbCBhcHAgYWxpYXMuXG4gICAqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBhbGlhcyBUaGUgYWxpYXMgbmFtZVxuICAgKiBAcGFyYW0ge3N0cmluZ30gcGFja2FnZU5hbWUgVGhlIHBhY2thZ2UgbmFtZVxuICAgKi9cbiAgQENvcmRvdmEoKVxuICBhZGRBbGlhcyhhbGlhczogc3RyaW5nLCBwYWNrYWdlTmFtZTogc3RyaW5nKTogdm9pZCB7fVxufVxuIl19

/***/ }),

/***/ "./src/app/core/auth/auth.service.ts":
/*!*******************************************!*\
  !*** ./src/app/core/auth/auth.service.ts ***!
  \*******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _user_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../user/user.service */ "./src/app/core/user/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//const API_URL = 'http://159.203.181.9:3000';
var API_URL = 'http://localhost:3000';
var AuthService = /** @class */ (function () {
    function AuthService(http, userService) {
        this.http = http;
        this.userService = userService;
    }
    AuthService.prototype.authenticate = function (phone, password) {
        var _this = this;
        return this.http.post(API_URL + "/user/login", { phone: phone, password: password }, { observe: 'response' })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function (res) {
            var authToken = res.headers.get('x-access-token');
            _this.userService.setToken(authToken);
            console.log("User " + phone + " authenticated with token " + authToken);
        }));
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/pages/login/login.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.module.ts ***!
  \*********************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_email_composer_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/email-composer/ngx */ "./node_modules/@ionic-native/email-composer/ngx/index.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/login/login.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }
];
var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]],
            providers: [_ionic_native_email_composer_ngx__WEBPACK_IMPORTED_MODULE_5__["EmailComposer"]]
        })
    ], LoginPageModule);
    return LoginPageModule;
}());



/***/ }),

/***/ "./src/app/pages/login/login.page.html":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<ion-header>\r\n  <title>Uai Leva / Login</title>\r\n  <meta charset=\"utf-8\" />\r\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n\r\n</ion-header>\r\n\r\n\r\n<ion-content class=\"login\">\r\n\r\n  <div class=\"container\">\r\n    <br>\r\n      <div class=\"logoLogin\">\r\n          <img src='/assets/img/logo-branco.png' alt=\"Uai Leva\">\r\n          \r\n      </div>\r\n      <br>\r\n      <div class=\"boxConteudo\">      \r\n        <form [formGroup]=\"onLoginForm\" class=\"list-form\">\r\n              <div class=\"form-group\">\r\n                <div class=\"position\"><img src='/assets/icon/brasil.png' style=\"width: 2.5em; height: 2.5em;\"></div>\r\n                <input type=\"phone\" formControlName=\"phone\" class=\"form-control cel-sp-mask\" id=\"celular\" placeholder=\"Telefone\" onkeypress=\"$(this).mask('(00) 90000-0000')\"/>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <input type=\"password\" formControlName=\"password\" class=\"form-control cel-sp-mask\" id=\"celular\" placeholder=\"Senha\"/>\r\n              </div>\r\n                \r\n        </form>\r\n        <div class=\"text-center buttonArea\">\r\n          <button class=\"btn btn-primary btn-lg rounded-pill btnLogin\" (click)=\"goToHome()\">\r\n              ENTRAR\r\n          </button>\r\n        </div>\r\n          <div class=\"linkEsqueciSenha\">\r\n              <a href=\"\">Esqueci minha Senha</a>\r\n          </div>\r\n\r\n          <ion-grid>\r\n              <p class=\"textLogin\">OU LOGAR COM:</p>\r\n            <ion-row>\r\n              <ion-col size=\"6\">\r\n              <ion-button shape=\"round\" expand=\"full\" color=\"tertiary\" (click)=\"loginFb()\">\r\n                  <img [src]=\"user.img\" alt=\"\"/>\r\n                  <ion-icon slot=\"icon-only\" name=\"logo-facebook\" style=\"color: #1b2c7b;\"></ion-icon>\r\n                </ion-button>\r\n              </ion-col>\r\n              <ion-col size=\"6\">\r\n                <ion-button shape=\"round\" expand=\"full\" color=\"tertiary\">\r\n                  <ion-icon slot=\"icon-only\" name=\"logo-googleplus\" style=\"color: #1b2c7b;\"></ion-icon>\r\n                </ion-button>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-grid>\r\n\r\n          <div class=\"linkCadastrar\">\r\n              <a (click)=\"goToRegister()\">Quero Cadastrar > </a>\r\n          </div>\r\n\r\n          \r\n      </div>\r\n\r\n  </div>\r\n\r\n</ion-content>\r\n\r\n"

/***/ }),

/***/ "./src/app/pages/login/login.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@import url(\"https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,400;0,500;1,400;1,500&display=swap\");\n.login {\n  --background: url('bg-login.jpg') !important; }\n@media (max-width: 992px) {\n  ion-content {\n    --background: white;\n    background-size: cover;\n    height: inherit; }\n  .navbar-collapse {\n    position: fixed;\n    top: 0px;\n    left: 0;\n    padding-left: 15px;\n    padding-right: 15px;\n    padding-bottom: 15px;\n    width: 75%;\n    height: 100%;\n    background: #1765e2;\n    text-transform: uppercase;\n    z-index: 9999;\n    box-shadow: 0px 0px 90px black; }\n  .navbar-collapse a.nav-link {\n    color: white !important;\n    font-weight: bold;\n    font-size: 19px; }\n  a.nav-link svg {\n    font-size: 25px;\n    margin-right: 10px; }\n  .navbar-dark .navbar-toggler {\n    background: #1b2c7b; }\n  .navbar-collapse.collapsing {\n    left: -75%;\n    transition: height 0s ease; }\n  .navbar-collapse.show {\n    margin-top: 0px;\n    left: 0;\n    transition: left 300ms ease-in-out; }\n  .navbar-toggler.collapsed ~ .navbar-collapse {\n    transition: left 500ms ease-in-out; } }\nbody {\n  background: url('bg-uai-leva.png') no-repeat #eaeaea;\n  font-family: 'Rubik', sans-serif;\n  background-position: 80px bottom;\n  background-size: contain;\n  height: 100vh; }\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  text-transform: uppercase;\n  margin: 20px 0;\n  font-weight: bold;\n  color: #123b7d; }\nh3 {\n  font-size: 0.75rem;\n  text-align: left;\n  text-transform: uppercase;\n  margin: 20px 0;\n  font-weight: bold;\n  color: #1c72fd; }\n.navbar.bg-dark {\n  background: #1c72fd !important; }\n.boxConteudo {\n  background: white;\n  padding: 20px;\n  border-radius: 20px;\n  position: relative;\n  box-shadow: 0px 0px 10px #e4e4e4; }\n.boxConteudo label {\n  text-transform: uppercase;\n  font-size: 12px; }\n/*.boxConteudo .form-control {\r\n        height: calc(1.5em + .75rem + 14px);\r\n        border-radius: 100px;\r\n    }*/\n.boxConteudo .form-control {\n  height: calc(1.5em + .75rem + 14px);\n  border-radius: 100px; }\n/*body.login {\r\n        --background: url('src/assets/img/bg-login.jpg') !important;\r\n        background-size: cover;\r\n        height: inherit;\r\n    }*/\n.position {\n  position: absolute;\n  top: 6px;\n  left: 10px; }\n.logoLogin img {\n  margin: 30px auto;\n  display: block;\n  width: 30%; }\n.login .boxConteudo .form-control {\n  font-size: 13px;\n  text-align: center; }\n.login .boxConteudo {\n  background: none;\n  box-shadow: none;\n  margin-top: 40%; }\n.login button.btnLogin {\n  background: #b1ff49;\n  border: none;\n  color: #002752;\n  font-weight: normal; }\n.login button.btnLogin:active {\n  background-color: #99dd3e !important;\n  border-color: #99dd3e !important;\n  color: #002752 !important; }\ninput#inputLogin, input#inputSenha {\n  padding-right: 45px; }\n.login input[type=text]:focus, .login input[type=password]:focus {\n  color: #1c72fd; }\n.login .form-group {\n  position: relative; }\n.login .form-group svg {\n  position: absolute;\n  left: 20px;\n  top: 11px;\n  font-size: 20px; }\n.linkEsqueciSenha a {\n  color: white;\n  text-transform: uppercase;\n  font-size: 12px;\n  display: block;\n  text-align: center;\n  margin-top: 10px; }\n.linkCadastrar a {\n  color: #b1ff49;\n  text-transform: uppercase;\n  font-size: 12px;\n  display: block;\n  text-align: center;\n  margin-top: 10px; }\n/*.buttonArea {\r\n    margin: 20px -20px 0px -20px;\r\n    background: #101a4c;\r\n    width: 100%;\r\n    display: block;\r\n    position: absolute;\r\n    z-index: 0;\r\n    bottom: -50px;\r\n    padding: 10px;\r\n    border-radius: 0 0 20px 20px;\r\n    }*/\n.buttonArea button {\n  width: 100%; }\n.buttonArea svg {\n  font-size: 25px;\n  margin-right: 5px; }\nbutton.navbar-toggler {\n  border-radius: 100px;\n  height: 55px !important;\n  background: #ffffff;\n  border: #92f328 solid 0px;\n  outline: none; }\n.boxConteudo .btn {\n  font-weight: bold; }\n.mapa iframe {\n  border: 0;\n  width: 100%;\n  height: 100vh;\n  position: absolute;\n  top: 0px;\n  z-index: -1; }\n.saudacao {\n  text-align: center; }\n.homePassageiro .boxConteudo {\n  position: relative;\n  top: 30px;\n  width: calc(100% - 0px);\n  margin: 0 auto;\n  padding: 15px 5px; }\n.homePassageiro .buttonArea button.rounded-pill {\n  font-size: 18px; }\nbutton.escolherDestino {\n  width: calc(100% - 60px);\n  float: left; }\nbutton.embarqueRapido {\n  float: right;\n  padding: 0px 12px;\n  width: 55px;\n  height: 55px; }\nbutton.embarqueRapido svg {\n  width: 35px;\n  height: 35x;\n  color: white; }\nbutton.btn.btn-primary.btn-lg.rounded-pill.embarqueRapido {\n  background: #1c72fd;\n  border: none; }\n.formDestino {\n  border: none;\n  margin-top: 5px; }\n.endDestinoRecente {\n  font-size: 11px;\n  background: #eee;\n  padding: 14px;\n  border-radius: 5px; }\n.endDestinoRecente b {\n  font-weight: normal;\n  color: darkgray; }\n.tituloDestinoRecente {\n  text-transform: uppercase;\n  font-weight: bold; }\n.bEntrar {\n  background: #1765e2;\n  padding: 20px;\n  border-radius: 100%;\n  position: relative;\n  box-shadow: 0px 0px 10px #e4e4e4; }\na.btnEditar {\n  float: right; }\n.labelCadastrado {\n  float: left;\n  text-transform: uppercase;\n  font-size: 13px; }\n.valorCadastrado {\n  clear: both;\n  color: #1c72fd;\n  font-size: 18px; }\na.btnEditar {\n  float: right;\n  background: #fff949;\n  color: #9a9615;\n  padding: 2px 10px;\n  border-radius: 20px;\n  font-size: 13px; }\na.btn.btnNeutro {\n  font-size: 14px;\n  width: 100%;\n  font-weight: normal; }\na.btn.btnNeutro svg {\n  margin-top: -3px;\n  margin-right: 5px; }\n.textLogin {\n  color: white;\n  text-transform: uppercase;\n  font-size: 12px;\n  display: block;\n  text-align: center;\n  margin-top: 10px;\n  padding-bottom: 5px; }\n/*:host {\r\n    ion-content {\r\n        --background: linear-gradient(135deg, var(--ion-color-light), var(--ion-color-light));\r\n    }\r\n}\r\n\r\n.paz {\r\n    position: relative;\r\n    z-index: 10;\r\n}*/\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbG9naW4vQzpcXFVzZXJzXFxqZXNzaVxcRG9jdW1lbnRzXFxiaXRidWNrZXRcXHVhaUxldmEvc3JjXFxhcHBcXHBhZ2VzXFxsb2dpblxcbG9naW4ucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsNEdBQVk7QUFFWjtFQUVJLDRDQUFhLEVBQUE7QUFHakI7RUFDSTtJQUVJLG1CQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLGVBQWUsRUFBQTtFQUduQjtJQUNJLGVBQWU7SUFDZixRQUFRO0lBQ1IsT0FBTztJQUNQLGtCQUFrQjtJQUNsQixtQkFBbUI7SUFDbkIsb0JBQW9CO0lBQ3BCLFVBQVU7SUFDVixZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLHlCQUF5QjtJQUN6QixhQUFhO0lBQ2IsOEJBQThCLEVBQUE7RUFHbEM7SUFDQSx1QkFBdUI7SUFDdkIsaUJBQWlCO0lBQ2pCLGVBQWUsRUFBQTtFQUdmO0lBQ0EsZUFBZTtJQUNmLGtCQUFrQixFQUFBO0VBR2xCO0lBQ0ksbUJBQW1CLEVBQUE7RUFPdkI7SUFDSSxVQUFVO0lBQ1YsMEJBQTBCLEVBQUE7RUFHOUI7SUFDSSxlQUFlO0lBQ2YsT0FBTztJQUNQLGtDQUFrQyxFQUFBO0VBR3RDO0lBQ0ksa0NBQWtDLEVBQUEsRUFDckM7QUFJRjtFQUNDLG9EQUFtRTtFQUNuRSxnQ0FBZ0M7RUFDaEMsZ0NBQWdDO0VBQ2hDLHdCQUF3QjtFQUN4QixhQUFhLEVBQUE7QUFHYjtFQUNBLGlCQUFpQjtFQUNqQixrQkFBa0I7RUFDbEIseUJBQXlCO0VBQ3pCLGNBQWM7RUFDZCxpQkFBaUI7RUFDakIsY0FBYyxFQUFBO0FBR2Q7RUFDSSxrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLHlCQUF5QjtFQUN6QixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGNBQWMsRUFBQTtBQUdsQjtFQUNJLDhCQUE0QixFQUFBO0FBR2hDO0VBQ0ksaUJBQWlCO0VBQ2pCLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLGdDQUFnQyxFQUFBO0FBR3BDO0VBQ0EseUJBQXlCO0VBQ3pCLGVBQWUsRUFBQTtBQUdmOzs7TUM1QkU7QURpQ0Y7RUFDSSxtQ0FBbUM7RUFDbkMsb0JBQW9CLEVBQUE7QUFJeEI7Ozs7TUM5QkU7QURvQ0Y7RUFDSSxrQkFBa0I7RUFDbEIsUUFBUTtFQUNSLFVBQVUsRUFBQTtBQUdkO0VBQ0ksaUJBQWlCO0VBQ2pCLGNBQWM7RUFDZCxVQUFVLEVBQUE7QUFHZDtFQUNJLGVBQWU7RUFDZixrQkFBa0IsRUFBQTtBQUd0QjtFQUNJLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsZUFBZSxFQUFBO0FBR25CO0VBQ0ksbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixjQUFjO0VBQ2QsbUJBQW1CLEVBQUE7QUFHdkI7RUFDSSxvQ0FBb0M7RUFDcEMsZ0NBQWdDO0VBQ2hDLHlCQUF5QixFQUFBO0FBRzdCO0VBQ0ksbUJBQW1CLEVBQUE7QUFHdkI7RUFDSSxjQUFjLEVBQUE7QUFHbEI7RUFDSSxrQkFBa0IsRUFBQTtBQUd0QjtFQUNJLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsU0FBUztFQUNULGVBQWUsRUFBQTtBQUduQjtFQUNJLFlBQVk7RUFDWix5QkFBeUI7RUFDekIsZUFBZTtFQUNmLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsZ0JBQWdCLEVBQUE7QUFHcEI7RUFDSSxjQUFjO0VBQ2QseUJBQXlCO0VBQ3pCLGVBQWU7RUFDZixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGdCQUFnQixFQUFBO0FBR3BCOzs7Ozs7Ozs7O01DckNFO0FEaURGO0VBQ0ksV0FBVSxFQUFBO0FBR2Q7RUFDQSxlQUFlO0VBQ2YsaUJBQWlCLEVBQUE7QUFHakI7RUFDQSxvQkFBb0I7RUFDcEIsdUJBQXVCO0VBQ3ZCLG1CQUFtQjtFQUNuQix5QkFBeUI7RUFDekIsYUFBYSxFQUFBO0FBR2I7RUFDQSxpQkFBaUIsRUFBQTtBQUdqQjtFQUNJLFNBQVM7RUFDVCxXQUFXO0VBQ1gsYUFBWTtFQUNaLGtCQUFrQjtFQUNsQixRQUFRO0VBQ1IsV0FBVyxFQUFBO0FBR2Y7RUFDSSxrQkFBa0IsRUFBQTtBQUd0QjtFQUNJLGtCQUFrQjtFQUNsQixTQUFTO0VBQ1QsdUJBQXVCO0VBQ3ZCLGNBQWM7RUFDZCxpQkFBaUIsRUFBQTtBQUdyQjtFQUNJLGVBQWUsRUFBQTtBQUduQjtFQUNJLHdCQUF3QjtFQUN4QixXQUFXLEVBQUE7QUFHZjtFQUNJLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsV0FBVztFQUNYLFlBQVksRUFBQTtBQUdoQjtFQUNJLFdBQVc7RUFDWCxXQUFXO0VBQ1gsWUFBWSxFQUFBO0FBR2hCO0VBQ0ksbUJBQW1CO0VBQ25CLFlBQVksRUFBQTtBQUdoQjtFQUNJLFlBQVk7RUFDWixlQUFlLEVBQUE7QUFHbkI7RUFDSSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYixrQkFBa0IsRUFBQTtBQUd0QjtFQUNJLG1CQUFtQjtFQUNuQixlQUFlLEVBQUE7QUFHbkI7RUFDSSx5QkFBeUI7RUFDekIsaUJBQWlCLEVBQUE7QUFHckI7RUFFSSxtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsZ0NBQWdDLEVBQUE7QUFHeEM7RUFDSSxZQUFZLEVBQUE7QUFHaEI7RUFDSSxXQUFXO0VBQ1gseUJBQXlCO0VBQ3pCLGVBQWUsRUFBQTtBQUduQjtFQUNJLFdBQVc7RUFDWCxjQUFjO0VBQ2QsZUFBZSxFQUFBO0FBR25CO0VBQ0ksWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixlQUFlLEVBQUE7QUFHbkI7RUFDSSxlQUFlO0VBQ2YsV0FBVztFQUNYLG1CQUFtQixFQUFBO0FBR3ZCO0VBQ0ksZ0JBQWdCO0VBQ2hCLGlCQUFpQixFQUFBO0FBR2pCO0VBQ0ksWUFBWTtFQUNaLHlCQUF5QjtFQUN6QixlQUFlO0VBQ2YsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsbUJBQW1CLEVBQUE7QUFHM0I7Ozs7Ozs7OztFQ2hFRSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBpbXBvcnQgdXJsKCdodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2NzczI/ZmFtaWx5PVJ1YmlrOml0YWwsd2dodEAwLDQwMDswLDUwMDsxLDQwMDsxLDUwMCZkaXNwbGF5PXN3YXAnKTtcclxuXHJcbi5sb2dpbiB7XHJcbiAgICAvLy0tYmFja2dyb3VuZDogdXJsKCdzcmMvYXNzZXRzL2ltZy9iZy11YWktbGV2YTIucG5nJykgI2VhZWFlYSBuby1yZXBlYXQgYm90dG9tIDc1cHggbGVmdCAzNXB4O1xyXG4gICAgLS1iYWNrZ3JvdW5kOiB1cmwoXCJzcmMvYXNzZXRzL2ltZy9iZy1sb2dpbi5qcGdcIikgIWltcG9ydGFudDtcclxuIFxyXG59XHJcbkBtZWRpYSAobWF4LXdpZHRoOiA5OTJweCkge1xyXG4gICAgaW9uLWNvbnRlbnQge1xyXG4gICAgICAgIC8vLS1iYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hc3NldHMvaW1nL2JnLXVhaS1sZXZhMi5wbmcnKSAjZWFlYWVhIG5vLXJlcGVhdCBib3R0b20gNzVweCBsZWZ0IDM1cHg7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgICAgIGhlaWdodDogaW5oZXJpdDtcclxuICAgIH1cclxuXHJcbiAgICAubmF2YmFyLWNvbGxhcHNlIHtcclxuICAgICAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAgICAgdG9wOiAwcHg7XHJcbiAgICAgICAgbGVmdDogMDtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDE1cHg7XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDogMTVweDtcclxuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMTVweDtcclxuICAgICAgICB3aWR0aDogNzUlO1xyXG4gICAgICAgIGhlaWdodDogMTAwJTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMTc2NWUyO1xyXG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICAgICAgei1pbmRleDogOTk5OTtcclxuICAgICAgICBib3gtc2hhZG93OiAwcHggMHB4IDkwcHggYmxhY2s7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5uYXZiYXItY29sbGFwc2UgYS5uYXYtbGluayB7XHJcbiAgICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgZm9udC1zaXplOiAxOXB4O1xyXG4gICAgfVxyXG5cclxuICAgIGEubmF2LWxpbmsgc3ZnIHtcclxuICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLm5hdmJhci1kYXJrIC5uYXZiYXItdG9nZ2xlciB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzFiMmM3YjtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLy8ubmF2YmFyLWRhcmsgc3Bhbi5uYXZiYXItdG9nZ2xlci1pY29uIHtcclxuICAgIC8vL2JhY2tncm91bmQtaW1hZ2U6IHVybChkYXRhOmltYWdlL3N2Zyt4bWwsJTNjc3ZnIHhtbG5zPSdodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2Zycgd2lkdGg9JzMwJyBoZWlnaHQ9JzMwJyB2aWV3Qm94PScwIDAgMzAgMzAnJTNlJTNjcGF0aCBzdHJva2U9J3JnYmElMjgyNTUsIDI1NSwgMjU1LCAwLjklMjknIHN0cm9rZS1saW5lY2FwPSdyb3VuZCcgc3Ryb2tlLW1pdGVybGltaXQ9JzEwJyBzdHJva2Utd2lkdGg9JzInIGQ9J000IDdoMjJNNCAxNWgyMk00IDIzaDIyJy8lM2UlM2Mvc3ZnJTNlKTtcclxuICAgIC8vfVxyXG5cclxuICAgIC5uYXZiYXItY29sbGFwc2UuY29sbGFwc2luZyB7XHJcbiAgICAgICAgbGVmdDogLTc1JTtcclxuICAgICAgICB0cmFuc2l0aW9uOiBoZWlnaHQgMHMgZWFzZTtcclxuICAgIH1cclxuXHJcbiAgICAubmF2YmFyLWNvbGxhcHNlLnNob3cge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDBweDtcclxuICAgICAgICBsZWZ0OiAwO1xyXG4gICAgICAgIHRyYW5zaXRpb246IGxlZnQgMzAwbXMgZWFzZS1pbi1vdXQ7XHJcbiAgICB9XHJcblxyXG4gICAgLm5hdmJhci10b2dnbGVyLmNvbGxhcHNlZCB+IC5uYXZiYXItY29sbGFwc2Uge1xyXG4gICAgICAgIHRyYW5zaXRpb246IGxlZnQgNTAwbXMgZWFzZS1pbi1vdXQ7XHJcbiAgICB9XHJcbiAgICBcclxufVxyXG5cclxuICAgYm9keSB7XHJcbiAgICBiYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hc3NldHMvaW1nL2JnLXVhaS1sZXZhLnBuZycpIG5vLXJlcGVhdCAjZWFlYWVhO1xyXG4gICAgZm9udC1mYW1pbHk6ICdSdWJpaycsIHNhbnMtc2VyaWY7XHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA4MHB4IGJvdHRvbTtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIGhlaWdodDogMTAwdmg7XHJcbn1cclxuXHJcbiAgICBoMSB7XHJcbiAgICBmb250LXNpemU6IDEuNXJlbTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBtYXJnaW46IDIwcHggMDtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgY29sb3I6ICMxMjNiN2Q7XHJcbiAgICB9XHJcblxyXG4gICAgaDMge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMC43NXJlbTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICAgICAgbWFyZ2luOiAyMHB4IDA7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICAgICAgY29sb3I6ICMxYzcyZmQ7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5uYXZiYXIuYmctZGFyayB7XHJcbiAgICAgICAgYmFja2dyb3VuZDojMWM3MmZkIWltcG9ydGFudFxyXG4gICAgfVxyXG5cclxuICAgIC5ib3hDb250ZXVkbyB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBib3gtc2hhZG93OiAwcHggMHB4IDEwcHggI2U0ZTRlNDtcclxuICAgIH1cclxuXHJcbiAgICAuYm94Q29udGV1ZG8gbGFiZWwge1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIH1cclxuXHJcbiAgICAvKi5ib3hDb250ZXVkbyAuZm9ybS1jb250cm9sIHtcclxuICAgICAgICBoZWlnaHQ6IGNhbGMoMS41ZW0gKyAuNzVyZW0gKyAxNHB4KTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMDBweDtcclxuICAgIH0qL1xyXG5cclxuICAgIC5ib3hDb250ZXVkbyAuZm9ybS1jb250cm9sIHtcclxuICAgICAgICBoZWlnaHQ6IGNhbGMoMS41ZW0gKyAuNzVyZW0gKyAxNHB4KTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMDBweDtcclxuICAgIH1cclxuXHJcbiAgICAvL2lvbi1jb250ZW50XHJcbiAgICAvKmJvZHkubG9naW4ge1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogdXJsKCdzcmMvYXNzZXRzL2ltZy9iZy1sb2dpbi5qcGcnKSAhaW1wb3J0YW50O1xyXG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICAgICAgaGVpZ2h0OiBpbmhlcml0O1xyXG4gICAgfSovXHJcblxyXG4gICAgLnBvc2l0aW9uIHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgdG9wOiA2cHg7XHJcbiAgICAgICAgbGVmdDogMTBweDtcclxuICAgIH1cclxuXHJcbiAgICAubG9nb0xvZ2luIGltZyB7XHJcbiAgICAgICAgbWFyZ2luOiAzMHB4IGF1dG87XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgd2lkdGg6IDMwJTtcclxuICAgIH1cclxuXHJcbiAgICAubG9naW4gLmJveENvbnRldWRvIC5mb3JtLWNvbnRyb2wge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgLmxvZ2luIC5ib3hDb250ZXVkbyB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogbm9uZTtcclxuICAgICAgICBib3gtc2hhZG93OiBub25lO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDQwJTtcclxuICAgIH1cclxuXHJcbiAgICAubG9naW4gYnV0dG9uLmJ0bkxvZ2luIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjYjFmZjQ5O1xyXG4gICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgICAgICBjb2xvcjogIzAwMjc1MjtcclxuICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgfVxyXG5cclxuICAgIC5sb2dpbiBidXR0b24uYnRuTG9naW46YWN0aXZlIHtcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjOTlkZDNlICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgYm9yZGVyLWNvbG9yOiAjOTlkZDNlICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgY29sb3I6ICMwMDI3NTIgIWltcG9ydGFudDtcclxuICAgIH1cclxuXHJcbiAgICBpbnB1dCNpbnB1dExvZ2luLCBpbnB1dCNpbnB1dFNlbmhhIHtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiA0NXB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5sb2dpbiBpbnB1dFt0eXBlPXRleHRdOmZvY3VzLCAubG9naW4gaW5wdXRbdHlwZT1wYXNzd29yZF06Zm9jdXMge1xyXG4gICAgICAgIGNvbG9yOiAjMWM3MmZkO1xyXG4gICAgfVxyXG5cclxuICAgIC5sb2dpbiAuZm9ybS1ncm91cCB7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgfVxyXG5cclxuICAgIC5sb2dpbiAuZm9ybS1ncm91cCBzdmcge1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICBsZWZ0OiAyMHB4O1xyXG4gICAgICAgIHRvcDogMTFweDtcclxuICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLmxpbmtFc3F1ZWNpU2VuaGEgYSB7XHJcbiAgICAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5saW5rQ2FkYXN0cmFyIGEge1xyXG4gICAgICAgIGNvbG9yOiAjYjFmZjQ5O1xyXG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAvKi5idXR0b25BcmVhIHtcclxuICAgIG1hcmdpbjogMjBweCAtMjBweCAwcHggLTIwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMTAxYTRjO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgICBib3R0b206IC01MHB4O1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDAgMCAyMHB4IDIwcHg7XHJcbiAgICB9Ki9cclxuICAgIFxyXG4gICAgLmJ1dHRvbkFyZWEgYnV0dG9uIHtcclxuICAgICAgICB3aWR0aDoxMDAlO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAuYnV0dG9uQXJlYSBzdmcge1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIGJ1dHRvbi5uYXZiYXItdG9nZ2xlciB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMDBweDtcclxuICAgIGhlaWdodDogNTVweCAhaW1wb3J0YW50O1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZmZmZjtcclxuICAgIGJvcmRlcjogIzkyZjMyOCBzb2xpZCAwcHg7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAuYm94Q29udGV1ZG8gLmJ0biB7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIH1cclxuXHJcbiAgICAubWFwYSBpZnJhbWUge1xyXG4gICAgICAgIGJvcmRlcjogMDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6MTAwdmg7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHRvcDogMHB4O1xyXG4gICAgICAgIHotaW5kZXg6IC0xO1xyXG4gICAgfVxyXG5cclxuICAgIC5zYXVkYWNhbyB7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG5cclxuICAgIC5ob21lUGFzc2FnZWlybyAuYm94Q29udGV1ZG8ge1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICB0b3A6IDMwcHg7XHJcbiAgICAgICAgd2lkdGg6IGNhbGMoMTAwJSAtIDBweCk7XHJcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgICAgcGFkZGluZzogMTVweCA1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgLmhvbWVQYXNzYWdlaXJvIC5idXR0b25BcmVhIGJ1dHRvbi5yb3VuZGVkLXBpbGwge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIH1cclxuXHJcbiAgICBidXR0b24uZXNjb2xoZXJEZXN0aW5vIHtcclxuICAgICAgICB3aWR0aDogY2FsYygxMDAlIC0gNjBweCk7XHJcbiAgICAgICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICB9XHJcblxyXG4gICAgYnV0dG9uLmVtYmFycXVlUmFwaWRvIHtcclxuICAgICAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICAgICAgcGFkZGluZzogMHB4IDEycHg7XHJcbiAgICAgICAgd2lkdGg6IDU1cHg7XHJcbiAgICAgICAgaGVpZ2h0OiA1NXB4O1xyXG4gICAgfVxyXG5cclxuICAgIGJ1dHRvbi5lbWJhcnF1ZVJhcGlkbyBzdmcge1xyXG4gICAgICAgIHdpZHRoOiAzNXB4O1xyXG4gICAgICAgIGhlaWdodDogMzV4O1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIH1cclxuXHJcbiAgICBidXR0b24uYnRuLmJ0bi1wcmltYXJ5LmJ0bi1sZy5yb3VuZGVkLXBpbGwuZW1iYXJxdWVSYXBpZG8ge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICMxYzcyZmQ7XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgfVxyXG5cclxuICAgIC5mb3JtRGVzdGlubyB7XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDVweDtcclxuICAgIH1cclxuXHJcbiAgICAuZW5kRGVzdGlub1JlY2VudGUge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTFweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjZWVlO1xyXG4gICAgICAgIHBhZGRpbmc6IDE0cHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5lbmREZXN0aW5vUmVjZW50ZSBiIHtcclxuICAgICAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgICAgIGNvbG9yOiBkYXJrZ3JheTtcclxuICAgIH1cclxuXHJcbiAgICAudGl0dWxvRGVzdGlub1JlY2VudGUge1xyXG4gICAgICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB9XHJcblxyXG4gICAgLmJFbnRyYXIge1xyXG4gICAgICAgIC8vcG9zaXRpb246IGZpeGVkO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICMxNzY1ZTI7XHJcbiAgICAgICAgcGFkZGluZzogMjBweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBib3gtc2hhZG93OiAwcHggMHB4IDEwcHggI2U0ZTRlNDtcclxuICAgIH1cclxuXHJcbmEuYnRuRWRpdGFyIHtcclxuICAgIGZsb2F0OiByaWdodDtcclxufVxyXG5cclxuLmxhYmVsQ2FkYXN0cmFkbyB7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbn1cclxuXHJcbi52YWxvckNhZGFzdHJhZG8ge1xyXG4gICAgY2xlYXI6IGJvdGg7XHJcbiAgICBjb2xvcjogIzFjNzJmZDtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxufVxyXG5cclxuYS5idG5FZGl0YXIge1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjk0OTtcclxuICAgIGNvbG9yOiAjOWE5NjE1O1xyXG4gICAgcGFkZGluZzogMnB4IDEwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG59XHJcblxyXG5hLmJ0bi5idG5OZXV0cm8ge1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG59XHJcblxyXG5hLmJ0bi5idG5OZXV0cm8gc3ZnIHtcclxuICAgIG1hcmdpbi10b3A6IC0zcHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDVweDtcclxufVxyXG5cclxuICAgIC50ZXh0TG9naW4ge1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xyXG4gICAgfVxyXG5cclxuLyo6aG9zdCB7XHJcbiAgICBpb24tY29udGVudCB7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMTM1ZGVnLCB2YXIoLS1pb24tY29sb3ItbGlnaHQpLCB2YXIoLS1pb24tY29sb3ItbGlnaHQpKTtcclxuICAgIH1cclxufVxyXG5cclxuLnBheiB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB6LWluZGV4OiAxMDtcclxufSovIiwiQGltcG9ydCB1cmwoXCJodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2NzczI/ZmFtaWx5PVJ1YmlrOml0YWwsd2dodEAwLDQwMDswLDUwMDsxLDQwMDsxLDUwMCZkaXNwbGF5PXN3YXBcIik7XG4ubG9naW4ge1xuICAtLWJhY2tncm91bmQ6IHVybChcInNyYy9hc3NldHMvaW1nL2JnLWxvZ2luLmpwZ1wiKSAhaW1wb3J0YW50OyB9XG5cbkBtZWRpYSAobWF4LXdpZHRoOiA5OTJweCkge1xuICBpb24tY29udGVudCB7XG4gICAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAgIGhlaWdodDogaW5oZXJpdDsgfVxuICAubmF2YmFyLWNvbGxhcHNlIHtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgdG9wOiAwcHg7XG4gICAgbGVmdDogMDtcbiAgICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gICAgcGFkZGluZy1yaWdodDogMTVweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgICB3aWR0aDogNzUlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBiYWNrZ3JvdW5kOiAjMTc2NWUyO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgei1pbmRleDogOTk5OTtcbiAgICBib3gtc2hhZG93OiAwcHggMHB4IDkwcHggYmxhY2s7IH1cbiAgLm5hdmJhci1jb2xsYXBzZSBhLm5hdi1saW5rIHtcbiAgICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDE5cHg7IH1cbiAgYS5uYXYtbGluayBzdmcge1xuICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7IH1cbiAgLm5hdmJhci1kYXJrIC5uYXZiYXItdG9nZ2xlciB7XG4gICAgYmFja2dyb3VuZDogIzFiMmM3YjsgfVxuICAubmF2YmFyLWNvbGxhcHNlLmNvbGxhcHNpbmcge1xuICAgIGxlZnQ6IC03NSU7XG4gICAgdHJhbnNpdGlvbjogaGVpZ2h0IDBzIGVhc2U7IH1cbiAgLm5hdmJhci1jb2xsYXBzZS5zaG93IHtcbiAgICBtYXJnaW4tdG9wOiAwcHg7XG4gICAgbGVmdDogMDtcbiAgICB0cmFuc2l0aW9uOiBsZWZ0IDMwMG1zIGVhc2UtaW4tb3V0OyB9XG4gIC5uYXZiYXItdG9nZ2xlci5jb2xsYXBzZWQgfiAubmF2YmFyLWNvbGxhcHNlIHtcbiAgICB0cmFuc2l0aW9uOiBsZWZ0IDUwMG1zIGVhc2UtaW4tb3V0OyB9IH1cblxuYm9keSB7XG4gIGJhY2tncm91bmQ6IHVybChcInNyYy9hc3NldHMvaW1nL2JnLXVhaS1sZXZhLnBuZ1wiKSBuby1yZXBlYXQgI2VhZWFlYTtcbiAgZm9udC1mYW1pbHk6ICdSdWJpaycsIHNhbnMtc2VyaWY7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IDgwcHggYm90dG9tO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gIGhlaWdodDogMTAwdmg7IH1cblxuaDEge1xuICBmb250LXNpemU6IDEuNXJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBtYXJnaW46IDIwcHggMDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjMTIzYjdkOyB9XG5cbmgzIHtcbiAgZm9udC1zaXplOiAwLjc1cmVtO1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBtYXJnaW46IDIwcHggMDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjMWM3MmZkOyB9XG5cbi5uYXZiYXIuYmctZGFyayB7XG4gIGJhY2tncm91bmQ6ICMxYzcyZmQgIWltcG9ydGFudDsgfVxuXG4uYm94Q29udGV1ZG8ge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgcGFkZGluZzogMjBweDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBib3gtc2hhZG93OiAwcHggMHB4IDEwcHggI2U0ZTRlNDsgfVxuXG4uYm94Q29udGV1ZG8gbGFiZWwge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXNpemU6IDEycHg7IH1cblxuLyouYm94Q29udGV1ZG8gLmZvcm0tY29udHJvbCB7XHJcbiAgICAgICAgaGVpZ2h0OiBjYWxjKDEuNWVtICsgLjc1cmVtICsgMTRweCk7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTAwcHg7XHJcbiAgICB9Ki9cbi5ib3hDb250ZXVkbyAuZm9ybS1jb250cm9sIHtcbiAgaGVpZ2h0OiBjYWxjKDEuNWVtICsgLjc1cmVtICsgMTRweCk7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4OyB9XG5cbi8qYm9keS5sb2dpbiB7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB1cmwoJ3NyYy9hc3NldHMvaW1nL2JnLWxvZ2luLmpwZycpICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgICAgICBoZWlnaHQ6IGluaGVyaXQ7XHJcbiAgICB9Ki9cbi5wb3NpdGlvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA2cHg7XG4gIGxlZnQ6IDEwcHg7IH1cblxuLmxvZ29Mb2dpbiBpbWcge1xuICBtYXJnaW46IDMwcHggYXV0bztcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiAzMCU7IH1cblxuLmxvZ2luIC5ib3hDb250ZXVkbyAuZm9ybS1jb250cm9sIHtcbiAgZm9udC1zaXplOiAxM3B4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7IH1cblxuLmxvZ2luIC5ib3hDb250ZXVkbyB7XG4gIGJhY2tncm91bmQ6IG5vbmU7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG4gIG1hcmdpbi10b3A6IDQwJTsgfVxuXG4ubG9naW4gYnV0dG9uLmJ0bkxvZ2luIHtcbiAgYmFja2dyb3VuZDogI2IxZmY0OTtcbiAgYm9yZGVyOiBub25lO1xuICBjb2xvcjogIzAwMjc1MjtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDsgfVxuXG4ubG9naW4gYnV0dG9uLmJ0bkxvZ2luOmFjdGl2ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM5OWRkM2UgIWltcG9ydGFudDtcbiAgYm9yZGVyLWNvbG9yOiAjOTlkZDNlICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjMDAyNzUyICFpbXBvcnRhbnQ7IH1cblxuaW5wdXQjaW5wdXRMb2dpbiwgaW5wdXQjaW5wdXRTZW5oYSB7XG4gIHBhZGRpbmctcmlnaHQ6IDQ1cHg7IH1cblxuLmxvZ2luIGlucHV0W3R5cGU9dGV4dF06Zm9jdXMsIC5sb2dpbiBpbnB1dFt0eXBlPXBhc3N3b3JkXTpmb2N1cyB7XG4gIGNvbG9yOiAjMWM3MmZkOyB9XG5cbi5sb2dpbiAuZm9ybS1ncm91cCB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTsgfVxuXG4ubG9naW4gLmZvcm0tZ3JvdXAgc3ZnIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAyMHB4O1xuICB0b3A6IDExcHg7XG4gIGZvbnQtc2l6ZTogMjBweDsgfVxuXG4ubGlua0VzcXVlY2lTZW5oYSBhIHtcbiAgY29sb3I6IHdoaXRlO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXNpemU6IDEycHg7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IDEwcHg7IH1cblxuLmxpbmtDYWRhc3RyYXIgYSB7XG4gIGNvbG9yOiAjYjFmZjQ5O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXNpemU6IDEycHg7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IDEwcHg7IH1cblxuLyouYnV0dG9uQXJlYSB7XHJcbiAgICBtYXJnaW46IDIwcHggLTIwcHggMHB4IC0yMHB4O1xyXG4gICAgYmFja2dyb3VuZDogIzEwMWE0YztcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB6LWluZGV4OiAwO1xyXG4gICAgYm90dG9tOiAtNTBweDtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwIDAgMjBweCAyMHB4O1xyXG4gICAgfSovXG4uYnV0dG9uQXJlYSBidXR0b24ge1xuICB3aWR0aDogMTAwJTsgfVxuXG4uYnV0dG9uQXJlYSBzdmcge1xuICBmb250LXNpemU6IDI1cHg7XG4gIG1hcmdpbi1yaWdodDogNXB4OyB9XG5cbmJ1dHRvbi5uYXZiYXItdG9nZ2xlciB7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xuICBoZWlnaHQ6IDU1cHggIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgYm9yZGVyOiAjOTJmMzI4IHNvbGlkIDBweDtcbiAgb3V0bGluZTogbm9uZTsgfVxuXG4uYm94Q29udGV1ZG8gLmJ0biB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkOyB9XG5cbi5tYXBhIGlmcmFtZSB7XG4gIGJvcmRlcjogMDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwdmg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwcHg7XG4gIHotaW5kZXg6IC0xOyB9XG5cbi5zYXVkYWNhbyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjsgfVxuXG4uaG9tZVBhc3NhZ2Vpcm8gLmJveENvbnRldWRvIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0b3A6IDMwcHg7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSAwcHgpO1xuICBtYXJnaW46IDAgYXV0bztcbiAgcGFkZGluZzogMTVweCA1cHg7IH1cblxuLmhvbWVQYXNzYWdlaXJvIC5idXR0b25BcmVhIGJ1dHRvbi5yb3VuZGVkLXBpbGwge1xuICBmb250LXNpemU6IDE4cHg7IH1cblxuYnV0dG9uLmVzY29saGVyRGVzdGlubyB7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSA2MHB4KTtcbiAgZmxvYXQ6IGxlZnQ7IH1cblxuYnV0dG9uLmVtYmFycXVlUmFwaWRvIHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBwYWRkaW5nOiAwcHggMTJweDtcbiAgd2lkdGg6IDU1cHg7XG4gIGhlaWdodDogNTVweDsgfVxuXG5idXR0b24uZW1iYXJxdWVSYXBpZG8gc3ZnIHtcbiAgd2lkdGg6IDM1cHg7XG4gIGhlaWdodDogMzV4O1xuICBjb2xvcjogd2hpdGU7IH1cblxuYnV0dG9uLmJ0bi5idG4tcHJpbWFyeS5idG4tbGcucm91bmRlZC1waWxsLmVtYmFycXVlUmFwaWRvIHtcbiAgYmFja2dyb3VuZDogIzFjNzJmZDtcbiAgYm9yZGVyOiBub25lOyB9XG5cbi5mb3JtRGVzdGlubyB7XG4gIGJvcmRlcjogbm9uZTtcbiAgbWFyZ2luLXRvcDogNXB4OyB9XG5cbi5lbmREZXN0aW5vUmVjZW50ZSB7XG4gIGZvbnQtc2l6ZTogMTFweDtcbiAgYmFja2dyb3VuZDogI2VlZTtcbiAgcGFkZGluZzogMTRweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4OyB9XG5cbi5lbmREZXN0aW5vUmVjZW50ZSBiIHtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgY29sb3I6IGRhcmtncmF5OyB9XG5cbi50aXR1bG9EZXN0aW5vUmVjZW50ZSB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtd2VpZ2h0OiBib2xkOyB9XG5cbi5iRW50cmFyIHtcbiAgYmFja2dyb3VuZDogIzE3NjVlMjtcbiAgcGFkZGluZzogMjBweDtcbiAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBib3gtc2hhZG93OiAwcHggMHB4IDEwcHggI2U0ZTRlNDsgfVxuXG5hLmJ0bkVkaXRhciB7XG4gIGZsb2F0OiByaWdodDsgfVxuXG4ubGFiZWxDYWRhc3RyYWRvIHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtc2l6ZTogMTNweDsgfVxuXG4udmFsb3JDYWRhc3RyYWRvIHtcbiAgY2xlYXI6IGJvdGg7XG4gIGNvbG9yOiAjMWM3MmZkO1xuICBmb250LXNpemU6IDE4cHg7IH1cblxuYS5idG5FZGl0YXIge1xuICBmbG9hdDogcmlnaHQ7XG4gIGJhY2tncm91bmQ6ICNmZmY5NDk7XG4gIGNvbG9yOiAjOWE5NjE1O1xuICBwYWRkaW5nOiAycHggMTBweDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgZm9udC1zaXplOiAxM3B4OyB9XG5cbmEuYnRuLmJ0bk5ldXRybyB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7IH1cblxuYS5idG4uYnRuTmV1dHJvIHN2ZyB7XG4gIG1hcmdpbi10b3A6IC0zcHg7XG4gIG1hcmdpbi1yaWdodDogNXB4OyB9XG5cbi50ZXh0TG9naW4ge1xuICBjb2xvcjogd2hpdGU7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtc2l6ZTogMTJweDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgcGFkZGluZy1ib3R0b206IDVweDsgfVxuXG4vKjpob3N0IHtcclxuICAgIGlvbi1jb250ZW50IHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgxMzVkZWcsIHZhcigtLWlvbi1jb2xvci1saWdodCksIHZhcigtLWlvbi1jb2xvci1saWdodCkpO1xyXG4gICAgfVxyXG59XHJcblxyXG4ucGF6IHtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHotaW5kZXg6IDEwO1xyXG59Ki9cbiJdfQ== */"

/***/ }),

/***/ "./src/app/pages/login/login.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/login/login.page.ts ***!
  \*******************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_core_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/auth/auth.service */ "./src/app/core/auth/auth.service.ts");
/* harmony import */ var _recuperarsenha_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./recuperarsenha.service */ "./src/app/pages/login/recuperarsenha.service.ts");
/* harmony import */ var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/facebook/ngx */ "./node_modules/@ionic-native/facebook/ngx/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, menuCtrl, toastCtrl, alertCtrl, loadingCtrl, recuperarSenha, router, formBuilder, authService, fb) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.recuperarSenha = recuperarSenha;
        this.router = router;
        this.formBuilder = formBuilder;
        this.authService = authService;
        this.fb = fb;
        this.users = [];
        this.user = {};
    }
    LoginPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(false);
    };
    LoginPage.prototype.ngOnInit = function () {
        this.onLoginForm = this.formBuilder.group({
            'phone': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])],
            'password': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])]
        });
    };
    LoginPage.prototype.forgotPass = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Esqueceu sua senha?',
                            message: 'Informe seu e-mail para enviarmos um novo link de recuperação.',
                            inputs: [
                                {
                                    name: 'email',
                                    type: 'email',
                                    placeholder: 'E-mail'
                                }
                            ],
                            buttons: [
                                {
                                    text: 'Cancelar',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                        console.log('Confirmar Cancelamento?');
                                    }
                                }, {
                                    text: 'Confirmar',
                                    handler: function (data) { return __awaiter(_this, void 0, void 0, function () {
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            console.log(data.email);
                                            this.recuperarSenha.sendemail(data.email)
                                                .subscribe(function () { return __awaiter(_this, void 0, void 0, function () {
                                                var toast;
                                                return __generator(this, function (_a) {
                                                    switch (_a.label) {
                                                        case 0:
                                                            this.navCtrl.navigateRoot('');
                                                            return [4 /*yield*/, this.toastCtrl.create({
                                                                    message: 'Email enviado com sucesso!',
                                                                    duration: 4000, position: 'top',
                                                                    color: 'success'
                                                                })];
                                                        case 1:
                                                            toast = _a.sent();
                                                            toast.present();
                                                            return [2 /*return*/];
                                                    }
                                                });
                                            }); }, function (err) { return __awaiter(_this, void 0, void 0, function () {
                                                var toast;
                                                return __generator(this, function (_a) {
                                                    switch (_a.label) {
                                                        case 0: return [4 /*yield*/, this.toastCtrl.create({
                                                                message: 'Usuário não cadastrado, por favor cadastre-se no APP!',
                                                                duration: 4000, position: 'top',
                                                                color: 'danger'
                                                            })];
                                                        case 1:
                                                            toast = _a.sent();
                                                            toast.present();
                                                            return [2 /*return*/];
                                                    }
                                                });
                                            }); });
                                            return [2 /*return*/];
                                        });
                                    }); }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    // // //
    LoginPage.prototype.goToRegister = function () {
        this.router.navigateByUrl('/register-driver');
    };
    LoginPage.prototype.goToHome = function () {
        var _this = this;
        var phone = this.onLoginForm.get('phone').value;
        var password = this.onLoginForm.get('password').value;
        console.log(phone);
        console.log(password);
        this.authService.authenticate(phone, password)
            .subscribe(function () { return _this.navCtrl.navigateRoot('/home'); }, function (err) { return __awaiter(_this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.onLoginForm.reset();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Usuário ou senha incorreto, por favor tente novamente!',
                                duration: 4000, position: 'top',
                                color: 'danger'
                            })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        }); });
    };
    LoginPage.prototype.loginFb = function () {
        var _this = this;
        this.fb.login(['public_profile', 'user_friends', 'email'])
            .then(function (res) {
            if (res.status === 'conected') {
                _this.user.img = 'http://graph.facebook.com/' + res.authResponse.userID + '/picture?type=square';
            }
            else {
                alert('Login failed');
            }
            console.log('Logged into Facebook!', res);
        })
            .catch(function (e) { return console.log('Error logging into Facebook', e); });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('userNameInput'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], LoginPage.prototype, "userNameInput", void 0);
    LoginPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.page.html */ "./src/app/pages/login/login.page.html"),
            styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/pages/login/login.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _recuperarsenha_service__WEBPACK_IMPORTED_MODULE_5__["RecuperarSenhaService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            src_app_core_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
            _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_6__["Facebook"]])
    ], LoginPage);
    return LoginPage;
}());



/***/ }),

/***/ "./src/app/pages/login/recuperarsenha.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/login/recuperarsenha.service.ts ***!
  \*******************************************************/
/*! exports provided: RecuperarSenhaService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecuperarSenhaService", function() { return RecuperarSenhaService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var API_URL = 'http://159.203.181.9:3000';
var RecuperarSenhaService = /** @class */ (function () {
    function RecuperarSenhaService(http) {
        this.http = http;
    }
    RecuperarSenhaService.prototype.sendemail = function (email) {
        return this.http.post(API_URL + "/sendEmail", { email: email });
    };
    ;
    RecuperarSenhaService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"]])
    ], RecuperarSenhaService);
    return RecuperarSenhaService;
}());



/***/ })

}]);
//# sourceMappingURL=pages-login-login-module.js.map