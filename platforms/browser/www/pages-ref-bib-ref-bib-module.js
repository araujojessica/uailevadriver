(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-ref-bib-ref-bib-module"],{

/***/ "./src/app/pages/ref-bib/ref-bib.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/ref-bib/ref-bib.module.ts ***!
  \*************************************************/
/*! exports provided: RefBibPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RefBibPageModule", function() { return RefBibPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ref_bib_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ref-bib.page */ "./src/app/pages/ref-bib/ref-bib.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _ref_bib_page__WEBPACK_IMPORTED_MODULE_5__["RefBibPage"]
    }
];
var RefBibPageModule = /** @class */ (function () {
    function RefBibPageModule() {
    }
    RefBibPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_ref_bib_page__WEBPACK_IMPORTED_MODULE_5__["RefBibPage"]]
        })
    ], RefBibPageModule);
    return RefBibPageModule;
}());



/***/ }),

/***/ "./src/app/pages/ref-bib/ref-bib.page.html":
/*!*************************************************!*\
  !*** ./src/app/pages/ref-bib/ref-bib.page.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"secondary\">\n\t<ion-buttons slot=\"start\">\n      <ion-menu-button color=\"light\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title>\n        Referências\n    </ion-title>\n  </ion-toolbar>\n<!--  <ion-toolbar color=\"light\"> \n    <ion-searchbar [(ngModel)]=\"searchKey\"></ion-searchbar>\n    \n  </ion-toolbar>-->\n</ion-header>\n\n<ion-content class=\"bg-white\">\n\n\t<ion-card class=\"bg-white\" no-margin>\n      <ion-card-content>\n        <h2 margin-bottom>\n          <ion-text color=\"dark\"><strong>Referências</strong></ion-text>\n\t\t</h2>\n\t\t<br>\n\t\t<p margin-bottom text-dark>\n\t\t\tAllen DC, Cameron RI. Histopathology Specimens: Clinical, Pathological and Laboratory Aspects. London: Springer; 2004.\n\t\t</p>\n\t\t<br>\n\t\t<p margin-bottom text-dark>\n\t\t\tBadiglian-Filho L, Menezes ANO, Faloppa CC, Fukazawa EM, Mantoan H, Kumagai LY, Baiocchi G. Intraoperative Ultrasound Leads to Conservative Management of Benign Ovarian Tumors: A Retrospective, Single-Center Study. Rev Bras Ginecol Obstet. 2019 Nov;41(11):673-678. English. doi: 10.1055/s-0039-1698774. Epub 2019 Nov 19. PMID: 31745961.\n\t\t</p>\n\t\t<br>\n\t\t<p margin-bottom text-dark>\t\n\t\t\tBarra DCC, Paim SMS, dal Sasso GTM, Colla GW. Métodos para desenvolvimento de aplicativos móveis em saúde: Revisão integrativa da literatura. Texto e Context Enferm. 2017;26(4):1–12.\n\t\t</p>\n\t\t<br>\n\t\t<p margin-bottom text-dark>\t\n\t\t\tBogoevski K, Woloszyk A, Blackwood K, Woodruff MA, Glatt V. Tissue Morphology and Antigenicity in Mouse and Rat Tibia: Comparing 12 Different Decalcification Conditions. J Histochem Cytochem. 2019 Aug;67(8):545-561. doi: 10.1369/0022155419850099. Epub 2019 May 15. PMID: 31090479; PMCID: PMC6669861.\n\t\t</p>\n\t\t<br>\n\t\t<p margin-bottom text-dark>\t\n\t\t\tCompton CC, Robb JA, Anderson MW, Berry AB, Birdsong GG, Bloom KJ, <i>et al</i>. Preanalytics and precision pathology: Pathology practices to ensure molecular integrity of cancer patient biospecimens for precision medicine. Arch Pathol Lab Med. 2019 Nov;143(11):1346-1363. doi: 10.5858/arpa.2019-0009-SA. Epub 2019 Jul 22. PMID: 31329478.\n\t\t</p>\n\t\t<br>\n\t\t\n\t\t<p margin-bottom text-dark>\t\n\t\t\tDonczo B, Guttman A. Biomedical analysis of formalin-fixed, paraffin-embedded tissue samples: The Holy Grail for molecular diagnostics. J Pharm Biomed Anal. 2018 Jun 5;155:125-134. doi: 10.1016/j.jpba.2018.03.065. Epub 2018 Apr 2. PMID: 29627729. \n\t\t</p>\n\t\t<br>\n\t\t<p margin-bottom text-dark>\n\t\t\tProtocols and Guidelines [Internet]. Northfield: College of American Pathologists; atualizada em 2020 [citado em 10 mar. 2020]. Cancer Protocol Templates – Gynecologic [cerca de 10 telas]. Disponível em: <a href=\"https://www.cap.org/protocols-and-guidelines/cancer-reporting-tools/cancer-protocol-templates\" target=\"_blank\">https://www.cap.org/protocols-and-guidelines/cancer-reporting-tools/cancer-protocol-templates</a>\n\t\t</p>\n\t\t<br>\n\t\t<p margin-bottom text-dark>\n\t\t\tPublicações da Sociedade Brasileira de Patologia [Internet]. São Paulo: Sociedade Brasileira de Patologia; atualizada em 2019 [citado em 02 Jan. 2020]. Manual de padronização de laudos histopatológicos; [5 telas]. Disponível em: <a href=\"http://www.sbp.org.br/manual-laudos-histopatologicos/\" target=\"_blank\">http://www.sbp.org.br/manual-laudos-histopatologicos/</a>\n\t\t\t\n\t\t</p>\n\t\t<br>\n\t\t<p margin-bottom text-dark>\n\t\t\tSavi FM, Brierly GI, Baldwin J, Theodoropoulos C, Woodruff MA. Comparison of Different Decalcification Methods Using Rat Mandibles as a Model. J Histochem Cytochem. 2017 Dec;65(12):705-722. doi: 10.1369/0022155417733708. Epub 2017 Sep 29. PMID: 28958188; PMCID: PMC5714097.\n\t\t</p>\n\t\t<br>\n\t\t<p margin-bottom text-dark>\n\t\t\tWestra WH, Hruban RH, Phelps TH, Isacson C, editores. Surgical Pathology Dissection: An Illustrated Guide. 2nd ed. New York: Springer; 2003.\n\t\t</p>\n\t\t\n\t  </ion-card-content>\n\t</ion-card>\n\t\t\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/ref-bib/ref-bib.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/ref-bib/ref-bib.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3JlZi1iaWIvcmVmLWJpYi5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/ref-bib/ref-bib.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/ref-bib/ref-bib.page.ts ***!
  \***********************************************/
/*! exports provided: RefBibPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RefBibPage", function() { return RefBibPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RefBibPage = /** @class */ (function () {
    function RefBibPage() {
    }
    RefBibPage.prototype.ngOnInit = function () {
    };
    RefBibPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-ref-bib',
            template: __webpack_require__(/*! ./ref-bib.page.html */ "./src/app/pages/ref-bib/ref-bib.page.html"),
            styles: [__webpack_require__(/*! ./ref-bib.page.scss */ "./src/app/pages/ref-bib/ref-bib.page.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], RefBibPage);
    return RefBibPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-ref-bib-ref-bib-module.js.map