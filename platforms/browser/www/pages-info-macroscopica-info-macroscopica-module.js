(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-info-macroscopica-info-macroscopica-module"],{

/***/ "./src/app/pages/info/macroscopica/info.macroscopica.html":
/*!****************************************************************!*\
  !*** ./src/app/pages/info/macroscopica/info.macroscopica.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n    <ion-toolbar color=\"secondary\">\r\n      <ion-buttons slot=\"start\">\r\n        <ion-menu-button color=\"light\"></ion-menu-button>\r\n      </ion-buttons>\r\n      <ion-title>\r\n          <b>Informações necessárias na descrição macroscópica</b>\r\n      </ion-title>\r\n    </ion-toolbar>\r\n   \r\n  </ion-header>\r\n  \r\n  <ion-content>\r\n   <ion-card class=\"bg-white\" no-margin>\r\n      <ion-card-content>\r\n        <h2 margin-bottom>\r\n          <ion-text color=\"dark\"><strong></strong></ion-text>\r\n        </h2>\r\n        <br>\t1.\tFixador no qual o material foi recebido no laboratório, quando possível. Informar, também, quando o material for recebido a fresco ou em escassa quantidade de fixador.\r\n        <br>\t2.\tQuantidade de frascos.\r\n        <br>\t3.\tIdentificação da(s) espécime(s), quando possível.\r\n        <br>\t4.\tLateralidade em órgãos pares.\r\n        <br>\t5.\tForma, medidas (três dimensões) e peso (quando pertinente).\r\n        <br>\t6.\tDescrição (forma, cor e consistência), localização e tamanho (comprimento, largura e espessura) da lesão na peça.\r\n        <br>\t7.\tDistâncias da lesão em relação as margens cirúrgicas e tecidos adjacentes, quando aplicáveis.\r\n        <br>\t8.\tExistência ou não existência de marcações cirúrgicas (referências) e suas respectivas legendas enunciadas na requisição.\r\n        <br>\r\n        <br>\tApós termino da descrição, realize os cortes quando aplicáveis e finalize a descrição informando:\r\n        <br>\r\n        <br> • Número de fragmentos e cassetes enviados para seção técnica/histologia. \r\n        <br> Quando necessário faça legenda informativa das respectivas áreas das secções representadas.\r\n        <br> <b>Ex.:</b> Toda espécime foi submetida a exame histológico (5fs/1bl).\r\n        <br> Fragmentos representativos da espécime foram submetidos a exame histológico.\r\n        <br> A: Tumor (3fs/3bls); B: Área não neoplásica (2fs/2bls); C: Margem cirúrgica (4fs/1bl).\r\n        <br> \r\n        <br><b>Notas:</b> \r\n        <br>•\tA descrição de peças complexas exige um bom conhecimento de anatomia e dos procedimentos cirúrgicos. Caso necessário não hesite em pedir ajuda a outros Patologistas e/ou ao cirurgião responsável para o esclarecimento de dúvidas.\r\n        <br>•\tRecomenda-se, ainda, fotografar as peças complexas, e quando necessário realize um desenho esquemático.\r\n        <br>•\tSomente utilizar os termos “tumor” ou “massa” quando há “neoplasia”.\r\n        <br>•\tQuando o mesmo frasco for constituído por materiais/peças diferentes, devidamente identificadas na requisição, utilizar os termos “aderidos a peça” ou “separados da peça”.  Quando não for possível identificá-los usar temos como: maior; menor. \r\n        <br>•\tAs dimensões devem ser em centímetros e com decimais, ex.: 3,0 x 2,0 x 0,8 cm.\r\n        <br>•\tQuanto mais precisas e claras as descrições macroscópicas, melhores e mais relevantes elas serão. Assim sendo, deve-se evitar excesso de palavras e enfoque em achados negativos pertinentes.\r\n        <br>•\tÉ necessário mencionar a presença de artefatos que dificultem os cortes macroscópicos, como qualquer outro fato que prejudique seu procedimento e/ou avaliação.\r\n        <br>\r\n        <br>\r\n      \r\n        \r\n  \r\n               \r\n          \r\n          \r\n          \r\n      </ion-card-content>\r\n      \r\n    </ion-card>\r\n    \r\n    \r\n  \r\n  </ion-content>\r\n  "

/***/ }),

/***/ "./src/app/pages/info/macroscopica/info.macroscopica.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/info/macroscopica/info.macroscopica.module.ts ***!
  \*********************************************************************/
/*! exports provided: InfoMacroscopicaModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfoMacroscopicaModule", function() { return InfoMacroscopicaModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _info_macroscopica__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./info.macroscopica */ "./src/app/pages/info/macroscopica/info.macroscopica.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _info_macroscopica__WEBPACK_IMPORTED_MODULE_5__["InfoMacroscopica"]
    }
];
var InfoMacroscopicaModule = /** @class */ (function () {
    function InfoMacroscopicaModule() {
    }
    InfoMacroscopicaModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_info_macroscopica__WEBPACK_IMPORTED_MODULE_5__["InfoMacroscopica"]]
        })
    ], InfoMacroscopicaModule);
    return InfoMacroscopicaModule;
}());



/***/ }),

/***/ "./src/app/pages/info/macroscopica/info.macroscopica.ts":
/*!**************************************************************!*\
  !*** ./src/app/pages/info/macroscopica/info.macroscopica.ts ***!
  \**************************************************************/
/*! exports provided: InfoMacroscopica */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfoMacroscopica", function() { return InfoMacroscopica; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};


var InfoMacroscopica = /** @class */ (function () {
    function InfoMacroscopica(modalController) {
        this.modalController = modalController;
    }
    InfoMacroscopica.prototype.viewImage = function (src, title, description) {
        if (title === void 0) { title = ''; }
        if (description === void 0) { description = ''; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/];
            });
        });
    };
    InfoMacroscopica = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-infomacroscopica',
            template: __webpack_require__(/*! ./info.macroscopica.html */ "./src/app/pages/info/macroscopica/info.macroscopica.html")
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"]])
    ], InfoMacroscopica);
    return InfoMacroscopica;
}());



/***/ })

}]);
//# sourceMappingURL=pages-info-macroscopica-info-macroscopica-module.js.map