(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-biopsia-biopsia-module"],{

/***/ "./src/app/pages/biopsia/biopsia.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/biopsia/biopsia.module.ts ***!
  \*************************************************/
/*! exports provided: BiopsiaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BiopsiaPageModule", function() { return BiopsiaPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _biopsia_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./biopsia.page */ "./src/app/pages/biopsia/biopsia.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _biopsia_page__WEBPACK_IMPORTED_MODULE_5__["BiopsiaPage"]
    }
];
var BiopsiaPageModule = /** @class */ (function () {
    function BiopsiaPageModule() {
    }
    BiopsiaPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_biopsia_page__WEBPACK_IMPORTED_MODULE_5__["BiopsiaPage"]]
        })
    ], BiopsiaPageModule);
    return BiopsiaPageModule;
}());



/***/ }),

/***/ "./src/app/pages/biopsia/biopsia.page.html":
/*!*************************************************!*\
  !*** ./src/app/pages/biopsia/biopsia.page.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.7.2/css/all.css\" integrity=\"sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr\" crossorigin=\"anonymous\">\r\n\r\n<ion-header>\r\n    <ion-toolbar color=\"secondary\">\r\n\t    <ion-buttons slot=\"start\">\r\n        <ion-menu-button color=\"light\"></ion-menu-button>\r\n      </ion-buttons>\r\n      <ion-title>Útero</ion-title>\r\n    </ion-toolbar>\r\n   <!-- <ion-toolbar color=\"light\"> \r\n      <ion-searchbar [(ngModel)]=\"searchKey\"></ion-searchbar>\r\n    </ion-toolbar>-->\r\n</ion-header>\r\n\r\n<ion-content>\r\n <ion-card class=\"bg-white\" no-margin>\r\n    <ion-card-content>\r\n      <h2 margin-bottom>\r\n        <ion-text color=\"dark\"><strong>Biópsia do colo do útero</strong></ion-text>\r\n      </h2>\r\n      <br>\r\n      <h3 margin-bottom text-dark><b>Procedimentos macroscópicos</b></h3><br>\r\n      <p margin-bottom text-dark>\r\n            1.\tEsteja atento ao conteúdo do recipiente, inclusive à parte inferior da tampa, uma vez que pode conter fragmentos minúsculos e sem fixador.\r\n          <br>2.\tUtilize uma forma confiável de retirar (filtrar) o material do frasco. \r\n          <br>&nbsp;\tSe necessário, utilize um papel filtro (cortar em tamanhos aproximados de 4x4 cm), e colocá-lo na superfície interna de coadores de plástico ou de metal para coar o material do recipiente.\r\n          <br>3.\tContar e analisar o formato e a coloração do material.\r\n          <br>4.\tMensurar em três dimensões todos os fragmentos, em conjunto.\r\n          <br>5.\tVerificar se há presença ou ausência de lesão epitelial, assim como observar se há erosões ou úlceras, cistos, irregularidades na espessura, e/ou outros.\r\n          <br>6.\tPintar os fragmentos com eosina e independente da necessidade de filtrá-los ou não, colocá-los em papel filtro.\r\n          <br>7.\tNão é necessário a realização da secção dos fragmentos desde que a dimensão seja menor que 4mm. \r\n      </p>\r\n      <br>\r\n      <h3 margin-bottom text-dark (click)=\"toggleDetails();\" style=\"width:100%; cursor:pointer\">\r\n        <div style=\"width: 100%; float:left; background: #878787; color: #fff; padding: 9px;\">\r\n            <b>Ex. de descrição macroscópica</b>\r\n          <div style=\"width: 4%; float:right;\"><i class=\"fa fa-angle-double-down\" style=\"color:#fff\" ></i></div>\r\n        </div>\r\n      </h3>\r\n      <br><br>\r\n      <div id=\"formDoc\" name=\"formDoc\" style=\"display:none;\">  \r\n              <div>\r\n                <ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n                  <ion-row>\r\n                    <ion-col>\r\n                      <div><label>Material recebido</label></div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                    <ion-col>\r\n                      <div>\r\n                        <ion-radio value= \"a fresco\"></ion-radio>&nbsp;\r\n                        <label>a fresco</label>\r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                    <ion-col>\r\n                      <div>\r\n                        <ion-radio value=\"em formol tamponado a 10%\"></ion-radio>&nbsp;\r\n                        <label>em formol tamponado a 10%</label>\r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                    <ion-col>\r\n                      <div>\r\n                        <ion-radio value=\"texto\"></ion-radio>&nbsp;\r\n                        <label>outro (citar)</label> &nbsp;\r\n                        <input style = \"width: 50%;\" type=\"text\" (keyup)=\"inputTexto1($event)\">\r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                  </ion-radio-group>\r\n                  </div>\r\n                  <div>\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                    <ion-col>\r\n                      <div>\r\n                        <label>que consiste de</label>&nbsp;\r\n                        <input style = \"width: 16%;\" type=\"text\" (keyup)=\"inputTexto2($event)\">&nbsp;\r\n                        <label for=\"inlineRadio3\">fragmento(s)</label>\r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                    <ion-col>\r\n                      <div>\r\n                          \r\n                              \r\n                              <label >irregular(es), de tonalidade</label> &nbsp;\r\n                              <input style = \"width: 18%;\" type=\"text\" (keyup)=\"inputTexto3($event)\">\r\n                              <label>macio(s) e brilhante(s)</label>\r\n                         \r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                 \r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                    <ion-col>\r\n                      <div>\r\n                            <label >que mede(m) em conjunto</label>&nbsp;\r\n                            <input style = \"width: 8%;\" type=\"text\" (keyup)=\"inputTexto4($event)\">&nbsp;\r\n                            <label>x</label>&nbsp;\r\n                            <input style = \"width: 8%;\" type=\"text\" (keyup)=\"inputTexto5($event)\">&nbsp;\r\n                            <label >x</label>&nbsp;\r\n                            <input style = \"width: 8%;\" type=\"text\" (keyup)=\"inputTexto6($event)\">\r\n                            <label>cm</label>\r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                    <ion-col>\r\n                      <div>\r\n                        <label>Todo material é submetido a exame</label>\r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                    <ion-col>\r\n                        <div>\r\n                          <label>histológico</label>&nbsp;\r\n                          <input style = \"width: 65%; text-align: center;\" placeholder=\"(_fs/_bl/legenda_)\" type=\"text\" (keyup)=\"inputTexto7($event)\">\r\n                        </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n              \r\n          </div>\r\n\r\n          <a class=\"btn btn-app\"style=\" width: 50%; padding: 5px 5px 5px 5px; border-radius: 3px;\" (click)='gerarPDF()' >\r\n              <i class=\"far fa-file\" style=\"color:#878787\"></i>&nbsp;\r\n              <span class=\"fontItem\">Baixar Arquivo</span>\r\n          </a>\r\n          <br><br>\r\n        </div>\r\n        <br>\r\n        <h3 margin-bottom><b>Cortes macroscópicos representativos</b></h3>\r\n        <br>Utilize o material por inteiro.<br>\r\n        <br><b>Nota: </b>Em materiais de curetagem endocervical, o muco deverá ser separado do restante do material e incluído em cassete distinto.<br>\r\n      <ion-grid>\r\n        <ion-row>\r\n            <ion-col>\r\n              <ion-img  class=\"img-thumbnail img-demo\" [src]=\"imgSource\" (click)=\"viewImage(imgSource, imgTitle, imgDescription)\"></ion-img>\r\n            </ion-col>\r\n            <ion-col>\r\n              <ion-img class=\"img-thumbnail img-demo\" [src]=\"imgSource2\" (click)=\"viewImage(imgSource2, imgTitle2, imgDescription2)\"></ion-img>\r\n            </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n\t\r\n    </ion-card-content>\r\n  </ion-card>\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/biopsia/biopsia.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/biopsia/biopsia.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ion-content {\n  --background: linear-gradient(-135deg, var(--ion-color-medium), var(--ion-color-light))\r\n    ; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYmlvcHNpYS9DOlxcVXNlcnNcXGplc3NpXFxEb2N1bWVudHNcXEFQTWFjcm8vc3JjXFxhcHBcXHBhZ2VzXFxiaW9wc2lhXFxiaW9wc2lhLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVRO0lBQWEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Jpb3BzaWEvYmlvcHNpYS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XHJcbiAgICBpb24tY29udGVudCB7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoLTEzNWRlZywgdmFyKC0taW9uLWNvbG9yLW1lZGl1bSksIHZhcigtLWlvbi1jb2xvci1saWdodCkpXHJcbiAgICB9XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/biopsia/biopsia.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/biopsia/biopsia.page.ts ***!
  \***********************************************/
/*! exports provided: BiopsiaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BiopsiaPage", function() { return BiopsiaPage; });
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! pdfmake/build/pdfmake */ "./node_modules/pdfmake/build/pdfmake.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! pdfmake/build/vfs_fonts */ "./node_modules/pdfmake/build/vfs_fonts.js");
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/image-viewer/image-viewer.component */ "./src/app/components/image-viewer/image-viewer.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/file-opener/ngx */ "./node_modules/@ionic-native/file-opener/ngx/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.vfs = pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default.a.pdfMake.vfs;





var BiopsiaPage = /** @class */ (function () {
    function BiopsiaPage(modalController, router, navCtrl, formBuilder, file, navController, fileOpener) {
        this.modalController = modalController;
        this.router = router;
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.file = file;
        this.navController = navController;
        this.fileOpener = fileOpener;
        this.imgSource = 'assets/img/bx_colo_do_utero/fotos/DSC00535.png';
        this.imgTitle = '';
        this.imgDescription = 'Foto 4: Fragmento do colo do útero. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource2 = 'assets/img/bx_colo_do_utero/fotos/DSC00533.png';
        this.imgTitle2 = '';
        this.imgDescription2 = 'Foto 4.6: Coloração com eosina e representação, fragmento do colo do útero. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.pdfObj = null;
    }
    BiopsiaPage.prototype.viewImage = function (src, title, description) {
        if (title === void 0) { title = ''; }
        if (description === void 0) { description = ''; }
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_5__["ImageViewerComponent"],
                            componentProps: {
                                imgSource: src,
                                imgTitle: title,
                                imgDescription: description
                            },
                            cssClass: 'modal-fullscreen',
                            keyboardClose: true,
                            showBackdrop: true
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    BiopsiaPage.prototype.ngOnInit = function () { };
    BiopsiaPage.prototype.gerarPDF = function () {
        var _this = this;
        if (this.option1 == undefined) {
            this.option1 = '';
        }
        if (this.option2 == undefined) {
            this.option2 = '';
        }
        if (this.texto1 == undefined) {
            this.texto1 = '';
        }
        if (this.texto2 == undefined) {
            this.texto2 = '';
        }
        if (this.texto3 == undefined) {
            this.texto3 = '';
        }
        if (this.texto4 == undefined) {
            this.texto4 = '';
        }
        if (this.texto5 == undefined) {
            this.texto5 = '';
        }
        if (this.texto6 == undefined) {
            this.texto6 = '';
        }
        if (this.texto7 == undefined) {
            this.texto7 = '';
        }
        if (this.option1 == 'texto') {
            this.option1 = this.texto1;
        }
        if (this.texto2 == '1' || this.texto3 == '0') {
            this.option = this.texto2 + ' fragmento irregular, de tonalidade ' + this.texto3 + ', macio e brilhante, que mede ' + this.texto4 + ' x ' + this.texto5 + ' x ' + this.texto6 + ' cm.';
        }
        else {
            this.option = this.texto2 + ' fragmentos irregulares, de tonalidade ' + this.texto3 + ', macios e brilhantes, que medem em conjunto ' + this.texto4 + ' x ' + this.texto5 + ' x ' + this.texto6 + ' cm.';
        }
        pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.vfs = pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default.a.pdfMake.vfs;
        var docDefinition = {
            content: [
                {
                    columns: [
                        [
                            { text: 'Biópsia do colo do útero', style: 'header' },
                            { text: '', style: 'sub_header' },
                            { text: 'Material recebido ' + this.option1 + ' que consiste de ' + this.option + ' Todo material é submetido a exame histológico ' + this.texto7 + '.', style: 'text' },
                        ]
                    ]
                }
            ],
            styles: {
                header: {
                    bold: true,
                    fontSize: 20,
                    alignment: 'center'
                },
                sub_header: {
                    fontSize: 18,
                    alignment: 'center'
                },
                text: {
                    fontSize: 16,
                    margin: [0, 30, 0, 0],
                    alignment: 'left'
                }
            },
            pageSize: 'A4',
            pageOrientation: 'portrait'
        };
        this.pdfObj = pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.createPdf(docDefinition);
        //console.log(this.pdfObj.getBuffer());
        this.pdfObj.getBuffer(function (buffer) {
            var blob = new Blob([buffer], { type: 'application/pdf' });
            // Save the PDF to the data Directory of our App
            _this.file.writeFile(_this.file.dataDirectory, 'Biopsia_do_colo_do_utero.pdf', blob, { replace: true });
            // Open the PDf with the correct OS tools
            _this.fileOpener.open(_this.file.dataDirectory + 'Biopsia_do_colo_do_utero.pdf', 'application/pdf');
        });
    };
    BiopsiaPage.prototype.toggleDetails = function () {
        if (document.getElementById("formDoc").style.display == 'none') {
            document.getElementById("formDoc").style.display = "block";
        }
        else {
            document.getElementById("formDoc").style.display = "none";
        }
    };
    BiopsiaPage.prototype.opt1 = function (event) {
        this.option1 = event.target.value;
        if (this.option1 == undefined) {
            this.option1 = '';
        }
    };
    BiopsiaPage.prototype.opt2 = function (event) {
        this.option2 = event.target.value;
        if (this.option2 == undefined) {
            this.option2 = '';
        }
    };
    BiopsiaPage.prototype.inputTexto1 = function (event) {
        this.texto1 = event.target.value;
    };
    BiopsiaPage.prototype.inputTexto2 = function (event) {
        this.texto2 = event.target.value;
    };
    BiopsiaPage.prototype.inputTexto3 = function (event) {
        this.texto3 = event.target.value;
    };
    BiopsiaPage.prototype.inputTexto4 = function (event) {
        this.texto4 = event.target.value;
    };
    BiopsiaPage.prototype.inputTexto5 = function (event) {
        this.texto5 = event.target.value;
    };
    BiopsiaPage.prototype.inputTexto6 = function (event) {
        this.texto6 = event.target.value;
    };
    BiopsiaPage.prototype.inputTexto7 = function (event) {
        this.texto7 = event.target.value;
    };
    BiopsiaPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-biopsia',
            template: __webpack_require__(/*! ./biopsia.page.html */ "./src/app/pages/biopsia/biopsia.page.html"),
            styles: [__webpack_require__(/*! ./biopsia.page.scss */ "./src/app/pages/biopsia/biopsia.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"], _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_0__["File"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
            _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_8__["FileOpener"]])
    ], BiopsiaPage);
    return BiopsiaPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-biopsia-biopsia-module.js.map