(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-biopsia_endometrial-biopsia-endometrial-page-module"],{

/***/ "./src/app/pages/biopsia_endometrial/biopsia.endometrial.page.html":
/*!*************************************************************************!*\
  !*** ./src/app/pages/biopsia_endometrial/biopsia.endometrial.page.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n    <ion-toolbar color=\"secondary\">\r\n      <ion-buttons slot=\"start\">\r\n        <ion-menu-button color=\"light\"></ion-menu-button>\r\n      </ion-buttons>\r\n      <ion-title>\r\n          Útero\r\n      </ion-title>\r\n    </ion-toolbar>\r\n <!--   <ion-toolbar color=\"light\"> \r\n      <ion-searchbar [(ngModel)]=\"searchKey\"></ion-searchbar>\r\n      \r\n    </ion-toolbar>-->\r\n  </ion-header>\r\n  \r\n  <ion-content>\r\n   <ion-card class=\"bg-white\" no-margin>\r\n      <ion-card-content>\r\n        <h2 margin-bottom>\r\n          <ion-text color=\"dark\"><strong>Biópsia de endométrio ou curetagem semiótica</strong></ion-text>\r\n        </h2>\r\n        <br>\r\n        Geralmente a peça é constituída de múltiplos fragmentos de epitélio associados à sangue e muco.\r\n        <br>\r\n        <b>Nota:</b> Este procedimento não deve ser realizado em casos suspeitos de abortamento.\r\n        <br>\r\n        <br>\r\n        <h3 margin-bottom text-dark><b>Procedimentos Macroscópicos</b></h3>\r\n      <br>\r\n        <p margin-bottom text-dark>\r\n          1.\tUtilize uma forma confiável de retirar (filtrar) o material do frasco\r\n          <br>&nbsp;\tSe necessário, utilize um papel filtro (cortar em tamanhos aproximados de 4x4 cm), e colocá-lo na superfície interna de coadores de plástico ou de metal para coar o material do recipiente.\r\n          <br>2.\tEm conjunto, meça as três dimensões e pese o material recebido.\r\n          <br>3.\tAnalise e descreva as características do material:\r\n          <br>&nbsp;\ta.\tTonalidade, consciência e formato.\r\n          <br>&nbsp;\tb.\tHá coágulos sanguíneos? (Descreva a proporção de coágulos em relação à totalidade do produto).\r\n          <br>&nbsp;\tc.\tHá evidência de necrose?\r\n          <br>&nbsp;\td.\tOutros (citar) _________________.\r\n          <br>4.\tCaso necessário pinte os fragmentos com eosina e coloque-os em papel filtro.\r\n          <br>\t\r\n        </p>\r\n        <br>\r\n        <h3 margin-bottom text-dark (click)=\"toggleDetails();\" style=\"width:100%; cursor:pointer\">\r\n            <div style=\"width: 100%; float:left; background: #878787; color: #fff; padding: 9px;\"><b>Ex. de descrição macroscópica</b>\r\n            <div style=\"width: 4%; float:right;\"><i class=\"fa fa-angle-double-down\" style=\"color:#fff\" ></i></div>\r\n          </div>\r\n            </h3>\r\n            \r\n            <br>\r\n          <br>\r\n            <div id=\"formDoc\" name=\"formDoc\" style=\"display:none;\">  \r\n            \r\n                <div>\r\n\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n\t\t\t\t\t\t\t<!--(ionSelect)=\"goFish($event)\" (ionBlur)=\"leaveFish($event)\"-->\r\n\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div><label>Material recebido</label></div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio value=\"a fresco\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\"> a fresco</ion-label>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio value=\"em formol tamponado a 10%\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">em formol tamponado a 10%</ion-label>\r\n\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio value = \"texto\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">outro (citar)</ion-label> &nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<input style = \"width: 50%;\" type=\"text\" (keyup)=\"inputTexto1($event)\">\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t</ion-radio-group>\r\n          </div>\r\n          <div>\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                    <ion-col>\r\n                      <div>\r\n                        <label >que consiste de </label>\r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                  <ion-radio-group allow-empty-selection (ionChange)=\"opt2($event)\">\r\n                    <ion-row style=\"margin-top: -10px;\">\r\n                      <ion-col>\r\n                        <div>\r\n                          <ion-radio value=\"diversos fragmentos vinhosos, macios, irregulares e brilhantes,\"></ion-radio>&nbsp;\r\n                          <label>diversos fragmentos </label>&nbsp;\r\n                          <label >vinhosos, macios, irregulares e brilhantes</label>\r\n                        </div>\r\n                      </ion-col>\r\n                    </ion-row>\r\n                    <ion-row style=\"margin-top: -10px;\">\r\n                      <ion-col>\r\n                        <div>\r\n                          <ion-radio value=\"diversos fragmentos irregulares de tonalidade\"></ion-radio>&nbsp;\r\n                          <label >diversos fragmentos</label>&nbsp;\r\n                          <label >irregulares de tonalidade</label>&nbsp;\r\n                          <input style = \"width: 20%;\" type=\"text\" (keyup)=\"inputTexto2($event)\">&nbsp;\r\n                          <label >macios</label>&nbsp;\r\n                          <label >e brilhantes</label>\r\n                        </div>\r\n                      </ion-col>\r\n                    </ion-row>\r\n                  </ion-radio-group>           \r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                    <ion-col>\r\n                      <div>\r\n                        <ion-radio-group allow-empty-selection (ionChange)=\"opt3($event)\">\r\n                          <ion-radio value=\" notando-se\"></ion-radio>&nbsp;\r\n                          <label >notando-se </label>\r\n                        </ion-radio-group>\r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                    <ion-col>\r\n                      <div>\r\n                        <ion-radio-group allow-empty-selection (ionChange)=\"opt4($event)\">\r\n                          <ion-radio value=\"texto\"></ion-radio>&nbsp;\r\n                          <input style = \"width: 13%;\" type=\"text\" (keyup)=\"inputTexto3($event)\"> &nbsp;\r\n                          <label >% de coágulos</label>&nbsp;\r\n                          <label >sanguíneos</label>\r\n                        </ion-radio-group>\r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                    <ion-col>\r\n                      <div>\r\n                        <ion-radio-group allow-empty-selection (ionChange)=\"opt5($event)\">\r\n                        <ion-radio value=\" muco,\"></ion-radio>&nbsp;\r\n                        <label>muco</label>\r\n                      </ion-radio-group>\r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                    <ion-col>\r\n                      <div>\r\n                        <ion-radio-group allow-empty-selection (ionChange)=\"opt6($event)\">\r\n                          <ion-radio value=\" material necrótico,\"></ion-radio>&nbsp;\r\n                          <label >material necrótico</label>\r\n                        </ion-radio-group>\r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                    <ion-col>\r\n                      <div>\r\n                        <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                        <ion-radio value=\"texto\"></ion-radio>&nbsp;\r\n                        <label >outro  (citar)</label>&nbsp;\r\n                        <input style = \"width: 50%;\" type=\"text\" (keyup)=\"inputTexto4($event)\">\r\n                      </ion-radio-group>\r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                    <ion-col>\r\n                      <div>\r\n                        <label >medindo em conjunto</label>\r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                    <ion-col>\r\n                      <div>\r\n                        <input style = \"width: 10%;\" type=\"text\" (keyup)=\"inputTexto5($event)\">&nbsp;\r\n                        <label > x </label>&nbsp;\r\n                        <input style = \"width: 10%;\" type=\"text\" (keyup)=\"inputTexto6($event)\">&nbsp;\r\n                        <label >x</label>&nbsp;\r\n                        <input style = \"width: 10%;\" type=\"text\" (keyup)=\"inputTexto7($event)\">&nbsp;\r\n                        <label >cm</label>\r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                    <ion-col>\r\n                      <div>\r\n                        <ion-radio-group allow-empty-selection (ionChange)=\"opt8($event)\">\r\n                          <ion-radio value=\"texto\"></ion-radio>&nbsp;\r\n                          <label >e pesando </label>&nbsp;\r\n                          <input style = \"width: 13%;\" type=\"text\" (keyup)=\"inputTexto8($event)\">&nbsp;\r\n                          <label >g.</label>\r\n                      </ion-radio-group>\r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                    <ion-col>\r\n                      <div>\r\n                        <label >Todo material é submetido a exame histológico </label>\r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                    <ion-col>\r\n                      <div>\r\n                        <input style = \"text-align: center; width: 65%;\" placeholder=\"(_fs/_bl/legenda_)\" type=\"text\" (keyup)=\"inputTexto9($event)\" >\r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                </div>\r\n\r\n            <a class=\"btn btn-app\"style=\" width: 50%; padding: 5px 5px 5px 5px; border-radius: 3px;\" (click)='gerarPDF()' >\r\n              <i class=\"far fa-file\" style=\"color:#878787\"></i>&nbsp;\r\n              <span class=\"fontItem\">Baixar Arquivo</span>\r\n            </a>\r\n    <br>\r\n    <br>\r\n    </div>\r\n      <br>\r\n        <h3 margin-bottom><b>Cortes macroscópicos representativos</b></h3><br>\r\n        <p>Para a biópsia endometrial ou curetagem diagnóstica, deve-se submeter todo o produto recebido. \r\n        <br>\r\n        <br><strong>NOTAS:</strong> Recomenda-se utilizar até a metade do cassete.\r\n          <br>&nbsp;\tCaso haja muco, incluí-lo, separadamente, em cassete distinto.\r\n  \r\n  <br><br>\r\n          <ion-grid>\r\n            <ion-row>\r\n          <ion-col>\r\n          \r\n            \r\n            <ion-img  class=\"img-thumbnail img-demo\" [src]=\"imgSource\" (click)=\"viewImage(imgSource, imgTitle, imgDescription)\"></ion-img>\r\n            \r\n          </ion-col>\r\n          <ion-col>\r\n          \r\n            \r\n            <ion-img class=\"img-thumbnail img-demo\" [src]=\"imgSource2\" (click)=\"viewImage(imgSource2, imgTitle2, imgDescription2)\"></ion-img>\r\n            \r\n          </ion-col>\r\n        </ion-row>\r\n        </ion-grid>\r\n          \r\n      </ion-card-content>\r\n      \r\n    </ion-card>\r\n    \r\n    \r\n  \r\n  </ion-content>\r\n  "

/***/ }),

/***/ "./src/app/pages/biopsia_endometrial/biopsia.endometrial.page.module.ts":
/*!******************************************************************************!*\
  !*** ./src/app/pages/biopsia_endometrial/biopsia.endometrial.page.module.ts ***!
  \******************************************************************************/
/*! exports provided: BiopsiaEndometrialPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BiopsiaEndometrialPageModule", function() { return BiopsiaEndometrialPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _biopsia_endometrial_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./biopsia.endometrial.page */ "./src/app/pages/biopsia_endometrial/biopsia.endometrial.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _biopsia_endometrial_page__WEBPACK_IMPORTED_MODULE_5__["BiopsiaEndometrialPage"]
    }
];
var BiopsiaEndometrialPageModule = /** @class */ (function () {
    function BiopsiaEndometrialPageModule() {
    }
    BiopsiaEndometrialPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_biopsia_endometrial_page__WEBPACK_IMPORTED_MODULE_5__["BiopsiaEndometrialPage"]]
        })
    ], BiopsiaEndometrialPageModule);
    return BiopsiaEndometrialPageModule;
}());



/***/ }),

/***/ "./src/app/pages/biopsia_endometrial/biopsia.endometrial.page.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/biopsia_endometrial/biopsia.endometrial.page.ts ***!
  \***********************************************************************/
/*! exports provided: BiopsiaEndometrialPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BiopsiaEndometrialPage", function() { return BiopsiaEndometrialPage; });
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! pdfmake/build/pdfmake */ "./node_modules/pdfmake/build/pdfmake.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! pdfmake/build/vfs_fonts */ "./node_modules/pdfmake/build/vfs_fonts.js");
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/image-viewer/image-viewer.component */ "./src/app/components/image-viewer/image-viewer.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/file-opener/ngx */ "./node_modules/@ionic-native/file-opener/ngx/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.vfs = pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default.a.pdfMake.vfs;





var BiopsiaEndometrialPage = /** @class */ (function () {
    function BiopsiaEndometrialPage(modalController, router, navCtrl, formBuilder, file, navController, fileOpener) {
        this.modalController = modalController;
        this.router = router;
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.file = file;
        this.navController = navController;
        this.fileOpener = fileOpener;
        this.imgSource = 'assets/img/biopsia_endometrial/fotos/DSC00555.png';
        this.imgTitle = '';
        this.imgDescription = 'Foto 6: Fragmentos de curetagem semiótica. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource2 = 'assets/img/biopsia_endometrial/fotos/DSC00563.png';
        this.imgTitle2 = '';
        this.imgDescription2 = 'Foto 6.4: Coloração com eosina e representação, fragmentos de curetagem semiótica. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.pdfObj = null;
    }
    BiopsiaEndometrialPage.prototype.viewImage = function (src, title, description) {
        if (title === void 0) { title = ''; }
        if (description === void 0) { description = ''; }
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_5__["ImageViewerComponent"],
                            componentProps: {
                                imgSource: src,
                                imgTitle: title,
                                imgDescription: description
                            },
                            cssClass: 'modal-fullscreen',
                            keyboardClose: true,
                            showBackdrop: true
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    BiopsiaEndometrialPage.prototype.ngOnInit = function () { };
    BiopsiaEndometrialPage.prototype.gerarPDF = function () {
        var _this = this;
        if (this.option1 == undefined) {
            this.option1 = '';
        }
        if (this.option2 == undefined) {
            this.option2 = '';
        }
        if (this.option3 == undefined) {
            this.option3 = '';
        }
        if (this.option4 == undefined) {
            this.option4 = '';
        }
        if (this.option5 == undefined) {
            this.option5 = '';
        }
        if (this.option6 == undefined) {
            this.option6 = '';
        }
        if (this.option7 == undefined) {
            this.option7 = '';
        }
        if (this.option8 == undefined) {
            this.option8 = '';
        }
        if (this.texto1 == undefined) {
            this.texto1 = '';
        }
        if (this.texto2 == undefined) {
            this.texto2 = '';
        }
        if (this.texto3 == undefined) {
            this.texto3 = '';
        }
        if (this.texto4 == undefined) {
            this.texto4 = '';
        }
        if (this.texto5 == undefined) {
            this.texto5 = '';
        }
        if (this.texto6 == undefined) {
            this.texto6 = '';
        }
        if (this.texto7 == undefined) {
            this.texto7 = '';
        }
        if (this.texto8 == undefined) {
            this.texto8 = '';
        }
        if (this.texto9 == undefined) {
            this.texto9 = '';
        }
        if (this.option1 == 'texto') {
            this.option1 = this.texto1;
        }
        if (this.option2 == 'diversos fragmentos irregulares de tonalidade') {
            this.option2 = this.option2 + ' ' + this.texto2 + ', macios e brilhantes,';
        }
        if (this.option4 == 'texto') {
            this.option4 = ' ' + this.texto3 + '% de coágulos sanguíneos,';
        }
        if (this.option7 == 'texto') {
            this.option7 = ' ' + this.texto4;
        }
        if (this.option8 == 'texto') {
            this.option8 = ' e pesando ' + this.texto8 + ' g.';
        }
        pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.vfs = pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default.a.pdfMake.vfs;
        var docDefinition = {
            content: [
                {
                    columns: [
                        [
                            { text: 'Biópsia de endométrio ou curetagem semiótica', style: 'header' },
                            { text: '', style: 'sub_header' },
                            { text: 'Material recebido ' + this.option1 + ' que consiste de ' + this.option2 + '' + this.option3 + '' + this.option4 + '' + this.option5 + '' + this.option6 + '' + this.option7 + '' +
                                    ' medindo em conjunto ' + this.texto5 + ' x ' + this.texto6 + ' x ' + this.texto7 + ' cm' + this.option8 + ' Todo material é submetido a exame histológico ' + this.texto9 + '.', style: 'text' },
                        ]
                    ]
                }
            ],
            styles: {
                header: {
                    bold: true,
                    fontSize: 20,
                    alignment: 'center'
                },
                sub_header: {
                    fontSize: 18,
                    alignment: 'center'
                },
                text: {
                    fontSize: 16,
                    margin: [0, 30, 0, 0],
                    alignment: 'left'
                }
            },
            pageSize: 'A4',
            pageOrientation: 'portrait'
        };
        //pdfMake.createPdf(docDefinition).open();
        this.pdfObj = pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.createPdf(docDefinition);
        //console.log(this.pdfObj.getBuffer());
        this.pdfObj.getBuffer(function (buffer) {
            var blob = new Blob([buffer], { type: 'application/pdf' });
            // Save the PDF to the data Directory of our App
            _this.file.writeFile(_this.file.dataDirectory, 'Biopsia_endometrial.pdf', blob, { replace: true });
            // Open the PDf with the correct OS tools
            _this.fileOpener.open(_this.file.dataDirectory + 'Biopsia_endometrial.pdf', 'application/pdf');
        });
    };
    BiopsiaEndometrialPage.prototype.toggleDetails = function () {
        if (document.getElementById("formDoc").style.display == 'none') {
            document.getElementById("formDoc").style.display = "block";
        }
        else {
            document.getElementById("formDoc").style.display = "none";
        }
    };
    BiopsiaEndometrialPage.prototype.opt1 = function (event) {
        this.option1 = event.target.value;
    };
    BiopsiaEndometrialPage.prototype.opt2 = function (event) {
        this.option2 = event.target.value;
    };
    BiopsiaEndometrialPage.prototype.opt3 = function (event) {
        this.option3 = event.target.value;
    };
    BiopsiaEndometrialPage.prototype.opt4 = function (event) {
        this.option4 = event.target.value;
    };
    BiopsiaEndometrialPage.prototype.opt5 = function (event) {
        this.option5 = event.target.value;
    };
    BiopsiaEndometrialPage.prototype.opt6 = function (event) {
        this.option6 = event.target.value;
    };
    BiopsiaEndometrialPage.prototype.opt7 = function (event) {
        this.option7 = event.target.value;
    };
    BiopsiaEndometrialPage.prototype.opt8 = function (event) {
        this.option8 = event.target.value;
    };
    BiopsiaEndometrialPage.prototype.inputTexto1 = function (event) {
        this.texto1 = event.target.value;
        console.log(this.texto1);
    };
    BiopsiaEndometrialPage.prototype.inputTexto2 = function (event) {
        this.texto2 = event.target.value;
    };
    BiopsiaEndometrialPage.prototype.inputTexto3 = function (event) {
        this.texto3 = event.target.value;
    };
    BiopsiaEndometrialPage.prototype.inputTexto4 = function (event) {
        this.texto4 = event.target.value;
    };
    BiopsiaEndometrialPage.prototype.inputTexto5 = function (event) {
        this.texto5 = event.target.value;
    };
    BiopsiaEndometrialPage.prototype.inputTexto6 = function (event) {
        this.texto6 = event.target.value;
    };
    BiopsiaEndometrialPage.prototype.inputTexto7 = function (event) {
        this.texto7 = event.target.value;
    };
    BiopsiaEndometrialPage.prototype.inputTexto8 = function (event) {
        this.texto8 = event.target.value;
    };
    BiopsiaEndometrialPage.prototype.inputTexto9 = function (event) {
        this.texto9 = event.target.value;
    };
    BiopsiaEndometrialPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-biopsia-endometrial',
            template: __webpack_require__(/*! ./biopsia.endometrial.page.html */ "./src/app/pages/biopsia_endometrial/biopsia.endometrial.page.html"),
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"], _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_0__["File"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
            _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_8__["FileOpener"]])
    ], BiopsiaEndometrialPage);
    return BiopsiaEndometrialPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-biopsia_endometrial-biopsia-endometrial-page-module.js.map