(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-info-form-info-form-module"],{

/***/ "./src/app/pages/info/form/info.form.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/info/form/info.form.module.ts ***!
  \*****************************************************/
/*! exports provided: InfoFormPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfoFormPageModule", function() { return InfoFormPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _info_form_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./info.form.page */ "./src/app/pages/info/form/info.form.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _info_form_page__WEBPACK_IMPORTED_MODULE_5__["InfoFormPage"]
    }
];
var InfoFormPageModule = /** @class */ (function () {
    function InfoFormPageModule() {
    }
    InfoFormPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_info_form_page__WEBPACK_IMPORTED_MODULE_5__["InfoFormPage"]]
        })
    ], InfoFormPageModule);
    return InfoFormPageModule;
}());



/***/ }),

/***/ "./src/app/pages/info/form/info.form.page.html":
/*!*****************************************************!*\
  !*** ./src/app/pages/info/form/info.form.page.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content padding class=\"login auth-page\">\r\n\r\n    <div class=\"auth-content\">\r\n      <!-- Logo -->\r\n      <div class=\"logo\"></div>\r\n      <div padding-horizontal text-center>\r\n        \r\n        <h3 no-margin>\r\n          <ion-text color=\"medium\" class=\"fw700\">\r\n            <ion-text color=\"secondary\" *ngIf=\"(user$ | async) as user;\">Editar Informações</ion-text>\r\n          </ion-text>\r\n        </h3>\r\n      </div>\r\n      \r\n      <!-- Register form -->\r\n      <form [formGroup]=\"onInfoForm\" class=\"list-form\" (submit) = \"sign\">\r\n        <ion-item no-padding class=\"animated fadeInUp\">\r\n          <ion-label position=\"floating\">\r\n            <ion-icon name=\"mail\" item-start></ion-icon>\r\n            Título\r\n          </ion-label>\r\n          <ion-input color=\"secondary\" type=\"text\" formControlName=\"title\" value = \"{{infos['title']}}\"></ion-input>\r\n        </ion-item>\r\n       \r\n  \r\n        <ion-item no-padding class=\"animated fadeInUp\">\r\n          <ion-label position=\"floating\">\r\n            <ion-icon name=\"mail\" item-start></ion-icon>\r\n          Informações\r\n          </ion-label>\r\n          <ion-textarea color=\"secondary\" type=\"mail\" formControlName=\"body\" value = \"{{infos['body']}}\"></ion-textarea>\r\n        </ion-item>\r\n        \r\n          <ion-item no-padding class=\"animated fadeInUp\">\r\n            <ion-label position=\"floating\">\r\n              <ion-icon name=\"mail\" item-start></ion-icon>\r\n              Rodapé\r\n            </ion-label> \r\n            <ion-textarea color=\"secondary\" type=\"text\" formControlName=\"foot\" value = \"{{infos['foot']}}\" ></ion-textarea>\r\n          </ion-item>\r\n          \r\n       </form>\r\n       \r\n  \r\n      <div margin-top>\r\n        <ion-button icon-left size=\"medium\" expand=\"full\" shape=\"round\" color=\"secondary\" (click)=\"infoEdit()\" >\r\n          <ion-icon name=\"log-in\"></ion-icon>\r\n          Salvar\r\n        </ion-button>\r\n  \r\n        \r\n  \r\n      </div>\r\n  \r\n   \r\n  \r\n    </div>\r\n  </ion-content>"

/***/ }),

/***/ "./src/app/pages/info/form/info.form.page.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/info/form/info.form.page.ts ***!
  \***************************************************/
/*! exports provided: InfoFormPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InfoFormPage", function() { return InfoFormPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _register_user_not_taken_validator_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../register/user-not-taken.validator.service */ "./src/app/pages/register/user-not-taken.validator.service.ts");
/* harmony import */ var _service_info_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../service/info.service */ "./src/app/pages/info/service/info.service.ts");
/* harmony import */ var src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/core/user/user.service */ "./src/app/core/user/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var InfoFormPage = /** @class */ (function () {
    function InfoFormPage(navCtrl, menuCtrl, loadingCtrl, router, userNotTakenValidatorService, infoService, userService, formBuilder) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.userNotTakenValidatorService = userNotTakenValidatorService;
        this.infoService = infoService;
        this.userService = userService;
        this.formBuilder = formBuilder;
        this.infos = [];
        this.user$ = userService.getUser();
        infoService.listFromInfo().subscribe(function (infos) {
            console.log(infos);
            _this.infos = infos;
        });
    }
    InfoFormPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(true);
    };
    InfoFormPage.prototype.ngOnInit = function () {
        this.onInfoForm = this.formBuilder.group({
            title: [''],
            body: [''],
            foot: [''],
            user: ['']
        });
    };
    InfoFormPage.prototype.infoEdit = function () {
        return __awaiter(this, void 0, void 0, function () {
            var newInfo;
            var _this = this;
            return __generator(this, function (_a) {
                console.log('entrou para editar informacao');
                newInfo = this.onInfoForm.getRawValue();
                newInfo.user = 'jessica';
                this.infoService.updateInfo(newInfo).subscribe(function () { return _this.router.navigate(['/info/edit']); }, function (err) { return console.log(err); });
                return [2 /*return*/];
            });
        });
    };
    InfoFormPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! ./info.form.page.html */ "./src/app/pages/info/form/info.form.page.html"),
            styles: [__webpack_require__(/*! ./info.page.scss */ "./src/app/pages/info/form/info.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _register_user_not_taken_validator_service__WEBPACK_IMPORTED_MODULE_4__["UserNotTakenValidatorService"],
            _service_info_service__WEBPACK_IMPORTED_MODULE_5__["InfoService"],
            src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_6__["UserService"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], InfoFormPage);
    return InfoFormPage;
}());



/***/ }),

/***/ "./src/app/pages/info/form/info.page.scss":
/*!************************************************!*\
  !*** ./src/app/pages/info/form/info.page.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2luZm8vZm9ybS9pbmZvLnBhZ2Uuc2NzcyJ9 */"

/***/ })

}]);
//# sourceMappingURL=pages-info-form-info-form-module.js.map