(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-ooforectomia-ooforectomia-module"],{

/***/ "./src/app/pages/ooforectomia/ooforectomia.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/ooforectomia/ooforectomia.module.ts ***!
  \***********************************************************/
/*! exports provided: OoforectomiaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OoforectomiaPageModule", function() { return OoforectomiaPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ooforectomia_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./ooforectomia.page */ "./src/app/pages/ooforectomia/ooforectomia.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _ooforectomia_page__WEBPACK_IMPORTED_MODULE_5__["OoforectomiaPage"]
    }
];
var OoforectomiaPageModule = /** @class */ (function () {
    function OoforectomiaPageModule() {
    }
    OoforectomiaPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            providers: [],
            declarations: [_ooforectomia_page__WEBPACK_IMPORTED_MODULE_5__["OoforectomiaPage"]]
        })
    ], OoforectomiaPageModule);
    return OoforectomiaPageModule;
}());



/***/ }),

/***/ "./src/app/pages/ooforectomia/ooforectomia.page.html":
/*!***********************************************************!*\
  !*** ./src/app/pages/ooforectomia/ooforectomia.page.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n\t<ion-toolbar color=\"secondary\">\r\n\t  <ion-buttons slot=\"start\">\r\n\t\t<ion-menu-button color=\"light\"></ion-menu-button>\r\n\t  </ion-buttons>\r\n\t  <ion-title>\r\n\t\t  Ovário\r\n\t  </ion-title>\r\n\t</ion-toolbar>\r\n<!--<ion-toolbar color=\"light\"> \r\n\t  <ion-searchbar [(ngModel)]=\"searchKey\"></ion-searchbar>\r\n\t  \r\n\t</ion-toolbar>-->\t\r\n\r\n  </ion-header>\r\n<ion-content>\r\n\t<ion-card class=\"bg-white\" no-margin>\r\n\r\n\t\t \r\n\t  \t<ion-card-content>\r\n\t\t\t<h2 margin-bottom>\r\n\t\t\t<ion-text color=\"dark\"><strong>Ooforectomia ou Ooforoplastia</strong></ion-text>\r\n\t\t\t</h2>\r\n\t\t\t<br>Procedimentos que consistem na remoção do(s) ovário(s) de maneira total, parcial ou apenas da lesão (cirurgia poupadora do ovário).\r\n\t\t\t\tO melhor método a ser realizado dependerá dos dados/história clínica do paciente.\r\n\t\t\t\r\n\t\t\t<br><br>\r\n\t\t\t<h3 margin-bottom text-dark><b>Procedimentos macroscópicos</b></h3>\r\n\t\t\t<br>\r\n\t\t\t<p margin-bottom text-dark>\r\n\t\t\t\t1.\tMensurar as três dimensões e pesar a peça.\r\n\t\t\t\r\n\t\t\t<br>2.\tAnalisar a superfície externa do órgão (cápsula): Se há presença de aderência, hemorragia, rotura, irregularidade e/ou outros.\r\n\t\t\t<br>3.\tEm caso de lesão tumoral pintar com tinta <i>Nankin</i> a superfície externa.\r\n\t\t\t<br>4.\tCaso a peça seja recebida no laboratório a fresco:\r\n\t\t\t<br>&nbsp;\ta.\tSe tamanho normal ou próximo do normal, realizar um corte longitudinal central, do parênquima ovariano total com o hilo. Fixar a peça por algumas horas   <a href = '#' (click)=\"viewImage(imgSource12, imgTitle12, imgDescription12)\"> (Figura 1.4.a)</a><a href = '#' (click)=\"viewImage(imgSource2, imgTitle2, imgDescription2)\"> (Foto 1.4a)</a>.\r\n\t\t\t<br>&nbsp;\tb.\tSe tamanho alterado, optar por cortes paralelos, no maior eixo, iniciando pela região central, cujos intervalos necessitam conter, aproximadamente, 1cm de distância e, também, deve ser fixada a peça <a href = '#' (click)=\"viewImage(imgSource8, imgTitle8, imgDescription8)\"> (Foto 1.4b)</a>.\r\n\t\t\t<br>5.\tAnalisar a superfície de corte: Há calcificação, hemorragia, corpo amarelo e/ou outros? Atentar-se as características do córtex, da medula e do hilo. Se presença de cisto, medir e descrever o conteúdo.\r\n\t\t\t<br>6.\tSe existência de tumor, é preciso medir as dimensões, avaliar a aparência externa –lisa, rugosa, vegetante, –sólida e/ou cística– e descrever o conteúdo –seroso, viscoso, tecido adiposo, dentes, pelos– caso a massa seja cística. Por fim, definir se há evidência de necrose e/ou calcificação.\r\n\t\t\t</p>\r\n\t\t\t<br>\r\n\t\t\t<h3 margin-bottom text-dark (click)=\"toggleDetails();\" style=\"width:100%; cursor:pointer\">\r\n\t\t\t\t<div style=\"width: 100%; float:left; background: #878787; color: #fff; padding: 9px;\">\r\n\t\t\t\t\t<b>Ex. de descrição macroscópica</b>\r\n\t\t\t\t\t<div style=\"width: 4%; float:right;\"><i class=\"fa fa-angle-double-down\" style=\"color:#fff\" ></i></div>\r\n\t\t\t\t</div>\r\n\t\t\t</h3>\r\n\t\t\t<br><br>\r\n\r\n\t\t\t<div id=\"formDoc\" name=\"formDoc\" style=\"display:none;\">  \r\n\t\t\t\t\r\n\t\t\t\t\t<div>\r\n\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n\t\t\t\t\t\t\t<!--(ionSelect)=\"goFish($event)\" (ionBlur)=\"leaveFish($event)\"-->\r\n\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div><label>Material recebido</label></div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio value=\"a fresco\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\" > a fresco</ion-label>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio value=\"em formol tamponado a 10%\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">em formol tamponado a 10%</ion-label>\r\n\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio value = \"texto\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">outro (citar)</ion-label> &nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<input style = \"width: 50%;\" type=\"text\" (keyup)=\"inputTexto1($event)\">\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t</div>\r\n\r\n\t\t\t\t\t<div>\r\n\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt2($event)\">\r\n\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t<ion-col><div><label>que consiste de</label></div></ion-col>\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio value = \"ovário\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">ovário</ion-label>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio value = \"texto\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">outro (citar)</ion-label> &nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<input style = \"width: 50%;\" type=\"text\" (keyup)=\"inputTexto2($event)\">\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<br>\r\n\t\t\t\t\t<div>\r\n\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt3($event)\">\r\n\t\t\t\t\t\t\t<ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio value = \"esquerdo,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">esquerdo</ion-label>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio value = \"direito,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\"> direito</ion-label>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div>\r\n\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt4($event)\">\r\n\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio value = \" total,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\"> total</ion-label>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio value = \" parcial,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\"> parcial</ion-label>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div>\r\n\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt5($event)\">\r\n\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio value = \" fragmentado,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\"> fragmentado</ion-label>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt6($event)\">\r\n\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio value = \" previamente seccionado,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\"> previamente seccionado</ion-label>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<label>medindo</label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<input style = \"width: 13%;\" type=\"text\" (keyup)=\"inputTexto3($event)\">&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<label >x</label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<input  style = \"width: 13%;\" type=\"text\" (keyup)=\"inputTexto4($event)\">&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<label >x</label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<input  style = \"width: 13%;\" type=\"text\" (keyup)=\"inputTexto5($event)\">&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<label >cm</label>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<label>e pesando</label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<input  style = \"width: 15%;\" type=\"text\" (keyup)=\"inputTexto6($event)\">&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t<label>g</label>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<label>Apresenta superfície externa</label>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n\t\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\" lisa,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">lisa</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\" rugosa,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">rugosa</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt8($event)\">\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\" vegetante,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\" >vegetante</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt9($event)\">\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\" irregular,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\" >irregular</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt10($event)\">\r\n\t\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\" lobulada\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">lobulada</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\" multilobulada\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">multilobulada</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t</ion-col>\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt11($event)\">\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\", com aderências\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">com aderências</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\t\t\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt12($event)\">\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\", com hemorragia\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">com hemorragia</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\t\t\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt13($event)\">\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\", com rotura\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\"> com rotura</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t</div>\t\r\n\t\t\t\t\t\t\t\t</ion-col>\t\t\t\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt14($event)\">\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\"texto\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\"> outros (citar)</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t<input style = \"width: 50%;\"  type=\"text\" (keyup)=\"inputTexto7($event)\">\r\n\t\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<label>Aos cortes </label>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt15($event)\">\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\", o parênquima ovariano\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\"> o parênquima ovariano</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt16($event)\">\r\n\t\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\" é elástico com pequenos cistos e áreas pardo-amareladas.\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">é elástico com pequenos cistos</ion-label><br>\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-label>e áreas pardo-amareladas.</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t</ion-col>\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\" é multiloculado, com cistos de tamanhos variados, de revestimento liso e preenchidos por líquido amarelo-citrino.\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">é multiloculado, com cistos de</ion-label><br>\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-label>tamanhos variados, de revestimento liso e preenchidos por líquido amarelo-citrino. </ion-label>\r\n\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t</ion-col>\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-radio id = \"massa\" value=\" apresenta massa\" (click)=\"apMassa();\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\" >apresenta massa</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t</ion-col>\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t<div id=\"apMassa\" style=\"display: none;\">\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt17($event)\">\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio  value=\" sólida,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\" >sólida</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</ion-col>\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\" cística,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\" >cística</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</ion-col>\t\r\n\t\t\t\t\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\" sólido-cística,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\" >sólido-cística</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</ion-col>\t\r\n\t\t\t\t\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt18($event)\">\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\" firme,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\" >firme</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</ion-col>\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\" macia,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\" >macia</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</ion-col>\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t\t\t\t</ion-radio-group>\t\r\n\t\t\t\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt19($event)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio  value=\" preenchida por conteúdo\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\" >preenchida por conteúdo</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</ion-col>\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt20($event)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio  value=\" seroso,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">seroso</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</ion-col>\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt21($event)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio type=\"radio\" value=\" viscoso,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\" >viscoso</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</ion-col>\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt22($event)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\" pastoso,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\" >pastoso</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</ion-col>\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt23($event)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio  value=\" gelatinoide,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\" >gelatinoide</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</ion-col>\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt24($event)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio  value=\" com\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\" >com</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</ion-col>\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt25($event)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio  value=\" pelos,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\" >pelos</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</ion-col>\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt26($event)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\" conteúdo hemorrágico,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\" >conteúdo hemorrágico</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</ion-col>\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt27($event)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\" conteúdo necrótico,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">conteúdo necrótico</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</ion-col>\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt28($event)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio  value=\" calcificado,\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\"> calcificado </ion-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</ion-col>\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t\t\t\t<br>\r\n\t\t\t\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt29($event)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\" cuja dimensão é de\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\" >cuja dimensão é de </ion-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</ion-col>\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<input style = \"width: 13%;\" type=\"text\"  (keyup)=\"inputTexto8($event)\">&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<label>x</label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<input  style = \"width: 13%;\" type=\"text\"  (keyup)=\"inputTexto9($event)\">&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<label>x</label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<input style = \"width: 13%;\" type=\"text\" (keyup)=\"inputTexto10($event)\">&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<label>cm</label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</ion-col>\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt30($event)\">\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\"Com\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">Com</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</ion-col>\t\r\n\t\t\t\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio value=\"Sem\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">Sem</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</ion-col>\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio  value=\"Não pode ser determinado\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">Não pode ser determinado</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</ion-col>\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt31($event)\">\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-radio  value=\" envolvimento da superfície ovariana.\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">envolvimento da superfície ovariana</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t\t\t</ion-col>\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t<ion-radio-group allow-empty-selection (ionChange)=\"opt32($event)\">\r\n\t\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-radio  value=\"Toda espécime é submetida a exame histológico\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">Toda espécime é submetida </ion-label>\r\n\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t</ion-col>\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-radio  value=\" Fragmentos representativos são submetidos a exame histológico\"></ion-radio>&nbsp;\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-label id=\"radio-form\">Fragmentos representativos são</ion-label><br>\r\n\t\t\t\t\t\t\t\t\t\t\t<ion-label >submetidos a exame histológico</ion-label>\r\n\t\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t\t</ion-col>\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t\t\t</ion-radio-group>\r\n\t\t\t\t\t\t\t<ion-row>\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n\t\t\t\t\t\t\t\t\t\t<input style = \"width: 70%; text-align: center;\" type=\"text\" placeholder=\"(__fs/__bls/legenda__)\" (keyup)=\"inputTexto11($event)\">&nbsp;\r\n\t\t\t\t\t\t\t\t\t</div>\r\n\t\t\t\t\t\t\t\t</ion-col>\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t</ion-row>\r\n\t\t\t\t\t</div>\r\n\t\t\t\t<br>\r\n\t\t\t\t\r\n\t\t\t\t<a class=\"btn btn-app\"style=\" width: 50%; padding: 5px 5px 5px 5px; border-radius: 3px;\" (click)='gerarPDF()' >\r\n\t\t\t\t\t<i class=\"far fa-file\" style=\"color:#878787\"></i>&nbsp;\r\n\t\t\t\t\t<span class=\"fontItem\">Baixar Arquivo</span>\r\n\t\t\t\t</a>\r\n\t\t\t\t\r\n\t\t\t\t\t\t\t\t\r\n\t\t\t</div>\r\n\t\t\t<br>\r\n\t\t\t<h3 margin-bottom><b>Cortes macroscópicos representativos</b></h3>\r\n\t\t\t<p>\r\n\t\t\t\t<br>1.\tOvário normal \r\n\t\t\t\t<br>&nbsp;&nbsp;<b>a.</b>Peça ≤ que 2,0 cm em seu maior diâmetro: um corte longitudinal central do parênquima ovariano total com o hilo <a href = '#' (click)=\"viewImage(imgSource3, imgTitle3, imgDescription3)\" > (Foto 1.1a) </a> .\r\n\t\t\t\t<br>&nbsp;&nbsp;<b>b.</b>Peça > que 2,0 cm em seu maior diâmetro, porém com aspecto de normalidade, seccionar transversalmente em cortes paralelos, para visualização de todas as superfícies e representar um corte a cada 2,0 cm <a href = '#' (click)=\"viewImage(imgSource5, imgTitle5, imgDescription5)\">  (Foto 1.1b.1)</a> <a href = '#' (click)=\"viewImage(imgSource6, imgTitle6, imgDescription6)\"> (Foto 1.1b.2)</a> .\r\n\t\t\t\t<br>2. Cistos\r\n\t\t\t\t<br>&nbsp;&nbsp;<b>Cassete A: </b>três cortes da parede cística (Todas as cavidades devem ser abertas. Ademais, se presente é preciso selecionar as áreas sólidas e/ou vegetantes). \r\n\t\t\t\t<br>&nbsp;&nbsp;<b>Cassete B: </b>um corte do parênquima ovariano normal (quando presente) e transição se houver outras áreas diferentes.  \r\n\t\t\t\t<br>3.\tOvário tumoral <a href = '#' (click)=\"viewImage(imgSource11, imgTitle11, imgDescription11)\" > (Figura 1.3) </a> <a href = '#' (click)=\"viewImage(imgSource10, imgTitle10, imgDescription10)\" > (Foto 1.3.AB) </a>\r\n\t\t\t\t<br>&nbsp;&nbsp;<b>Cassete A1 à A3: </b>três cortes –caso tumor seja > que três centímetros, um corte a cada centímetro da maior dimensão do tumor.\r\n\t\t\t\t<br>&nbsp;&nbsp;<b>Cassete B: </b>um corte do parênquima ovariano normal (quando presente) e transição se houver outras áreas diferentes. \r\n\t\t\t</p>\r\n\t\t\t<br>\r\n\t\t\t<p text-center>\r\n\t\t\t\t<a class=\"btn btn-app\" style=\" width: 45%\"  (click)=\"desenhos()\">\r\n\t\t\t\t\t<i class=\"fa fa-image fa-2x\" style=\"color:#e74a92\"></i><br>\r\n\t\t\t\t\t<span>Figuras Ilustrativos</span>\r\n\t\t\t\t</a>\r\n\t\t\t\t<a class=\"btn btn-app\" style=\" width: 45%\" (click)=\"fotos()\">\r\n\t\t\t\t\t<i class=\"fas fa-camera fa-2x\" style=\"color:#e74a92\"></i><br>\r\n\t\t\t\t\t<span>Fotos Ilustrativas</span>\r\n\t\t\t\t</a>\r\n\t\t\t</p>       \r\n\t\t</ion-card-content>\r\n\t</ion-card>\r\n</ion-content>\r\n  "

/***/ }),

/***/ "./src/app/pages/ooforectomia/ooforectomia.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/ooforectomia/ooforectomia.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ion-content {\n  --background: linear-gradient(-135deg, var(--ion-color-medium), var(--ion-color-light))\r\n    ; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvb29mb3JlY3RvbWlhL0M6XFxVc2Vyc1xcamVzc2lcXERvY3VtZW50c1xcQVBNYWNyby9zcmNcXGFwcFxccGFnZXNcXG9vZm9yZWN0b21pYVxcb29mb3JlY3RvbWlhLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUVRO0lBQWEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL29vZm9yZWN0b21pYS9vb2ZvcmVjdG9taWEucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xyXG4gICAgaW9uLWNvbnRlbnQge1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KC0xMzVkZWcsIHZhcigtLWlvbi1jb2xvci1tZWRpdW0pLCB2YXIoLS1pb24tY29sb3ItbGlnaHQpKVxyXG4gICAgfVxyXG59Il19 */"

/***/ }),

/***/ "./src/app/pages/ooforectomia/ooforectomia.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/ooforectomia/ooforectomia.page.ts ***!
  \*********************************************************/
/*! exports provided: OoforectomiaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OoforectomiaPage", function() { return OoforectomiaPage; });
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! pdfmake/build/pdfmake */ "./node_modules/pdfmake/build/pdfmake.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! pdfmake/build/vfs_fonts */ "./node_modules/pdfmake/build/vfs_fonts.js");
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/image-viewer/image-viewer.component */ "./src/app/components/image-viewer/image-viewer.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/file-opener/ngx */ "./node_modules/@ionic-native/file-opener/ngx/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.vfs = pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default.a.pdfMake.vfs;






var OoforectomiaPage = /** @class */ (function () {
    function OoforectomiaPage(modalController, toastCtrl, router, navCtrl, formBuilder, loadingCtrl, platfomr, file, navController, fileOpener, alertCtrl) {
        this.modalController = modalController;
        this.toastCtrl = toastCtrl;
        this.router = router;
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.loadingCtrl = loadingCtrl;
        this.platfomr = platfomr;
        this.file = file;
        this.navController = navController;
        this.fileOpener = fileOpener;
        this.alertCtrl = alertCtrl;
        this.imgSource = 'assets/img/ooferectomia/fotos/DSC00536.png';
        this.imgTitle = '';
        this.imgDescription = 'Foto 1: Ovário ≤ que 2,0 cm. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource2 = 'assets/img/ooferectomia/fotos/DSC00545.png';
        this.imgTitle2 = '';
        this.imgDescription2 = 'Foto 1.4a: Corte inicial para ovário ≤ que 2,0 cm. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource3 = 'assets/img/ooferectomia/fotos/DSC00543.png';
        this.imgTitle3 = '';
        this.imgDescription3 = 'Foto 1.1a: Corte representativo para ovário ≤ que 2,0 cm. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource4 = 'assets/img/ooferectomia/fotos/DSC00620.png';
        this.imgTitle4 = '';
        this.imgDescription4 = 'Foto 1: Ovário > que 2,0 cm, com aspecto de normalidade. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource5 = 'assets/img/ooferectomia/fotos/DSC00627.png';
        this.imgTitle5 = '';
        this.imgDescription5 = 'Foto 1.1b.1: Cortes gerais para ovário > que 2,0 cm, com aspecto de normalidade. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource6 = 'assets/img/ooferectomia/fotos/DSC00630.png';
        this.imgTitle6 = '';
        this.imgDescription6 = 'Foto 1.1b.2: Cortes representativos para ovário > que 2,0 cm, com aspecto de normalidade. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource7 = 'assets/img/ooferectomia/fotos/DSC00650.png';
        this.imgTitle7 = '';
        this.imgDescription7 = 'Foto 1: Ovário tumoral. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource8 = 'assets/img/ooferectomia/fotos/DSC00651.png';
        this.imgTitle8 = '';
        this.imgDescription8 = 'Foto 1.4b: Corte inicial para ovário tumoral. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource9 = 'assets/img/ooferectomia/fotos/DSC00659.png';
        this.imgTitle9 = '';
        this.imgDescription9 = 'Foto 1.3: Cortes representativos para ovário tumoral. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource10 = 'assets/img/ooferectomia/fotos/DSC00661.png';
        this.imgTitle10 = '';
        this.imgDescription10 = 'Foto 1.3.AB: Cortes representativos para ovário tumoral. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource11 = 'assets/img/ooferectomia/img/tumor.png';
        this.imgTitle11 = '';
        this.imgDescription11 = 'Figura 1.3: Cortes representativos para ovário tumoral (adaptada in Westra WH, et al. Surgical pathology dissection: an illustrated guide).';
        this.imgSource12 = 'assets/img/ooferectomia/img/1591711558652.png';
        this.imgTitle12 = '';
        this.imgDescription12 = 'Figura 1.4.a: Corte inicial para ovário normal ≤ que 2,0 cm (adaptada in Westra WH, et al. Surgical pathology dissection: an illustrated guide).';
        this.pdfObj = null;
    }
    OoforectomiaPage.prototype.viewImage = function (src, title, description) {
        if (title === void 0) { title = ''; }
        if (description === void 0) { description = ''; }
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_5__["ImageViewerComponent"],
                            componentProps: {
                                imgSource: src,
                                imgTitle: title,
                                imgDescription: description
                            },
                            cssClass: 'modal-fullscreen',
                            keyboardClose: true,
                            showBackdrop: true
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    OoforectomiaPage.prototype.ngOnInit = function () { };
    OoforectomiaPage.prototype.fotos = function () {
        this.router.navigateByUrl('/ooforectomia/fotos');
    };
    OoforectomiaPage.prototype.desenhos = function () {
        this.router.navigateByUrl('/ooforectomia/images');
    };
    OoforectomiaPage.prototype.gerarPDF = function () {
        return __awaiter(this, void 0, void 0, function () {
            var docDefinition;
            var _this = this;
            return __generator(this, function (_a) {
                if (this.option1 == undefined) {
                    this.option1 = '';
                }
                if (this.option2 == undefined) {
                    this.option2 = '';
                }
                if (this.option3 == undefined) {
                    this.option3 = '';
                }
                if (this.option4 == undefined) {
                    this.option4 = '';
                }
                if (this.option5 == undefined) {
                    this.option5 = '';
                }
                if (this.option6 == undefined) {
                    this.option6 = '';
                }
                if (this.option7 == undefined) {
                    this.option7 = '';
                }
                if (this.option8 == undefined) {
                    this.option8 = '';
                }
                if (this.option9 == undefined) {
                    this.option9 = '';
                }
                if (this.option10 == undefined) {
                    this.option10 = '';
                }
                if (this.option11 == undefined) {
                    this.option11 = '';
                }
                if (this.option12 == undefined) {
                    this.option12 = '';
                }
                if (this.option13 == undefined) {
                    this.option13 = '';
                }
                if (this.option14 == undefined) {
                    this.option14 = '';
                }
                if (this.option15 == undefined) {
                    this.option15 = '';
                }
                if (this.option16 == undefined) {
                    this.option16 = '';
                }
                if (this.option17 == undefined) {
                    this.option17 = '';
                }
                if (this.option18 == undefined) {
                    this.option18 = '';
                }
                if (this.option19 == undefined) {
                    this.option19 = '';
                }
                if (this.option20 == undefined) {
                    this.option20 = '';
                }
                if (this.option21 == undefined) {
                    this.option21 = '';
                }
                if (this.option22 == undefined) {
                    this.option22 = '';
                }
                if (this.option23 == undefined) {
                    this.option23 = '';
                }
                if (this.option5 == undefined) {
                    this.option5 = '';
                }
                if (this.option24 == undefined) {
                    this.option24 = '';
                }
                if (this.option25 == undefined) {
                    this.option25 = '';
                }
                if (this.option26 == undefined) {
                    this.option26 = '';
                }
                if (this.option27 == undefined) {
                    this.option27 = '';
                }
                if (this.option28 == undefined) {
                    this.option28 = '';
                }
                if (this.option29 == undefined) {
                    this.option29 = '';
                }
                if (this.option30 == undefined) {
                    this.option30 = '';
                }
                if (this.option31 == undefined) {
                    this.option31 = '';
                }
                if (this.option32 == undefined) {
                    this.option32 = '';
                }
                if (this.texto == undefined) {
                    this.texto = '';
                }
                if (this.texto2 == undefined) {
                    this.texto2 = '';
                }
                if (this.texto3 == undefined) {
                    this.texto3 = '';
                }
                if (this.texto4 == undefined) {
                    this.texto4 = '';
                }
                if (this.texto5 == undefined) {
                    this.texto5 = '';
                }
                if (this.texto6 == undefined) {
                    this.texto6 = '';
                }
                if (this.texto7 == undefined) {
                    this.texto7 = '';
                }
                if (this.texto8 == undefined) {
                    this.texto8 = '';
                }
                if (this.texto9 == undefined) {
                    this.texto9 = '';
                }
                if (this.texto10 == undefined) {
                    this.texto10 = '';
                }
                if (this.texto11 == undefined) {
                    this.texto11 = '';
                }
                if (this.option1 == 'texto') {
                    this.option1 = this.texto;
                }
                if (this.option2 == 'texto') {
                    this.option2 = this.texto2;
                }
                if (this.option14 == 'texto') {
                    this.option14 = this.texto7;
                }
                if (this.option29 == ' cuja dimensão é de') {
                    this.option29 = this.option29 + ' ' + this.texto8 + ' x ' + this.texto9 + ' x ' + this.texto10 + " cm. ";
                }
                if (this.option16 == ' apresenta massa') {
                    this.option16 = this.option16 + "" + this.option17 + "" + this.option18 + "" + this.option19 + "" + this.option20 + "" + this.option21 + "" + this.option22 + "" + this.option23 + "" + this.option24 + "" + this.option25 + "" + this.option26 + "" + this.option27 + "" + this.option28 + "" + this.option29 + "" + this.option30 + "" + this.option31;
                }
                pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.vfs = pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default.a.pdfMake.vfs;
                docDefinition = {
                    content: [
                        {
                            columns: [
                                [
                                    { text: 'Ooforectomia ou Ooforoplastia', style: 'header' },
                                    { text: '', style: 'sub_header' },
                                    { text: 'Material recebido ' + this.option1 + ' que consiste de ' + this.option2 + ' ' + this.option3 + '' + this.option4 + '' + this.option5 + '' + this.option6 + '' +
                                            ' medindo ' + this.texto3 + ' x ' + this.texto4 + ' x ' + this.texto5 + ' cm e pesando ' + this.texto6 + ' g. ' +
                                            'Apresenta superfície externa' + this.option7 + '' + this.option8 + '' + this.option9 + '' + this.option10 + '' + this.option11 + '' + this.option12 + '' + this.option13 + '' + this.option14 + '' +
                                            '. Aos cortes' + this.option15 + '' + this.option16 + '' +
                                            '' + this.option32 + ' ' + this.texto11 + '.', style: 'text' },
                                ]
                            ]
                        }
                    ],
                    styles: {
                        header: {
                            bold: true,
                            fontSize: 20,
                            alignment: 'center'
                        },
                        sub_header: {
                            fontSize: 18,
                            alignment: 'center'
                        },
                        text: {
                            fontSize: 16,
                            margin: [0, 30, 0, 0],
                            alignment: 'left'
                        }
                    },
                    pageSize: 'A4',
                    pageOrientation: 'portrait'
                };
                this.pdfObj = pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.createPdf(docDefinition);
                //console.log(this.pdfObj.getBuffer());
                this.pdfObj.getBuffer(function (buffer) {
                    var blob = new Blob([buffer], { type: 'application/pdf' });
                    // Save the PDF to the data Directory of our App
                    _this.file.writeFile(_this.file.dataDirectory, 'Ooforectomia_Ooforoplastia.pdf', blob, { replace: true });
                    // Open the PDf with the correct OS tools
                    _this.fileOpener.open(_this.file.dataDirectory + 'Ooforectomia_Ooforoplastia.pdf', 'application/pdf');
                });
                return [2 /*return*/];
            });
        });
    };
    OoforectomiaPage.prototype.toggleDetails = function () {
        if (document.getElementById("formDoc").style.display == 'none') {
            document.getElementById("formDoc").style.display = "block";
        }
        else {
            document.getElementById("formDoc").style.display = "none";
        }
    };
    OoforectomiaPage.prototype.opt1 = function (event) {
        this.option1 = event.target.value;
    };
    OoforectomiaPage.prototype.opt2 = function (event) {
        this.option2 = event.target.value;
    };
    OoforectomiaPage.prototype.opt3 = function (event) {
        this.option3 = event.target.value;
    };
    OoforectomiaPage.prototype.opt4 = function (event) {
        this.option4 = event.target.value;
    };
    OoforectomiaPage.prototype.opt5 = function (event) {
        this.option5 = event.target.value;
    };
    OoforectomiaPage.prototype.opt6 = function (event) {
        this.option6 = event.target.value;
    };
    OoforectomiaPage.prototype.opt7 = function (event) {
        this.option7 = event.target.value;
    };
    OoforectomiaPage.prototype.opt8 = function (event) {
        this.option8 = event.target.value;
    };
    OoforectomiaPage.prototype.opt9 = function (event) {
        this.option9 = event.target.value;
    };
    OoforectomiaPage.prototype.opt10 = function (event) {
        this.option10 = event.target.value;
    };
    OoforectomiaPage.prototype.opt11 = function (event) {
        this.option11 = event.target.value;
    };
    OoforectomiaPage.prototype.opt12 = function (event) {
        this.option12 = event.target.value;
    };
    OoforectomiaPage.prototype.opt13 = function (event) {
        this.option13 = event.target.value;
    };
    OoforectomiaPage.prototype.opt14 = function (event) {
        this.option14 = event.target.value;
    };
    OoforectomiaPage.prototype.opt15 = function (event) {
        this.option15 = event.target.value;
    };
    OoforectomiaPage.prototype.opt16 = function (event) {
        this.option16 = event.target.value;
    };
    OoforectomiaPage.prototype.opt17 = function (event) {
        this.option17 = event.target.value;
    };
    OoforectomiaPage.prototype.opt18 = function (event) {
        this.option18 = event.target.value;
    };
    OoforectomiaPage.prototype.opt19 = function (event) {
        this.option19 = event.target.value;
    };
    OoforectomiaPage.prototype.opt20 = function (event) {
        this.option20 = event.target.value;
    };
    OoforectomiaPage.prototype.opt21 = function (event) {
        this.option21 = event.target.value;
    };
    OoforectomiaPage.prototype.opt22 = function (event) {
        this.option22 = event.target.value;
    };
    OoforectomiaPage.prototype.opt23 = function (event) {
        this.option23 = event.target.value;
    };
    OoforectomiaPage.prototype.opt24 = function (event) {
        this.option24 = event.target.value;
    };
    OoforectomiaPage.prototype.opt25 = function (event) {
        this.option25 = event.target.value;
    };
    OoforectomiaPage.prototype.opt26 = function (event) {
        this.option26 = event.target.value;
    };
    OoforectomiaPage.prototype.opt27 = function (event) {
        this.option27 = event.target.value;
    };
    OoforectomiaPage.prototype.opt28 = function (event) {
        this.option28 = event.target.value;
    };
    OoforectomiaPage.prototype.opt29 = function (event) {
        this.option29 = event.target.value;
    };
    OoforectomiaPage.prototype.opt30 = function (event) {
        this.option30 = event.target.value;
    };
    OoforectomiaPage.prototype.opt31 = function (event) {
        this.option31 = event.target.value;
    };
    OoforectomiaPage.prototype.opt32 = function (event) {
        this.option32 = event.target.value;
    };
    OoforectomiaPage.prototype.inputTexto1 = function (event) {
        this.texto = event.target.value;
    };
    OoforectomiaPage.prototype.inputTexto2 = function (event) {
        this.texto2 = event.target.value;
    };
    OoforectomiaPage.prototype.inputTexto3 = function (event) {
        this.texto3 = event.target.value;
    };
    OoforectomiaPage.prototype.inputTexto4 = function (event) {
        this.texto4 = event.target.value;
    };
    OoforectomiaPage.prototype.inputTexto5 = function (event) {
        this.texto5 = event.target.value;
    };
    OoforectomiaPage.prototype.inputTexto6 = function (event) {
        this.texto6 = event.target.value;
    };
    OoforectomiaPage.prototype.inputTexto7 = function (event) {
        this.texto7 = event.target.value;
    };
    OoforectomiaPage.prototype.inputTexto8 = function (event) {
        this.texto8 = event.target.value;
    };
    OoforectomiaPage.prototype.inputTexto9 = function (event) {
        this.texto9 = event.target.value;
    };
    OoforectomiaPage.prototype.inputTexto10 = function (event) {
        this.texto10 = event.target.value;
    };
    OoforectomiaPage.prototype.inputTexto11 = function (event) {
        this.texto11 = event.target.value;
    };
    OoforectomiaPage.prototype.apMassa = function () {
        if (document.getElementById("apMassa").style.display == 'none') {
            document.getElementById("apMassa").style.display = "block";
        }
        else {
            document.getElementById("apMassa").style.display = "none";
        }
    };
    OoforectomiaPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-ooforectomia',
            template: __webpack_require__(/*! ./ooforectomia.page.html */ "./src/app/pages/ooforectomia/ooforectomia.page.html"),
            styles: [__webpack_require__(/*! ./ooforectomia.page.scss */ "./src/app/pages/ooforectomia/ooforectomia.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"],
            _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_0__["File"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
            _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_8__["FileOpener"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]])
    ], OoforectomiaPage);
    return OoforectomiaPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-ooforectomia-ooforectomia-module.js.map