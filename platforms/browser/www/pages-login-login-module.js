(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-login-module"],{

/***/ "./node_modules/@ionic-native/email-composer/ngx/index.js":
/*!****************************************************************!*\
  !*** ./node_modules/@ionic-native/email-composer/ngx/index.js ***!
  \****************************************************************/
/*! exports provided: EmailComposer */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EmailComposer", function() { return EmailComposer; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/core */ "./node_modules/@ionic-native/core/index.js");



var EmailComposer = /** @class */ (function (_super) {
    Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"])(EmailComposer, _super);
    function EmailComposer() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    EmailComposer.prototype.hasPermission = function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "hasPermission", { "successIndex": 0, "errorIndex": 2 }, arguments); };
    EmailComposer.prototype.requestPermission = function () { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "requestPermission", { "successIndex": 0, "errorIndex": 2 }, arguments); };
    EmailComposer.prototype.hasAccount = function () {
        var _this = this;
        return (function () {
            if (Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["checkAvailability"])(_this) === true) {
                return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["getPromise"])(function (resolve) {
                    EmailComposer.getPlugin().hasAccount(function (result) {
                        if (result) {
                            resolve(true);
                        }
                        else {
                            resolve(false);
                        }
                    });
                });
            }
        })();
    };
    EmailComposer.prototype.hasClient = function (app) {
        var _this = this;
        return (function () {
            if (Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["checkAvailability"])(_this) === true) {
                return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["getPromise"])(function (resolve) {
                    if (app) {
                        EmailComposer.getPlugin().hasClient(app, function (result) {
                            if (result) {
                                resolve(true);
                            }
                            else {
                                resolve(false);
                            }
                        });
                    }
                    else {
                        EmailComposer.getPlugin().getClients(function (apps) {
                            resolve(apps && apps.length > 0);
                        });
                    }
                });
            }
        })();
    };
    EmailComposer.prototype.getClients = function () {
        var _this = this;
        return (function () {
            if (Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["checkAvailability"])(_this) === true) {
                return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["getPromise"])(function (resolve) {
                    EmailComposer.getPlugin().getClients(function (apps) {
                        if (Object.prototype.toString.call(apps) === '[object String]') {
                            apps = [apps];
                        }
                        resolve(apps);
                    });
                });
            }
        })();
    };
    EmailComposer.prototype.isAvailable = function (app) {
        var _this = this;
        return (function () {
            if (Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["checkAvailability"])(_this) === true) {
                return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["getPromise"])(function (resolve) {
                    Promise.all([_this.hasAccount, _this.hasClient(app)]).then(function (results) {
                        return resolve(results.length === 2 && results[0] && results[1]);
                    });
                });
            }
        })();
    };
    EmailComposer.prototype.open = function (options, scope) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "open", { "successIndex": 1, "errorIndex": 3 }, arguments); };
    EmailComposer.prototype.addAlias = function (alias, packageName) { return Object(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["cordova"])(this, "addAlias", {}, arguments); };
    EmailComposer.pluginName = "EmailComposer";
    EmailComposer.plugin = "cordova-plugin-email-composer";
    EmailComposer.pluginRef = "cordova.plugins.email";
    EmailComposer.repo = "https://github.com/katzer/cordova-plugin-email-composer";
    EmailComposer.platforms = ["Amazon Fire OS", "Android", "Browser", "iOS", "Windows", "macOS"];
    EmailComposer = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], EmailComposer);
    return EmailComposer;
}(_ionic_native_core__WEBPACK_IMPORTED_MODULE_2__["IonicNativePlugin"]));

//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi8uLi8uLi9zcmMvQGlvbmljLW5hdGl2ZS9wbHVnaW5zL2VtYWlsLWNvbXBvc2VyL25neC9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLGlEQUFvRCxVQUFVLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQzs7SUFtSS9ELGlDQUFpQjs7OztJQVNsRCxxQ0FBYTtJQVliLHlDQUFpQjtJQVVqQixrQ0FBVTs7O21EQUFpQjtnQkFDekIsT0FBTyxVQUFVLENBQVUsVUFBQSxPQUFPO29CQUNoQyxhQUFhLENBQUMsU0FBUyxFQUFFLENBQUMsVUFBVSxDQUFDLFVBQUMsTUFBZTt3QkFDbkQsSUFBSSxNQUFNLEVBQUU7NEJBQ1YsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO3lCQUNmOzZCQUFNOzRCQUNMLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDaEI7b0JBQ0gsQ0FBQyxDQUFDLENBQUM7Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7YUFDSjs7O0lBVUQsaUNBQVMsYUFBQyxHQUFZOzs7bURBQWdCO2dCQUNwQyxPQUFPLFVBQVUsQ0FBVSxVQUFBLE9BQU87b0JBQ2hDLElBQUksR0FBRyxFQUFFO3dCQUNQLGFBQWEsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLFVBQUMsTUFBZTs0QkFDdkQsSUFBSSxNQUFNLEVBQUU7Z0NBQ1YsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDOzZCQUNmO2lDQUFNO2dDQUNMLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQzs2QkFDaEI7d0JBQ0gsQ0FBQyxDQUFDLENBQUM7cUJBQ0o7eUJBQU07d0JBQ0wsYUFBYSxDQUFDLFNBQVMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxVQUFDLElBQWM7NEJBQ2xELE9BQU8sQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQzt3QkFDbkMsQ0FBQyxDQUFDLENBQUM7cUJBQ0o7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7YUFDSjs7O0lBU0Qsa0NBQVU7OzttREFBc0I7Z0JBQzlCLE9BQU8sVUFBVSxDQUFXLFVBQUEsT0FBTztvQkFDakMsYUFBYSxDQUFDLFNBQVMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxVQUFDLElBQVM7d0JBQzdDLElBQUksTUFBTSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLGlCQUFpQixFQUFFOzRCQUM5RCxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQzt5QkFDZjt3QkFDRCxPQUFPLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ2hCLENBQUMsQ0FBQyxDQUFDO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2FBQ0o7OztJQVNELG1DQUFXLGFBQUMsR0FBWTs7O21EQUFnQjtnQkFDdEMsT0FBTyxVQUFVLENBQVUsVUFBQSxPQUFPO29CQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSSxDQUFDLFVBQVUsRUFBRSxLQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQSxPQUFPO3dCQUM5RCxPQUFPLE9BQU8sQ0FBQyxPQUFPLENBQUMsTUFBTSxLQUFLLENBQUMsSUFBSSxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ25FLENBQUMsQ0FBQyxDQUFDO2dCQUNMLENBQUMsQ0FBQyxDQUFDO2FBQ0o7OztJQWFELDRCQUFJLGFBQUMsT0FBNkIsRUFBRSxLQUFXO0lBVy9DLGdDQUFRLGFBQUMsS0FBYSxFQUFFLFdBQW1COzs7Ozs7SUE1SGhDLGFBQWE7UUFEekIsVUFBVSxFQUFFO09BQ0EsYUFBYTt3QkFwSTFCO0VBb0ltQyxpQkFBaUI7U0FBdkMsYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENvcmRvdmEsIENvcmRvdmFDaGVjaywgSW9uaWNOYXRpdmVQbHVnaW4sIFBsdWdpbiwgZ2V0UHJvbWlzZSB9IGZyb20gJ0Bpb25pYy1uYXRpdmUvY29yZSc7XG5cbmV4cG9ydCBpbnRlcmZhY2UgRW1haWxDb21wb3Nlck9wdGlvbnMge1xuICAvKipcbiAgICogQXBwIHRvIHNlbmQgdGhlIGVtYWlsIHdpdGhcbiAgICovXG4gIGFwcD86IHN0cmluZztcblxuICAvKipcbiAgICogRW1haWwgYWRkcmVzcyhlcykgZm9yIFRvIGZpZWxkXG4gICAqL1xuICB0bz86IHN0cmluZyB8IHN0cmluZ1tdO1xuXG4gIC8qKlxuICAgKiBFbWFpbCBhZGRyZXNzKGVzKSBmb3IgQ0MgZmllbGRcbiAgICovXG4gIGNjPzogc3RyaW5nIHwgc3RyaW5nW107XG5cbiAgLyoqXG4gICAqIEVtYWlsIGFkZHJlc3MoZXMpIGZvciBCQ0MgZmllbGRcbiAgICovXG4gIGJjYz86IHN0cmluZyB8IHN0cmluZ1tdO1xuXG4gIC8qKlxuICAgKiBGaWxlIHBhdGhzIG9yIGJhc2U2NCBkYXRhIHN0cmVhbXNcbiAgICovXG4gIGF0dGFjaG1lbnRzPzogc3RyaW5nW107XG5cbiAgLyoqXG4gICAqIFN1YmplY3Qgb2YgdGhlIGVtYWlsXG4gICAqL1xuICBzdWJqZWN0Pzogc3RyaW5nO1xuXG4gIC8qKlxuICAgKiBFbWFpbCBib2R5IChmb3IgSFRNTCwgc2V0IGlzSHRtbCB0byB0cnVlKVxuICAgKi9cbiAgYm9keT86IHN0cmluZztcblxuICAvKipcbiAgICogSW5kaWNhdGVzIGlmIHRoZSBib2R5IGlzIEhUTUwgb3IgcGxhaW4gdGV4dFxuICAgKi9cbiAgaXNIdG1sPzogYm9vbGVhbjtcblxuICAvKipcbiAgICogIENvbnRlbnQgdHlwZSBvZiB0aGUgZW1haWwgKEFuZHJvaWQgb25seSlcbiAgICovXG4gIHR5cGU/OiBzdHJpbmc7XG59XG5cbi8qKlxuICogQG5hbWUgRW1haWwgQ29tcG9zZXJcbiAqIEBwcmVtaWVyIGVtYWlsLWNvbXBvc2VyXG4gKiBAZGVzY3JpcHRpb25cbiAqXG4gKiBSZXF1aXJlcyBDb3Jkb3ZhIHBsdWdpbjogY29yZG92YS1wbHVnaW4tZW1haWwtY29tcG9zZXIuIEZvciBtb3JlIGluZm8sIHBsZWFzZSBzZWUgdGhlIFtFbWFpbCBDb21wb3NlciBwbHVnaW4gZG9jc10oaHR0cHM6Ly9naXRodWIuY29tL2h5cGVyeTJrL2NvcmRvdmEtZW1haWwtcGx1Z2luKS5cbiAqXG4gKlxuICogQHVzYWdlXG4gKiBgYGB0eXBlc2NyaXB0XG4gKiBpbXBvcnQgeyBFbWFpbENvbXBvc2VyIH0gZnJvbSAnQGlvbmljLW5hdGl2ZS9lbWFpbC1jb21wb3Nlci9uZ3gnO1xuICpcbiAqIGNvbnN0cnVjdG9yKHByaXZhdGUgZW1haWxDb21wb3NlcjogRW1haWxDb21wb3NlcikgeyB9XG4gKlxuICogLi4uXG4gKlxuICpcbiAqIHRoaXMuZW1haWxDb21wb3Nlci5nZXRDbGllbnRzKCkudGhlbigoYXBwczogW10pID0+IHtcbiAqICAgIC8vIFJldHVybnMgYW4gYXJyYXkgb2YgY29uZmlndXJlZCBlbWFpbCBjbGllbnRzIGZvciB0aGUgZGV2aWNlXG4gKiB9KTtcbiAqXG4gKiB0aGlzLmVtYWlsQ29tcG9zZXIuaGFzQ2xpZW50KCkudGhlbihhcHAsIChpc1ZhbGlkOiBib29sZWFuKSA9PiB7XG4gKiAgaWYgKGlzVmFsaWQpIHtcbiAqICAgIC8vIE5vdyB3ZSBrbm93IHdlIGhhdmUgYSB2YWxpZCBlbWFpbCBjbGllbnQgY29uZmlndXJlZFxuICogICAgLy8gTm90IHNwZWNpZnlpbmcgYW4gYXBwIHdpbGwgcmV0dXJuIHRydWUgaWYgYXQgbGVhc3Qgb25lIGVtYWlsIGNsaWVudCBpcyBjb25maWd1cmVkXG4gKiAgfVxuICogfSk7XG4gKlxuICogdGhpcy5lbWFpbENvbXBvc2VyLmhhc0FjY291bnQoKS50aGVuKChpc1ZhbGlkOiBib29sZWFuKSA9PiB7XG4gKiAgaWYgKGlzVmFsaWQpIHtcbiAqICAgIC8vIE5vdyB3ZSBrbm93IHdlIGhhdmUgYSB2YWxpZCBlbWFpbCBhY2NvdW50IGNvbmZpZ3VyZWRcbiAqICB9XG4gKiB9KTtcbiAqXG4gKiB0aGlzLmVtYWlsQ29tcG9zZXIuaXNBdmFpbGFibGUoKS50aGVuKGFwcCwgKGF2YWlsYWJsZTogYm9vbGVhbikgPT4ge1xuICogIGlmKGF2YWlsYWJsZSkge1xuICogICAgLy8gTm93IHdlIGtub3cgd2UgY2FuIHNlbmQgYW4gZW1haWwsIGNhbGxzIGhhc0NsaWVudCBhbmQgaGFzQWNjb3VudFxuICogICAgLy8gTm90IHNwZWNpZnlpbmcgYW4gYXBwIHdpbGwgcmV0dXJuIHRydWUgaWYgYXQgbGVhc3Qgb25lIGVtYWlsIGNsaWVudCBpcyBjb25maWd1cmVkXG4gKiAgfVxuICogfSk7XG4gKlxuICogbGV0IGVtYWlsID0ge1xuICogICB0bzogJ21heEBtdXN0ZXJtYW5uLmRlJyxcbiAqICAgY2M6ICdlcmlrYUBtdXN0ZXJtYW5uLmRlJyxcbiAqICAgYmNjOiBbJ2pvaG5AZG9lLmNvbScsICdqYW5lQGRvZS5jb20nXSxcbiAqICAgYXR0YWNobWVudHM6IFtcbiAqICAgICAnZmlsZTovL2ltZy9sb2dvLnBuZycsXG4gKiAgICAgJ3JlczovL2ljb24ucG5nJyxcbiAqICAgICAnYmFzZTY0Omljb24ucG5nLy9pVkJPUncwS0dnb0FBQUFOU1VoRVVnLi4uJyxcbiAqICAgICAnZmlsZTovL1JFQURNRS5wZGYnXG4gKiAgIF0sXG4gKiAgIHN1YmplY3Q6ICdDb3Jkb3ZhIEljb25zJyxcbiAqICAgYm9keTogJ0hvdyBhcmUgeW91PyBOaWNlIGdyZWV0aW5ncyBmcm9tIExlaXB6aWcnLFxuICogICBpc0h0bWw6IHRydWVcbiAqIH1cbiAqXG4gKiAvLyBTZW5kIGEgdGV4dCBtZXNzYWdlIHVzaW5nIGRlZmF1bHQgb3B0aW9uc1xuICogdGhpcy5lbWFpbENvbXBvc2VyLm9wZW4oZW1haWwpO1xuICogYGBgXG4gKlxuICogWW91IGNhbiBhbHNvIGFzc2lnbiBhbGlhc2VzIHRvIGVtYWlsIGFwcHNcbiAqIGBgYHRzXG4gKiAvLyBhZGQgYWxpYXNcbiAqIHRoaXMuZW1haWwuYWRkQWxpYXMoJ2dtYWlsJywgJ2NvbS5nb29nbGUuYW5kcm9pZC5nbScpO1xuICpcbiAqIC8vIHRoZW4gdXNlIGFsaWFzIHdoZW4gc2VuZGluZyBlbWFpbFxuICogdGhpcy5lbWFpbC5vcGVuKHtcbiAqICAgYXBwOiAnZ21haWwnLFxuICogICAuLi5cbiAqIH0pO1xuICogYGBgXG4gKiBAaW50ZXJmYWNlc1xuICogRW1haWxDb21wb3Nlck9wdGlvbnNcbiAqL1xuQFBsdWdpbih7XG4gIHBsdWdpbk5hbWU6ICdFbWFpbENvbXBvc2VyJyxcbiAgcGx1Z2luOiAnY29yZG92YS1wbHVnaW4tZW1haWwtY29tcG9zZXInLFxuICBwbHVnaW5SZWY6ICdjb3Jkb3ZhLnBsdWdpbnMuZW1haWwnLFxuICByZXBvOiAnaHR0cHM6Ly9naXRodWIuY29tL2thdHplci9jb3Jkb3ZhLXBsdWdpbi1lbWFpbC1jb21wb3NlcicsXG4gIHBsYXRmb3JtczogWydBbWF6b24gRmlyZSBPUycsICdBbmRyb2lkJywgJ0Jyb3dzZXInLCAnaU9TJywgJ1dpbmRvd3MnLCAnbWFjT1MnXSxcbn0pXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgRW1haWxDb21wb3NlciBleHRlbmRzIElvbmljTmF0aXZlUGx1Z2luIHtcbiAgLyoqXG4gICAqIENoZWNrcyBpZiB0aGUgYXBwIGhhcyBhIHBlcm1pc3Npb24gdG8gYWNjZXNzIGVtYWlsIGFjY291bnRzIGluZm9ybWF0aW9uXG4gICAqIEByZXR1cm4ge1Byb21pc2U8Ym9vbGVhbj59IHJldHVybnMgYSBwcm9taXNlIHRoYXQgcmVzb2x2ZXMgd2l0aCBhIGJvb2xlYW4gdGhhdCBpbmRpY2F0ZXMgaWYgdGhlIHBlcm1pc3Npb24gd2FzIGdyYW50ZWRcbiAgICovXG4gIEBDb3Jkb3ZhKHtcbiAgICBzdWNjZXNzSW5kZXg6IDAsXG4gICAgZXJyb3JJbmRleDogMixcbiAgfSlcbiAgaGFzUGVybWlzc2lvbigpOiBQcm9taXNlPGJvb2xlYW4+IHtcbiAgICByZXR1cm47XG4gIH1cblxuICAvKipcbiAgICogUmVxdWVzdCBwZXJtaXNzaW9uIHRvIGFjY2VzcyBlbWFpbCBhY2NvdW50cyBpbmZvcm1hdGlvblxuICAgKiBAcmV0dXJuIHtQcm9taXNlPGJvb2xlYW4+fSByZXR1cm5zIGEgcHJvbWlzZSB0aGF0IHJlc29sdmVzIHdpdGggYSBib29sZWFuIHRoYXQgaW5kaWNhdGVzIGlmIHRoZSBwZXJtaXNzaW9uIHdhcyBncmFudGVkXG4gICAqL1xuICBAQ29yZG92YSh7XG4gICAgc3VjY2Vzc0luZGV4OiAwLFxuICAgIGVycm9ySW5kZXg6IDIsXG4gIH0pXG4gIHJlcXVlc3RQZXJtaXNzaW9uKCk6IFByb21pc2U8Ym9vbGVhbj4ge1xuICAgIHJldHVybjtcbiAgfVxuXG4gIC8qKlxuICAgKiBWZXJpZmllcyBpZiBhbiBlbWFpbCBhY2NvdW50IGlzIGNvbmZpZ3VyZWQgb24gdGhlIGRldmljZS5cbiAgICpcbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn0gUmVzb2x2ZXMgaWYgYXZhaWxhYmxlLCByZWplY3RzIGlmIG5vdCBhdmFpbGFibGVcbiAgICovXG4gIEBDb3Jkb3ZhQ2hlY2soKVxuICBoYXNBY2NvdW50KCk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuIGdldFByb21pc2U8Ym9vbGVhbj4ocmVzb2x2ZSA9PiB7XG4gICAgICBFbWFpbENvbXBvc2VyLmdldFBsdWdpbigpLmhhc0FjY291bnQoKHJlc3VsdDogYm9vbGVhbikgPT4ge1xuICAgICAgICBpZiAocmVzdWx0KSB7XG4gICAgICAgICAgcmVzb2x2ZSh0cnVlKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICByZXNvbHZlKGZhbHNlKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cblxuICAvKipcbiAgICogVmVyaWZpZXMgaWYgYSBzcGVjaWZpYyBlbWFpbCBjbGllbnQgaXMgaW5zdGFsbGVkIG9uIHRoZSBkZXZpY2UuXG4gICAqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBbYXBwXSBBcHAgaWQgb3IgdXJpIHNjaGVtZS5cbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn0gUmVzb2x2ZXMgaWYgYXZhaWxhYmxlLCByZWplY3RzIGlmIG5vdCBhdmFpbGFibGVcbiAgICovXG5cbiAgQENvcmRvdmFDaGVjaygpXG4gIGhhc0NsaWVudChhcHA/OiBzdHJpbmcpOiBQcm9taXNlPGFueT4ge1xuICAgIHJldHVybiBnZXRQcm9taXNlPGJvb2xlYW4+KHJlc29sdmUgPT4ge1xuICAgICAgaWYgKGFwcCkge1xuICAgICAgICBFbWFpbENvbXBvc2VyLmdldFBsdWdpbigpLmhhc0NsaWVudChhcHAsIChyZXN1bHQ6IGJvb2xlYW4pID0+IHtcbiAgICAgICAgICBpZiAocmVzdWx0KSB7XG4gICAgICAgICAgICByZXNvbHZlKHRydWUpO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICByZXNvbHZlKGZhbHNlKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgRW1haWxDb21wb3Nlci5nZXRQbHVnaW4oKS5nZXRDbGllbnRzKChhcHBzOiBzdHJpbmdbXSkgPT4ge1xuICAgICAgICAgIHJlc29sdmUoYXBwcyAmJiBhcHBzLmxlbmd0aCA+IDApO1xuICAgICAgICB9KTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBSZXR1cm5zIGFuIGFycmF5IG9mIGVtYWlsIGNsaWVudHMgaW5zdGFsbGVkIG9uIHRoZSBkZXZpY2UuXG4gICAqXG4gICAqIEByZXR1cm5zIHtQcm9taXNlPHN0cmluZ1tdPn0gUmVzb2x2ZXMgaWYgYXZhaWxhYmxlLCByZWplY3RzIGlmIG5vdCBhdmFpbGFibGVcbiAgICovXG4gIEBDb3Jkb3ZhQ2hlY2soKVxuICBAQ29yZG92YSh7IHBsYXRmb3JtczogWydBbmRyb2lkJ10gfSlcbiAgZ2V0Q2xpZW50cygpOiBQcm9taXNlPHN0cmluZ1tdPiB7XG4gICAgcmV0dXJuIGdldFByb21pc2U8c3RyaW5nW10+KHJlc29sdmUgPT4ge1xuICAgICAgRW1haWxDb21wb3Nlci5nZXRQbHVnaW4oKS5nZXRDbGllbnRzKChhcHBzOiBhbnkpID0+IHtcbiAgICAgICAgaWYgKE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChhcHBzKSA9PT0gJ1tvYmplY3QgU3RyaW5nXScpIHtcbiAgICAgICAgICBhcHBzID0gW2FwcHNdO1xuICAgICAgICB9XG4gICAgICAgIHJlc29sdmUoYXBwcyk7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxuXG4gIC8qKlxuICAgKiBWZXJpZmllcyBpZiBzZW5kaW5nIGVtYWlscyBpcyBzdXBwb3J0ZWQgb24gdGhlIGRldmljZS5cbiAgICpcbiAgICogQHBhcmFtIHtzdHJpbmd9IFthcHBdIEFwcCBpZCBvciB1cmkgc2NoZW1lLlxuICAgKiBAcmV0dXJucyB7UHJvbWlzZTxhbnk+fSBSZXNvbHZlcyBpZiBhdmFpbGFibGUsIHJlamVjdHMgaWYgbm90IGF2YWlsYWJsZVxuICAgKi9cbiAgQENvcmRvdmFDaGVjaygpXG4gIGlzQXZhaWxhYmxlKGFwcD86IHN0cmluZyk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuIGdldFByb21pc2U8Ym9vbGVhbj4ocmVzb2x2ZSA9PiB7XG4gICAgICBQcm9taXNlLmFsbChbdGhpcy5oYXNBY2NvdW50LCB0aGlzLmhhc0NsaWVudChhcHApXSkudGhlbihyZXN1bHRzID0+IHtcbiAgICAgICAgcmV0dXJuIHJlc29sdmUocmVzdWx0cy5sZW5ndGggPT09IDIgJiYgcmVzdWx0c1swXSAmJiByZXN1bHRzWzFdKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG5cbiAgLyoqXG4gICAqIERpc3BsYXlzIHRoZSBlbWFpbCBjb21wb3NlciBwcmUtZmlsbGVkIHdpdGggZGF0YS5cbiAgICpcbiAgICogQHBhcmFtIHtFbWFpbENvbXBvc2VyT3B0aW9uc30gb3B0aW9ucyBFbWFpbFxuICAgKiBAcGFyYW0ge2FueX0gW3Njb3BlXSBTY29wZSBmb3IgdGhlIHByb21pc2VcbiAgICogQHJldHVybnMge1Byb21pc2U8YW55Pn0gUmVzb2x2ZXMgcHJvbWlzZSB3aGVuIHRoZSBFbWFpbENvbXBvc2VyIGhhcyBiZWVuIG9wZW5lZFxuICAgKi9cbiAgQENvcmRvdmEoe1xuICAgIHN1Y2Nlc3NJbmRleDogMSxcbiAgICBlcnJvckluZGV4OiAzLFxuICB9KVxuICBvcGVuKG9wdGlvbnM6IEVtYWlsQ29tcG9zZXJPcHRpb25zLCBzY29wZT86IGFueSk6IFByb21pc2U8YW55PiB7XG4gICAgcmV0dXJuO1xuICB9XG5cbiAgLyoqXG4gICAqIEFkZHMgYSBuZXcgbWFpbCBhcHAgYWxpYXMuXG4gICAqXG4gICAqIEBwYXJhbSB7c3RyaW5nfSBhbGlhcyBUaGUgYWxpYXMgbmFtZVxuICAgKiBAcGFyYW0ge3N0cmluZ30gcGFja2FnZU5hbWUgVGhlIHBhY2thZ2UgbmFtZVxuICAgKi9cbiAgQENvcmRvdmEoKVxuICBhZGRBbGlhcyhhbGlhczogc3RyaW5nLCBwYWNrYWdlTmFtZTogc3RyaW5nKTogdm9pZCB7fVxufVxuIl19

/***/ }),

/***/ "./src/app/core/auth/auth.service.ts":
/*!*******************************************!*\
  !*** ./src/app/core/auth/auth.service.ts ***!
  \*******************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _user_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../user/user.service */ "./src/app/core/user/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//const API_URL = 'http://159.203.181.9:3000';
var API_URL = 'http://localhost:3000';
var AuthService = /** @class */ (function () {
    function AuthService(http, userService) {
        this.http = http;
        this.userService = userService;
    }
    AuthService.prototype.authenticate = function (phone, password) {
        var _this = this;
        return this.http.post(API_URL + "/user/login/mot", { phone: phone, password: password }, { observe: 'response' })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["tap"])(function (res) {
            var authToken = res.headers.get('x-access-token');
            _this.userService.setToken(authToken);
            console.log("User " + phone + " authenticated with token " + authToken);
        }));
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _user_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/pages/login/login.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.module.ts ***!
  \*********************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_email_composer_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/email-composer/ngx */ "./node_modules/@ionic-native/email-composer/ngx/index.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/login/login.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }
];
var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]],
            providers: [_ionic_native_email_composer_ngx__WEBPACK_IMPORTED_MODULE_5__["EmailComposer"]]
        })
    ], LoginPageModule);
    return LoginPageModule;
}());



/***/ }),

/***/ "./src/app/pages/login/login.page.html":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<ion-header>\r\n  <title>Uai Leva / Login</title>\r\n  <meta charset=\"utf-8\" />\r\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\r\n\r\n</ion-header>\r\n\r\n\r\n<ion-content class=\"login\">\r\n\r\n  <div class=\"container\">\r\n    <br>\r\n      <div class=\"logoLogin\">\r\n          <img src='/assets/img/logo-branco.png' alt=\"Uai Leva\">\r\n          \r\n      </div>\r\n      <br>\r\n      <div class=\"boxConteudo\">      \r\n        <form [formGroup]=\"onLoginForm\" class=\"list-form\">\r\n              <div class=\"form-group\">\r\n                <div class=\"position\"><img src='/assets/icon/brasil.png' style=\"width: 2.5em; height: 2.5em;\"></div>\r\n                <input type=\"phone\" formControlName=\"phone\" class=\"form-control cel-sp-mask\" id=\"celular\" placeholder=\"Telefone\" onkeypress=\"$(this).mask('(00) 90000-0000')\"/>\r\n              </div>\r\n              <div class=\"form-group\">\r\n                <input type=\"password\" formControlName=\"password\" class=\"form-control cel-sp-mask\" id=\"celular\" placeholder=\"Senha\"/>\r\n              </div>\r\n                \r\n        </form>\r\n        <div class=\"text-center buttonArea\">\r\n          <button class=\"btn btn-primary btn-lg rounded-pill btnLogin\" (click)=\"goToHome()\">\r\n              ENTRAR\r\n          </button>\r\n        </div>\r\n          <div class=\"linkEsqueciSenha\">\r\n              <a href=\"\">Esqueci minha Senha</a>\r\n          </div>\r\n\r\n          <ion-grid>\r\n              <p class=\"textLogin\">OU LOGAR COM:</p>\r\n            <ion-row>\r\n              <ion-col size=\"6\">\r\n              <ion-button shape=\"round\" expand=\"full\" color=\"tertiary\" (click)=\"loginFb()\">\r\n                  <img [src]=\"user.img\" alt=\"\"/>\r\n                  <ion-icon slot=\"icon-only\" name=\"logo-facebook\" style=\"color: #1b2c7b;\"></ion-icon>\r\n                </ion-button>\r\n              </ion-col>\r\n              <ion-col size=\"6\">\r\n                <ion-button shape=\"round\" expand=\"full\" color=\"tertiary\">\r\n                  <ion-icon slot=\"icon-only\" name=\"logo-googleplus\" style=\"color: #1b2c7b;\"></ion-icon>\r\n                </ion-button>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-grid>\r\n\r\n          <div class=\"linkCadastrar\">\r\n              <a (click)=\"goToRegister()\">Quero Cadastrar > </a>\r\n          </div>\r\n\r\n          \r\n      </div>\r\n\r\n  </div>\r\n\r\n</ion-content>\r\n\r\n"

/***/ }),

/***/ "./src/app/pages/login/login.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@import url(\"https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,400;0,500;1,400;1,500&display=swap\");\n.login {\n  --background: url('bg-login.jpg') !important; }\n@media (max-width: 992px) {\n  ion-content {\n    --background: white;\n    background-size: cover;\n    height: inherit; }\n  .navbar-collapse {\n    position: fixed;\n    top: 0px;\n    left: 0;\n    padding-left: 15px;\n    padding-right: 15px;\n    padding-bottom: 15px;\n    width: 75%;\n    height: 100%;\n    background: #1765e2;\n    text-transform: uppercase;\n    z-index: 9999;\n    box-shadow: 0px 0px 90px black; }\n  .navbar-collapse a.nav-link {\n    color: white !important;\n    font-weight: bold;\n    font-size: 19px; }\n  a.nav-link svg {\n    font-size: 25px;\n    margin-right: 10px; }\n  .navbar-dark .navbar-toggler {\n    background: #1b2c7b; }\n  .navbar-collapse.collapsing {\n    left: -75%;\n    transition: height 0s ease; }\n  .navbar-collapse.show {\n    margin-top: 0px;\n    left: 0;\n    transition: left 300ms ease-in-out; }\n  .navbar-toggler.collapsed ~ .navbar-collapse {\n    transition: left 500ms ease-in-out; } }\nbody {\n  background: url('bg-uai-leva.png') no-repeat #eaeaea;\n  font-family: 'Rubik', sans-serif;\n  background-position: 80px bottom;\n  background-size: contain;\n  height: 100vh; }\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  text-transform: uppercase;\n  margin: 20px 0;\n  font-weight: bold;\n  color: #123b7d; }\nh3 {\n  font-size: 0.75rem;\n  text-align: left;\n  text-transform: uppercase;\n  margin: 20px 0;\n  font-weight: bold;\n  color: #1c72fd; }\n.navbar.bg-dark {\n  background: #1c72fd !important; }\n.boxConteudo {\n  background: white;\n  padding: 20px;\n  border-radius: 20px;\n  position: relative;\n  box-shadow: 0px 0px 10px #e4e4e4; }\n.boxConteudo label {\n  text-transform: uppercase;\n  font-size: 12px; }\n/*.boxConteudo .form-control {\r\n        height: calc(1.5em + .75rem + 14px);\r\n        border-radius: 100px;\r\n    }*/\n.boxConteudo .form-control {\n  height: calc(1.5em + .75rem + 14px);\n  border-radius: 100px; }\n/*body.login {\r\n        --background: url('src/assets/img/bg-login.jpg') !important;\r\n        background-size: cover;\r\n        height: inherit;\r\n    }*/\n.position {\n  position: absolute;\n  top: 6px;\n  left: 10px; }\n.logoLogin img {\n  margin: 30px auto;\n  display: block;\n  width: 30%; }\n.login .boxConteudo .form-control {\n  font-size: 13px;\n  text-align: center; }\n.login .boxConteudo {\n  background: none;\n  box-shadow: none;\n  margin-top: 40%; }\n.login button.btnLogin {\n  background: #b1ff49;\n  border: none;\n  color: #002752;\n  font-weight: normal; }\n.login button.btnLogin:active {\n  background-color: #99dd3e !important;\n  border-color: #99dd3e !important;\n  color: #002752 !important; }\ninput#inputLogin, input#inputSenha {\n  padding-right: 45px; }\n.login input[type=text]:focus, .login input[type=password]:focus {\n  color: #1c72fd; }\n.login .form-group {\n  position: relative; }\n.login .form-group svg {\n  position: absolute;\n  left: 20px;\n  top: 11px;\n  font-size: 20px; }\n.linkEsqueciSenha a {\n  color: white;\n  text-transform: uppercase;\n  font-size: 12px;\n  display: block;\n  text-align: center;\n  margin-top: 10px; }\n.linkCadastrar a {\n  color: #b1ff49;\n  text-transform: uppercase;\n  font-size: 12px;\n  display: block;\n  text-align: center;\n  margin-top: 10px; }\n/*.buttonArea {\r\n    margin: 20px -20px 0px -20px;\r\n    background: #101a4c;\r\n    width: 100%;\r\n    display: block;\r\n    position: absolute;\r\n    z-index: 0;\r\n    bottom: -50px;\r\n    padding: 10px;\r\n    border-radius: 0 0 20px 20px;\r\n    }*/\n.buttonArea button {\n  width: 100%; }\n.buttonArea svg {\n  font-size: 25px;\n  margin-right: 5px; }\nbutton.navbar-toggler {\n  border-radius: 100px;\n  height: 55px !important;\n  background: #ffffff;\n  border: #92f328 solid 0px;\n  outline: none; }\n.boxConteudo .btn {\n  font-weight: bold; }\n.mapa iframe {\n  border: 0;\n  width: 100%;\n  height: 100vh;\n  position: absolute;\n  top: 0px;\n  z-index: -1; }\n.saudacao {\n  text-align: center; }\n.homePassageiro .boxConteudo {\n  position: relative;\n  top: 30px;\n  width: calc(100% - 0px);\n  margin: 0 auto;\n  padding: 15px 5px; }\n.homePassageiro .buttonArea button.rounded-pill {\n  font-size: 18px; }\nbutton.escolherDestino {\n  width: calc(100% - 60px);\n  float: left; }\nbutton.embarqueRapido {\n  float: right;\n  padding: 0px 12px;\n  width: 55px;\n  height: 55px; }\nbutton.embarqueRapido svg {\n  width: 35px;\n  height: 35x;\n  color: white; }\nbutton.btn.btn-primary.btn-lg.rounded-pill.embarqueRapido {\n  background: #1c72fd;\n  border: none; }\n.formDestino {\n  border: none;\n  margin-top: 5px; }\n.endDestinoRecente {\n  font-size: 11px;\n  background: #eee;\n  padding: 14px;\n  border-radius: 5px; }\n.endDestinoRecente b {\n  font-weight: normal;\n  color: darkgray; }\n.tituloDestinoRecente {\n  text-transform: uppercase;\n  font-weight: bold; }\n.bEntrar {\n  background: #1765e2;\n  padding: 20px;\n  border-radius: 100%;\n  position: relative;\n  box-shadow: 0px 0px 10px #e4e4e4; }\na.btnEditar {\n  float: right; }\n.labelCadastrado {\n  float: left;\n  text-transform: uppercase;\n  font-size: 13px; }\n.valorCadastrado {\n  clear: both;\n  color: #1c72fd;\n  font-size: 18px; }\na.btnEditar {\n  float: right;\n  background: #fff949;\n  color: #9a9615;\n  padding: 2px 10px;\n  border-radius: 20px;\n  font-size: 13px; }\na.btn.btnNeutro {\n  font-size: 14px;\n  width: 100%;\n  font-weight: normal; }\na.btn.btnNeutro svg {\n  margin-top: -3px;\n  margin-right: 5px; }\n.textLogin {\n  color: white;\n  text-transform: uppercase;\n  font-size: 12px;\n  display: block;\n  text-align: center;\n  margin-top: 10px;\n  padding-bottom: 5px; }\n/*:host {\r\n    ion-content {\r\n        --background: linear-gradient(135deg, var(--ion-color-light), var(--ion-color-light));\r\n    }\r\n}\r\n\r\n.paz {\r\n    position: relative;\r\n    z-index: 10;\r\n}*/\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbG9naW4vQzpcXFVzZXJzXFxqZXNzaVxcRG9jdW1lbnRzXFx1YWlsZXZhZHJpdmVyL3NyY1xcYXBwXFxwYWdlc1xcbG9naW5cXGxvZ2luLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLDRHQUFZO0FBRVo7RUFFSSw0Q0FBYSxFQUFBO0FBR2pCO0VBQ0k7SUFFSSxtQkFBYTtJQUNiLHNCQUFzQjtJQUN0QixlQUFlLEVBQUE7RUFHbkI7SUFDSSxlQUFlO0lBQ2YsUUFBUTtJQUNSLE9BQU87SUFDUCxrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLG9CQUFvQjtJQUNwQixVQUFVO0lBQ1YsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQix5QkFBeUI7SUFDekIsYUFBYTtJQUNiLDhCQUE4QixFQUFBO0VBR2xDO0lBQ0EsdUJBQXVCO0lBQ3ZCLGlCQUFpQjtJQUNqQixlQUFlLEVBQUE7RUFHZjtJQUNBLGVBQWU7SUFDZixrQkFBa0IsRUFBQTtFQUdsQjtJQUNJLG1CQUFtQixFQUFBO0VBT3ZCO0lBQ0ksVUFBVTtJQUNWLDBCQUEwQixFQUFBO0VBRzlCO0lBQ0ksZUFBZTtJQUNmLE9BQU87SUFDUCxrQ0FBa0MsRUFBQTtFQUd0QztJQUNJLGtDQUFrQyxFQUFBLEVBQ3JDO0FBSUY7RUFDQyxvREFBbUU7RUFDbkUsZ0NBQWdDO0VBQ2hDLGdDQUFnQztFQUNoQyx3QkFBd0I7RUFDeEIsYUFBYSxFQUFBO0FBR2I7RUFDQSxpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLHlCQUF5QjtFQUN6QixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGNBQWMsRUFBQTtBQUdkO0VBQ0ksa0JBQWtCO0VBQ2xCLGdCQUFnQjtFQUNoQix5QkFBeUI7RUFDekIsY0FBYztFQUNkLGlCQUFpQjtFQUNqQixjQUFjLEVBQUE7QUFHbEI7RUFDSSw4QkFBNEIsRUFBQTtBQUdoQztFQUNJLGlCQUFpQjtFQUNqQixhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixnQ0FBZ0MsRUFBQTtBQUdwQztFQUNBLHlCQUF5QjtFQUN6QixlQUFlLEVBQUE7QUFHZjs7O01DNUJFO0FEaUNGO0VBQ0ksbUNBQW1DO0VBQ25DLG9CQUFvQixFQUFBO0FBSXhCOzs7O01DOUJFO0FEb0NGO0VBQ0ksa0JBQWtCO0VBQ2xCLFFBQVE7RUFDUixVQUFVLEVBQUE7QUFHZDtFQUNJLGlCQUFpQjtFQUNqQixjQUFjO0VBQ2QsVUFBVSxFQUFBO0FBR2Q7RUFDSSxlQUFlO0VBQ2Ysa0JBQWtCLEVBQUE7QUFHdEI7RUFDSSxnQkFBZ0I7RUFDaEIsZ0JBQWdCO0VBQ2hCLGVBQWUsRUFBQTtBQUduQjtFQUNJLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osY0FBYztFQUNkLG1CQUFtQixFQUFBO0FBR3ZCO0VBQ0ksb0NBQW9DO0VBQ3BDLGdDQUFnQztFQUNoQyx5QkFBeUIsRUFBQTtBQUc3QjtFQUNJLG1CQUFtQixFQUFBO0FBR3ZCO0VBQ0ksY0FBYyxFQUFBO0FBR2xCO0VBQ0ksa0JBQWtCLEVBQUE7QUFHdEI7RUFDSSxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLFNBQVM7RUFDVCxlQUFlLEVBQUE7QUFHbkI7RUFDSSxZQUFZO0VBQ1oseUJBQXlCO0VBQ3pCLGVBQWU7RUFDZixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGdCQUFnQixFQUFBO0FBR3BCO0VBQ0ksY0FBYztFQUNkLHlCQUF5QjtFQUN6QixlQUFlO0VBQ2YsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixnQkFBZ0IsRUFBQTtBQUdwQjs7Ozs7Ozs7OztNQ3JDRTtBRGlERjtFQUNJLFdBQVUsRUFBQTtBQUdkO0VBQ0EsZUFBZTtFQUNmLGlCQUFpQixFQUFBO0FBR2pCO0VBQ0Esb0JBQW9CO0VBQ3BCLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIseUJBQXlCO0VBQ3pCLGFBQWEsRUFBQTtBQUdiO0VBQ0EsaUJBQWlCLEVBQUE7QUFHakI7RUFDSSxTQUFTO0VBQ1QsV0FBVztFQUNYLGFBQVk7RUFDWixrQkFBa0I7RUFDbEIsUUFBUTtFQUNSLFdBQVcsRUFBQTtBQUdmO0VBQ0ksa0JBQWtCLEVBQUE7QUFHdEI7RUFDSSxrQkFBa0I7RUFDbEIsU0FBUztFQUNULHVCQUF1QjtFQUN2QixjQUFjO0VBQ2QsaUJBQWlCLEVBQUE7QUFHckI7RUFDSSxlQUFlLEVBQUE7QUFHbkI7RUFDSSx3QkFBd0I7RUFDeEIsV0FBVyxFQUFBO0FBR2Y7RUFDSSxZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLFdBQVc7RUFDWCxZQUFZLEVBQUE7QUFHaEI7RUFDSSxXQUFXO0VBQ1gsV0FBVztFQUNYLFlBQVksRUFBQTtBQUdoQjtFQUNJLG1CQUFtQjtFQUNuQixZQUFZLEVBQUE7QUFHaEI7RUFDSSxZQUFZO0VBQ1osZUFBZSxFQUFBO0FBR25CO0VBQ0ksZUFBZTtFQUNmLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2Isa0JBQWtCLEVBQUE7QUFHdEI7RUFDSSxtQkFBbUI7RUFDbkIsZUFBZSxFQUFBO0FBR25CO0VBQ0kseUJBQXlCO0VBQ3pCLGlCQUFpQixFQUFBO0FBR3JCO0VBRUksbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLGdDQUFnQyxFQUFBO0FBR3hDO0VBQ0ksWUFBWSxFQUFBO0FBR2hCO0VBQ0ksV0FBVztFQUNYLHlCQUF5QjtFQUN6QixlQUFlLEVBQUE7QUFHbkI7RUFDSSxXQUFXO0VBQ1gsY0FBYztFQUNkLGVBQWUsRUFBQTtBQUduQjtFQUNJLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsY0FBYztFQUNkLGlCQUFpQjtFQUNqQixtQkFBbUI7RUFDbkIsZUFBZSxFQUFBO0FBR25CO0VBQ0ksZUFBZTtFQUNmLFdBQVc7RUFDWCxtQkFBbUIsRUFBQTtBQUd2QjtFQUNJLGdCQUFnQjtFQUNoQixpQkFBaUIsRUFBQTtBQUdqQjtFQUNJLFlBQVk7RUFDWix5QkFBeUI7RUFDekIsZUFBZTtFQUNmLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsZ0JBQWdCO0VBQ2hCLG1CQUFtQixFQUFBO0FBRzNCOzs7Ozs7Ozs7RUNoRUUiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAaW1wb3J0IHVybCgnaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3MyP2ZhbWlseT1SdWJpazppdGFsLHdnaHRAMCw0MDA7MCw1MDA7MSw0MDA7MSw1MDAmZGlzcGxheT1zd2FwJyk7XHJcblxyXG4ubG9naW4ge1xyXG4gICAgLy8tLWJhY2tncm91bmQ6IHVybCgnc3JjL2Fzc2V0cy9pbWcvYmctdWFpLWxldmEyLnBuZycpICNlYWVhZWEgbm8tcmVwZWF0IGJvdHRvbSA3NXB4IGxlZnQgMzVweDtcclxuICAgIC0tYmFja2dyb3VuZDogdXJsKFwic3JjL2Fzc2V0cy9pbWcvYmctbG9naW4uanBnXCIpICFpbXBvcnRhbnQ7XHJcbiBcclxufVxyXG5AbWVkaWEgKG1heC13aWR0aDogOTkycHgpIHtcclxuICAgIGlvbi1jb250ZW50IHtcclxuICAgICAgICAvLy0tYmFja2dyb3VuZDogdXJsKCdzcmMvYXNzZXRzL2ltZy9iZy11YWktbGV2YTIucG5nJykgI2VhZWFlYSBuby1yZXBlYXQgYm90dG9tIDc1cHggbGVmdCAzNXB4O1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgICAgICBoZWlnaHQ6IGluaGVyaXQ7XHJcbiAgICB9XHJcblxyXG4gICAgLm5hdmJhci1jb2xsYXBzZSB7XHJcbiAgICAgICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgICAgIHRvcDogMHB4O1xyXG4gICAgICAgIGxlZnQ6IDA7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xyXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDE1cHg7XHJcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDE1cHg7XHJcbiAgICAgICAgd2lkdGg6IDc1JTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzE3NjVlMjtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgIHotaW5kZXg6IDk5OTk7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDBweCA5MHB4IGJsYWNrO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAubmF2YmFyLWNvbGxhcHNlIGEubmF2LWxpbmsge1xyXG4gICAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGZvbnQtc2l6ZTogMTlweDtcclxuICAgIH1cclxuXHJcbiAgICBhLm5hdi1saW5rIHN2ZyB7XHJcbiAgICBmb250LXNpemU6IDI1cHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5uYXZiYXItZGFyayAubmF2YmFyLXRvZ2dsZXIge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICMxYjJjN2I7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC8vLm5hdmJhci1kYXJrIHNwYW4ubmF2YmFyLXRvZ2dsZXItaWNvbiB7XHJcbiAgICAvLy9iYWNrZ3JvdW5kLWltYWdlOiB1cmwoZGF0YTppbWFnZS9zdmcreG1sLCUzY3N2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPSczMCcgaGVpZ2h0PSczMCcgdmlld0JveD0nMCAwIDMwIDMwJyUzZSUzY3BhdGggc3Ryb2tlPSdyZ2JhJTI4MjU1LCAyNTUsIDI1NSwgMC45JTI5JyBzdHJva2UtbGluZWNhcD0ncm91bmQnIHN0cm9rZS1taXRlcmxpbWl0PScxMCcgc3Ryb2tlLXdpZHRoPScyJyBkPSdNNCA3aDIyTTQgMTVoMjJNNCAyM2gyMicvJTNlJTNjL3N2ZyUzZSk7XHJcbiAgICAvL31cclxuXHJcbiAgICAubmF2YmFyLWNvbGxhcHNlLmNvbGxhcHNpbmcge1xyXG4gICAgICAgIGxlZnQ6IC03NSU7XHJcbiAgICAgICAgdHJhbnNpdGlvbjogaGVpZ2h0IDBzIGVhc2U7XHJcbiAgICB9XHJcblxyXG4gICAgLm5hdmJhci1jb2xsYXBzZS5zaG93IHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgICAgICAgbGVmdDogMDtcclxuICAgICAgICB0cmFuc2l0aW9uOiBsZWZ0IDMwMG1zIGVhc2UtaW4tb3V0O1xyXG4gICAgfVxyXG5cclxuICAgIC5uYXZiYXItdG9nZ2xlci5jb2xsYXBzZWQgfiAubmF2YmFyLWNvbGxhcHNlIHtcclxuICAgICAgICB0cmFuc2l0aW9uOiBsZWZ0IDUwMG1zIGVhc2UtaW4tb3V0O1xyXG4gICAgfVxyXG4gICAgXHJcbn1cclxuXHJcbiAgIGJvZHkge1xyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXNzZXRzL2ltZy9iZy11YWktbGV2YS5wbmcnKSBuby1yZXBlYXQgI2VhZWFlYTtcclxuICAgIGZvbnQtZmFtaWx5OiAnUnViaWsnLCBzYW5zLXNlcmlmO1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogODBweCBib3R0b207XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICBoZWlnaHQ6IDEwMHZoO1xyXG59XHJcblxyXG4gICAgaDEge1xyXG4gICAgZm9udC1zaXplOiAxLjVyZW07XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgbWFyZ2luOiAyMHB4IDA7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGNvbG9yOiAjMTIzYjdkO1xyXG4gICAgfVxyXG5cclxuICAgIGgzIHtcclxuICAgICAgICBmb250LXNpemU6IDAuNzVyZW07XHJcbiAgICAgICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgIG1hcmdpbjogMjBweCAwO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICAgIGNvbG9yOiAjMWM3MmZkO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAubmF2YmFyLmJnLWRhcmsge1xyXG4gICAgICAgIGJhY2tncm91bmQ6IzFjNzJmZCFpbXBvcnRhbnRcclxuICAgIH1cclxuXHJcbiAgICAuYm94Q29udGV1ZG8ge1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gICAgICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDBweCAxMHB4ICNlNGU0ZTQ7XHJcbiAgICB9XHJcblxyXG4gICAgLmJveENvbnRldWRvIGxhYmVsIHtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICB9XHJcblxyXG4gICAgLyouYm94Q29udGV1ZG8gLmZvcm0tY29udHJvbCB7XHJcbiAgICAgICAgaGVpZ2h0OiBjYWxjKDEuNWVtICsgLjc1cmVtICsgMTRweCk7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTAwcHg7XHJcbiAgICB9Ki9cclxuXHJcbiAgICAuYm94Q29udGV1ZG8gLmZvcm0tY29udHJvbCB7XHJcbiAgICAgICAgaGVpZ2h0OiBjYWxjKDEuNWVtICsgLjc1cmVtICsgMTRweCk7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTAwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLy9pb24tY29udGVudFxyXG4gICAgLypib2R5LmxvZ2luIHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHVybCgnc3JjL2Fzc2V0cy9pbWcvYmctbG9naW4uanBnJykgIWltcG9ydGFudDtcclxuICAgICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICAgICAgIGhlaWdodDogaW5oZXJpdDtcclxuICAgIH0qL1xyXG5cclxuICAgIC5wb3NpdGlvbiB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHRvcDogNnB4O1xyXG4gICAgICAgIGxlZnQ6IDEwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLmxvZ29Mb2dpbiBpbWcge1xyXG4gICAgICAgIG1hcmdpbjogMzBweCBhdXRvO1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIHdpZHRoOiAzMCU7XHJcbiAgICB9XHJcblxyXG4gICAgLmxvZ2luIC5ib3hDb250ZXVkbyAuZm9ybS1jb250cm9sIHtcclxuICAgICAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG5cclxuICAgIC5sb2dpbiAuYm94Q29udGV1ZG8ge1xyXG4gICAgICAgIGJhY2tncm91bmQ6IG5vbmU7XHJcbiAgICAgICAgYm94LXNoYWRvdzogbm9uZTtcclxuICAgICAgICBtYXJnaW4tdG9wOiA0MCU7XHJcbiAgICB9XHJcblxyXG4gICAgLmxvZ2luIGJ1dHRvbi5idG5Mb2dpbiB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2IxZmY0OTtcclxuICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgY29sb3I6ICMwMDI3NTI7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgIH1cclxuXHJcbiAgICAubG9naW4gYnV0dG9uLmJ0bkxvZ2luOmFjdGl2ZSB7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzk5ZGQzZSAhaW1wb3J0YW50O1xyXG4gICAgICAgIGJvcmRlci1jb2xvcjogIzk5ZGQzZSAhaW1wb3J0YW50O1xyXG4gICAgICAgIGNvbG9yOiAjMDAyNzUyICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgaW5wdXQjaW5wdXRMb2dpbiwgaW5wdXQjaW5wdXRTZW5oYSB7XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDogNDVweDtcclxuICAgIH1cclxuXHJcbiAgICAubG9naW4gaW5wdXRbdHlwZT10ZXh0XTpmb2N1cywgLmxvZ2luIGlucHV0W3R5cGU9cGFzc3dvcmRdOmZvY3VzIHtcclxuICAgICAgICBjb2xvcjogIzFjNzJmZDtcclxuICAgIH1cclxuXHJcbiAgICAubG9naW4gLmZvcm0tZ3JvdXAge1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIH1cclxuXHJcbiAgICAubG9naW4gLmZvcm0tZ3JvdXAgc3ZnIHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgbGVmdDogMjBweDtcclxuICAgICAgICB0b3A6IDExcHg7XHJcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5saW5rRXNxdWVjaVNlbmhhIGEge1xyXG4gICAgICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgIH1cclxuXHJcbiAgICAubGlua0NhZGFzdHJhciBhIHtcclxuICAgICAgICBjb2xvcjogI2IxZmY0OTtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLyouYnV0dG9uQXJlYSB7XHJcbiAgICBtYXJnaW46IDIwcHggLTIwcHggMHB4IC0yMHB4O1xyXG4gICAgYmFja2dyb3VuZDogIzEwMWE0YztcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB6LWluZGV4OiAwO1xyXG4gICAgYm90dG9tOiAtNTBweDtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwIDAgMjBweCAyMHB4O1xyXG4gICAgfSovXHJcbiAgICBcclxuICAgIC5idXR0b25BcmVhIGJ1dHRvbiB7XHJcbiAgICAgICAgd2lkdGg6MTAwJTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLmJ1dHRvbkFyZWEgc3ZnIHtcclxuICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgIG1hcmdpbi1yaWdodDogNXB4O1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICBidXR0b24ubmF2YmFyLXRvZ2dsZXIge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTAwcHg7XHJcbiAgICBoZWlnaHQ6IDU1cHggIWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQ6ICNmZmZmZmY7XHJcbiAgICBib3JkZXI6ICM5MmYzMjggc29saWQgMHB4O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLmJveENvbnRldWRvIC5idG4ge1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB9XHJcblxyXG4gICAgLm1hcGEgaWZyYW1lIHtcclxuICAgICAgICBib3JkZXI6IDA7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OjEwMHZoO1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICB0b3A6IDBweDtcclxuICAgICAgICB6LWluZGV4OiAtMTtcclxuICAgIH1cclxuXHJcbiAgICAuc2F1ZGFjYW8ge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuXHJcbiAgICAuaG9tZVBhc3NhZ2Vpcm8gLmJveENvbnRldWRvIHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgdG9wOiAzMHB4O1xyXG4gICAgICAgIHdpZHRoOiBjYWxjKDEwMCUgLSAwcHgpO1xyXG4gICAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHggNXB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5ob21lUGFzc2FnZWlybyAuYnV0dG9uQXJlYSBidXR0b24ucm91bmRlZC1waWxsIHtcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICB9XHJcblxyXG4gICAgYnV0dG9uLmVzY29saGVyRGVzdGlubyB7XHJcbiAgICAgICAgd2lkdGg6IGNhbGMoMTAwJSAtIDYwcHgpO1xyXG4gICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgfVxyXG5cclxuICAgIGJ1dHRvbi5lbWJhcnF1ZVJhcGlkbyB7XHJcbiAgICAgICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgICAgIHBhZGRpbmc6IDBweCAxMnB4O1xyXG4gICAgICAgIHdpZHRoOiA1NXB4O1xyXG4gICAgICAgIGhlaWdodDogNTVweDtcclxuICAgIH1cclxuXHJcbiAgICBidXR0b24uZW1iYXJxdWVSYXBpZG8gc3ZnIHtcclxuICAgICAgICB3aWR0aDogMzVweDtcclxuICAgICAgICBoZWlnaHQ6IDM1eDtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICB9XHJcblxyXG4gICAgYnV0dG9uLmJ0bi5idG4tcHJpbWFyeS5idG4tbGcucm91bmRlZC1waWxsLmVtYmFycXVlUmFwaWRvIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMWM3MmZkO1xyXG4gICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgIH1cclxuXHJcbiAgICAuZm9ybURlc3Rpbm8ge1xyXG4gICAgICAgIGJvcmRlcjogbm9uZTtcclxuICAgICAgICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgLmVuZERlc3Rpbm9SZWNlbnRlIHtcclxuICAgICAgICBmb250LXNpemU6IDExcHg7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2VlZTtcclxuICAgICAgICBwYWRkaW5nOiAxNHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDVweDtcclxuICAgIH1cclxuXHJcbiAgICAuZW5kRGVzdGlub1JlY2VudGUgYiB7XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICAgICAgICBjb2xvcjogZGFya2dyYXk7XHJcbiAgICB9XHJcblxyXG4gICAgLnRpdHVsb0Rlc3Rpbm9SZWNlbnRlIHtcclxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgfVxyXG5cclxuICAgIC5iRW50cmFyIHtcclxuICAgICAgICAvL3Bvc2l0aW9uOiBmaXhlZDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMTc2NWUyO1xyXG4gICAgICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTAwJTtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDBweCAxMHB4ICNlNGU0ZTQ7XHJcbiAgICB9XHJcblxyXG5hLmJ0bkVkaXRhciB7XHJcbiAgICBmbG9hdDogcmlnaHQ7XHJcbn1cclxuXHJcbi5sYWJlbENhZGFzdHJhZG8ge1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG59XHJcblxyXG4udmFsb3JDYWRhc3RyYWRvIHtcclxuICAgIGNsZWFyOiBib3RoO1xyXG4gICAgY29sb3I6ICMxYzcyZmQ7XHJcbiAgICBmb250LXNpemU6IDE4cHg7XHJcbn1cclxuXHJcbmEuYnRuRWRpdGFyIHtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICAgIGJhY2tncm91bmQ6ICNmZmY5NDk7XHJcbiAgICBjb2xvcjogIzlhOTYxNTtcclxuICAgIHBhZGRpbmc6IDJweCAxMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICAgIGZvbnQtc2l6ZTogMTNweDtcclxufVxyXG5cclxuYS5idG4uYnRuTmV1dHJvIHtcclxuICAgIGZvbnQtc2l6ZTogMTRweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxufVxyXG5cclxuYS5idG4uYnRuTmV1dHJvIHN2ZyB7XHJcbiAgICBtYXJnaW4tdG9wOiAtM3B4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA1cHg7XHJcbn1cclxuXHJcbiAgICAudGV4dExvZ2luIHtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgICAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcclxuICAgIH1cclxuXHJcbi8qOmhvc3Qge1xyXG4gICAgaW9uLWNvbnRlbnQge1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDEzNWRlZywgdmFyKC0taW9uLWNvbG9yLWxpZ2h0KSwgdmFyKC0taW9uLWNvbG9yLWxpZ2h0KSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5wYXoge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgei1pbmRleDogMTA7XHJcbn0qLyIsIkBpbXBvcnQgdXJsKFwiaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3MyP2ZhbWlseT1SdWJpazppdGFsLHdnaHRAMCw0MDA7MCw1MDA7MSw0MDA7MSw1MDAmZGlzcGxheT1zd2FwXCIpO1xuLmxvZ2luIHtcbiAgLS1iYWNrZ3JvdW5kOiB1cmwoXCJzcmMvYXNzZXRzL2ltZy9iZy1sb2dpbi5qcGdcIikgIWltcG9ydGFudDsgfVxuXG5AbWVkaWEgKG1heC13aWR0aDogOTkycHgpIHtcbiAgaW9uLWNvbnRlbnQge1xuICAgIC0tYmFja2dyb3VuZDogd2hpdGU7XG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgICBoZWlnaHQ6IGluaGVyaXQ7IH1cbiAgLm5hdmJhci1jb2xsYXBzZSB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIHRvcDogMHB4O1xuICAgIGxlZnQ6IDA7XG4gICAgcGFkZGluZy1sZWZ0OiAxNXB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDE1cHg7XG4gICAgcGFkZGluZy1ib3R0b206IDE1cHg7XG4gICAgd2lkdGg6IDc1JTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgYmFja2dyb3VuZDogIzE3NjVlMjtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIHotaW5kZXg6IDk5OTk7XG4gICAgYm94LXNoYWRvdzogMHB4IDBweCA5MHB4IGJsYWNrOyB9XG4gIC5uYXZiYXItY29sbGFwc2UgYS5uYXYtbGluayB7XG4gICAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgZm9udC1zaXplOiAxOXB4OyB9XG4gIGEubmF2LWxpbmsgc3ZnIHtcbiAgICBmb250LXNpemU6IDI1cHg7XG4gICAgbWFyZ2luLXJpZ2h0OiAxMHB4OyB9XG4gIC5uYXZiYXItZGFyayAubmF2YmFyLXRvZ2dsZXIge1xuICAgIGJhY2tncm91bmQ6ICMxYjJjN2I7IH1cbiAgLm5hdmJhci1jb2xsYXBzZS5jb2xsYXBzaW5nIHtcbiAgICBsZWZ0OiAtNzUlO1xuICAgIHRyYW5zaXRpb246IGhlaWdodCAwcyBlYXNlOyB9XG4gIC5uYXZiYXItY29sbGFwc2Uuc2hvdyB7XG4gICAgbWFyZ2luLXRvcDogMHB4O1xuICAgIGxlZnQ6IDA7XG4gICAgdHJhbnNpdGlvbjogbGVmdCAzMDBtcyBlYXNlLWluLW91dDsgfVxuICAubmF2YmFyLXRvZ2dsZXIuY29sbGFwc2VkIH4gLm5hdmJhci1jb2xsYXBzZSB7XG4gICAgdHJhbnNpdGlvbjogbGVmdCA1MDBtcyBlYXNlLWluLW91dDsgfSB9XG5cbmJvZHkge1xuICBiYWNrZ3JvdW5kOiB1cmwoXCJzcmMvYXNzZXRzL2ltZy9iZy11YWktbGV2YS5wbmdcIikgbm8tcmVwZWF0ICNlYWVhZWE7XG4gIGZvbnQtZmFtaWx5OiAnUnViaWsnLCBzYW5zLXNlcmlmO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA4MHB4IGJvdHRvbTtcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICBoZWlnaHQ6IDEwMHZoOyB9XG5cbmgxIHtcbiAgZm9udC1zaXplOiAxLjVyZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgbWFyZ2luOiAyMHB4IDA7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzEyM2I3ZDsgfVxuXG5oMyB7XG4gIGZvbnQtc2l6ZTogMC43NXJlbTtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgbWFyZ2luOiAyMHB4IDA7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzFjNzJmZDsgfVxuXG4ubmF2YmFyLmJnLWRhcmsge1xuICBiYWNrZ3JvdW5kOiAjMWM3MmZkICFpbXBvcnRhbnQ7IH1cblxuLmJveENvbnRldWRvIHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYm94LXNoYWRvdzogMHB4IDBweCAxMHB4ICNlNGU0ZTQ7IH1cblxuLmJveENvbnRldWRvIGxhYmVsIHtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC1zaXplOiAxMnB4OyB9XG5cbi8qLmJveENvbnRldWRvIC5mb3JtLWNvbnRyb2wge1xyXG4gICAgICAgIGhlaWdodDogY2FsYygxLjVlbSArIC43NXJlbSArIDE0cHgpO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xyXG4gICAgfSovXG4uYm94Q29udGV1ZG8gLmZvcm0tY29udHJvbCB7XG4gIGhlaWdodDogY2FsYygxLjVlbSArIC43NXJlbSArIDE0cHgpO1xuICBib3JkZXItcmFkaXVzOiAxMDBweDsgfVxuXG4vKmJvZHkubG9naW4ge1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogdXJsKCdzcmMvYXNzZXRzL2ltZy9iZy1sb2dpbi5qcGcnKSAhaW1wb3J0YW50O1xyXG4gICAgICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICAgICAgaGVpZ2h0OiBpbmhlcml0O1xyXG4gICAgfSovXG4ucG9zaXRpb24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNnB4O1xuICBsZWZ0OiAxMHB4OyB9XG5cbi5sb2dvTG9naW4gaW1nIHtcbiAgbWFyZ2luOiAzMHB4IGF1dG87XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB3aWR0aDogMzAlOyB9XG5cbi5sb2dpbiAuYm94Q29udGV1ZG8gLmZvcm0tY29udHJvbCB7XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyOyB9XG5cbi5sb2dpbiAuYm94Q29udGV1ZG8ge1xuICBiYWNrZ3JvdW5kOiBub25lO1xuICBib3gtc2hhZG93OiBub25lO1xuICBtYXJnaW4tdG9wOiA0MCU7IH1cblxuLmxvZ2luIGJ1dHRvbi5idG5Mb2dpbiB7XG4gIGJhY2tncm91bmQ6ICNiMWZmNDk7XG4gIGJvcmRlcjogbm9uZTtcbiAgY29sb3I6ICMwMDI3NTI7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7IH1cblxuLmxvZ2luIGJ1dHRvbi5idG5Mb2dpbjphY3RpdmUge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjOTlkZDNlICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1jb2xvcjogIzk5ZGQzZSAhaW1wb3J0YW50O1xuICBjb2xvcjogIzAwMjc1MiAhaW1wb3J0YW50OyB9XG5cbmlucHV0I2lucHV0TG9naW4sIGlucHV0I2lucHV0U2VuaGEge1xuICBwYWRkaW5nLXJpZ2h0OiA0NXB4OyB9XG5cbi5sb2dpbiBpbnB1dFt0eXBlPXRleHRdOmZvY3VzLCAubG9naW4gaW5wdXRbdHlwZT1wYXNzd29yZF06Zm9jdXMge1xuICBjb2xvcjogIzFjNzJmZDsgfVxuXG4ubG9naW4gLmZvcm0tZ3JvdXAge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7IH1cblxuLmxvZ2luIC5mb3JtLWdyb3VwIHN2ZyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMjBweDtcbiAgdG9wOiAxMXB4O1xuICBmb250LXNpemU6IDIwcHg7IH1cblxuLmxpbmtFc3F1ZWNpU2VuaGEgYSB7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBkaXNwbGF5OiBibG9jaztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tdG9wOiAxMHB4OyB9XG5cbi5saW5rQ2FkYXN0cmFyIGEge1xuICBjb2xvcjogI2IxZmY0OTtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBkaXNwbGF5OiBibG9jaztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tdG9wOiAxMHB4OyB9XG5cbi8qLmJ1dHRvbkFyZWEge1xyXG4gICAgbWFyZ2luOiAyMHB4IC0yMHB4IDBweCAtMjBweDtcclxuICAgIGJhY2tncm91bmQ6ICMxMDFhNGM7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgei1pbmRleDogMDtcclxuICAgIGJvdHRvbTogLTUwcHg7XHJcbiAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgYm9yZGVyLXJhZGl1czogMCAwIDIwcHggMjBweDtcclxuICAgIH0qL1xuLmJ1dHRvbkFyZWEgYnV0dG9uIHtcbiAgd2lkdGg6IDEwMCU7IH1cblxuLmJ1dHRvbkFyZWEgc3ZnIHtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDsgfVxuXG5idXR0b24ubmF2YmFyLXRvZ2dsZXIge1xuICBib3JkZXItcmFkaXVzOiAxMDBweDtcbiAgaGVpZ2h0OiA1NXB4ICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gIGJvcmRlcjogIzkyZjMyOCBzb2xpZCAwcHg7XG4gIG91dGxpbmU6IG5vbmU7IH1cblxuLmJveENvbnRldWRvIC5idG4ge1xuICBmb250LXdlaWdodDogYm9sZDsgfVxuXG4ubWFwYSBpZnJhbWUge1xuICBib3JkZXI6IDA7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMHZoO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMHB4O1xuICB6LWluZGV4OiAtMTsgfVxuXG4uc2F1ZGFjYW8ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7IH1cblxuLmhvbWVQYXNzYWdlaXJvIC5ib3hDb250ZXVkbyB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgdG9wOiAzMHB4O1xuICB3aWR0aDogY2FsYygxMDAlIC0gMHB4KTtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIHBhZGRpbmc6IDE1cHggNXB4OyB9XG5cbi5ob21lUGFzc2FnZWlybyAuYnV0dG9uQXJlYSBidXR0b24ucm91bmRlZC1waWxsIHtcbiAgZm9udC1zaXplOiAxOHB4OyB9XG5cbmJ1dHRvbi5lc2NvbGhlckRlc3Rpbm8ge1xuICB3aWR0aDogY2FsYygxMDAlIC0gNjBweCk7XG4gIGZsb2F0OiBsZWZ0OyB9XG5cbmJ1dHRvbi5lbWJhcnF1ZVJhcGlkbyB7XG4gIGZsb2F0OiByaWdodDtcbiAgcGFkZGluZzogMHB4IDEycHg7XG4gIHdpZHRoOiA1NXB4O1xuICBoZWlnaHQ6IDU1cHg7IH1cblxuYnV0dG9uLmVtYmFycXVlUmFwaWRvIHN2ZyB7XG4gIHdpZHRoOiAzNXB4O1xuICBoZWlnaHQ6IDM1eDtcbiAgY29sb3I6IHdoaXRlOyB9XG5cbmJ1dHRvbi5idG4uYnRuLXByaW1hcnkuYnRuLWxnLnJvdW5kZWQtcGlsbC5lbWJhcnF1ZVJhcGlkbyB7XG4gIGJhY2tncm91bmQ6ICMxYzcyZmQ7XG4gIGJvcmRlcjogbm9uZTsgfVxuXG4uZm9ybURlc3Rpbm8ge1xuICBib3JkZXI6IG5vbmU7XG4gIG1hcmdpbi10b3A6IDVweDsgfVxuXG4uZW5kRGVzdGlub1JlY2VudGUge1xuICBmb250LXNpemU6IDExcHg7XG4gIGJhY2tncm91bmQ6ICNlZWU7XG4gIHBhZGRpbmc6IDE0cHg7XG4gIGJvcmRlci1yYWRpdXM6IDVweDsgfVxuXG4uZW5kRGVzdGlub1JlY2VudGUgYiB7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIGNvbG9yOiBkYXJrZ3JheTsgfVxuXG4udGl0dWxvRGVzdGlub1JlY2VudGUge1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXdlaWdodDogYm9sZDsgfVxuXG4uYkVudHJhciB7XG4gIGJhY2tncm91bmQ6ICMxNzY1ZTI7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYm94LXNoYWRvdzogMHB4IDBweCAxMHB4ICNlNGU0ZTQ7IH1cblxuYS5idG5FZGl0YXIge1xuICBmbG9hdDogcmlnaHQ7IH1cblxuLmxhYmVsQ2FkYXN0cmFkbyB7XG4gIGZsb2F0OiBsZWZ0O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXNpemU6IDEzcHg7IH1cblxuLnZhbG9yQ2FkYXN0cmFkbyB7XG4gIGNsZWFyOiBib3RoO1xuICBjb2xvcjogIzFjNzJmZDtcbiAgZm9udC1zaXplOiAxOHB4OyB9XG5cbmEuYnRuRWRpdGFyIHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBiYWNrZ3JvdW5kOiAjZmZmOTQ5O1xuICBjb2xvcjogIzlhOTYxNTtcbiAgcGFkZGluZzogMnB4IDEwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIGZvbnQtc2l6ZTogMTNweDsgfVxuXG5hLmJ0bi5idG5OZXV0cm8ge1xuICBmb250LXNpemU6IDE0cHg7XG4gIHdpZHRoOiAxMDAlO1xuICBmb250LXdlaWdodDogbm9ybWFsOyB9XG5cbmEuYnRuLmJ0bk5ldXRybyBzdmcge1xuICBtYXJnaW4tdG9wOiAtM3B4O1xuICBtYXJnaW4tcmlnaHQ6IDVweDsgfVxuXG4udGV4dExvZ2luIHtcbiAgY29sb3I6IHdoaXRlO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBmb250LXNpemU6IDEycHg7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiA1cHg7IH1cblxuLyo6aG9zdCB7XHJcbiAgICBpb24tY29udGVudCB7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMTM1ZGVnLCB2YXIoLS1pb24tY29sb3ItbGlnaHQpLCB2YXIoLS1pb24tY29sb3ItbGlnaHQpKTtcclxuICAgIH1cclxufVxyXG5cclxuLnBheiB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB6LWluZGV4OiAxMDtcclxufSovXG4iXX0= */"

/***/ }),

/***/ "./src/app/pages/login/login.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/login/login.page.ts ***!
  \*******************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_core_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/auth/auth.service */ "./src/app/core/auth/auth.service.ts");
/* harmony import */ var _recuperarsenha_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./recuperarsenha.service */ "./src/app/pages/login/recuperarsenha.service.ts");
/* harmony import */ var _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/facebook/ngx */ "./node_modules/@ionic-native/facebook/ngx/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, menuCtrl, toastCtrl, alertCtrl, loadingCtrl, recuperarSenha, router, formBuilder, authService, fb) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.toastCtrl = toastCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.recuperarSenha = recuperarSenha;
        this.router = router;
        this.formBuilder = formBuilder;
        this.authService = authService;
        this.fb = fb;
        this.users = [];
        this.user = {};
    }
    LoginPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(false);
    };
    LoginPage.prototype.ngOnInit = function () {
        this.onLoginForm = this.formBuilder.group({
            'phone': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])],
            'password': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required])]
        });
    };
    LoginPage.prototype.forgotPass = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtrl.create({
                            header: 'Esqueceu sua senha?',
                            message: 'Informe seu e-mail para enviarmos um novo link de recuperação.',
                            inputs: [
                                {
                                    name: 'email',
                                    type: 'email',
                                    placeholder: 'E-mail'
                                }
                            ],
                            buttons: [
                                {
                                    text: 'Cancelar',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                        console.log('Confirmar Cancelamento?');
                                    }
                                }, {
                                    text: 'Confirmar',
                                    handler: function (data) { return __awaiter(_this, void 0, void 0, function () {
                                        var _this = this;
                                        return __generator(this, function (_a) {
                                            console.log(data.email);
                                            this.recuperarSenha.sendemail(data.email)
                                                .subscribe(function () { return __awaiter(_this, void 0, void 0, function () {
                                                var toast;
                                                return __generator(this, function (_a) {
                                                    switch (_a.label) {
                                                        case 0:
                                                            this.navCtrl.navigateRoot('');
                                                            return [4 /*yield*/, this.toastCtrl.create({
                                                                    message: 'Email enviado com sucesso!',
                                                                    duration: 4000, position: 'top',
                                                                    color: 'success'
                                                                })];
                                                        case 1:
                                                            toast = _a.sent();
                                                            toast.present();
                                                            return [2 /*return*/];
                                                    }
                                                });
                                            }); }, function (err) { return __awaiter(_this, void 0, void 0, function () {
                                                var toast;
                                                return __generator(this, function (_a) {
                                                    switch (_a.label) {
                                                        case 0: return [4 /*yield*/, this.toastCtrl.create({
                                                                message: 'Usuário não cadastrado, por favor cadastre-se no APP!',
                                                                duration: 4000, position: 'top',
                                                                color: 'danger'
                                                            })];
                                                        case 1:
                                                            toast = _a.sent();
                                                            toast.present();
                                                            return [2 /*return*/];
                                                    }
                                                });
                                            }); });
                                            return [2 /*return*/];
                                        });
                                    }); }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    // // //
    LoginPage.prototype.goToRegister = function () {
        this.router.navigateByUrl('/register-driver');
    };
    LoginPage.prototype.goToHome = function () {
        var _this = this;
        var phone = this.onLoginForm.get('phone').value;
        var password = this.onLoginForm.get('password').value;
        console.log(phone);
        console.log(password);
        this.authService.authenticate(phone, password)
            .subscribe(function () { return _this.navCtrl.navigateRoot('/home'); }, function (err) { return __awaiter(_this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.onLoginForm.reset();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'Usuário ou senha incorreto, por favor tente novamente!',
                                duration: 4000, position: 'top',
                                color: 'danger'
                            })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        }); });
    };
    LoginPage.prototype.loginFb = function () {
        var _this = this;
        this.fb.login(['public_profile', 'user_friends', 'email'])
            .then(function (res) {
            if (res.status === 'conected') {
                _this.user.img = 'http://graph.facebook.com/' + res.authResponse.userID + '/picture?type=square';
            }
            else {
                alert('Login failed');
            }
            console.log('Logged into Facebook!', res);
        })
            .catch(function (e) { return console.log('Error logging into Facebook', e); });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('userNameInput'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], LoginPage.prototype, "userNameInput", void 0);
    LoginPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.page.html */ "./src/app/pages/login/login.page.html"),
            styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/pages/login/login.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _recuperarsenha_service__WEBPACK_IMPORTED_MODULE_5__["RecuperarSenhaService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            src_app_core_auth_auth_service__WEBPACK_IMPORTED_MODULE_4__["AuthService"],
            _ionic_native_facebook_ngx__WEBPACK_IMPORTED_MODULE_6__["Facebook"]])
    ], LoginPage);
    return LoginPage;
}());



/***/ }),

/***/ "./src/app/pages/login/recuperarsenha.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/login/recuperarsenha.service.ts ***!
  \*******************************************************/
/*! exports provided: RecuperarSenhaService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RecuperarSenhaService", function() { return RecuperarSenhaService; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var API_URL = 'http://159.203.181.9:3000';
var RecuperarSenhaService = /** @class */ (function () {
    function RecuperarSenhaService(http) {
        this.http = http;
    }
    RecuperarSenhaService.prototype.sendemail = function (email) {
        return this.http.post(API_URL + "/sendEmail", { email: email });
    };
    ;
    RecuperarSenhaService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"]])
    ], RecuperarSenhaService);
    return RecuperarSenhaService;
}());



/***/ })

}]);
//# sourceMappingURL=pages-login-login-module.js.map