(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-minhas-viagens-minhas-viagens-module"],{

/***/ "./src/app/pages/minhas-viagens/minhas-viagens.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/minhas-viagens/minhas-viagens.module.ts ***!
  \***************************************************************/
/*! exports provided: MinhasViagensPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MinhasViagensPageModule", function() { return MinhasViagensPageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _minhas_viagens_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./minhas-viagens.page */ "./src/app/pages/minhas-viagens/minhas-viagens.page.ts");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "./node_modules/@ionic-native/splash-screen/ngx/index.js");
/* harmony import */ var _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/google-maps */ "./node_modules/@ionic-native/google-maps/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





//import { PopmenuComponent } from './../../components/popmenu/popmenu.component';



var routes = [
    {
        path: '',
        component: _minhas_viagens_page__WEBPACK_IMPORTED_MODULE_5__["MinhasViagensPage"]
    }
];
var MinhasViagensPageModule = /** @class */ (function () {
    function MinhasViagensPageModule() {
    }
    MinhasViagensPageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes),
            ],
            declarations: [_minhas_viagens_page__WEBPACK_IMPORTED_MODULE_5__["MinhasViagensPage"]],
            providers: [
                _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_6__["SplashScreen"],
                { provide: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ErrorHandler"] },
                _ionic_native_google_maps__WEBPACK_IMPORTED_MODULE_7__["GoogleMaps"]
            ]
        })
    ], MinhasViagensPageModule);
    return MinhasViagensPageModule;
}());



/***/ }),

/***/ "./src/app/pages/minhas-viagens/minhas-viagens.page.html":
/*!***************************************************************!*\
  !*** ./src/app/pages/minhas-viagens/minhas-viagens.page.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.7.2/css/all.css\" integrity=\"sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr\" crossorigin=\"anonymous\">\r\n<html lang=\"pt-br\" xmlns=\"http://www.w3.org/1999/xhtml\">\r\n\r\n\r\n    <ion-header>\r\n        <nav class=\"navbar navbar-expand-lg navbar-light bg-dark\">\r\n            <a class=\"navbar-brand\" href=\"#\">\r\n            <img src=\"assets/img/logo-uaileva.png\" />\r\n            </a>\r\n            <ion-menu-button>\r\n                <button class=\"navbar-toggler\" >\r\n                    <span class=\"navbar-toggler-icon\"></span>\r\n                </button>\r\n            </ion-menu-button>\r\n        </nav>\r\n    </ion-header>\r\n\r\n    <ion-content >\r\n\r\n    <div class=\"container\">\r\n        <h1>Minhas Viagens</h1>\r\n    </div>\r\n\r\n    <div class=\"footer\"></div>\r\n\r\n    <script src=\"https://code.jquery.com/jquery-3.5.1.slim.min.js\"></script>\r\n    <script src=\"js/bootstrap.min.js\"></script>\r\n\r\n    </ion-content>"

/***/ }),

/***/ "./src/app/pages/minhas-viagens/minhas-viagens.page.scss":
/*!***************************************************************!*\
  !*** ./src/app/pages/minhas-viagens/minhas-viagens.page.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@import url(\"https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,400;0,500;1,400;1,500&display=swap\");\n@media (max-width: 992px) {\n  ion-content {\n    --background: url('bg-uai-leva2.png') #eaeaea no-repeat bottom 75px left 20px; }\n  .navbar-collapse {\n    position: fixed;\n    top: 0px;\n    left: 0;\n    padding-left: 15px;\n    padding-right: 15px;\n    padding-bottom: 15px;\n    width: 75%;\n    height: 100%;\n    background: #1765e2;\n    text-transform: uppercase;\n    z-index: 9999;\n    box-shadow: 0px 0px 90px black; }\n  .navbar-collapse a.nav-link {\n    color: white !important;\n    font-weight: bold;\n    font-size: 19px; }\n  a.nav-link svg {\n    font-size: 25px;\n    margin-right: 10px; }\n  .navbar-dark .navbar-toggler {\n    background: #1b2c7b; }\n  .navbar-dark span.navbar-toggler-icon {\n    padding: 15px; }\n  .navbar-collapse.collapsing {\n    left: -75%;\n    transition: height 0s ease; }\n  .navbar-collapse.show {\n    margin-top: 0px;\n    left: 0;\n    transition: left 300ms ease-in-out; }\n  .navbar-toggler.collapsed ~ .navbar-collapse {\n    transition: left 500ms ease-in-out; } }\n/*  ion-content {\r\n    //body no html\r\n    background: url('src/assets/img/bg-uai-leva.png') no-repeat #eaeaea !important;\r\n    font-family: 'Rubik', sans-serif;\r\n    background-position: 80px bottom;\r\n    background-size: contain;\r\n    height: 100vh;\r\n}*/\nh1 {\n  font-size: 1.5rem;\n  text-align: center;\n  text-transform: uppercase;\n  margin: 20px 0;\n  font-weight: bold;\n  color: #123b7d; }\n.navbar.bg-dark {\n  background: #1c72fd !important; }\n.boxConteudo {\n  background: white;\n  padding: 20px;\n  border-radius: 20px;\n  position: relative;\n  box-shadow: 0px 0px 10px #e4e4e4; }\n.boxConteudo label {\n  text-transform: uppercase;\n  font-size: 12px; }\n.boxConteudo .form-control {\n  height: calc(1.5em + .75rem + 4px); }\n/*.buttonArea {\r\n    margin: 20px -20px 0px -20px;\r\n    background: #101a4c;\r\n    width: 100%;\r\n    display: block;\r\n    position: absolute;\r\n    z-index: 0;\r\n    bottom: -50px;\r\n    padding: 10px;\r\n    border-radius: 0 0 20px 20px;\r\n    }*/\n.buttonArea button {\n  width: 100%; }\n.buttonArea svg {\n  font-size: 25px;\n  margin-right: 5px; }\nbutton.navbar-toggler {\n  border-radius: 100px;\n  height: 55px !important;\n  background: #ffffff;\n  border: #92f328 solid 0px;\n  outline: none; }\n.boxConteudo .btn {\n  font-weight: bold; }\n.mapa iframe {\n  border: 0;\n  width: 100%;\n  height: 100vh;\n  position: absolute;\n  top: 0px;\n  z-index: -1; }\n.saudacao {\n  text-align: center; }\n.homePassageiro .boxConteudo {\n  position: relative;\n  top: 30px;\n  width: calc(100% - 0px);\n  margin: 0 auto;\n  padding: 15px 5px; }\n.homePassageiro .buttonArea button.rounded-pill {\n  font-size: 18px; }\nbutton.escolherDestino {\n  width: calc(100% - 60px);\n  float: left; }\nbutton.embarqueRapido {\n  float: right;\n  padding: 0px 12px;\n  width: 45px;\n  height: 45px; }\nbutton.embarqueRapido svg {\n  width: 19px;\n  color: #416313; }\nbutton.btn.btn-primary.btn-lg.rounded-pill.embarqueRapido {\n  background: #b1ff49;\n  border: none; }\n.formDestino {\n  border: none;\n  margin-top: 5px; }\n.endDestinoRecente {\n  font-size: 11px;\n  background: #eee;\n  padding: 14px;\n  border-radius: 5px; }\n.endDestinoRecente b {\n  font-weight: normal;\n  color: darkgray; }\n.tituloDestinoRecente {\n  text-transform: uppercase;\n  font-weight: bold; }\na.btnEditar {\n  float: right; }\n.labelCadastrado {\n  float: left;\n  text-transform: uppercase;\n  font-size: 13px; }\n.valorCadastrado {\n  clear: both;\n  color: #1c72fd;\n  font-size: 18px; }\na.btnEditar {\n  float: right;\n  background: #fff949;\n  color: #9a9615;\n  padding: 2px 10px;\n  border-radius: 20px;\n  font-size: 13px; }\na.btn.btnNeutro {\n  font-size: 14px;\n  width: 100%;\n  font-weight: normal; }\na.btn.btnNeutro svg {\n  margin-top: -3px;\n  margin-right: 5px; }\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbWluaGFzLXZpYWdlbnMvQzpcXFVzZXJzXFxqZXNzaVxcRG9jdW1lbnRzXFx1YWlsZXZhZHJpdmVyL3NyY1xcYXBwXFxwYWdlc1xcbWluaGFzLXZpYWdlbnNcXG1pbmhhcy12aWFnZW5zLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvbWluaGFzLXZpYWdlbnMvbWluaGFzLXZpYWdlbnMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLDRHQUFZO0FBR1o7RUFFSTtJQUNJLDZFQUFhLEVBQUE7RUFLakI7SUFDSSxlQUFlO0lBQ2YsUUFBUTtJQUNSLE9BQU87SUFDUCxrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLG9CQUFvQjtJQUNwQixVQUFVO0lBQ1YsWUFBWTtJQUNaLG1CQUFtQjtJQUNuQix5QkFBeUI7SUFDekIsYUFBYTtJQUNiLDhCQUE4QixFQUFBO0VBR2xDO0lBQ0EsdUJBQXVCO0lBQ3ZCLGlCQUFpQjtJQUNqQixlQUFlLEVBQUE7RUFHZjtJQUNBLGVBQWU7SUFDZixrQkFBa0IsRUFBQTtFQUdsQjtJQUNJLG1CQUFtQixFQUFBO0VBR3ZCO0lBQ0ksYUFBYSxFQUFBO0VBSWpCO0lBQ0ksVUFBVTtJQUNWLDBCQUEwQixFQUFBO0VBRzlCO0lBQ0ksZUFBZTtJQUNmLE9BQU87SUFDUCxrQ0FBa0MsRUFBQTtFQUd0QztJQUNJLGtDQUFrQyxFQUFBLEVBQ3JDO0FBSUg7Ozs7Ozs7RUNsQkE7QUQyQkU7RUFDQSxpQkFBaUI7RUFDakIsa0JBQWtCO0VBQ2xCLHlCQUF5QjtFQUN6QixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGNBQWMsRUFBQTtBQUdkO0VBQ0ksOEJBQTRCLEVBQUE7QUFHaEM7RUFDSSxpQkFBaUI7RUFDakIsYUFBYTtFQUNiLG1CQUFtQjtFQUNuQixrQkFBa0I7RUFDbEIsZ0NBQWdDLEVBQUE7QUFHcEM7RUFDQSx5QkFBeUI7RUFDekIsZUFBZSxFQUFBO0FBR2Y7RUFDSSxrQ0FBa0MsRUFBQTtBQUd0Qzs7Ozs7Ozs7OztNQ3JCRTtBRGlDRjtFQUNJLFdBQVUsRUFBQTtBQUdkO0VBQ0EsZUFBZTtFQUNmLGlCQUFpQixFQUFBO0FBR2pCO0VBQ0Esb0JBQW9CO0VBQ3BCLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIseUJBQXlCO0VBQ3pCLGFBQWEsRUFBQTtBQUdiO0VBQ0EsaUJBQWlCLEVBQUE7QUFHakI7RUFDSSxTQUFTO0VBQ1QsV0FBVztFQUNYLGFBQVk7RUFDWixrQkFBa0I7RUFDbEIsUUFBUTtFQUNSLFdBQVcsRUFBQTtBQUdmO0VBQ0ksa0JBQWtCLEVBQUE7QUFHdEI7RUFDSSxrQkFBa0I7RUFDbEIsU0FBUztFQUNULHVCQUF1QjtFQUN2QixjQUFjO0VBQ2QsaUJBQWlCLEVBQUE7QUFHckI7RUFDSSxlQUFlLEVBQUE7QUFHbkI7RUFDSSx3QkFBd0I7RUFDeEIsV0FBVyxFQUFBO0FBR2Y7RUFDSSxZQUFZO0VBQ1osaUJBQWlCO0VBQ2pCLFdBQVc7RUFDWCxZQUFZLEVBQUE7QUFHaEI7RUFDSSxXQUFXO0VBQ1gsY0FBYyxFQUFBO0FBR2xCO0VBQ0ssbUJBQW1CO0VBQ25CLFlBQVksRUFBQTtBQUdqQjtFQUNJLFlBQVk7RUFDWixlQUFlLEVBQUE7QUFHbkI7RUFDSSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGFBQWE7RUFDYixrQkFBa0IsRUFBQTtBQUd0QjtFQUNJLG1CQUFtQjtFQUNuQixlQUFlLEVBQUE7QUFHbkI7RUFDSSx5QkFBeUI7RUFDekIsaUJBQWlCLEVBQUE7QUFHekI7RUFDSSxZQUFZLEVBQUE7QUFHaEI7RUFDSSxXQUFXO0VBQ1gseUJBQXlCO0VBQ3pCLGVBQWUsRUFBQTtBQUduQjtFQUNJLFdBQVc7RUFDWCxjQUFjO0VBQ2QsZUFBZSxFQUFBO0FBR25CO0VBQ0ksWUFBWTtFQUNaLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLG1CQUFtQjtFQUNuQixlQUFlLEVBQUE7QUFHbkI7RUFDSSxlQUFlO0VBQ2YsV0FBVztFQUNYLG1CQUFtQixFQUFBO0FBR3ZCO0VBQ0ksZ0JBQWdCO0VBQ2hCLGlCQUFpQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbWluaGFzLXZpYWdlbnMvbWluaGFzLXZpYWdlbnMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzMj9mYW1pbHk9UnViaWs6aXRhbCx3Z2h0QDAsNDAwOzAsNTAwOzEsNDAwOzEsNTAwJmRpc3BsYXk9c3dhcCcpO1xyXG5cclxuXHJcbkBtZWRpYSAobWF4LXdpZHRoOiA5OTJweCkge1xyXG5cclxuICAgIGlvbi1jb250ZW50IHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHVybCgnc3JjL2Fzc2V0cy9pbWcvYmctdWFpLWxldmEyLnBuZycpICNlYWVhZWEgbm8tcmVwZWF0IGJvdHRvbSA3NXB4IGxlZnQgMjBweDtcclxuICAgICAgICAvLy0tYmFja2dyb3VuZDogdXJsKCdzcmMvYXNzZXRzL2ltZy9iZy11YWktbGV2YTIucG5nJykgbm8tcmVwZWF0IC0xNXB4IGJvdHRvbSAhaW1wb3J0YW50O1xyXG4gICAgIH1cclxuIFxyXG5cclxuICAgIC5uYXZiYXItY29sbGFwc2Uge1xyXG4gICAgICAgIHBvc2l0aW9uOiBmaXhlZDtcclxuICAgICAgICB0b3A6IDBweDtcclxuICAgICAgICBsZWZ0OiAwO1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDogMTVweDtcclxuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAxNXB4O1xyXG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAxNXB4O1xyXG4gICAgICAgIHdpZHRoOiA3NSU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICMxNzY1ZTI7XHJcbiAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgICAgICB6LWluZGV4OiA5OTk5O1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDBweCAwcHggOTBweCBibGFjaztcclxuICAgIH1cclxuICAgIFxyXG4gICAgLm5hdmJhci1jb2xsYXBzZSBhLm5hdi1saW5rIHtcclxuICAgIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBmb250LXNpemU6IDE5cHg7XHJcbiAgICB9XHJcblxyXG4gICAgYS5uYXYtbGluayBzdmcge1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG4gICAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAubmF2YmFyLWRhcmsgLm5hdmJhci10b2dnbGVyIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMWIyYzdiO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAubmF2YmFyLWRhcmsgc3Bhbi5uYXZiYXItdG9nZ2xlci1pY29uIHtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAvLyBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoZGF0YTppbWFnZS9zdmcreG1sLCUzY3N2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHdpZHRoPSczMCcgaGVpZ2h0PSczMCcgdmlld0JveD0nMCAwIDMwIDMwJyUzZSUzY3BhdGggc3Ryb2tlPSdyZ2JhJTI4MjU1LCAyNTUsIDI1NSwgMC45JTI5JyBzdHJva2UtbGluZWNhcD0ncm91bmQnIHN0cm9rZS1taXRlcmxpbWl0PScxMCcgc3Ryb2tlLXdpZHRoPScyJyBkPSdNNCA3aDIyTTQgMTVoMjJNNCAyM2gyMicvJTNlJTNjL3N2ZyUzZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLm5hdmJhci1jb2xsYXBzZS5jb2xsYXBzaW5nIHtcclxuICAgICAgICBsZWZ0OiAtNzUlO1xyXG4gICAgICAgIHRyYW5zaXRpb246IGhlaWdodCAwcyBlYXNlO1xyXG4gICAgfVxyXG5cclxuICAgIC5uYXZiYXItY29sbGFwc2Uuc2hvdyB7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMHB4O1xyXG4gICAgICAgIGxlZnQ6IDA7XHJcbiAgICAgICAgdHJhbnNpdGlvbjogbGVmdCAzMDBtcyBlYXNlLWluLW91dDtcclxuICAgIH1cclxuXHJcbiAgICAubmF2YmFyLXRvZ2dsZXIuY29sbGFwc2VkIH4gLm5hdmJhci1jb2xsYXBzZSB7XHJcbiAgICAgICAgdHJhbnNpdGlvbjogbGVmdCA1MDBtcyBlYXNlLWluLW91dDtcclxuICAgIH1cclxuICAgIFxyXG59XHJcblxyXG4gIC8qICBpb24tY29udGVudCB7XHJcbiAgICAvL2JvZHkgbm8gaHRtbFxyXG4gICAgYmFja2dyb3VuZDogdXJsKCdzcmMvYXNzZXRzL2ltZy9iZy11YWktbGV2YS5wbmcnKSBuby1yZXBlYXQgI2VhZWFlYSAhaW1wb3J0YW50O1xyXG4gICAgZm9udC1mYW1pbHk6ICdSdWJpaycsIHNhbnMtc2VyaWY7XHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiA4MHB4IGJvdHRvbTtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcclxuICAgIGhlaWdodDogMTAwdmg7XHJcbn0qL1xyXG5cclxuICAgIGgxIHtcclxuICAgIGZvbnQtc2l6ZTogMS41cmVtO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIG1hcmdpbjogMjBweCAwO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICBjb2xvcjogIzEyM2I3ZDtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLm5hdmJhci5iZy1kYXJrIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiMxYzcyZmQhaW1wb3J0YW50XHJcbiAgICB9XHJcblxyXG4gICAgLmJveENvbnRldWRvIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgICAgICBwYWRkaW5nOiAyMHB4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIGJveC1zaGFkb3c6IDBweCAwcHggMTBweCAjZTRlNGU0O1xyXG4gICAgfVxyXG5cclxuICAgIC5ib3hDb250ZXVkbyBsYWJlbCB7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAuYm94Q29udGV1ZG8gLmZvcm0tY29udHJvbCB7XHJcbiAgICAgICAgaGVpZ2h0OiBjYWxjKDEuNWVtICsgLjc1cmVtICsgNHB4KTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLyouYnV0dG9uQXJlYSB7XHJcbiAgICBtYXJnaW46IDIwcHggLTIwcHggMHB4IC0yMHB4O1xyXG4gICAgYmFja2dyb3VuZDogIzEwMWE0YztcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICB6LWluZGV4OiAwO1xyXG4gICAgYm90dG9tOiAtNTBweDtcclxuICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwIDAgMjBweCAyMHB4O1xyXG4gICAgfSovXHJcbiAgICBcclxuICAgIC5idXR0b25BcmVhIGJ1dHRvbiB7XHJcbiAgICAgICAgd2lkdGg6MTAwJTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLmJ1dHRvbkFyZWEgc3ZnIHtcclxuICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgIG1hcmdpbi1yaWdodDogNXB4O1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICBidXR0b24ubmF2YmFyLXRvZ2dsZXIge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTAwcHg7XHJcbiAgICBoZWlnaHQ6IDU1cHggIWltcG9ydGFudDtcclxuICAgIGJhY2tncm91bmQ6ICNmZmZmZmY7XHJcbiAgICBib3JkZXI6ICM5MmYzMjggc29saWQgMHB4O1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLmJveENvbnRldWRvIC5idG4ge1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB9XHJcblxyXG4gICAgLm1hcGEgaWZyYW1lIHtcclxuICAgICAgICBib3JkZXI6IDA7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgaGVpZ2h0OjEwMHZoO1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICB0b3A6IDBweDtcclxuICAgICAgICB6LWluZGV4OiAtMTtcclxuICAgIH1cclxuXHJcbiAgICAuc2F1ZGFjYW8ge1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuXHJcbiAgICAuaG9tZVBhc3NhZ2Vpcm8gLmJveENvbnRldWRvIHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgdG9wOiAzMHB4O1xyXG4gICAgICAgIHdpZHRoOiBjYWxjKDEwMCUgLSAwcHgpO1xyXG4gICAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHggNXB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5ob21lUGFzc2FnZWlybyAuYnV0dG9uQXJlYSBidXR0b24ucm91bmRlZC1waWxsIHtcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICB9XHJcblxyXG4gICAgYnV0dG9uLmVzY29saGVyRGVzdGlubyB7XHJcbiAgICAgICAgd2lkdGg6IGNhbGMoMTAwJSAtIDYwcHgpO1xyXG4gICAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgfVxyXG5cclxuICAgIGJ1dHRvbi5lbWJhcnF1ZVJhcGlkbyB7XHJcbiAgICAgICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgICAgIHBhZGRpbmc6IDBweCAxMnB4O1xyXG4gICAgICAgIHdpZHRoOiA0NXB4O1xyXG4gICAgICAgIGhlaWdodDogNDVweDtcclxuICAgIH1cclxuXHJcbiAgICBidXR0b24uZW1iYXJxdWVSYXBpZG8gc3ZnIHtcclxuICAgICAgICB3aWR0aDogMTlweDtcclxuICAgICAgICBjb2xvcjogIzQxNjMxMztcclxuICAgIH1cclxuXHJcbiAgICBidXR0b24uYnRuLmJ0bi1wcmltYXJ5LmJ0bi1sZy5yb3VuZGVkLXBpbGwuZW1iYXJxdWVSYXBpZG8ge1xyXG4gICAgICAgICBiYWNrZ3JvdW5kOiAjYjFmZjQ5O1xyXG4gICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICB9XHJcblxyXG4gICAgLmZvcm1EZXN0aW5vIHtcclxuICAgICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5lbmREZXN0aW5vUmVjZW50ZSB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMXB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNlZWU7XHJcbiAgICAgICAgcGFkZGluZzogMTRweDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgLmVuZERlc3Rpbm9SZWNlbnRlIGIge1xyXG4gICAgICAgIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgICAgICAgY29sb3I6IGRhcmtncmF5O1xyXG4gICAgfVxyXG5cclxuICAgIC50aXR1bG9EZXN0aW5vUmVjZW50ZSB7XHJcbiAgICAgICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIH1cclxuXHJcbmEuYnRuRWRpdGFyIHtcclxuICAgIGZsb2F0OiByaWdodDtcclxufVxyXG5cclxuLmxhYmVsQ2FkYXN0cmFkbyB7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBmb250LXNpemU6IDEzcHg7XHJcbn1cclxuXHJcbi52YWxvckNhZGFzdHJhZG8ge1xyXG4gICAgY2xlYXI6IGJvdGg7XHJcbiAgICBjb2xvcjogIzFjNzJmZDtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxufVxyXG5cclxuYS5idG5FZGl0YXIge1xyXG4gICAgZmxvYXQ6IHJpZ2h0O1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjk0OTtcclxuICAgIGNvbG9yOiAjOWE5NjE1O1xyXG4gICAgcGFkZGluZzogMnB4IDEwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgZm9udC1zaXplOiAxM3B4O1xyXG59XHJcblxyXG5hLmJ0bi5idG5OZXV0cm8ge1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG59XHJcblxyXG5hLmJ0bi5idG5OZXV0cm8gc3ZnIHtcclxuICAgIG1hcmdpbi10b3A6IC0zcHg7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDVweDtcclxufSIsIkBpbXBvcnQgdXJsKFwiaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3MyP2ZhbWlseT1SdWJpazppdGFsLHdnaHRAMCw0MDA7MCw1MDA7MSw0MDA7MSw1MDAmZGlzcGxheT1zd2FwXCIpO1xuQG1lZGlhIChtYXgtd2lkdGg6IDk5MnB4KSB7XG4gIGlvbi1jb250ZW50IHtcbiAgICAtLWJhY2tncm91bmQ6IHVybCgnc3JjL2Fzc2V0cy9pbWcvYmctdWFpLWxldmEyLnBuZycpICNlYWVhZWEgbm8tcmVwZWF0IGJvdHRvbSA3NXB4IGxlZnQgMjBweDsgfVxuICAubmF2YmFyLWNvbGxhcHNlIHtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgdG9wOiAwcHg7XG4gICAgbGVmdDogMDtcbiAgICBwYWRkaW5nLWxlZnQ6IDE1cHg7XG4gICAgcGFkZGluZy1yaWdodDogMTVweDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbiAgICB3aWR0aDogNzUlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBiYWNrZ3JvdW5kOiAjMTc2NWUyO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgei1pbmRleDogOTk5OTtcbiAgICBib3gtc2hhZG93OiAwcHggMHB4IDkwcHggYmxhY2s7IH1cbiAgLm5hdmJhci1jb2xsYXBzZSBhLm5hdi1saW5rIHtcbiAgICBjb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDE5cHg7IH1cbiAgYS5uYXYtbGluayBzdmcge1xuICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7IH1cbiAgLm5hdmJhci1kYXJrIC5uYXZiYXItdG9nZ2xlciB7XG4gICAgYmFja2dyb3VuZDogIzFiMmM3YjsgfVxuICAubmF2YmFyLWRhcmsgc3Bhbi5uYXZiYXItdG9nZ2xlci1pY29uIHtcbiAgICBwYWRkaW5nOiAxNXB4OyB9XG4gIC5uYXZiYXItY29sbGFwc2UuY29sbGFwc2luZyB7XG4gICAgbGVmdDogLTc1JTtcbiAgICB0cmFuc2l0aW9uOiBoZWlnaHQgMHMgZWFzZTsgfVxuICAubmF2YmFyLWNvbGxhcHNlLnNob3cge1xuICAgIG1hcmdpbi10b3A6IDBweDtcbiAgICBsZWZ0OiAwO1xuICAgIHRyYW5zaXRpb246IGxlZnQgMzAwbXMgZWFzZS1pbi1vdXQ7IH1cbiAgLm5hdmJhci10b2dnbGVyLmNvbGxhcHNlZCB+IC5uYXZiYXItY29sbGFwc2Uge1xuICAgIHRyYW5zaXRpb246IGxlZnQgNTAwbXMgZWFzZS1pbi1vdXQ7IH0gfVxuXG4vKiAgaW9uLWNvbnRlbnQge1xyXG4gICAgLy9ib2R5IG5vIGh0bWxcclxuICAgIGJhY2tncm91bmQ6IHVybCgnc3JjL2Fzc2V0cy9pbWcvYmctdWFpLWxldmEucG5nJykgbm8tcmVwZWF0ICNlYWVhZWEgIWltcG9ydGFudDtcclxuICAgIGZvbnQtZmFtaWx5OiAnUnViaWsnLCBzYW5zLXNlcmlmO1xyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogODBweCBib3R0b207XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcbiAgICBoZWlnaHQ6IDEwMHZoO1xyXG59Ki9cbmgxIHtcbiAgZm9udC1zaXplOiAxLjVyZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgbWFyZ2luOiAyMHB4IDA7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzEyM2I3ZDsgfVxuXG4ubmF2YmFyLmJnLWRhcmsge1xuICBiYWNrZ3JvdW5kOiAjMWM3MmZkICFpbXBvcnRhbnQ7IH1cblxuLmJveENvbnRldWRvIHtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYm94LXNoYWRvdzogMHB4IDBweCAxMHB4ICNlNGU0ZTQ7IH1cblxuLmJveENvbnRldWRvIGxhYmVsIHtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC1zaXplOiAxMnB4OyB9XG5cbi5ib3hDb250ZXVkbyAuZm9ybS1jb250cm9sIHtcbiAgaGVpZ2h0OiBjYWxjKDEuNWVtICsgLjc1cmVtICsgNHB4KTsgfVxuXG4vKi5idXR0b25BcmVhIHtcclxuICAgIG1hcmdpbjogMjBweCAtMjBweCAwcHggLTIwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiAjMTAxYTRjO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHotaW5kZXg6IDA7XHJcbiAgICBib3R0b206IC01MHB4O1xyXG4gICAgcGFkZGluZzogMTBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDAgMCAyMHB4IDIwcHg7XHJcbiAgICB9Ki9cbi5idXR0b25BcmVhIGJ1dHRvbiB7XG4gIHdpZHRoOiAxMDAlOyB9XG5cbi5idXR0b25BcmVhIHN2ZyB7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7IH1cblxuYnV0dG9uLm5hdmJhci10b2dnbGVyIHtcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XG4gIGhlaWdodDogNTVweCAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xuICBib3JkZXI6ICM5MmYzMjggc29saWQgMHB4O1xuICBvdXRsaW5lOiBub25lOyB9XG5cbi5ib3hDb250ZXVkbyAuYnRuIHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7IH1cblxuLm1hcGEgaWZyYW1lIHtcbiAgYm9yZGVyOiAwO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDB2aDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDBweDtcbiAgei1pbmRleDogLTE7IH1cblxuLnNhdWRhY2FvIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyOyB9XG5cbi5ob21lUGFzc2FnZWlybyAuYm94Q29udGV1ZG8ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogMzBweDtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDBweCk7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBwYWRkaW5nOiAxNXB4IDVweDsgfVxuXG4uaG9tZVBhc3NhZ2Vpcm8gLmJ1dHRvbkFyZWEgYnV0dG9uLnJvdW5kZWQtcGlsbCB7XG4gIGZvbnQtc2l6ZTogMThweDsgfVxuXG5idXR0b24uZXNjb2xoZXJEZXN0aW5vIHtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDYwcHgpO1xuICBmbG9hdDogbGVmdDsgfVxuXG5idXR0b24uZW1iYXJxdWVSYXBpZG8ge1xuICBmbG9hdDogcmlnaHQ7XG4gIHBhZGRpbmc6IDBweCAxMnB4O1xuICB3aWR0aDogNDVweDtcbiAgaGVpZ2h0OiA0NXB4OyB9XG5cbmJ1dHRvbi5lbWJhcnF1ZVJhcGlkbyBzdmcge1xuICB3aWR0aDogMTlweDtcbiAgY29sb3I6ICM0MTYzMTM7IH1cblxuYnV0dG9uLmJ0bi5idG4tcHJpbWFyeS5idG4tbGcucm91bmRlZC1waWxsLmVtYmFycXVlUmFwaWRvIHtcbiAgYmFja2dyb3VuZDogI2IxZmY0OTtcbiAgYm9yZGVyOiBub25lOyB9XG5cbi5mb3JtRGVzdGlubyB7XG4gIGJvcmRlcjogbm9uZTtcbiAgbWFyZ2luLXRvcDogNXB4OyB9XG5cbi5lbmREZXN0aW5vUmVjZW50ZSB7XG4gIGZvbnQtc2l6ZTogMTFweDtcbiAgYmFja2dyb3VuZDogI2VlZTtcbiAgcGFkZGluZzogMTRweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4OyB9XG5cbi5lbmREZXN0aW5vUmVjZW50ZSBiIHtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgY29sb3I6IGRhcmtncmF5OyB9XG5cbi50aXR1bG9EZXN0aW5vUmVjZW50ZSB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGZvbnQtd2VpZ2h0OiBib2xkOyB9XG5cbmEuYnRuRWRpdGFyIHtcbiAgZmxvYXQ6IHJpZ2h0OyB9XG5cbi5sYWJlbENhZGFzdHJhZG8ge1xuICBmbG9hdDogbGVmdDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgZm9udC1zaXplOiAxM3B4OyB9XG5cbi52YWxvckNhZGFzdHJhZG8ge1xuICBjbGVhcjogYm90aDtcbiAgY29sb3I6ICMxYzcyZmQ7XG4gIGZvbnQtc2l6ZTogMThweDsgfVxuXG5hLmJ0bkVkaXRhciB7XG4gIGZsb2F0OiByaWdodDtcbiAgYmFja2dyb3VuZDogI2ZmZjk0OTtcbiAgY29sb3I6ICM5YTk2MTU7XG4gIHBhZGRpbmc6IDJweCAxMHB4O1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICBmb250LXNpemU6IDEzcHg7IH1cblxuYS5idG4uYnRuTmV1dHJvIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICB3aWR0aDogMTAwJTtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDsgfVxuXG5hLmJ0bi5idG5OZXV0cm8gc3ZnIHtcbiAgbWFyZ2luLXRvcDogLTNweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7IH1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/pages/minhas-viagens/minhas-viagens.page.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/minhas-viagens/minhas-viagens.page.ts ***!
  \*************************************************************/
/*! exports provided: MinhasViagensPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MinhasViagensPage", function() { return MinhasViagensPage; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MinhasViagensPage = /** @class */ (function () {
    function MinhasViagensPage(navCtrl, menuCtrl, popoverCtrl, alertCtrl, modalCtrl, router, toastCtrl) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.router = router;
        this.toastCtrl = toastCtrl;
    }
    MinhasViagensPage.prototype.ngOnInit = function () {
    };
    MinhasViagensPage.prototype.ionViewWillEnter = function () {
        this.menuCtrl.enable(true);
        this.menuCtrl.toggle();
    };
    MinhasViagensPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./minhas-viagens.page.html */ "./src/app/pages/minhas-viagens/minhas-viagens.page.html"),
            styles: [__webpack_require__(/*! ./minhas-viagens.page.scss */ "./src/app/pages/minhas-viagens/minhas-viagens.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["NavController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["MenuController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["PopoverController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ModalController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_1__["ToastController"]])
    ], MinhasViagensPage);
    return MinhasViagensPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-minhas-viagens-minhas-viagens-module.js.map