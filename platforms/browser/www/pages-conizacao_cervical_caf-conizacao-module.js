(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-conizacao_cervical_caf-conizacao-module"],{

/***/ "./src/app/pages/conizacao_cervical_caf/conizacao.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/pages/conizacao_cervical_caf/conizacao.module.ts ***!
  \******************************************************************/
/*! exports provided: ConizacaoModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConizacaoModule", function() { return ConizacaoModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _conizacao_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./conizacao.page */ "./src/app/pages/conizacao_cervical_caf/conizacao.page.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    {
        path: '',
        component: _conizacao_page__WEBPACK_IMPORTED_MODULE_5__["ConizacaoPage"]
    }
];
var ConizacaoModule = /** @class */ (function () {
    function ConizacaoModule() {
    }
    ConizacaoModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ],
            declarations: [_conizacao_page__WEBPACK_IMPORTED_MODULE_5__["ConizacaoPage"]]
        })
    ], ConizacaoModule);
    return ConizacaoModule;
}());



/***/ }),

/***/ "./src/app/pages/conizacao_cervical_caf/conizacao.page.html":
/*!******************************************************************!*\
  !*** ./src/app/pages/conizacao_cervical_caf/conizacao.page.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n    <ion-toolbar color=\"secondary\">\r\n      <ion-buttons slot=\"start\">\r\n        <ion-menu-button color=\"light\"></ion-menu-button>\r\n      </ion-buttons>\r\n      <ion-title>Útero</ion-title>\r\n    </ion-toolbar>\r\n <!--   <ion-toolbar color=\"light\"> \r\n      <ion-searchbar [(ngModel)]=\"searchKey\"></ion-searchbar>\r\n    </ion-toolbar>-->\r\n  </ion-header>\r\n  \r\n  <ion-content>\r\n    <ion-card class=\"bg-white\" no-margin>\r\n        <ion-card-content>\r\n          <h2 margin-bottom>\r\n            <ion-text color=\"dark\"><strong>Conização</strong></ion-text>\r\n          </h2>\r\n          <br>Trata-se de uma excisão, normalmente, em formato de cone realizada por alça (Caf/Leep) ou a frio.\r\n          <br><b>Nota: </b> Caso o material seja obtido por eletrocautério, o cone pode se mostrar menor do que quando obtido por método convencional. Por isso, a amostra tende a ser mais difícil de se manusear.\r\n          <br><br>\r\n          <h3 margin-bottom text-dark><b>Procedimentos macroscópicos</b></h3>\r\n          <br>\r\n          <p margin-bottom text-dark>\r\n            1.\tO ideal é que a peça seja recebida no laboratório intacta e com a referência do fio de sutura que indica a posição 12 horas.\r\n              <br>2.\tCaso necessário, fotografe a peça.\r\n              <br>3.\tMeça a base nas duas dimensões e a profundidade do material. Além disso, avalie o orifício externo e se o material está em sua totalidade ou há apenas fragmentos.\r\n              <br>&nbsp;\tEm casos de cones fragmentados, provavelmente ocorrerá a dificuldade na identificação das margens, assim deve-se analisar com muito cuidado para que os cortes sejam providos da melhor informação possível (incluir na descrição macroscópica as dificuldades encontradas na identificação das margens, caso encontradas).\r\n              <br>4.\tAnalise o epitélio do ectocérvice quanto a cor, presença de erosão, laceração, irregularidade, cicatrização, massa –tamanho, formato e localização–, cistos –tamanho e conteúdo–, e por fim, sítio de biópsia prévia.\r\n              <br>5.\tEm posse de uma pinça, colocá-la no orifício externo e cortar o canal cervical de maneira longitudinal no sentido das 12 horas.\r\n              <br>&nbsp;\tCaso não haja referência na peça com fio de sutura ou outra forma de marcação, pode-se abrir em qualquer posição, desde que o sentido longitudinal seja respeitado. \r\n              <br>6.\tCaso a peça seja recebida no laboratório a fresco:\r\n              <br>&nbsp;\tPegue alfinetes e prenda a peça aberta em uma lâmina de cortiça ou papelão, com a superfície mucosa para cima. Após isso, realize a fixação em formol tamponado por algumas horas.\r\n              <br>7.\tApós fixação, pintar com tinta <i>Nankin</i> as margens cirúrgicas; é necessário impregnar bem toda a extensão das margens cruentas. As peças de conização devem possuir duas colorações diferentes: uma para as margens estromais laterais (ectocervical) e, outra, para a margem profunda (endocervial).\r\n              <br>8.\tSeccionar a peça por completo, em cortes radiais (quando fechada) ou paralelos (quando aberta), cujas distâncias devem ser de aproximadamente 3mm. É necessário seguir a ordem dos ponteiros do relógio para realizar este procedimento. Todos os cortes deverão conter os diferentes tipos de epitélio, incluindo a junção escamocolunar. Se necessário, apare o estroma\t<a href = '#' (click)=\"viewImage(imgSource3, imgTitle3, imgDescription3)\" > (Figura 5.7.8) </a>.\r\n              <br>\r\n          </p>\r\n          <br>\r\n          <h3 margin-bottom text-dark (click)=\"toggleDetails();\" style=\"width:100%; cursor:pointer\">\r\n            <div style=\"width: 100%; float:left; background: #878787; color: #fff; padding: 9px;\">\r\n              <b>Ex. de descrição macroscópica</b>\r\n              <div style=\"width: 4%; float:right;\"><i class=\"fa fa-angle-double-down\" style=\"color:#fff\" ></i></div>\r\n            </div>\r\n          </h3>\r\n          <br><br>\r\n          \r\n        <div id=\"formDoc\" name=\"formDoc\" style=\"display:none;\">  \r\n         <div>\r\n              <ion-radio-group allow-empty-selection (ionChange)=\"opt1($event)\">\r\n                <!--(ionSelect)=\"goFish($event)\" (ionBlur)=\"leaveFish($event)\"-->\r\n                <ion-row>\r\n                  <ion-col>\r\n                    <div><label>Material recebido</label></div>\r\n                  </ion-col>\r\n                </ion-row>\r\n                <ion-row style=\"margin-top: -10px;\">\r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-radio value=\" a fresco\"></ion-radio>&nbsp;\r\n                      <ion-label id=\"radio-form\" > a fresco</ion-label>\r\n                    </div>\r\n                  </ion-col>\r\n                </ion-row>\r\n                <ion-row style=\"margin-top: -10px;\">\r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-radio value=\" em formol tamponado a 10%\"></ion-radio>&nbsp;\r\n                      <ion-label id=\"radio-form\">em formol tamponado a 10%</ion-label>\r\n                    \r\n                    </div>\r\n                  </ion-col>\r\n                </ion-row>\r\n                <ion-row style=\"margin-top: -10px;\">\r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-radio value = \"texto\"></ion-radio>&nbsp;\r\n                      <ion-label id=\"radio-form\">outro (citar)</ion-label> &nbsp;\r\n                      <input style = \"width: 50%;\" type=\"text\" (keyup)=\"inputTexto1($event)\">\r\n                    </div>\r\n                  </ion-col>\r\n                </ion-row>\r\n              </ion-radio-group>\r\n        </div>\r\n        <div>\r\n          <ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t<div>\r\n                  <ion-label>que consiste de segmento de colo uterino</ion-label>&nbsp;\r\n\t\t\t\t\t\t\t</div>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-radio-group allow-empty-selection (ionChange)=\"opt2($event)\">\r\n            <ion-row style=\"margin-top: -10px;\">\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio value=\" íntegro,\"></ion-radio>&nbsp;\r\n                  <ion-label >íntegro</ion-label>\r\n                </div>\r\n              </ion-col>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio value=\" fragmentado,\"></ion-radio>&nbsp;\r\n                  <ion-label>fragmentado</ion-label>\r\n                </div>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-radio-group>\r\n          <ion-row style=\"margin-top: -10px;\">\r\n            <ion-col>\r\n              <div>\r\n                <ion-label>medindo</ion-label>&nbsp;\r\n                <input style = \"width: 13%;\" type=\"text\" (keyup)=\"inputTexto2($event)\">&nbsp;\r\n                <ion-label >x</ion-label>&nbsp;\r\n                <input style = \"width: 13%;\" type=\"text\" (keyup)=\"inputTexto3($event)\">&nbsp;\r\n                <ion-label>x</ion-label>&nbsp;\r\n                <input style = \"width: 13%;\" type=\"text\" (keyup)=\"inputTexto4($event)\">&nbsp;\r\n                <ion-label>cm</ion-label>\r\n              </div>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-radio-group allow-empty-selection (ionChange)=\"opt3($event)\">\r\n            <ion-row style=\"margin-top: -10px;\">\r\n                  <ion-col>\r\n                    <div>\r\n                      <ion-label>O ectocérvice é</ion-label>\r\n                    </div>\r\n                  </ion-col>\r\n                </ion-row>\r\n                <ion-row style=\"margin-top: -10px;\">\r\n                  <ion-col>\r\n                    <div>\r\n                        <ion-radio value=\" liso,\"></ion-radio>&nbsp;\r\n                        <ion-label>liso</ion-label>\r\n\r\n                    </div>\r\n                  </ion-col>\r\n                  <ion-col>\r\n                    <div>\r\n                        <ion-radio value=\" rugoso,\"></ion-radio>&nbsp;\r\n                        <ion-label>rugoso</ion-label>&nbsp;\r\n                      \r\n                    </div>\r\n                  </ion-col>\r\n                  <ion-col>\r\n                    <div>\r\n                        <ion-radio value=\" irregular,\"></ion-radio>&nbsp;\r\n                        <ion-label>irregular</ion-label>\r\n                    </div>\r\n                  </ion-col>\r\n            </ion-row>\r\n          </ion-radio-group>\r\n              <ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n                    <ion-radio-group allow-empty-selection (ionChange)=\"opt4($event)\">\r\n                      <ion-radio value=\" de tonalidade róseo-esbranquiçada,\"></ion-radio>&nbsp;\r\n                      <ion-label>de tonalidade róseo-esbranquiçada</ion-label>\r\n                    </ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n                </ion-col>\r\n              </ion-row>\r\n              <ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n                    <ion-radio-group allow-empty-selection (ionChange)=\"opt5($event)\">\r\n                      <ion-radio value=\" com lesão\"></ion-radio>&nbsp;\r\n                      <ion-label>com lesão</ion-label>&nbsp;\r\n                      <input style = \"width: 13%;\"  type=\"text\" (keyup)=\"inputTexto5($event)\"> &nbsp;\r\n                      <ion-label>medindo</ion-label>&nbsp;\r\n                      <input style = \"width: 13%;\"  type=\"text\" (keyup)=\"inputTexto6($event)\">&nbsp;\r\n                      <ion-label>cm em sua maior dimensão</ion-label>\r\n                    </ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n                </ion-col>\r\n              </ion-row>\r\n              \r\n              <ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n                    <ion-radio-group allow-empty-selection (ionChange)=\"opt6($event)\">\r\n                      <ion-radio value=\" e apresenta orificio externo\"></ion-radio>&nbsp;\r\n                      <ion-label >e apresenta orificio externo</ion-label>\r\n                    </ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n                </ion-col>\r\n              </ion-row>\r\n              <ion-radio-group allow-empty-selection (ionChange)=\"opt7($event)\">\r\n                  <ion-row style=\"margin-top: -10px;\">\r\n                    <ion-col>\r\n                      <div>\r\n                        <ion-radio value=\" linear.\"></ion-radio>&nbsp;\r\n                        <ion-label>linear</ion-label>\r\n                      \r\n                      </div>\r\n                    </ion-col>\r\n                    <ion-col>\r\n                      <div>\r\n                        <ion-radio value=\" irregular.\"></ion-radio>&nbsp;\r\n                        <ion-label>irregular</ion-label>\r\n                      \r\n                      </div>\r\n                    </ion-col>\r\n                    <ion-col>\r\n                      <div>\r\n                        <ion-radio value=\" circular.\"></ion-radio>&nbsp;\r\n                        <ion-label >circular</ion-label>\r\n                      \r\n                      </div>\r\n                    </ion-col>\r\n                  </ion-row>\r\n                </ion-radio-group>\r\n              <ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n                    <ion-label>Aos cortes, o aspecto é usual</ion-label>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n                </ion-col>\r\n              </ion-row>\r\n              <ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n                    <ion-radio-group allow-empty-selection (ionChange)=\"opt8($event)\">\r\n                      <ion-radio value=\" com pequenos cistos preenchido por material gelatinoide\"></ion-radio>&nbsp;\r\n                      <ion-label >com pequenos cistos preenchido por material</ion-label>&nbsp;\r\n                      <ion-label >gelatinoide.</ion-label>\r\n                    </ion-radio-group>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n                </ion-col>\r\n              </ion-row>\r\n              <ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n                    <ion-label>Todo material é submetido a exame histológico.</ion-label>\r\n\t\t\t\t\t\t\t\t\t</div>\r\n                </ion-col>\r\n              </ion-row>\r\n              <ion-row style=\"margin-top: -10px;\">\r\n\t\t\t\t\t\t\t\t<ion-col>\r\n\t\t\t\t\t\t\t\t\t<div>\r\n                    <ion-label>Legenda : </ion-label>&nbsp;\r\n                    <input style = \"width: 30%;\" type=\"text\" (keyup)=\"inputTexto7($event)\">\r\n                  </div>\r\n                </ion-col>\r\n              </ion-row>\r\n\r\n            </div>\r\n\r\n          \r\n          <br>\r\n          <a class=\"btn btn-app\"style=\" width: 50%; padding: 5px 5px 5px 5px; border-radius: 3px;\" (click)='gerarPDF()' >\r\n            <i class=\"far fa-file\" style=\"color:#878787\"></i>&nbsp;\r\n            <span class=\"fontItem\">Baixar Arquivo</span>\r\n          </a>\r\n        </div>\r\n        <br>\r\n        <h3 margin-bottom><b>Cortes macroscópicos representativos</b></h3><br>\r\n        \r\n        Todo o material deverá ser utilizado.<br>\r\n        <br><b>1.</b> Peça com referência às 12 horas, representar os cortes em sentido horário, identificando a sequência <a href = '#' (click)=\"viewImage(imgSource3, imgTitle3, imgDescription3)\" > (Figura 5.7.8) </a>.\r\n        <ion-row style=\"margin-top: -10px;\">\r\n          <ion-col>\r\n            <div>\r\n              <label><b>Cassete A: </b>12h às 3h (___fs/___bl); </label>&nbsp;\r\n              \r\n            </div>\r\n          </ion-col>\r\n          <ion-col>\r\n            <div>\r\n              <label><b>Cassete C: </b>6h às 9h\t(___fs/___bl);</label>&nbsp;\r\n              \r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n        <ion-row style=\"margin-top: -10px;\">\r\n          <ion-col>\r\n            <div>\r\n              <label><b>Cassete A: </b>12h às 3h (___fs/___bl); </label>&nbsp;\r\n              \r\n            </div>\r\n          </ion-col>\r\n          <ion-col>\r\n            <div>\r\n              <label><b>Cassete D: </b>9h às 12h (___fs/___bl).</label>&nbsp;\r\n              \r\n            </div>\r\n          </ion-col>\r\n        </ion-row>\r\n        \r\n              \t\t\t\r\n     \r\n        <br><b>2.</b> Peça sem referência às 12 horas, cortes paralelos sequenciais <a href = '#' (click)=\"viewImage(imgSource4, imgTitle4, imgDescription4)\" > (Foto 5) </a> <a href = '#' (click)=\"viewImage(imgSource5, imgTitle5, imgDescription5)\" > (Foto 5.2) </a>.\r\n        <br><b>Cassete A à __</b>\r\n        <br>\r\n        <br><b>3.</b> Peça fragmentada, cortes paralelos e identificando sequencialmente os fragmentos representados.\r\n        <br><b>Cassete A1 à __: </b>fragmento maior (___fs/___bl);\t\r\n        <br>\r\n        <b>Cassete B:</b>\tfragmento menor (___fs/___bl).\r\n        <br>\r\n        <br><b>4.</b> Quando não houver a possibilidade de pintar as margens estromais laterais e endocervical com cores distintas, ou em casos em que a peça apresentar mais que 2,0 cm de profundidade, realize um corte transversal separando a margem profunda e posteriormente corte o cone em secções radiais sequenciais <a href = '#' (click)=\"viewImage(imgSource2, imgTitle2, imgDescription2)\" > (Figura 5.7.4) </a> <a href = '#' (click)=\"viewImage(imgSource, imgTitle, imgDescription)\" > (Foto 5.4) </a> .\r\n        <br><b>Cassete A1 à __: </b> Cone/margem estromal lateral;\t\r\n        <br>\r\n        <b>Cassete B:</b>\tMargem profunda/endocervical (___fs/___bl).\r\n        <br>\r\n \r\n        <br><b>NOTA: </b> Desenhe a peça, efetuando o esquema dos cortes representativos, quando pertinentes. \r\n        <br><br>\r\n        <p text-center>\r\n            <a class=\"btn btn-app\" style=\" width: 45%\"  (click)=\"desenhos()\">\r\n              <i class=\"fa fa-image fa-2x\" style=\"color:#e74a92\"></i><br>\r\n              <span>Figuras Ilustrativas</span>\r\n            </a>\r\n            <a class=\"btn btn-app\" style=\" width: 45%\" (click)=\"fotos()\">\r\n              <i class=\"fas fa-camera fa-2x\" style=\"color:#e74a92\"></i><br>\r\n              <span>Fotos Ilustrativas</span>\r\n            </a>\r\n        </p>\r\n          \r\n      </ion-card-content>\r\n   </ion-card>\r\n</ion-content>\r\n  "

/***/ }),

/***/ "./src/app/pages/conizacao_cervical_caf/conizacao.page.scss":
/*!******************************************************************!*\
  !*** ./src/app/pages/conizacao_cervical_caf/conizacao.page.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ":host ion-content {\n  --background: var(--ion-color-light); }\n\n:host ion-item {\n  border-radius: 0;\n  border-bottom: 1px dotted var(--ion-color-medium); }\n\n:host ion-card.no-radius {\n  border-radius: 0; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY29uaXphY2FvX2NlcnZpY2FsX2NhZi9DOlxcVXNlcnNcXGplc3NpXFxEb2N1bWVudHNcXEFQTWFjcm8vc3JjXFxhcHBcXHBhZ2VzXFxjb25pemFjYW9fY2VydmljYWxfY2FmXFxjb25pemFjYW8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBRVEsb0NBQWEsRUFBQTs7QUFGckI7RUFNUSxnQkFBZ0I7RUFDaEIsaURBQWlELEVBQUE7O0FBUHpEO0VBWVksZ0JBQWdCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9jb25pemFjYW9fY2VydmljYWxfY2FmL2Nvbml6YWNhby5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XHJcbiAgICBpb24tY29udGVudCB7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24tY29sb3ItbGlnaHQpO1xyXG4gICAgfVxyXG5cclxuICAgIGlvbi1pdGVtIHtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgICAgIGJvcmRlci1ib3R0b206IDFweCBkb3R0ZWQgdmFyKC0taW9uLWNvbG9yLW1lZGl1bSk7XHJcbiAgICB9XHJcblxyXG4gICAgaW9uLWNhcmQge1xyXG4gICAgICAgICYubm8tcmFkaXVzIHtcclxuICAgICAgICAgICAgYm9yZGVyLXJhZGl1czogMDtcclxuICAgICAgICB9XHJcblx0XHRcclxuICAgIH1cclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/conizacao_cervical_caf/conizacao.page.ts":
/*!****************************************************************!*\
  !*** ./src/app/pages/conizacao_cervical_caf/conizacao.page.ts ***!
  \****************************************************************/
/*! exports provided: ConizacaoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConizacaoPage", function() { return ConizacaoPage; });
/* harmony import */ var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic-native/file/ngx */ "./node_modules/@ionic-native/file/ngx/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! pdfmake/build/pdfmake */ "./node_modules/pdfmake/build/pdfmake.js");
/* harmony import */ var pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! pdfmake/build/vfs_fonts */ "./node_modules/pdfmake/build/vfs_fonts.js");
/* harmony import */ var pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/image-viewer/image-viewer.component */ "./src/app/components/image-viewer/image-viewer.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/file-opener/ngx */ "./node_modules/@ionic-native/file-opener/ngx/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.vfs = pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default.a.pdfMake.vfs;





var ConizacaoPage = /** @class */ (function () {
    function ConizacaoPage(modalController, router, navCtrl, formBuilder, file, navController, fileOpener) {
        this.modalController = modalController;
        this.router = router;
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.file = file;
        this.navController = navController;
        this.fileOpener = fileOpener;
        this.imgSource = 'assets/img/conizacao_colo/fotos/DSC00491.png';
        this.imgTitle = '';
        this.imgDescription = 'Foto 5.4: Cortes representativos para cone do colo do útero. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource2 = 'assets/img/conizacao_colo/img/1591711139196.png';
        this.imgTitle2 = '';
        this.imgDescription2 = 'Figura 5.7.4: Procedimentos macroscópicos para cones sem referência (adaptada in Westra WH, et al. Surgical pathology dissection: an illustrated guide).';
        this.imgSource3 = 'assets/img/conizacao_colo/img/1591642826961.png';
        this.imgTitle3 = '';
        this.imgDescription3 = 'Figura 5.7.8: Procedimentos macroscópicos para cones com referência (adaptada in Westra WH, et al. Surgical pathology dissection: an illustrated guide).';
        this.imgSource4 = 'assets/img/conizacao_colo/fotos/DSC00517-1.png';
        this.imgTitle4 = '';
        this.imgDescription4 = 'Foto 5: Cone do colo do útero sem referência às 12 horas. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.imgSource5 = 'assets/img/conizacao_colo/fotos/DSC00521-3.png';
        this.imgTitle5 = '';
        this.imgDescription5 = 'Foto 5.2: Cortes representativos para cone do colo do útero. (Fonte: Próprio autor (2020). Fotografia obtida no laboratório de Anatomia Patológica do Hospital das Clínicas Samuel Libânio – HCSL).';
        this.pdfObj = null;
    }
    ConizacaoPage.prototype.viewImage = function (src, title, description) {
        if (title === void 0) { title = ''; }
        if (description === void 0) { description = ''; }
        return __awaiter(this, void 0, void 0, function () {
            var modal;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _components_image_viewer_image_viewer_component__WEBPACK_IMPORTED_MODULE_5__["ImageViewerComponent"],
                            componentProps: {
                                imgSource: src,
                                imgTitle: title,
                                imgDescription: description
                            },
                            cssClass: 'modal-fullscreen',
                            keyboardClose: true,
                            showBackdrop: true
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    ConizacaoPage.prototype.ngOnInit = function () { };
    ConizacaoPage.prototype.gerarPDF = function () {
        var _this = this;
        if (this.option1 == undefined) {
            this.option1 = '';
        }
        if (this.option2 == undefined) {
            this.option2 = '';
        }
        if (this.option3 == undefined) {
            this.option3 = '';
        }
        if (this.option4 == undefined) {
            this.option4 = '';
        }
        if (this.option5 == undefined) {
            this.option5 = '';
        }
        if (this.option6 == undefined) {
            this.option6 = '';
        }
        if (this.option7 == undefined) {
            this.option7 = '';
        }
        if (this.option8 == undefined) {
            this.option8 = '';
        }
        if (this.texto1 == undefined) {
            this.texto1 = '';
        }
        if (this.texto2 == undefined) {
            this.texto2 = '';
        }
        if (this.texto3 == undefined) {
            this.texto3 = '';
        }
        if (this.texto4 == undefined) {
            this.texto4 = '';
        }
        if (this.texto5 == undefined) {
            this.texto5 = '';
        }
        if (this.texto6 == undefined) {
            this.texto6 = '';
        }
        if (this.texto7 == undefined) {
            this.texto7 = '';
        }
        if (this.option1 == 'texto') {
            this.option1 = ' ' + this.texto1;
        }
        if (this.option5 == ' com lesão') {
            this.option5 = this.option5 + ' ' + this.texto5 + ' medindo ' + this.texto6 + ' cm em sua maior dimensão,';
        }
        if (this.texto7 != '') {
            this.texto7 = this.texto7;
        }
        pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.vfs = pdfmake_build_vfs_fonts__WEBPACK_IMPORTED_MODULE_3___default.a.pdfMake.vfs;
        var docDefinition = {
            content: [
                {
                    columns: [
                        [
                            { text: 'Conização', style: 'header' },
                            { text: '', style: 'sub_header' },
                            { text: 'Material recebido' + this.option1 + ' que consiste de segmento de colo uterino' + this.option2 + ' medindo ' + this.texto2 + ' x ' + this.texto3 + ' x ' + this.texto4 + ' cm.' +
                                    ' O ectocérvice é' + this.option3 + '' + this.option4 + '' + this.option5 + '' + this.option6 + '' + this.option7 + ' ' +
                                    'Aos cortes, o aspecto é usual' + this.option8 + '. Todo material é submetido a exame histológico. Legenda: ' + this.texto7 + '.', style: 'text' },
                        ]
                    ]
                }
            ],
            styles: {
                header: {
                    bold: true,
                    fontSize: 20,
                    alignment: 'center'
                },
                sub_header: {
                    fontSize: 18,
                    alignment: 'center'
                },
                text: {
                    fontSize: 16,
                    margin: [0, 30, 0, 0],
                    alignment: 'left'
                }
            },
            pageSize: 'A4',
            pageOrientation: 'portrait'
        };
        //pdfMake.createPdf(docDefinition).open();
        this.pdfObj = pdfmake_build_pdfmake__WEBPACK_IMPORTED_MODULE_2___default.a.createPdf(docDefinition);
        //console.log(this.pdfObj.getBuffer());
        this.pdfObj.getBuffer(function (buffer) {
            var blob = new Blob([buffer], { type: 'application/pdf' });
            // Save the PDF to the data Directory of our App
            _this.file.writeFile(_this.file.dataDirectory, 'Conizacao.pdf', blob, { replace: true });
            // Open the PDf with the correct OS tools
            _this.fileOpener.open(_this.file.dataDirectory + 'Conizacao.pdf', 'application/pdf');
        });
    };
    ConizacaoPage.prototype.fotos = function () {
        this.router.navigateByUrl('/conizacao_colo/fotos');
    };
    ConizacaoPage.prototype.desenhos = function () {
        this.router.navigateByUrl('/conizacao_colo/imagens');
    };
    ConizacaoPage.prototype.toggleDetails = function () {
        if (document.getElementById("formDoc").style.display == 'none') {
            document.getElementById("formDoc").style.display = "block";
        }
        else {
            document.getElementById("formDoc").style.display = "none";
        }
    };
    ConizacaoPage.prototype.opt1 = function (event) {
        this.option1 = event.target.value;
    };
    ConizacaoPage.prototype.opt2 = function (event) {
        this.option2 = event.target.value;
    };
    ConizacaoPage.prototype.opt3 = function (event) {
        this.option3 = event.target.value;
    };
    ConizacaoPage.prototype.opt4 = function (event) {
        this.option4 = event.target.value;
    };
    ConizacaoPage.prototype.opt5 = function (event) {
        this.option5 = event.target.value;
    };
    ConizacaoPage.prototype.opt6 = function (event) {
        this.option6 = event.target.value;
    };
    ConizacaoPage.prototype.opt7 = function (event) {
        this.option7 = event.target.value;
    };
    ConizacaoPage.prototype.opt8 = function (event) {
        this.option8 = event.target.value;
    };
    ConizacaoPage.prototype.inputTexto1 = function (event) {
        this.texto1 = event.target.value;
    };
    ConizacaoPage.prototype.inputTexto2 = function (event) {
        this.texto2 = event.target.value;
    };
    ConizacaoPage.prototype.inputTexto3 = function (event) {
        this.texto3 = event.target.value;
    };
    ConizacaoPage.prototype.inputTexto4 = function (event) {
        this.texto4 = event.target.value;
    };
    ConizacaoPage.prototype.inputTexto5 = function (event) {
        this.texto5 = event.target.value;
    };
    ConizacaoPage.prototype.inputTexto6 = function (event) {
        this.texto6 = event.target.value;
    };
    ConizacaoPage.prototype.inputTexto7 = function (event) {
        this.texto7 = event.target.value;
    };
    ConizacaoPage = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'ap-conizacao',
            template: __webpack_require__(/*! ./conizacao.page.html */ "./src/app/pages/conizacao_cervical_caf/conizacao.page.html"),
            styles: [__webpack_require__(/*! ./conizacao.page.scss */ "./src/app/pages/conizacao_cervical_caf/conizacao.page.scss")]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_7__["FormBuilder"], _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_0__["File"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"],
            _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_8__["FileOpener"]])
    ], ConizacaoPage);
    return ConizacaoPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-conizacao_cervical_caf-conizacao-module.js.map