import { Component, ViewChildren, QueryList  } from '@angular/core';
import { Platform, NavController, ActionSheetController,IonRouterOutlet } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { Pages } from './interfaces/pages';

import { Router } from '@angular/router';
import { UserService } from 'src/app/core/user/user.service';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public appPages: Array<Pages>;
  
  lastTimeBackPress = 0;
  timePeriodToExit = 2000;
	user$: Observable<User>;
 @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;
  
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private actionSheetCtrl: ActionSheetController,
    private userService: UserService,
	  private router: Router,
    public navCtrl: NavController
  ) {

    this.appPages = [
     /* {
        title: 'HOME',
        url: '/home',
        direct: 'root',
        icon: 'home'
      },
      {
        title: 'MENSAGEM',
        url: '/',
        direct: 'forward',
        icon: 'mail'
      },

      {
        title: 'MINHAS VIAGENS',
        url: '/',
        direct: 'forward',
        icon: 'flag'
      },
      {
        title: 'MEUS PAGAMENTOS',
        url: '/',
        direct: 'forward',
        icon: 'card'
      },
	  {
        title: 'CONFIGURAÇÕES',
        url: '/',
        direct: 'forward',
        icon: 'settings'
      },
      {
        title: 'MEUS DADOS',
        url: '/',
        direct: 'forward',
        icon: 'person'
      },
      {
        title: 'AVALIAÇÕES',
        url: '/',
        direct: 'forward',
        icon: 'star'
      }*/
    ];

   // this.user$ = userService.getUser();
    this.initializeApp();
	  this.statusBar.backgroundColorByHexString('#ffffff');
  }

  initializeApp() {
      this.platform.ready().then(() => {
      this.statusBar.styleDefault();
	    this.statusBar.backgroundColorByHexString('#ffffff');
      this.splashScreen.hide();
    }).catch(() => {});

    
    
  }

  goToEditProgile() {
    this.navCtrl.navigateForward('edit-profile');
  }

  toHome(){
    this.router.navigateByUrl('/home');
  }

  toMensagens(){
    this.router.navigateByUrl('/mensagens');
  }

  toViagens(){
    this.router.navigateByUrl('/minhas-viagens');
  }

  toPagamentos(){
    this.router.navigateByUrl('/meus-pagamentos');
  }

  toConfiguracoes(){
    this.router.navigateByUrl('/configuracoes');
  }

  toDados(){
    this.router.navigateByUrl('/meus-dados');
  }

  toAvaliacoes(){
    this.router.navigateByUrl('/avaliacoes');
  }


  logout() {
    this.userService.logout();
    this.router.navigate(['']);
    //navigator['app'].exitApp();

  }
  
  // active hardware back button
    backButtonEvent() {
		
        this.platform.backButton.subscribe(async () => {
            // close action sheet
            try {
                const element = await this.actionSheetCtrl.getTop();
                if (element) {
                    element.dismiss();
                    return;
                }
            } catch (error) {
            }

            

            this.routerOutlets.forEach((outlet: IonRouterOutlet) => {
                if (outlet && outlet.canGoBack()) {
                    outlet.pop();

                } else if (this.router.url === '/home-results') {
                    if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
                      this.userService.logout();
                      this.router.navigate(['']);
                        // this.platform.exitApp(); // Exit from app
                        navigator['app'].exitApp(); // work in ionic 4

                    } else {
                        
                        this.lastTimeBackPress = new Date().getTime();
                    }
                }
            });
        });
    }
}
