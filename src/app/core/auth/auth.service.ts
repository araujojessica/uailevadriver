import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {tap} from 'rxjs/operators';
import { UserService } from '../user/user.service';


//const API_URL = 'http://159.203.181.9:3000';
const API_URL = 'http://localhost:3000';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http : HttpClient, private userService: UserService) { }

  authenticate( phone:string, password: string) {

    return this.http.post(`${API_URL}/user/login/mot`,{phone, password},{observe:'response'})
      .pipe(tap(res => {
        const authToken = res.headers.get('x-access-token');
        this.userService.setToken(authToken);
        console.log(`User ${phone} authenticated with token ${authToken}`)
      }))
  }
}