import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/auth/auth.guard';

const routes: Routes = [
  { path: '',loadChildren: './pages/login/login.module#LoginPageModule', canActivate:[AuthGuard] },
  { path: 'home', loadChildren: './pages/home-results/home-results.module#HomeResultsPageModule' },
  { path: 'register-driver', loadChildren: './pages/register_driver/register-driver.module#RegisterDriverModule'},
  { path: 'mensagens', loadChildren: './pages/mensagens/mensagens.module#MensagensPageModule'},
  { path: 'minhas-viagens', loadChildren: './pages/minhas-viagens/minhas-viagens.module#MinhasViagensPageModule'},
  { path: 'meus-pagamentos', loadChildren: './pages/meus-pagamentos/meus-pagamentos.module#MeusPagamentosPageModule'},
  { path: 'configuracoes', loadChildren: './pages/configuracoes/configuracoes.module#ConfiguracoesPageModule'},
  { path: 'meus-dados', loadChildren: './pages/meus-dados/meus-dados.module#MeusDadosPageModule'},
  { path: 'avaliacoes', loadChildren: './pages/avaliacoes/avaliacoes.module#AvaliacoesPageModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
