import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, MenuController, ToastController, AlertController, LoadingController, Platform } from '@ionic/angular';
import {Router} from '@angular/router';
import { AuthService } from 'src/app/core/auth/auth.service';
import { PlatformDectorService } from 'src/app/core/plataform-dector/plataform-dector.service';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { RecuperarSenhaService } from './recuperarsenha.service';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public onLoginForm: FormGroup;
  users = [];
  user: any = {};
  @ViewChild('userNameInput') userNameInput: ElementRef<HTMLInputElement>;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private recuperarSenha: RecuperarSenhaService,
	  private router: Router,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private fb: Facebook,
    ) {}

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  

  ngOnInit() {
   this.onLoginForm = this.formBuilder.group({
      'phone': [null, Validators.compose([Validators.required])],
      'password': [null, Validators.compose([Validators.required])]
    });
  }

  
  async forgotPass() {
  const alert = await this.alertCtrl.create({
      header: 'Esqueceu sua senha?',
      message: 'Informe seu e-mail para enviarmos um novo link de recuperação.',
      inputs: [
        {
          name: 'email',
          type: 'email',
          placeholder: 'E-mail'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirmar Cancelamento?');
          }
        }, {
          text: 'Confirmar',
          handler: async data => {
            console.log(data.email)
            this.recuperarSenha.sendemail(data.email)
            .subscribe(
                async () =>{ 
                  this.navCtrl.navigateRoot('')
                  const toast = await this.toastCtrl.create({
                            
                    message:'Email enviado com sucesso!', 
                    duration:4000, position:'top',
                    color:'success'
                    });
            
                  
                  toast.present();
                },
                async err => {
             
                   const toast = await this.toastCtrl.create({
                            
                            message:'Usuário não cadastrado, por favor cadastre-se no APP!', 
                            duration:4000, position:'top',
                            color:'danger'
                    });
            
                   
                   toast.present();
    
                }
                
            );
            
          }
        }
      ]
    });

    await alert.present();
  }


 
  // // //
  goToRegister() {
    this.router.navigateByUrl('/register-driver');
    
  }

  goToHome() {

    const phone = this.onLoginForm.get('phone').value;
    const password = this.onLoginForm.get('password').value;

    console.log(phone);
    console.log(password)
    this.authService.authenticate(phone, password)
        .subscribe(
            () => this.navCtrl.navigateRoot('/home'),
            async err => {
              this.onLoginForm.reset();
               const toast = await this.toastCtrl.create({
                  message:'Usuário ou senha incorreto, por favor tente novamente!', 
                  duration:4000, position:'top',
                  color:'danger'
                });
                       
               toast.present();

            }
        );
    
  }

  loginFb(){
    this.fb.login(['public_profile', 'user_friends', 'email'])
    .then((res: FacebookLoginResponse) => {
      if(res.status==='conected'){
          this.user.img = 'http://graph.facebook.com/'+res.authResponse.userID+'/picture?type=square';
      } else {
        alert('Login failed');
      }
      console.log('Logged into Facebook!', res)
    })
    .catch(e => console.log('Error logging into Facebook', e));
  }
  
}

