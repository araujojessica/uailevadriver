import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinhasViagensPage } from './minhas-viagens.page';

describe('MinhasViagensPage', () => {
  let component: MinhasViagensPage;
  let fixture: ComponentFixture<MinhasViagensPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinhasViagensPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinhasViagensPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
