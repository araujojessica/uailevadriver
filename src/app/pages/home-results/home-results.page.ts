import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { LoadingController, Platform } from '@ionic/angular';
import {Environment, Geocoder, GoogleMap, GoogleMapOptions, GoogleMaps, GoogleMapsAnimation, GoogleMapsEvent, ILatLng, Marker, MyLocation} from '@ionic-native/google-maps'
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController,  
  ModalController, } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';
import { UserService } from 'src/app/core/user/user.service';
import { Router } from '@angular/router';
import { ViagensService } from './viagensService';
import { async } from '@angular/core/testing';
import { Viagem } from 'src/app/core/viagem/viagem';

declare var google: any;

@Component({
  selector: 'app-home',
  templateUrl: 'home-results.page.html',
  styleUrls: ['home-results.page.scss'],
})
export class HomeResultsPage implements OnInit{

  @ViewChild('map') mapElement: any;
  private loading: any;
  private map: GoogleMap;
  public search: string = '';
  public search2: string = '';
  private googleAutocomplete = new google.maps.places.AutocompleteService();
  public searchResults = new Array<any>();
  public searchResults2 = new Array<any>();
  private originMarker: Marker;
  private destination: any;
  private origem: any;
  private googleDirectionsService = new google.maps.DirectionsService();
  private matrix = new google.maps.DistanceMatrixService();
 
  endOrigin: string ='';
  corrida: string;
  tempoEstimadoCorrida: string ;
  valorCorrida: string;
  user$: Observable<User>;
  users = [];

  constructor(
    private platform: Platform, 
    private loadCtrl: LoadingController,
    private ngZone: NgZone,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    private userService: UserService,
    private router: Router,
    private viagensService: ViagensService,
    
  ) {

    this.user$ = userService.getUser();
    viagensService.authenticate();
      
    
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
    this.menuCtrl.close();
  }

  ngOnInit(): void {

    this.mapElement = this.mapElement.nativeElement;
    this.mapElement.style.width = this.platform.width() + 'px';
    this.mapElement.style.height = this.platform.height() + 'px';

    this.loadMap();
    
  }
 
  async loadMap(){
    this.loading = await this.loadCtrl.create({message: 'Por favor aguarde ...'});
    await this.loading.present();
    
    Environment.setEnv({
      'API_KEY_FOR_BROWSER_RELEASE': 'AIzaSyBBSA6wXmuyhUqg1fDMH4-WP3QZSRSDtr4',
      'API_KEY_FOR_BROWSER_DEBUG': 'AIzaSyBBSA6wXmuyhUqg1fDMH4-WP3QZSRSDtr4'
    });

    const mapOptions: GoogleMapOptions = {
      controls: {
        zoom: false
      }
    }
    this.map = GoogleMaps.create(this.mapElement, mapOptions);

    try {
      await this.map.one(GoogleMapsEvent.MAP_READY);
      this.addOrigenMarker();
    } catch (error) {
      console.error(error)
    }
      
  }
 
  async addOrigenMarker() {

    try {
      const myLocation: MyLocation = await this.map.getMyLocation();
      await this.map.moveCamera({
        target: myLocation.latLng,
        zoom: 18
      }) ;
     
     this.originMarker=  this.map.addMarkerSync({
        title: 'Origem',
        icon: '#000',
        animation: GoogleMapsAnimation.DROP,
        position: myLocation.latLng
      });
    } catch (error) {
      console.error(error)
    }finally{
      this.loading.dismiss();
    }
    
  }
  searchChanged(){
    if (!this.search.trim().length) return;

    this.googleAutocomplete.getPlacePredictions({input: this.search}, predictions => {
      this.ngZone.run(() =>{
        this.searchResults = predictions;
      });
    
      
    });
  }

  searchChanged2(){
    if (!this.search2.trim().length) return;

    this.googleAutocomplete.getPlacePredictions({input: this.search2}, predictions => {
      this.ngZone.run(() =>{
        this.searchResults2 = predictions;
      });
    
      
    });
  }
  async calcRoute2(item: any){
    this.search2 = '';
    this.origem = item;
    console.log(this.origem);
   
  }

  async calcRoute(item: any){
    this.search = '';
     
   this.destination = item;
   console.log(this.origem);
   
   const info: any = await Geocoder.geocode({address: this.destination.description})


   let markerDestination: Marker = this.map.addMarkerSync({
     title: this.destination.description,
     icon: '#000',
     animation: GoogleMapsAnimation.DROP,
     position: info[0].position
   });
  // console.log('distancia origem: ' + this.originMarker.getPosition().lat + " long :  " + this.originMarker.getPosition().lng)
  // console.log('distancia destino: ' + markerDestination.getPosition());
   
   this.googleDirectionsService.route({
     origin: this.originMarker.getPosition(),
     destination: markerDestination.getPosition(),
     travelMode: 'DRIVING'
   }, async results =>{
      const points = new Array<ILatLng>();
      const routes = results.routes[0].overview_path;
     
      
      for(let i = 0; i < routes.length; i++){
        points[i] = {
          lat: routes[i].lat(),
          lng: routes[i].lng()
        }
        
      }
      await this.map.addPolyline({
        points: points,
        color: '#000',
        width: 3
   });
   var origin1 = new google.maps.LatLng(this.originMarker.getPosition().lat, this.originMarker.getPosition().lng);
   var destinationB = new google.maps.LatLng(markerDestination.getPosition().lat, markerDestination.getPosition().lng);
   this.matrix.getDistanceMatrix(
    {
      origins: [origin1],
      destinations: [destinationB],
      travelMode: google.maps.TravelMode.DRIVING
    }, callback);
  
  function callback(response, status) {
    let travelDetailsObject;
    if (status !== "OK") {
      alert("Error was: " + status);
    } else {
      var origins = response.originAddresses;
       var destinations = response.destinationAddresses;
       console.log(origins)
       console.log(destinations)
       for (var i = 0; i < origins.length; i++) {
         var results = response.rows[i].elements;
         for (var j = 0; j < results.length; j++) {
            var element = results[j];
            var distance = element.distance.text;
            var duration = element.duration.text;
            var from = origins[i];
            var to = destinations[j];
            travelDetailsObject = {
               distance: distance,
               duration: duration
            }
         }
       }
       this.travelDetailsObject = travelDetailsObject;
       
       this.corrida = this.travelDetailsObject.distance;
       var res = this.travelDetailsObject.distance;
       this.valorCorrida = "R$ 50,00";
       this.tempoEstimadoCorrida = this.travelDetailsObject.duration;
       console.log('distancia : ' + this.travelDetailsObject.distance + ' no tempo de : ' + this.travelDetailsObject.duration)
    }
    
  }
 
      this.map.moveCamera({target:points});
      //this.map.panBy(0,100); de acordo com a div que colocou em baixa
   });

  }
  

  async back(){

    try {
      await this.map.clear();
      this.destination = null;
      
      this.addOrigenMarker();
    } catch (error) {
      console.error(error)
    }
  }


  toggleTeste() {
    if (document.getElementById("collapseExample").style.display == 'none') {
        document.getElementById("collapseExample").style.display = "block";

    } else {
        document.getElementById("collapseExample").style.display = "none";
    }
  }

  toggleTeste1() {
    if (document.getElementById("collapseExample1").style.display == 'none') {
        document.getElementById("collapseExample1").style.display = "block";

    } else {
        document.getElementById("collapseExample1").style.display = "none";
    }
  }

  async embarqueRapido() {
    const alert = await this.alertCtrl.create({
      header: '',
      cssClass: '',
      subHeader: '',
      message: 'Embarque rápido : <br> • Pegue um UaiLeva <br> • Escaneie o QR Code <br> • Inicie a corrida <br> Registrar o Cartão',
      buttons: ['X']
    });

    await alert.present();
  }
  logout(){
    this.userService.logout();
    this.router.navigate(['']);
  }


  aceitarViagem() {
    if (document.getElementById("aceitar").style.display == 'none') {
        document.getElementById("aceitar").style.display = "block";

    } else {
        document.getElementById("aceitar").style.display = "none";
    }
  }
}
