import { ErrorHandler, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PopmenuComponent } from './../../components/popmenu/popmenu.component';
import { HomeResultsPage } from './home-results.page';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleMaps } from '@ionic-native/google-maps';

const routes: Routes = [
  {
    path: '',
    component: HomeResultsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    

  ],
  declarations: [HomeResultsPage, PopmenuComponent],
  providers: [
    SplashScreen,
    {provide: ErrorHandler},
    GoogleMaps
      
  ]
})
export class HomeResultsPageModule {}
