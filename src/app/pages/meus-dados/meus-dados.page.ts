  
import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';

import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController,
  
  ModalController, Platform } from '@ionic/angular';
  
import {Router} from '@angular/router';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';


@Component({
  selector: 'app-home',
  templateUrl: 'meus-dados.page.html',
  styleUrls: ['meus-dados.page.scss'],
})
export class MeusDadosPage implements OnInit {


  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private router: Router,
    public toastCtrl: ToastController,
  ) {}



  ngOnInit() {
    
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
    this.menuCtrl.toggle();
  }

}