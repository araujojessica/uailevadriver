import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterDriver } from './register-driver';

describe('RegisterDriver', () => {
  let component: RegisterDriver;
  let fixture: ComponentFixture<RegisterDriver>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterDriver ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterDriver);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
