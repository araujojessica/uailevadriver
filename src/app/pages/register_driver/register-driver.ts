import { Component, OnInit } from '@angular/core';
import {
  NavController,
  AlertController,
  MenuController,
  ToastController,
  PopoverController,
  
  ModalController, Platform } from '@ionic/angular';
  
import {Router} from '@angular/router';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';

 

@Component({
  selector: 'app-register-driver',
  templateUrl: './register-driver.html',
  styleUrls: ['./register-driver.scss']
})

export class RegisterDriver{
  searchKey = '';
  yourLocation = '123 Test Street';
  themeCover = 'assets/img/ionic4-Start-Theme-cover.jpg';
  user$: Observable<User>;
  letterObj = {
    to: '',
    from: '',
    text: ''
  }

  pdfObj = null;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private router: Router,
    public toastCtrl: ToastController,

  ) {
   
    
  }

 
  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  settings() {
    this.navCtrl.navigateForward('settings');
  }

  



 




}
