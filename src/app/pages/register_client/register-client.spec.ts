import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterClient } from './register-client';

describe('RegisterClient', () => {
  let component: RegisterClient;
  let fixture: ComponentFixture<RegisterClient>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterClient],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterClient);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
