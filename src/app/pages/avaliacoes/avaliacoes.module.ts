import { ErrorHandler, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
//import { PopmenuComponent } from './../../components/popmenu/popmenu.component';
import { AvaliacoesPage } from './avaliacoes.page';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleMaps } from '@ionic-native/google-maps';

const routes: Routes = [
  {
    path: '',
    component: AvaliacoesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    

  ],
  declarations: [AvaliacoesPage],
  providers: [
    SplashScreen,
    {provide: ErrorHandler},
    GoogleMaps
      
  ]
})
export class AvaliacoesPageModule {}
