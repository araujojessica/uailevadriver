import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeusPagamentosPage } from './meus-pagamentos.page';

describe('MeusPagamentosPage', () => {
  let component: MeusPagamentosPage;
  let fixture: ComponentFixture<MeusPagamentosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeusPagamentosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeusPagamentosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
