import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HashLocationStrategy, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ImagePageModule } from './pages/modal/image/image.module';
import { SearchFilterPageModule } from './pages/modal/search-filter/search-filter.module';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { ImageViewerComponent } from './components/image-viewer/image-viewer.component';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';

@NgModule({
  declarations: [AppComponent, NotificationsComponent, ImageViewerComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    ImagePageModule,
    SearchFilterPageModule 

  ],
  entryComponents: [NotificationsComponent, ImageViewerComponent],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: LocationStrategy,useClass: HashLocationStrategy},
    Geolocation,
    Facebook,
  ],
  bootstrap: [AppComponent]

})

export class AppModule {}
